<?php

use App\Http\Controllers\AffiliationController;
use App\Http\Controllers\BaptismalController;
use App\Http\Controllers\BaptismalSponsorController;
use App\Http\Controllers\BarangayController;
use App\Http\Controllers\CemeteryController;
use App\Http\Controllers\CityController;
use App\Http\Controllers\ConfirmationController;
use App\Http\Controllers\ConfirmationSponsorController;
use App\Http\Controllers\DeathController;
use App\Http\Controllers\DesignationController;
use App\Http\Controllers\DioceseController;
use App\Http\Controllers\MarriageController;
use App\Http\Controllers\MarriageSponsorController;
use App\Http\Controllers\ParishController;
use App\Http\Controllers\PriestController;
use App\Http\Controllers\RelationshipController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', ['middleware'=>'auth','as'=> 'home', function () {
    return view('welcome');
}]);
*/

/* User */
Route::get('login', [UserController::class, 'getLogin'])->name('login');
Route::post('login', [UserController::class, 'postLogin'])->name('user.postlogin');
Route::get('logout', [UserController::class, 'logout'])->name('user.logout');

Route::middleware(['auth'])->group(function () {
    Route::get('/', [UserController::class, 'index'])->name('home');
    Route::get('user/changepassword', [UserController::class, 'changepassword'])->name('user.changepassword');
    Route::post('user/updatepassword', [UserController::class, 'updatepassword'])->name('user.updatepassword');
    Route::get('users', [UserController::class, 'userlist'])->name('users.index');
    Route::get('user/create', [UserController::class, 'create'])->name('user.create');
    Route::post('user/create', [UserController::class, 'save'])->name('user.save');
    Route::get('user/{id}/edit', [UserController::class, 'edit'])->name('user.edit');
    Route::post('user/{id}/edit', [UserController::class, 'update'])->name('user.update');
    Route::get('user/{id}/delete', [UserController::class, 'delete'])->name('user.delete');
});

/*  Baptismal Route */
Route::get('baptismal', [BaptismalController::class, 'show'])->name('baptismal.show');
Route::get('baptismallist', [BaptismalController::class, 'lists'])->name('baptismal.lists');
Route::get('baptismal/create', [BaptismalController::class, 'create'])->name('baptismal.create');
Route::post('baptismal/save', [BaptismalController::class, 'save'])->name('baptismal.save');
Route::get('baptismal/report', [BaptismalController::class, 'report'])->name('baptismal.report');
Route::get('baptismal/excel', [BaptismalController::class, 'excel'])->name('baptismal.excel');
Route::get('baptismal/{id}/edit', [BaptismalController::class, 'edit'])->name('baptismal.edit');
Route::post('baptismal/{id}/edit', [BaptismalController::class, 'update'])->name('baptismal.update');
Route::get('baptismal/{id}/pdf', [BaptismalController::class, 'pdf'])->name('baptismal.pdf');
Route::get('baptismal/search', [BaptismalController::class, 'search'])->name('baptismal.search');
Route::post('baptismal/search', [BaptismalController::class, 'postsearch'])->name('baptismal.postsearch');

/*
Route::get('baptismal/city/list', function() {
    $city_id = Input::get('city_id');
    $cities = \DB::table('barangays')->select('name','id')->where('city_id','=',$city_id)->get();
    return $cities;
});
*/

/* Baptismal Sponsor */
Route::post('baptismalsponsor', [BaptismalSponsorController::class, 'save'])->name('baptismalsponsor.save');
Route::get('baptismalsponsor/{id}/destroy', [BaptismalSponsorController::class, 'destroy'])->name('baptismalsponsor.destroy');

/* Confirmation Route */

Route::get('confirmations', [ConfirmationController::class, 'show'])->name('confirmation.show');
Route::get('confirmation/search', [ConfirmationController::class, 'search'])->name('confirmation.search');
Route::post('confirmation/search', [ConfirmationController::class, 'postsearch'])->name('confirmation.postsearch');
Route::get('confirmation/create', [ConfirmationController::class, 'create'])->name('confirmation.create');
Route::post('confirmation/create', [ConfirmationController::class, 'save'])->name('confirmation.save');
Route::get('confirmation/excel', [ConfirmationController::class, 'excel'])->name('confirmation.excel');
Route::get('confirmation/{id}/edit', [ConfirmationController::class, 'edit'])->name('confirmation.edit');
Route::post('confirmation/{id}/edit', [ConfirmationController::class, 'update'])->name('confirmation.update');
Route::get('confirmationlist', [ConfirmationController::class, 'lists'])->name('confirmation.list');
/* Confirmation Sponsor */
Route::post('confirmation/{confirmId}/sponsor', [ConfirmationSponsorController::class, 'save'])->name('confirmation.sponsor.save');
Route::get('confirmation/sponsor/{id}/delete', [ConfirmationSponsorController::class, 'delete'])->name('confirmation.sponsor.delete');

/* Marriage Route */
Route::get('marriages', [MarriageController::class, 'show'])->name('marriage.show');
Route::get('marriage/create', [MarriageController::class, 'create'])->name('marriage.create');

/* Death Route */
Route::get('deaths', [DeathController::class, 'show'])->name('death.show');
Route::get('death/create', [DeathController::class, 'create'])->name('death.create');
Route::post('death/create', [DeathController::class, 'save'])->name('death.save');
Route::get('death/search', [DeathController::class, 'search'])->name('death.search');
Route::post('death/search', [DeathController::class, 'postsearch'])->name('death.postsearch');
Route::get('death/excel', [DeathController::class, 'excel'])->name('death.excel');
Route::get('death/{id}/edit', [DeathController::class, 'edit'])->name('death.edit');
Route::post('death/{id}/edit', [DeathController::class, 'update'])->name('death.update');
Route::post('death/sacrament/{deathId}', [DeathController::class, 'savesacrament'])->name('death.sacrament.save');
Route::get('death/sacrament/{deathId}/{sacramentId}/delete', [DeathController::class, 'deletesac'])->name('death.sacrament.delete');
Route::get('deathlist', [DeathController::class, 'lists'])->name('death.list');

/* City Route */
Route::get('city', [CityController::class, 'index'])->name('city.index');
Route::get('city/list', [CityController::class, 'list'])->name('city.list');
Route::get('city/create', [CityController::class, 'create'])->name('city.create');
Route::post('city/save', [CityController::class, 'save'])->name('city.save');
Route::get('city/{id}/edit', [CityController::class, 'edit'])->name('city.edit');
Route::patch('city/{id}', [CityController::class, 'update'])->name('city.update');

/* affiliation */

Route::resource('affiliation', AffiliationController::class);

/* Barangay */
Route::post('barangay/save', [BarangayController::class, 'save'])->name('barangay.save');
Route::get('barangays/list/{cityid}', [BarangayController::class, 'list'])->name('barangay.list');
Route::get('barangay/{cityid}/create', [BarangayController::class, 'create'])->name('barangay.create');
Route::get('barangays/{cityid}', [BarangayController::class, 'index'])->name('barangay.index');
Route::get('barangay/{id}/edit', [BarangayController::class, 'edit'])->name('barangay.edit');
Route::post('barangay/{id}/update', [BarangayController::class, 'update'])->name('barangay.update');

//Route::post('savebarangay', ['as'=>'savebarangay.save', 'uses'=>'BarangayController::class,'savebarangay'])->name('');

/* Parish */

Route::get('parish', [ParishController::class, 'index'])->name('parish.index');
Route::get('parish/parishlist', [ParishController::class, 'parishlist'])->name('parish.parishlist');
Route::get('parish/create', [ParishController::class, 'create'])->name('parish.create');
Route::post('parish/create', [ParishController::class, 'save'])->name('parish.save');
Route::post('parish/jquerysave', [ParishController::class, 'jquerysave'])->name('parish.jquerysave');
Route::get('parish/{id}/edit', [ParishController::class, 'edit'])->name('parish.edit');
Route::PATCH('parish/{id}', [ParishController::class, 'update'])->name('parish.update');

/* Priest */
Route::get('priest', [PriestController::class, 'index'])->name('priest.index');
Route::get('priest/load', [PriestController::class, 'loadpriest'])->name('priest.load');
Route::get('priest/create', [PriestController::class, 'create'])->name('priest.create');
Route::post('priest', [PriestController::class, 'save'])->name('priest.save');
Route::get('priest/{id}/edit', [PriestController::class, 'edit'])->name('priest.edit');
Route::PATCH('priest/{id}', [PriestController::class, 'update'])->name('priest.update');
Route::get('priest/{id}/setassignatory', [PriestController::class, 'setassignatory'])->name('priest.setassignatory');

/* Cemetery */
Route::get('cemeterys', [CemeteryController::class, 'index'])->name('cemetery.index');
Route::get('cemetery/lists', [CemeteryController::class, 'lists'])->name('cemetery.lists');
Route::get('cemetery/save', [CemeteryController::class, 'new'])->name('cemetery.new');
Route::post('cemetery/save', [CemeteryController::class, 'save'])->name('cemetery.save');
Route::get('cemetery/{id}/edit', [CemeteryController::class, 'edit'])->name('cemetery.edit');
Route::post('cemetery/{id}/edit', [CemeteryController::class, 'update'])->name('cemetery.update');

/* Relationships */
Route::get('relationships', [RelationshipController::class, 'index'])->name('relationship.index');
Route::get('relationship/create', [RelationshipController::class, 'create'])->name('relationship.create');
Route::post('relationship/create', [RelationshipController::class, 'save'])->name('relationship.save');
Route::get('relationship/lists', [RelationshipController::class, 'lists'])->name('relationship.lists');
Route::get('relationship/{id}/edit', [RelationshipController::class, 'edit'])->name('relationship.edit');
Route::post('relationship/{id}/edit', [RelationshipController::class, 'update'])->name('relationship.update');

/* Diocese */
Route::get('dioceses', [DioceseController::class, 'index'])->name('diocese.index');
Route::get('diocese/create', [DioceseController::class, 'create'])->name('diocese.create');
Route::post('diocese/save', [DioceseController::class, 'save'])->name('diocese.save');
Route::get('diocese/{id}/edit', [DioceseController::class, 'edit'])->name('diocese.edit');
Route::post('diocese/{id}/update', [DioceseController::class, 'update'])->name('diocese.update');

/* Affiliation */
Route::get('affiliations', [AffiliationController::class, 'index'])->name('affiliation.index');
Route::get('affiliation/create', [AffiliationController::class, 'create'])->name('affiliation.create');
Route::post('affiliation/create', [AffiliationController::class, 'save'])->name('affiliation.save');
Route::get('affiliation/{id}/edit', [AffiliationController::class, 'edit'])->name('affiliation.edit');
Route::post('affiliation/{id}/edit', [AffiliationController::class, 'update'])->name('affiliation.update');

/* Designations */
Route::get('designations', [DesignationController::class, 'index'])->name('designation.index');
Route::get('designation/create', [DesignationController::class, 'create'])->name('designation.create');
Route::post('designation/create', [DesignationController::class, 'save'])->name('designation.save');
Route::get('designation/{id}/edit', [DesignationController::class, 'edit'])->name('designation.edit');
Route::post('designation/{id}/edit', [DesignationController::class, 'update'])->name('designation.update');

/* Marriage */
Route::get('marriages', [MarriageController::class, 'index'])->name('marriage.index');
Route::get('marriage/search', [MarriageController::class, 'search'])->name('marriage.search');
Route::post('marriage/search', [MarriageController::class, 'searchresults'])->name('marriage.searchresults');
Route::get('marriage/create', [MarriageController::class, 'create'])->name('marriage.create');
Route::post('marriage/create', [MarriageController::class, 'save'])->name('marriage.save');
Route::get('marriage/{id}/edit', [MarriageController::class, 'edit'])->name('marriage.edit');
Route::post('marriage/{id}/edit', [MarriageController::class, 'update'])->name('marriage.update');

Route::post('marriage/{id}/sponsor/save', [MarriageSponsorController::class, 'save'])->name('marriage.sponsor.save');
Route::get('marriagesponsor/{sponid}/delete', [MarriageSponsorController::class, 'delete'])->name('marriage.sponsor.delete');