<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMarriagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marriages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('hfname');
            $table->string('hmname')->nullable();
            $table->string('hlname');
            $table->integer('hnationality_id')->unsigned();
            $table->foreign('hnationality_id')->references('id')->on('nationalities');
            $table->integer('hcstatus_id')->unsigned();
            $table->foreign('hcstatus_id')->references('id')->on('civilstatus');
            $table->date('hdob')->nullable();
            $table->integer('hage');
            $table->string('hpob');
            $table->string('hresidence');
            $table->string('hffname')->nullable();
            $table->string('hfmname')->nullable();
            $table->string('hflname')->nullable();
            $table->integer('hfcitizenship')->unsigned();
            $table->foreign('hfcitizenship')->references('id')->on('nationalities');
            $table->string('hmfname')->nullable();
            $table->string('hmmname')->nullable();
            $table->string('hmlname')->nullable();
            $table->integer('hmcitizenship')->unsigned();
            $table->foreign('hmcitizenship')->references('id')->on('nationalities');
            $table->string('hadvicefname')->nullable();
            $table->string('hadvicemname')->nullable();
            $table->string('hadvicelname')->nullable();
            $table->string('hadvicerelation')->nullable();
            $table->string('hadviceresidence')->nullable();

            $table->string('wfname');
            $table->string('wmname')->nullable();
            $table->string('wlname');
            $table->integer('wnationality_id')->unsigned();
            $table->foreign('wnationality_id')->references('id')->on('nationalities');
            $table->integer('wcstatus_id')->unsigned();
            $table->foreign('wcstatus_id')->references('id')->on('civilstatus');
            $table->date('wdob')->nullable();
            $table->integer('wage');
            $table->string('wpob');
            $table->string('wresidence');
            $table->string('wffname')->nullable();
            $table->string('wfmname')->nullable();
            $table->string('wflname')->nullable();
            $table->integer('wfcitizenship')->unsigned();
            $table->foreign('wfcitizenship')->references('id')->on('nationalities');
            $table->string('wmfname')->nullable();
            $table->string('wmmname')->nullable();
            $table->string('wmlname')->nullable();
            $table->integer('wmcitizenship')->unsigned();
            $table->foreign('wmcitizenship')->references('id')->on('nationalities');
            $table->string('wadvicefname')->nullable();
            $table->string('wadvicemname')->nullable();
            $table->string('wadvicelname')->nullable();
            $table->string('wadvicerelation')->nullable();
            $table->string('wadviceresidence')->nullable();

            $table->string('license');

            $table->string('placeofmar');
            $table->string('addressofmar');
            $table->date('dateofmar');
            $table->string('timeofmar');

            $table->string('officiating');
            $table->string('civilofficiating');
            $table->date('civildate');
            $table->string('civiladdress');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marriages');
    }
}