<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePriestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('priests', function(Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('firstname');
            $table->string('middlename')->nullable();
            $table->string('lastname');
            $table->integer('affiliation_id')->unsigned();
            $table->foreign('affiliation_id')->references('id')->on('affiliations');
            $table->integer('designation_id')->unsigned();
            $table->foreign('designation_id')->references('id')->on('designations');
            $table->string('licenseno')->nullable();
            $table->timestamp('expiry')->nullable();
            $table->integer('isAssignatory');
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('priests');
    }
}
