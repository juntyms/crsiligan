<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBaptismalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('baptismals', function(Blueprint $table) {
            $table->increments('id');
            $table->string('fname');
            $table->string('mname');
            $table->string('lname');
            $table->string('gender', 1);
            $table->dateTime('dob')->nullable();
            $table->string('pobstreet')->nullable();
            $table->integer('pobBarangay_id')->unsigned();
            $table->foreign('pobBarangay_id')->references('id')->on('barangays');
            $table->integer('pobCity_id')->unsigned();
            $table->foreign('pobCity_id')->references('id')->on('cities');
            $table->string('fatherFn')->nullable();
            $table->string('fatherMn')->nullable();
            $table->string('fatherLn')->nullable();
            $table->string('motherFn');
            $table->string('motherMn');
            $table->string('motherLn');
            $table->integer('baptizedby_id')->unsigned();
            $table->foreign('baptizedby_id')->references('id')->on('priests');
            $table->dateTime('dateofbaptism')->nullable();
            $table->string('placeStreet')->nullable();
            $table->integer('placeBarangay_id')->unsigned();
            $table->foreign('placeBarangay_id')->references('id')->on('barangays');
            $table->integer('placeCity_id')->unsigned();
            $table->foreign('placeCity_id')->references('id')->on('cities');
            $table->integer('placeParish_id')->unsigned();
            $table->foreign('placeParish_id')->references('id')->on('parishes');
            $table->text('purpose')->nullable();
            $table->text('annotation')->nullable();
            $table->text('remarks')->nullable();
            $table->integer('volume')->default(0);
            $table->integer('book')->default(0);
            $table->integer('page')->default(0);
            $table->string('ornumber')->nullable();
            $table->timestamp('datepaid')->nullable();
            $table->timestamp('dateIssue')->nullable();
            $table->integer('currentPriest_id');
            $table->integer('currentPriest_designationId');
            $table->integer('recordParish_id')->unsigned();
            $table->foreign('recordParish_id')->references('id')->on('parishes');
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('baptismals');
    }
}
