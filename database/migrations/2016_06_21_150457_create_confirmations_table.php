<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfirmationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('confirmations', function(Blueprint $table) {
            $table->increments('id');
            $table->string('fname');
            $table->string('mname');
            $table->string('lname');
            $table->string('gender');
            $table->datetime('dob');
            $table->datetime('dofconfirmation');
            $table->integer('age');
            $table->string('fatherFn')->nullable();
            $table->string('fatherMn')->nullable();
            $table->string('fatherLn')->nullable();
            $table->string('motherFn');
            $table->string('motherMn');
            $table->string('motherLn');
            $table->integer('episcopalMinister_id')->unsigned();
            $table->foreign('episcopalMinister_id')->references('id')->on('priests');
            $table->integer('placeParish_id')->unsigned();
            $table->foreign('placeParish_id')->references('id')->on('parishes');
            $table->integer('placeCity_id')->unsigned();
            $table->foreign('placeCity_id')->references('id')->on('cities');
            $table->integer('placeBarangay_id')->unsigned();
            $table->foreign('placeBarangay_id')->references('id')->on('barangays');
            $table->integer('volume')->default(0);
            $table->integer('book')->default();
            $table->integer('page')->default();
            $table->text('purpose')->nullable();
            $table->text('annotation')->nullable();
            $table->text('remarks')->nullable();
            $table->integer('currentPriest_id');
            $table->integer('currentPriest_designationId');
            $table->integer('recordParish_id');            
            $table->string('ornumber')->nullable();
            $table->timestamp('datepaid')->nullable();
            $table->timestamp('dateIssue')->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('confirmations');
    }
}
