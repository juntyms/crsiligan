<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParishesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parishes', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('diocese_id')->unsigned();
            $table->foreign('diocese_id')->references('id')->on('dioceses');  
            $table->integer('city_id')->unsigned();
            $table->foreign('city_id')->references('id')->on('cities');          
            $table->string('address')->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('parishes');
    }
}
