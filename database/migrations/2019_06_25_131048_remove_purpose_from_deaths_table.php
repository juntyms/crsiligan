<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemovePurposeFromDeathsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deaths', function (Blueprint $table) {
            $table->dropColumn('purpose');
            $table->dropColumn('annotation');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deaths', function (Blueprint $table) {
            $table->string('purpose');
            $table->string('annotation');
        });
    }
}
