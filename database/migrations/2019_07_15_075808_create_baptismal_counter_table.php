<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBaptismalCounterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('baptismal_counter', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('volume');            
            $table->integer('page');
            $table->integer('book_per_page');
            $table->integer('book_per_volume');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('baptismal_counter');
    }
}
