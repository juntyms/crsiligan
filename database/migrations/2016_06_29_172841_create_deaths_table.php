<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeathsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deaths', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fname');
            $table->string('mname');
            $table->string('lname');
            $table->integer('age');
            $table->datetime('dob');
            $table->string('gender');
            $table->integer('civilstatus_id')->unsigned();
            $table->foreign('civilstatus_id')->references('id')->on('civilstatus');            
            $table->string('street');
            $table->integer('barangay_id')->unsigned();
            $table->foreign('barangay_id')->references('id')->on('barangays');
            $table->integer('city_id')->unsigned();
            $table->foreign('city_id')->references('id')->on('cities');            
            $table->integer('minister_id')->unsigned();
            $table->foreign('minister_id')->references('id')->on('priests');
            $table->string('placeOfBurial');
            $table->string('causeOfDeath');
            $table->datetime('dateOfDeath')->nullable();
            $table->datetime('dateOfBurial')->nullable();
            $table->integer('volume');
            $table->integer('page');
            $table->integer('book');
            $table->string('burialPermitNo');
            $table->integer('currentPriest_id');
            $table->integer('currentPriest_designationId');
            $table->text('purpose')->nullable();
            $table->text('annotation')->nullable();            
            $table->string('informantFname');
            $table->string('informantMname');
            $table->string('informantLname');
            $table->integer('informantRelationship_id')->unsigned();
            $table->foreign('informantRelationship_id')->references('id')->on('relationships');
            $table->string('informantAddress');
            $table->integer('informantBarangay_id');
            $table->integer('informantCity_id');
            $table->datetime('dateIssue')->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('deaths');
    }
}
