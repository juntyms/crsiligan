<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeathSacramentPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('death_sacrament', function (Blueprint $table) {
            $table->integer('death_id')->unsigned()->index();
            $table->foreign('death_id')->references('id')->on('deaths')->onDelete('cascade');
            $table->integer('sacrament_id')->unsigned()->index();
            $table->foreign('sacrament_id')->references('id')->on('sacraments')->onDelete('cascade');
            $table->primary(['death_id', 'sacrament_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('death_sacrament');
    }
}
