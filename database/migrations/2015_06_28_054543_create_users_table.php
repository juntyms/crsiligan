<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function(Blueprint $table) {
            $table->increments('id');
            $table->string('username');
            $table->string('password', 60);
            $table->string('firstname');
            $table->string('middlename');
            $table->string('lastname');            
            $table->integer('isUser')->default(1);
            $table->integer('isSecretary')->default(0);
            $table->integer('isPriest')->default(0);
            $table->integer('isFinance')->default(0);
            $table->integer('isAdmin')->default(0);
            $table->integer('superAdmin')->default(0);
            $table->integer('parishId')->default(0);
            $table->string('remember_token')->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
