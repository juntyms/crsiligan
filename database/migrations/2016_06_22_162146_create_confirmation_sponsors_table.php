<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfirmationSponsorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('confirmation_sponsors', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('confirmation_id')->unsigned();
            $table->foreign('confirmation_id')->references('id')->on('confirmations');
            $table->string('fname');
            $table->string('mname');
            $table->string('lname');
            $table->nullabletimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('confirmation_sponsors');
    }
}
