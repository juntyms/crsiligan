<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParishPriestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parish_priest', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('parish_id')->unsigned();
            $table->foreign('parish_id')->references('id')->on('parishes');
            $table->integer('priest_id')->unsigned();
            $table->foreign('priest_id')->references('id')->on('priests');
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('parish_priest');
    }
}
