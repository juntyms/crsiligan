<?php
namespace Database\Seeders;
use App\Models\Designation;
use Illuminate\Database\Seeder;

class DesignationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('designations')->delete();

        Designation::create(['designation' => 'Parish Administrator']);
        Designation::create(['designation' => 'Asst. Parish Administrator']);
        Designation::create(['designation' => 'Parish Priest']);
        Designation::create(['designation' => 'Asst. Parish Priest']);
        Designation::create(['designation' => 'Guest Priest']);
    }
}
