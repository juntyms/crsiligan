<?php

namespace Database\Seeders;

use App\Models\Religion;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ReligionTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('religions')->delete();

        Religion::create(['religion' => 'Roman Catholic']);
        Religion::create(['religion' => 'Aglipay']);
        Religion::create(['religion' => 'Anglican']);
        Religion::create(['religion' => 'Assemblies of God']);
        Religion::create(['religion' => 'Berian Baptist']);
        Religion::create(['religion' => 'Buddish']);
        Religion::create(['religion' => 'Christian']);
        Religion::create(['religion' => 'Latter Day Saints (Mormon)']);
        Religion::create(['religion' => 'Lutheran']);
        Religion::create(['religion' => 'Islam']);
        Religion::create(['religion' => 'Pentecostal']);
        Religion::create(['religion' => 'Philippine Independent Church']);
        Religion::create(['religion' => 'Protestant']);
        Religion::create(['religion' => 'United Church of Christ']);
        Religion::create(['religion' => 'Alliance']);
        Religion::create(['religion' => 'Baptist']);
        Religion::create(['religion' => 'Filipinista']);
        Religion::create(['religion' => 'Iglesia Katolika']);
        Religion::create(['religion' => 'Iglesia ni Kristo']);
        Religion::create(['religion' => 'Iglesia sa Dios Espiritu Santo']);
        Religion::create(['religion' => 'Jhovas Witness']);
        Religion::create(['religion' => 'Methodist']);
        Religion::create(['religion' => 'Missionary Alliance Churches']);
        Religion::create(['religion' => 'Rock Christ Jesus']);
        Religion::create(['religion' => 'Seventh Day Adventist']);
        Religion::create(['religion' => 'UCCP']);
        Religion::create(['religion' => 'World International Ministry']);
        Religion::create(['religion' => 'Episcopal Adventist']);
        Religion::create(['religion' => 'Sagrado Corazon Sr.']);
    }
}