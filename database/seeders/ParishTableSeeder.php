<?php
namespace Database\Seeders;
use App\Models\Parish;
use Illuminate\Database\Seeder;

class ParishTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('parishes')->delete();

        Parish::create(['name' => 'San Isidro Cathedral', 'diocese_id' => '1', 'city_id' => '1']);
        Parish::create(['name' => 'Sto. Nino Chapel', 'diocese_id' => '1', 'city_id' => '1']);
        Parish::create(['name' => 'Birhen sa Lourdes Chapel', 'diocese_id' => '1', 'city_id' => '1']);
        Parish::create(['name' => 'Birhen sa Regla Chapel', 'diocese_id' => '1', 'city_id' => '1']);
        Parish::create(['name' => 'San Jose Chapel', 'diocese_id' => '1', 'city_id' => '1']);
        Parish::create(['name' => 'San Vicente Ferrer Chapel', 'diocese_id' => '1', 'city_id' => '1']);
        Parish::create(['name' => 'San Isidro Chapel', 'diocese_id' => '1', 'city_id' => '1']);
        Parish::create(['name' => 'Birhen sa Fatima Chapel', 'diocese_id' => '1', 'city_id' => '1']);
        Parish::create(['name' => 'Immaculate Heart of Mary Chapel', 'diocese_id' => '1', 'city_id' => '1']);
        Parish::create(['name' => 'San Agustine Chapel', 'diocese_id' => '1', 'city_id' => '1']);
        Parish::create(['name' => 'San Roque Chapel', 'diocese_id' => '1', 'city_id' => '1']);
        Parish::create(['name' => 'Santa Ana Chapel', 'diocese_id' => '1', 'city_id' => '1']);
        Parish::create(['name' => 'Our Lady of Peace Chapel', 'diocese_id' => '1', 'city_id' => '1']);
        Parish::create(['name' => 'Our Lady of Perpetual Help Chapel', 'diocese_id' => '1', 'city_id' => '1']);
        Parish::create(['name' => 'San Lorenzo Ruiz Chapel', 'diocese_id' => '1', 'city_id' => '1']);
        Parish::create(['name' => 'San Miguel Chapel', 'diocese_id' => '1', 'city_id' => '1']);
        Parish::create(['name' => 'Sagrado Corazon Chapel', 'diocese_id' => '1', 'city_id' => '1']);
        Parish::create(['name' => 'Sto. Rosario Chapel', 'diocese_id' => '1', 'city_id' => '1']);
        Parish::create(['name' => 'San Francisco Javier Chapel', 'diocese_id' => '1', 'city_id' => '1']);
        Parish::create(['name' => 'Birhen Salvacion Chapel', 'diocese_id' => '1', 'city_id' => '1']);
        Parish::create(['name' => 'San Antonio Chapel', 'diocese_id' => '1', 'city_id' => '1']);
        Parish::create(['name' => 'Carmel of the Hearts of Jesus and Mary Chapel', 'diocese_id' => '1', 'city_id' => '1']);
        Parish::create(['name' => 'Immaculate Conception Chapel', 'diocese_id' => '1', 'city_id' => '1']);
        Parish::create(['name' => 'Sta. Josefina Bakhita Chapel', 'diocese_id' => '1', 'city_id' => '1']);
        Parish::create(['name' => 'Our Lady of the Immaculate Conception Parish', 'diocese_id' => '1', 'city_id' => '2']);
    }
}