<?php

namespace Database\Seeders;

use App\Models\Affiliation;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AffiliationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('affiliations')->delete();

        Affiliation::create(['affiliation' => 'Roman Catholic Priest']);
        Affiliation::create(['affiliation' => 'Roman Catholic Deacon']);
        Affiliation::create(['affiliation' => 'Lay Minister (Alagad)']);
        Affiliation::create(['affiliation' => 'Seminarian']);
        Affiliation::create(['affiliation' => 'Bishop']);
        Affiliation::create(['affiliation' => 'Cannocian Priest']);
        Affiliation::create(['affiliation' => 'Benedictine Priest']);
        Affiliation::create(['affiliation' => 'Guest Priest']);
        Affiliation::create(['affiliation' => 'Redemptorist Priest']);
        Affiliation::create(['affiliation' => 'Diocesan Priest']);
    }
}