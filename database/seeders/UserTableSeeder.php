<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->delete();

        User::create(['username' => 'benals', 'password' => Hash::make('123456'), 'firstname' => 'Ruben', 'middlename' => 'E', 'lastname' => 'Alce', 'superAdmin' => '1', 'parishId' => '1']);
        User::create(['username' => 'juntyms', 'password' => Hash::make('123456'), 'firstname' => 'Junn', 'middlename' => 'T', 'lastname' => 'Timoteo', 'superAdmin' => '1', 'parishId' => '1']);
    }
}