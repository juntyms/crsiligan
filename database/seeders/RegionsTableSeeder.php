<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RegionsTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('regions')->delete();

        \DB::table('regions')->insert([
            0 => [
                'id' => 1,
            'name' => 'REGION I (ILOCOS REGION)',
                'created_at' => null,
                'updated_at' => null,
            ],
            1 => [
                'id' => 2,
            'name' => 'REGION II (CAGAYAN VALLEY)',
                'created_at' => null,
                'updated_at' => null,
            ],
            2 => [
                'id' => 3,
            'name' => 'REGION III (CENTRAL LUZON)',
                'created_at' => null,
                'updated_at' => null,
            ],
            3 => [
                'id' => 4,
            'name' => 'REGION IV-A (CALABARZON)',
                'created_at' => null,
                'updated_at' => null,
            ],
            4 => [
                'id' => 5,
            'name' => 'REGION IV-B (MIMAROPA)',
                'created_at' => null,
                'updated_at' => null,
            ],
            5 => [
                'id' => 6,
            'name' => 'REGION V (BICOL REGION)',
                'created_at' => null,
                'updated_at' => null,
            ],
            6 => [
                'id' => 7,
            'name' => 'REGION VI (WESTERN VISAYAS)',
                'created_at' => null,
                'updated_at' => null,
            ],
            7 => [
                'id' => 8,
            'name' => 'REGION VII (CENTRAL VISAYAS)',
                'created_at' => null,
                'updated_at' => null,
            ],
            8 => [
                'id' => 9,
            'name' => 'REGION VIII (EASTERN VISAYAS)',
                'created_at' => null,
                'updated_at' => null,
            ],
            9 => [
                'id' => 10,
            'name' => 'REGION IX (ZAMBOANGA PENINSULA)',
                'created_at' => null,
                'updated_at' => null,
            ],
            10 => [
                'id' => 11,
            'name' => 'REGION X (NORTHERN MINDANAO)',
                'created_at' => null,
                'updated_at' => null,
            ],
            11 => [
                'id' => 12,
            'name' => 'REGION XI (DAVAO REGION)',
                'created_at' => null,
                'updated_at' => null,
            ],
            12 => [
                'id' => 13,
            'name' => 'REGION XII (SOCCSKSARGEN)',
                'created_at' => null,
                'updated_at' => null,
            ],
            13 => [
                'id' => 14,
            'name' => 'NATIONAL CAPITAL REGION (NCR)',
                'created_at' => null,
                'updated_at' => null,
            ],
            14 => [
                'id' => 15,
            'name' => 'CORDILLERA ADMINISTRATIVE REGION (CAR)',
                'created_at' => null,
                'updated_at' => null,
            ],
            15 => [
                'id' => 16,
            'name' => 'AUTONOMOUS REGION IN MUSLIM MINDANAO (ARMM)',
                'created_at' => null,
                'updated_at' => null,
            ],
            16 => [
                'id' => 17,
            'name' => 'REGION XIII (Caraga)',
                'created_at' => null,
                'updated_at' => null,
            ],
        ]);
    }
}