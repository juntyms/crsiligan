<?php

namespace Database\Seeders;

use App\Models\Sacrament;
use Illuminate\Database\Seeder;

class SacramentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('sacraments')->delete();

        Sacrament::create(['sacrament' => 'Baptism']);
        Sacrament::create(['sacrament' => 'Confirmation']);
        Sacrament::create(['sacrament' => 'Confession']);
        Sacrament::create(['sacrament' => 'Holy Communion']);
        Sacrament::create(['sacrament' => 'Matrimony']);
        Sacrament::create(['sacrament' => 'Anointing of the Sick']);
        Sacrament::create(['sacrament' => 'Holy Orders']);
        Sacrament::create(['sacrament' => 'Extreme Unction']);
    }
}