<?php
namespace Database\Seeders;
use App\Models\Civilstatus;
use Illuminate\Database\Seeder;

class CivilstatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('civilstatus')->delete();

        Civilstatus::create(['status' => 'Single']);
        Civilstatus::create(['status' => 'Married']);
        Civilstatus::create(['status' => 'Widow']);
        Civilstatus::create(['status' => 'Widower']);
        Civilstatus::create(['status' => 'Divorced']);
    }
}