<?php
namespace Database\Seeders;
use App\Models\City;
use Illuminate\Database\Seeder;

class CityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('cities')->delete();

        City::create(['name' => 'Malaybalay City']);
        City::create(['name' => 'Manolo Fortich']);
        City::create(['name' => 'Valencia City']);
        City::create(['name' => 'Valencia City']);
    }
}