<?php
namespace Database\Seeders;
use App\Models\Diocese;
use Illuminate\Database\Seeder;

class DioceseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('dioceses')->delete();

        Diocese::create(['name' => 'Diocese of Malaybalay']);
    }
}
