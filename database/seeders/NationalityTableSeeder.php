<?php
namespace Database\Seeders;
use App\Models\Nationality;
use Illuminate\Database\Seeder;

class NationalityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('nationalities')->delete();

        Nationality::create(['nationality' => 'Afghan']);
        Nationality::create(['nationality' => 'Albanian']);
        Nationality::create(['nationality' => 'Algerian']);
        Nationality::create(['nationality' => 'American']);
        Nationality::create(['nationality' => 'Andorran']);
        Nationality::create(['nationality' => 'Angolan']);
        Nationality::create(['nationality' => 'Antiguans']);
        Nationality::create(['nationality' => 'Argentinean']);
        Nationality::create(['nationality' => 'Armenian']);
        Nationality::create(['nationality' => 'Australian']);
        Nationality::create(['nationality' => 'Austrian']);
        Nationality::create(['nationality' => 'Azerbaijani']);
        Nationality::create(['nationality' => 'Bahamian']);
        Nationality::create(['nationality' => 'Bahraini']);
        Nationality::create(['nationality' => 'Bangladeshi']);
        Nationality::create(['nationality' => 'Barbadian']);
        Nationality::create(['nationality' => 'Barbudans']);
        Nationality::create(['nationality' => 'Batswana']);
        Nationality::create(['nationality' => 'Belarusian']);
        Nationality::create(['nationality' => 'Belgian']);
        Nationality::create(['nationality' => 'Belizean']);
        Nationality::create(['nationality' => 'Beninese']);
        Nationality::create(['nationality' => 'Bhutanese']);
        Nationality::create(['nationality' => 'Bolivian']);
        Nationality::create(['nationality' => 'Bosnian']);
        Nationality::create(['nationality' => 'Brazilian']);
        Nationality::create(['nationality' => 'British']);
        Nationality::create(['nationality' => 'Bruneian']);
        Nationality::create(['nationality' => 'Bulgarian']);
        Nationality::create(['nationality' => 'Burkinabe']);
        Nationality::create(['nationality' => 'Burmese']);
        Nationality::create(['nationality' => 'Burundian']);
        Nationality::create(['nationality' => 'Cambodian']);
        Nationality::create(['nationality' => 'Cameroonian']);
        Nationality::create(['nationality' => 'Canadian']);
        Nationality::create(['nationality' => 'Cape Verdean']);
        Nationality::create(['nationality' => 'Central African']);
        Nationality::create(['nationality' => 'Chadian']);
        Nationality::create(['nationality' => 'Chilean']);
        Nationality::create(['nationality' => 'Chinese']);
        Nationality::create(['nationality' => 'Colombian']);
        Nationality::create(['nationality' => 'Comoran']);
        Nationality::create(['nationality' => 'Congolese']);
        Nationality::create(['nationality' => 'Costa Rican']);
        Nationality::create(['nationality' => 'Croatian']);
        Nationality::create(['nationality' => 'Cuban']);
        Nationality::create(['nationality' => 'Cypriot']);
        Nationality::create(['nationality' => 'Czech']);
        Nationality::create(['nationality' => 'Danish']);
        Nationality::create(['nationality' => 'Djibouti']);
        Nationality::create(['nationality' => 'Dominican']);
        Nationality::create(['nationality' => 'Dutch']);
        Nationality::create(['nationality' => 'East Timorese']);
        Nationality::create(['nationality' => 'Ecuadorean']);
        Nationality::create(['nationality' => 'Egyptian']);
        Nationality::create(['nationality' => 'Emirian']);
        Nationality::create(['nationality' => 'Equatorial Guinean']);
        Nationality::create(['nationality' => 'Eritrean']);
        Nationality::create(['nationality' => 'Estonian']);
        Nationality::create(['nationality' => 'Ethiopian']);
        Nationality::create(['nationality' => 'Fijian']);
        Nationality::create(['nationality' => 'Filipino']);
        Nationality::create(['nationality' => 'Finnish']);
        Nationality::create(['nationality' => 'French']);
        Nationality::create(['nationality' => 'Gabonese']);
        Nationality::create(['nationality' => 'Gambian']);
        Nationality::create(['nationality' => 'Georgian']);
        Nationality::create(['nationality' => 'German']);
        Nationality::create(['nationality' => 'Ghanaian']);
        Nationality::create(['nationality' => 'Greek']);
        Nationality::create(['nationality' => 'Grenadian']);
        Nationality::create(['nationality' => 'Guatemalan']);
        Nationality::create(['nationality' => 'Guinea-Bissauan']);
        Nationality::create(['nationality' => 'Guinean']);
        Nationality::create(['nationality' => 'Guyanese']);
        Nationality::create(['nationality' => 'Haitian']);
        Nationality::create(['nationality' => 'Herzegovinian']);
        Nationality::create(['nationality' => 'Honduran']);
        Nationality::create(['nationality' => 'Hungarian']);
        Nationality::create(['nationality' => 'I-Kiribati']);
        Nationality::create(['nationality' => 'Icelander']);
        Nationality::create(['nationality' => 'Indian']);
        Nationality::create(['nationality' => 'Indonesian']);
        Nationality::create(['nationality' => 'Iranian']);
        Nationality::create(['nationality' => 'Iraqi']);
        Nationality::create(['nationality' => 'Irish']);
        Nationality::create(['nationality' => 'Israeli']);
        Nationality::create(['nationality' => 'Italian']);
        Nationality::create(['nationality' => 'Ivorian']);
        Nationality::create(['nationality' => 'Jamaican']);
        Nationality::create(['nationality' => 'Japanese']);
        Nationality::create(['nationality' => 'Jordanian']);
        Nationality::create(['nationality' => 'Kazakhstani']);
        Nationality::create(['nationality' => 'Kenyan']);
        Nationality::create(['nationality' => 'Kittian and Nevisian']);
        Nationality::create(['nationality' => 'Kuwaiti']);
        Nationality::create(['nationality' => 'Kyrgyz']);
        Nationality::create(['nationality' => 'Laotian']);
        Nationality::create(['nationality' => 'Latvian']);
        Nationality::create(['nationality' => 'Lebanese']);
        Nationality::create(['nationality' => 'Liberian']);
        Nationality::create(['nationality' => 'Libyan']);
        Nationality::create(['nationality' => 'Liechtensteiner']);
        Nationality::create(['nationality' => 'Lithuanian']);
        Nationality::create(['nationality' => 'Luxembourger']);
        Nationality::create(['nationality' => 'Macedonian']);
        Nationality::create(['nationality' => 'Malagasy']);
        Nationality::create(['nationality' => 'Malawian']);
        Nationality::create(['nationality' => 'Malaysian']);
        Nationality::create(['nationality' => 'Maldivian']);
        Nationality::create(['nationality' => 'Malian']);
        Nationality::create(['nationality' => 'Maltese']);
        Nationality::create(['nationality' => 'Marshallese']);
        Nationality::create(['nationality' => 'Mauritanian']);
        Nationality::create(['nationality' => 'Mauritian']);
        Nationality::create(['nationality' => 'Mexican']);
        Nationality::create(['nationality' => 'Micronesian']);
        Nationality::create(['nationality' => 'Moldovan']);
        Nationality::create(['nationality' => 'Monacan']);
        Nationality::create(['nationality' => 'Mongolian']);
        Nationality::create(['nationality' => 'Moroccan']);
        Nationality::create(['nationality' => 'Mosotho']);
        Nationality::create(['nationality' => 'Motswana']);
        Nationality::create(['nationality' => 'Mozambican']);
        Nationality::create(['nationality' => 'Namibian']);
        Nationality::create(['nationality' => 'Nauruan']);
        Nationality::create(['nationality' => 'Nepalese']);
        Nationality::create(['nationality' => 'New Zealander']);
        Nationality::create(['nationality' => 'Ni-Vanuatu']);
        Nationality::create(['nationality' => 'Nicaraguan']);
        Nationality::create(['nationality' => 'Nigerian']);
        Nationality::create(['nationality' => 'Nigerien']);
        Nationality::create(['nationality' => 'North Korean']);
        Nationality::create(['nationality' => 'Northern Irish']);
        Nationality::create(['nationality' => 'Norwegian']);
        Nationality::create(['nationality' => 'Omani']);
        Nationality::create(['nationality' => 'Pakistani']);
        Nationality::create(['nationality' => 'Palauan']);
        Nationality::create(['nationality' => 'Panamanian']);
        Nationality::create(['nationality' => 'Papua New Guinean']);
        Nationality::create(['nationality' => 'Paraguayan']);
        Nationality::create(['nationality' => 'Peruvian']);
        Nationality::create(['nationality' => 'Polish']);
        Nationality::create(['nationality' => 'Portuguese']);
        Nationality::create(['nationality' => 'Qatari']);
        Nationality::create(['nationality' => 'Romanian']);
        Nationality::create(['nationality' => 'Russian']);
        Nationality::create(['nationality' => 'Rwandan']);
        Nationality::create(['nationality' => 'Saint Lucian']);
        Nationality::create(['nationality' => 'Salvadoran']);
        Nationality::create(['nationality' => 'Samoan']);
        Nationality::create(['nationality' => 'San Marinese']);
        Nationality::create(['nationality' => 'Sao Tomean']);
        Nationality::create(['nationality' => 'Saudi']);
        Nationality::create(['nationality' => 'Scottish']);
        Nationality::create(['nationality' => 'Senegalese']);
        Nationality::create(['nationality' => 'Serbian']);
        Nationality::create(['nationality' => 'Seychellois']);
        Nationality::create(['nationality' => 'Sierra Leonean']);
        Nationality::create(['nationality' => 'Singaporean']);
        Nationality::create(['nationality' => 'Slovakian']);
        Nationality::create(['nationality' => 'Slovenian']);
        Nationality::create(['nationality' => 'Solomon Islander']);
        Nationality::create(['nationality' => 'Somali']);
        Nationality::create(['nationality' => 'South African']);
        Nationality::create(['nationality' => 'South Korean']);
        Nationality::create(['nationality' => 'Spanish']);
        Nationality::create(['nationality' => 'Sri Lankan']);
        Nationality::create(['nationality' => 'Sudanese']);
        Nationality::create(['nationality' => 'Surinamer']);
        Nationality::create(['nationality' => 'Swazi']);
        Nationality::create(['nationality' => 'Swedish']);
        Nationality::create(['nationality' => 'Swiss']);
        Nationality::create(['nationality' => 'Syrian']);
        Nationality::create(['nationality' => 'Taiwanese']);
        Nationality::create(['nationality' => 'Tajik']);
        Nationality::create(['nationality' => 'Tanzanian']);
        Nationality::create(['nationality' => 'Thai']);
        Nationality::create(['nationality' => 'Togolese']);
        Nationality::create(['nationality' => 'Tongan']);
        Nationality::create(['nationality' => 'Trinidadian or Tobagonian']);
        Nationality::create(['nationality' => 'Tunisian']);
        Nationality::create(['nationality' => 'Turkish']);
        Nationality::create(['nationality' => 'Tuvaluan']);
        Nationality::create(['nationality' => 'Ugandan']);
        Nationality::create(['nationality' => 'Ukrainian']);
        Nationality::create(['nationality' => 'Uruguayan']);
        Nationality::create(['nationality' => 'Uzbekistani']);
        Nationality::create(['nationality' => 'Venezuelan']);
        Nationality::create(['nationality' => 'Vietnamese']);
        Nationality::create(['nationality' => 'Welsh']);
        Nationality::create(['nationality' => 'Yemenite']);
        Nationality::create(['nationality' => 'Zambian']);
        Nationality::create(['nationality' => 'Zimbabwean']);
    }
}
