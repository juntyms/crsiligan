<?php

namespace Database\Seeders;

use App\Models\Relationship;
use Illuminate\Database\Seeder;

class RelationshipTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('relationships')->delete();

        Relationship::create(['name' => 'Father']);
        Relationship::create(['name' => 'Mother']);
        Relationship::create(['name' => 'Son']);
        Relationship::create(['name' => 'Daughter']);
        Relationship::create(['name' => 'Brother']);
        Relationship::create(['name' => 'Sister']);
        Relationship::create(['name' => 'Brother in-law']);
        Relationship::create(['name' => 'Sister in-law']);
        Relationship::create(['name' => 'Mother in-law']);
        Relationship::create(['name' => 'Father in-law']);
    }
}