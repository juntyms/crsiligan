<?php
namespace Database\Seeders;
use App\Models\Barangay;
use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
//use Laracasts\TestDummy\Factory as TestDummy;

class BarangayTableSeeder extends Seeder
{
    public function run()
    {
        //Barangay::all()->forcedelete();
        \DB::table('barangays')->delete();
        Barangay::create(['city_id'=>'1','name'=>'Barangay 1 (Pob)']);
        Barangay::create(['city_id'=>'1','name'=>'Barangay 2 (Pob)']);
        Barangay::create(['city_id'=>'1','name'=>'Barangay 3 (Pob)']);
        Barangay::create(['city_id'=>'1','name'=>'Barangay 4 (Pob)']);
        Barangay::create(['city_id'=>'1','name'=>'Barangay 5 (Pob)']);
        Barangay::create(['city_id'=>'1','name'=>'Barangay 6 (Pob)']);
        Barangay::create(['city_id'=>'1','name'=>'Barangay 7 (Pob)']);
        Barangay::create(['city_id'=>'1','name'=>'Barangay 8 (Pob)']);
        Barangay::create(['city_id'=>'1','name'=>'Barangay 9 (Pob)']);
        Barangay::create(['city_id'=>'1','name'=>'Barangay 10 (Pob)']);
        Barangay::create(['city_id'=>'1','name'=>'Barangay 11 (Pob)']);
        Barangay::create(['city_id'=>'1','name'=>'Aglayan']);
        Barangay::create(['city_id'=>'1','name'=>'Apo Macote']);
        Barangay::create(['city_id'=>'1','name'=>'Bangcud']);
        Barangay::create(['city_id'=>'1','name'=>'Busdi']);
        Barangay::create(['city_id'=>'1','name'=>'Cabangahan']);
        Barangay::create(['city_id'=>'1','name'=>'Caburacanan']);
        Barangay::create(['city_id'=>'1','name'=>'Can-ayan']);
        Barangay::create(['city_id'=>'1','name'=>'Capital Angel']);
        Barangay::create(['city_id'=>'1','name'=>'Casisang']);
        Barangay::create(['city_id'=>'1','name'=>'Dalwangan']);
        Barangay::create(['city_id'=>'1','name'=>'Imbayao']);
        Barangay::create(['city_id'=>'1','name'=>'Indalasa']);
        Barangay::create(['city_id'=>'1','name'=>'Kalasungay']);
        Barangay::create(['city_id'=>'1','name'=>'Kibalabag']);
        Barangay::create(['city_id'=>'1','name'=>'Kulaman']);
        Barangay::create(['city_id'=>'1','name'=>'Laguitas']);
        Barangay::create(['city_id'=>'1','name'=>'Linabo']);
        Barangay::create(['city_id'=>'1','name'=>'Miglamin']);
        Barangay::create(['city_id'=>'1','name'=>'Magsaysay']);
        Barangay::create(['city_id'=>'1','name'=>'Maligaya']);
        Barangay::create(['city_id'=>'1','name'=>'Managok']);
        Barangay::create(['city_id'=>'1','name'=>'Manalog']);
        Barangay::create(['city_id'=>'1','name'=>'Mapayag']);
        Barangay::create(['city_id'=>'1','name'=>'Mapulo']);
        Barangay::create(['city_id'=>'1','name'=>'Patpat']);
        Barangay::create(['city_id'=>'1','name'=>'Saint Peter']);
        Barangay::create(['city_id'=>'1','name'=>'San Jose']);
        Barangay::create(['city_id'=>'1','name'=>'San Martin']);
        Barangay::create(['city_id'=>'1','name'=>'Santo Nino']);
        Barangay::create(['city_id'=>'1','name'=>'Silae']);
        Barangay::create(['city_id'=>'1','name'=>'Simaya']);
        Barangay::create(['city_id'=>'1','name'=>'Sinanglanan']);
        Barangay::create(['city_id'=>'1','name'=>'Sumpong']);
        Barangay::create(['city_id'=>'1','name'=>'Violeta']);
        Barangay::create(['city_id'=>'1','name'=>'Zamboanguita']);

    }
}