<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('cities')->delete();
        
        \DB::table('cities')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'ADAMS',
                'province_id' => 1,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'BACARRA',
                'province_id' => 1,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'BADOC',
                'province_id' => 1,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'BANGUI',
                'province_id' => 1,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'CITY OF BATAC',
                'province_id' => 1,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'BURGOS',
                'province_id' => 1,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'CARASI',
                'province_id' => 1,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'CURRIMAO',
                'province_id' => 1,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'DINGRAS',
                'province_id' => 1,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'DUMALNEG',
                'province_id' => 1,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
            'name' => 'BANNA (ESPIRITU)',
                'province_id' => 1,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
            'name' => 'LAOAG CITY (Capital)',
                'province_id' => 1,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'MARCOS',
                'province_id' => 1,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'NUEVA ERA',
                'province_id' => 1,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'PAGUDPUD',
                'province_id' => 1,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'PAOAY',
                'province_id' => 1,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'name' => 'PASUQUIN',
                'province_id' => 1,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'name' => 'PIDDIG',
                'province_id' => 1,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'name' => 'PINILI',
                'province_id' => 1,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'name' => 'SAN NICOLAS',
                'province_id' => 1,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'name' => 'SARRAT',
                'province_id' => 1,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'name' => 'SOLSONA',
                'province_id' => 1,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'name' => 'VINTAR',
                'province_id' => 1,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'name' => 'ALILEM',
                'province_id' => 2,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'id' => 25,
                'name' => 'BANAYOYO',
                'province_id' => 2,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            25 => 
            array (
                'id' => 26,
                'name' => 'BANTAY',
                'province_id' => 2,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'id' => 27,
                'name' => 'BURGOS',
                'province_id' => 2,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            27 => 
            array (
                'id' => 28,
                'name' => 'CABUGAO',
                'province_id' => 2,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            28 => 
            array (
                'id' => 29,
                'name' => 'CITY OF CANDON',
                'province_id' => 2,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            29 => 
            array (
                'id' => 30,
                'name' => 'CAOAYAN',
                'province_id' => 2,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            30 => 
            array (
                'id' => 31,
                'name' => 'CERVANTES',
                'province_id' => 2,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            31 => 
            array (
                'id' => 32,
                'name' => 'GALIMUYOD',
                'province_id' => 2,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            32 => 
            array (
                'id' => 33,
            'name' => 'GREGORIO DEL PILAR (CONCEPCION)',
                'province_id' => 2,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            33 => 
            array (
                'id' => 34,
                'name' => 'LIDLIDDA',
                'province_id' => 2,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            34 => 
            array (
                'id' => 35,
                'name' => 'MAGSINGAL',
                'province_id' => 2,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            35 => 
            array (
                'id' => 36,
                'name' => 'NAGBUKEL',
                'province_id' => 2,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            36 => 
            array (
                'id' => 37,
                'name' => 'NARVACAN',
                'province_id' => 2,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            37 => 
            array (
                'id' => 38,
            'name' => 'QUIRINO (ANGKAKI)',
                'province_id' => 2,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            38 => 
            array (
                'id' => 39,
            'name' => 'SALCEDO (BAUGEN)',
                'province_id' => 2,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            39 => 
            array (
                'id' => 40,
                'name' => 'SAN EMILIO',
                'province_id' => 2,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            40 => 
            array (
                'id' => 41,
                'name' => 'SAN ESTEBAN',
                'province_id' => 2,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            41 => 
            array (
                'id' => 42,
                'name' => 'SAN ILDEFONSO',
                'province_id' => 2,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            42 => 
            array (
                'id' => 43,
            'name' => 'SAN JUAN (LAPOG)',
                'province_id' => 2,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            43 => 
            array (
                'id' => 44,
                'name' => 'SAN VICENTE',
                'province_id' => 2,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            44 => 
            array (
                'id' => 45,
                'name' => 'SANTA',
                'province_id' => 2,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            45 => 
            array (
                'id' => 46,
                'name' => 'SANTA CATALINA',
                'province_id' => 2,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            46 => 
            array (
                'id' => 47,
                'name' => 'SANTA CRUZ',
                'province_id' => 2,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            47 => 
            array (
                'id' => 48,
                'name' => 'SANTA LUCIA',
                'province_id' => 2,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            48 => 
            array (
                'id' => 49,
                'name' => 'SANTA MARIA',
                'province_id' => 2,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            49 => 
            array (
                'id' => 50,
                'name' => 'SANTIAGO',
                'province_id' => 2,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            50 => 
            array (
                'id' => 51,
                'name' => 'SANTO DOMINGO',
                'province_id' => 2,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            51 => 
            array (
                'id' => 52,
                'name' => 'SIGAY',
                'province_id' => 2,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            52 => 
            array (
                'id' => 53,
                'name' => 'SINAIT',
                'province_id' => 2,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            53 => 
            array (
                'id' => 54,
                'name' => 'SUGPON',
                'province_id' => 2,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            54 => 
            array (
                'id' => 55,
                'name' => 'SUYO',
                'province_id' => 2,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            55 => 
            array (
                'id' => 56,
                'name' => 'TAGUDIN',
                'province_id' => 2,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            56 => 
            array (
                'id' => 57,
            'name' => 'CITY OF VIGAN (Capital)',
                'province_id' => 2,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            57 => 
            array (
                'id' => 58,
                'name' => 'AGOO',
                'province_id' => 3,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            58 => 
            array (
                'id' => 59,
                'name' => 'ARINGAY',
                'province_id' => 3,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            59 => 
            array (
                'id' => 60,
                'name' => 'BACNOTAN',
                'province_id' => 3,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            60 => 
            array (
                'id' => 61,
                'name' => 'BAGULIN',
                'province_id' => 3,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            61 => 
            array (
                'id' => 62,
                'name' => 'BALAOAN',
                'province_id' => 3,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            62 => 
            array (
                'id' => 63,
                'name' => 'BANGAR',
                'province_id' => 3,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            63 => 
            array (
                'id' => 64,
                'name' => 'BAUANG',
                'province_id' => 3,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            64 => 
            array (
                'id' => 65,
                'name' => 'BURGOS',
                'province_id' => 3,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            65 => 
            array (
                'id' => 66,
                'name' => 'CABA',
                'province_id' => 3,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            66 => 
            array (
                'id' => 67,
                'name' => 'LUNA',
                'province_id' => 3,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            67 => 
            array (
                'id' => 68,
                'name' => 'NAGUILIAN',
                'province_id' => 3,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            68 => 
            array (
                'id' => 69,
                'name' => 'PUGO',
                'province_id' => 3,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            69 => 
            array (
                'id' => 70,
                'name' => 'ROSARIO',
                'province_id' => 3,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            70 => 
            array (
                'id' => 71,
            'name' => 'CITY OF SAN FERNANDO (Capital)',
                'province_id' => 3,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            71 => 
            array (
                'id' => 72,
                'name' => 'SAN GABRIEL',
                'province_id' => 3,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            72 => 
            array (
                'id' => 73,
                'name' => 'SAN JUAN',
                'province_id' => 3,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            73 => 
            array (
                'id' => 74,
                'name' => 'SANTO TOMAS',
                'province_id' => 3,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            74 => 
            array (
                'id' => 75,
                'name' => 'SANTOL',
                'province_id' => 3,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            75 => 
            array (
                'id' => 76,
                'name' => 'SUDIPEN',
                'province_id' => 3,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            76 => 
            array (
                'id' => 77,
                'name' => 'TUBAO',
                'province_id' => 3,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            77 => 
            array (
                'id' => 78,
                'name' => 'AGNO',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            78 => 
            array (
                'id' => 79,
                'name' => 'AGUILAR',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            79 => 
            array (
                'id' => 80,
                'name' => 'CITY OF ALAMINOS',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            80 => 
            array (
                'id' => 81,
                'name' => 'ALCALA',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            81 => 
            array (
                'id' => 82,
                'name' => 'ANDA',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            82 => 
            array (
                'id' => 83,
                'name' => 'ASINGAN',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            83 => 
            array (
                'id' => 84,
                'name' => 'BALUNGAO',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            84 => 
            array (
                'id' => 85,
                'name' => 'BANI',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            85 => 
            array (
                'id' => 86,
                'name' => 'BASISTA',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            86 => 
            array (
                'id' => 87,
                'name' => 'BAUTISTA',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            87 => 
            array (
                'id' => 88,
                'name' => 'BAYAMBANG',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            88 => 
            array (
                'id' => 89,
                'name' => 'BINALONAN',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            89 => 
            array (
                'id' => 90,
                'name' => 'BINMALEY',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            90 => 
            array (
                'id' => 91,
                'name' => 'BOLINAO',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            91 => 
            array (
                'id' => 92,
                'name' => 'BUGALLON',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            92 => 
            array (
                'id' => 93,
                'name' => 'BURGOS',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            93 => 
            array (
                'id' => 94,
                'name' => 'CALASIAO',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            94 => 
            array (
                'id' => 95,
                'name' => 'DAGUPAN CITY',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            95 => 
            array (
                'id' => 96,
                'name' => 'DASOL',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            96 => 
            array (
                'id' => 97,
                'name' => 'INFANTA',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            97 => 
            array (
                'id' => 98,
                'name' => 'LABRADOR',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            98 => 
            array (
                'id' => 99,
            'name' => 'LINGAYEN (Capital)',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            99 => 
            array (
                'id' => 100,
                'name' => 'MABINI',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            100 => 
            array (
                'id' => 101,
                'name' => 'MALASIQUI',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            101 => 
            array (
                'id' => 102,
                'name' => 'MANAOAG',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            102 => 
            array (
                'id' => 103,
                'name' => 'MANGALDAN',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            103 => 
            array (
                'id' => 104,
                'name' => 'MANGATAREM',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            104 => 
            array (
                'id' => 105,
                'name' => 'MAPANDAN',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            105 => 
            array (
                'id' => 106,
                'name' => 'NATIVIDAD',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            106 => 
            array (
                'id' => 107,
                'name' => 'POZORRUBIO',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            107 => 
            array (
                'id' => 108,
                'name' => 'ROSALES',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            108 => 
            array (
                'id' => 109,
                'name' => 'SAN CARLOS CITY',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            109 => 
            array (
                'id' => 110,
                'name' => 'SAN FABIAN',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            110 => 
            array (
                'id' => 111,
                'name' => 'SAN JACINTO',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            111 => 
            array (
                'id' => 112,
                'name' => 'SAN MANUEL',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            112 => 
            array (
                'id' => 113,
                'name' => 'SAN NICOLAS',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            113 => 
            array (
                'id' => 114,
                'name' => 'SAN QUINTIN',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            114 => 
            array (
                'id' => 115,
                'name' => 'SANTA BARBARA',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            115 => 
            array (
                'id' => 116,
                'name' => 'SANTA MARIA',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            116 => 
            array (
                'id' => 117,
                'name' => 'SANTO TOMAS',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            117 => 
            array (
                'id' => 118,
                'name' => 'SISON',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            118 => 
            array (
                'id' => 119,
                'name' => 'SUAL',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            119 => 
            array (
                'id' => 120,
                'name' => 'TAYUG',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            120 => 
            array (
                'id' => 121,
                'name' => 'UMINGAN',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            121 => 
            array (
                'id' => 122,
                'name' => 'URBIZTONDO',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            122 => 
            array (
                'id' => 123,
                'name' => 'CITY OF URDANETA',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            123 => 
            array (
                'id' => 124,
                'name' => 'VILLASIS',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            124 => 
            array (
                'id' => 125,
                'name' => 'LAOAC',
                'province_id' => 4,
                'region_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            125 => 
            array (
                'id' => 126,
            'name' => 'BASCO (Capital)',
                'province_id' => 5,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            126 => 
            array (
                'id' => 127,
                'name' => 'ITBAYAT',
                'province_id' => 5,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            127 => 
            array (
                'id' => 128,
                'name' => 'IVANA',
                'province_id' => 5,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            128 => 
            array (
                'id' => 129,
                'name' => 'MAHATAO',
                'province_id' => 5,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            129 => 
            array (
                'id' => 130,
                'name' => 'SABTANG',
                'province_id' => 5,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            130 => 
            array (
                'id' => 131,
                'name' => 'UYUGAN',
                'province_id' => 5,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            131 => 
            array (
                'id' => 132,
                'name' => 'ABULUG',
                'province_id' => 6,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            132 => 
            array (
                'id' => 133,
                'name' => 'ALCALA',
                'province_id' => 6,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            133 => 
            array (
                'id' => 134,
                'name' => 'ALLACAPAN',
                'province_id' => 6,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            134 => 
            array (
                'id' => 135,
                'name' => 'AMULUNG',
                'province_id' => 6,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            135 => 
            array (
                'id' => 136,
                'name' => 'APARRI',
                'province_id' => 6,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            136 => 
            array (
                'id' => 137,
                'name' => 'BAGGAO',
                'province_id' => 6,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            137 => 
            array (
                'id' => 138,
                'name' => 'BALLESTEROS',
                'province_id' => 6,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            138 => 
            array (
                'id' => 139,
                'name' => 'BUGUEY',
                'province_id' => 6,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            139 => 
            array (
                'id' => 140,
                'name' => 'CALAYAN',
                'province_id' => 6,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            140 => 
            array (
                'id' => 141,
                'name' => 'CAMALANIUGAN',
                'province_id' => 6,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            141 => 
            array (
                'id' => 142,
                'name' => 'CLAVERIA',
                'province_id' => 6,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            142 => 
            array (
                'id' => 143,
                'name' => 'ENRILE',
                'province_id' => 6,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            143 => 
            array (
                'id' => 144,
                'name' => 'GATTARAN',
                'province_id' => 6,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            144 => 
            array (
                'id' => 145,
                'name' => 'GONZAGA',
                'province_id' => 6,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            145 => 
            array (
                'id' => 146,
                'name' => 'IGUIG',
                'province_id' => 6,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            146 => 
            array (
                'id' => 147,
                'name' => 'LAL-LO',
                'province_id' => 6,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            147 => 
            array (
                'id' => 148,
                'name' => 'LASAM',
                'province_id' => 6,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            148 => 
            array (
                'id' => 149,
                'name' => 'PAMPLONA',
                'province_id' => 6,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            149 => 
            array (
                'id' => 150,
                'name' => 'PEÑABLANCA',
                'province_id' => 6,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            150 => 
            array (
                'id' => 151,
                'name' => 'PIAT',
                'province_id' => 6,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            151 => 
            array (
                'id' => 152,
                'name' => 'RIZAL',
                'province_id' => 6,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            152 => 
            array (
                'id' => 153,
                'name' => 'SANCHEZ-MIRA',
                'province_id' => 6,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            153 => 
            array (
                'id' => 154,
                'name' => 'SANTA ANA',
                'province_id' => 6,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            154 => 
            array (
                'id' => 155,
                'name' => 'SANTA PRAXEDES',
                'province_id' => 6,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            155 => 
            array (
                'id' => 156,
                'name' => 'SANTA TERESITA',
                'province_id' => 6,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            156 => 
            array (
                'id' => 157,
            'name' => 'SANTO NIÑO (FAIRE)',
                'province_id' => 6,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            157 => 
            array (
                'id' => 158,
                'name' => 'SOLANA',
                'province_id' => 6,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            158 => 
            array (
                'id' => 159,
                'name' => 'TUAO',
                'province_id' => 6,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            159 => 
            array (
                'id' => 160,
            'name' => 'TUGUEGARAO CITY (Capital)',
                'province_id' => 6,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            160 => 
            array (
                'id' => 161,
                'name' => 'ALICIA',
                'province_id' => 7,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            161 => 
            array (
                'id' => 162,
                'name' => 'ANGADANAN',
                'province_id' => 7,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            162 => 
            array (
                'id' => 163,
                'name' => 'AURORA',
                'province_id' => 7,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            163 => 
            array (
                'id' => 164,
                'name' => 'BENITO SOLIVEN',
                'province_id' => 7,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            164 => 
            array (
                'id' => 165,
                'name' => 'BURGOS',
                'province_id' => 7,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            165 => 
            array (
                'id' => 166,
                'name' => 'CABAGAN',
                'province_id' => 7,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            166 => 
            array (
                'id' => 167,
                'name' => 'CABATUAN',
                'province_id' => 7,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            167 => 
            array (
                'id' => 168,
                'name' => 'CITY OF CAUAYAN',
                'province_id' => 7,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            168 => 
            array (
                'id' => 169,
                'name' => 'CORDON',
                'province_id' => 7,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            169 => 
            array (
                'id' => 170,
                'name' => 'DINAPIGUE',
                'province_id' => 7,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            170 => 
            array (
                'id' => 171,
                'name' => 'DIVILACAN',
                'province_id' => 7,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            171 => 
            array (
                'id' => 172,
                'name' => 'ECHAGUE',
                'province_id' => 7,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            172 => 
            array (
                'id' => 173,
                'name' => 'GAMU',
                'province_id' => 7,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            173 => 
            array (
                'id' => 174,
            'name' => 'ILAGAN CITY (Capital)',
                'province_id' => 7,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            174 => 
            array (
                'id' => 175,
                'name' => 'JONES',
                'province_id' => 7,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            175 => 
            array (
                'id' => 176,
                'name' => 'LUNA',
                'province_id' => 7,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            176 => 
            array (
                'id' => 177,
                'name' => 'MACONACON',
                'province_id' => 7,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            177 => 
            array (
                'id' => 178,
            'name' => 'DELFIN ALBANO (MAGSAYSAY)',
                'province_id' => 7,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            178 => 
            array (
                'id' => 179,
                'name' => 'MALLIG',
                'province_id' => 7,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            179 => 
            array (
                'id' => 180,
                'name' => 'NAGUILIAN',
                'province_id' => 7,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            180 => 
            array (
                'id' => 181,
                'name' => 'PALANAN',
                'province_id' => 7,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            181 => 
            array (
                'id' => 182,
                'name' => 'QUEZON',
                'province_id' => 7,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            182 => 
            array (
                'id' => 183,
                'name' => 'QUIRINO',
                'province_id' => 7,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            183 => 
            array (
                'id' => 184,
                'name' => 'RAMON',
                'province_id' => 7,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            184 => 
            array (
                'id' => 185,
                'name' => 'REINA MERCEDES',
                'province_id' => 7,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            185 => 
            array (
                'id' => 186,
                'name' => 'ROXAS',
                'province_id' => 7,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            186 => 
            array (
                'id' => 187,
                'name' => 'SAN AGUSTIN',
                'province_id' => 7,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            187 => 
            array (
                'id' => 188,
                'name' => 'SAN GUILLERMO',
                'province_id' => 7,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            188 => 
            array (
                'id' => 189,
                'name' => 'SAN ISIDRO',
                'province_id' => 7,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            189 => 
            array (
                'id' => 190,
                'name' => 'SAN MANUEL',
                'province_id' => 7,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            190 => 
            array (
                'id' => 191,
                'name' => 'SAN MARIANO',
                'province_id' => 7,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            191 => 
            array (
                'id' => 192,
                'name' => 'SAN MATEO',
                'province_id' => 7,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            192 => 
            array (
                'id' => 193,
                'name' => 'SAN PABLO',
                'province_id' => 7,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            193 => 
            array (
                'id' => 194,
                'name' => 'SANTA MARIA',
                'province_id' => 7,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            194 => 
            array (
                'id' => 195,
                'name' => 'CITY OF SANTIAGO',
                'province_id' => 7,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            195 => 
            array (
                'id' => 196,
                'name' => 'SANTO TOMAS',
                'province_id' => 7,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            196 => 
            array (
                'id' => 197,
                'name' => 'TUMAUINI',
                'province_id' => 7,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            197 => 
            array (
                'id' => 198,
                'name' => 'AMBAGUIO',
                'province_id' => 8,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            198 => 
            array (
                'id' => 199,
                'name' => 'ARITAO',
                'province_id' => 8,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            199 => 
            array (
                'id' => 200,
                'name' => 'BAGABAG',
                'province_id' => 8,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            200 => 
            array (
                'id' => 201,
                'name' => 'BAMBANG',
                'province_id' => 8,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            201 => 
            array (
                'id' => 202,
            'name' => 'BAYOMBONG (Capital)',
                'province_id' => 8,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            202 => 
            array (
                'id' => 203,
                'name' => 'DIADI',
                'province_id' => 8,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            203 => 
            array (
                'id' => 204,
                'name' => 'DUPAX DEL NORTE',
                'province_id' => 8,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            204 => 
            array (
                'id' => 205,
                'name' => 'DUPAX DEL SUR',
                'province_id' => 8,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            205 => 
            array (
                'id' => 206,
                'name' => 'KASIBU',
                'province_id' => 8,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            206 => 
            array (
                'id' => 207,
                'name' => 'KAYAPA',
                'province_id' => 8,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            207 => 
            array (
                'id' => 208,
                'name' => 'QUEZON',
                'province_id' => 8,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            208 => 
            array (
                'id' => 209,
                'name' => 'SANTA FE',
                'province_id' => 8,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            209 => 
            array (
                'id' => 210,
                'name' => 'SOLANO',
                'province_id' => 8,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            210 => 
            array (
                'id' => 211,
                'name' => 'VILLAVERDE',
                'province_id' => 8,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            211 => 
            array (
                'id' => 212,
                'name' => 'ALFONSO CASTANEDA',
                'province_id' => 8,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            212 => 
            array (
                'id' => 213,
                'name' => 'AGLIPAY',
                'province_id' => 9,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            213 => 
            array (
                'id' => 214,
            'name' => 'CABARROGUIS (Capital)',
                'province_id' => 9,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            214 => 
            array (
                'id' => 215,
                'name' => 'DIFFUN',
                'province_id' => 9,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            215 => 
            array (
                'id' => 216,
                'name' => 'MADDELA',
                'province_id' => 9,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            216 => 
            array (
                'id' => 217,
                'name' => 'SAGUDAY',
                'province_id' => 9,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            217 => 
            array (
                'id' => 218,
                'name' => 'NAGTIPUNAN',
                'province_id' => 9,
                'region_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            218 => 
            array (
                'id' => 219,
                'name' => 'ABUCAY',
                'province_id' => 10,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            219 => 
            array (
                'id' => 220,
                'name' => 'BAGAC',
                'province_id' => 10,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            220 => 
            array (
                'id' => 221,
            'name' => 'CITY OF BALANGA (Capital)',
                'province_id' => 10,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            221 => 
            array (
                'id' => 222,
                'name' => 'DINALUPIHAN',
                'province_id' => 10,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            222 => 
            array (
                'id' => 223,
                'name' => 'HERMOSA',
                'province_id' => 10,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            223 => 
            array (
                'id' => 224,
                'name' => 'LIMAY',
                'province_id' => 10,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            224 => 
            array (
                'id' => 225,
                'name' => 'MARIVELES',
                'province_id' => 10,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            225 => 
            array (
                'id' => 226,
                'name' => 'MORONG',
                'province_id' => 10,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            226 => 
            array (
                'id' => 227,
                'name' => 'ORANI',
                'province_id' => 10,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            227 => 
            array (
                'id' => 228,
                'name' => 'ORION',
                'province_id' => 10,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            228 => 
            array (
                'id' => 229,
                'name' => 'PILAR',
                'province_id' => 10,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            229 => 
            array (
                'id' => 230,
                'name' => 'SAMAL',
                'province_id' => 10,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            230 => 
            array (
                'id' => 231,
                'name' => 'ANGAT',
                'province_id' => 11,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            231 => 
            array (
                'id' => 232,
            'name' => 'BALAGTAS (BIGAA)',
                'province_id' => 11,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            232 => 
            array (
                'id' => 233,
                'name' => 'BALIUAG',
                'province_id' => 11,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            233 => 
            array (
                'id' => 234,
                'name' => 'BOCAUE',
                'province_id' => 11,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            234 => 
            array (
                'id' => 235,
                'name' => 'BULACAN',
                'province_id' => 11,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            235 => 
            array (
                'id' => 236,
                'name' => 'BUSTOS',
                'province_id' => 11,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            236 => 
            array (
                'id' => 237,
                'name' => 'CALUMPIT',
                'province_id' => 11,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            237 => 
            array (
                'id' => 238,
                'name' => 'GUIGUINTO',
                'province_id' => 11,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            238 => 
            array (
                'id' => 239,
                'name' => 'HAGONOY',
                'province_id' => 11,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            239 => 
            array (
                'id' => 240,
            'name' => 'CITY OF MALOLOS (Capital)',
                'province_id' => 11,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            240 => 
            array (
                'id' => 241,
                'name' => 'MARILAO',
                'province_id' => 11,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            241 => 
            array (
                'id' => 242,
                'name' => 'CITY OF MEYCAUAYAN',
                'province_id' => 11,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            242 => 
            array (
                'id' => 243,
                'name' => 'NORZAGARAY',
                'province_id' => 11,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            243 => 
            array (
                'id' => 244,
                'name' => 'OBANDO',
                'province_id' => 11,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            244 => 
            array (
                'id' => 245,
                'name' => 'PANDI',
                'province_id' => 11,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            245 => 
            array (
                'id' => 246,
                'name' => 'PAOMBONG',
                'province_id' => 11,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            246 => 
            array (
                'id' => 247,
                'name' => 'PLARIDEL',
                'province_id' => 11,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            247 => 
            array (
                'id' => 248,
                'name' => 'PULILAN',
                'province_id' => 11,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            248 => 
            array (
                'id' => 249,
                'name' => 'SAN ILDEFONSO',
                'province_id' => 11,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            249 => 
            array (
                'id' => 250,
                'name' => 'CITY OF SAN JOSE DEL MONTE',
                'province_id' => 11,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            250 => 
            array (
                'id' => 251,
                'name' => 'SAN MIGUEL',
                'province_id' => 11,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            251 => 
            array (
                'id' => 252,
                'name' => 'SAN RAFAEL',
                'province_id' => 11,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            252 => 
            array (
                'id' => 253,
                'name' => 'SANTA MARIA',
                'province_id' => 11,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            253 => 
            array (
                'id' => 254,
                'name' => 'DOÑA REMEDIOS TRINIDAD',
                'province_id' => 11,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            254 => 
            array (
                'id' => 255,
                'name' => 'ALIAGA',
                'province_id' => 12,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            255 => 
            array (
                'id' => 256,
                'name' => 'BONGABON',
                'province_id' => 12,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            256 => 
            array (
                'id' => 257,
                'name' => 'CABANATUAN CITY',
                'province_id' => 12,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            257 => 
            array (
                'id' => 258,
                'name' => 'CABIAO',
                'province_id' => 12,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            258 => 
            array (
                'id' => 259,
                'name' => 'CARRANGLAN',
                'province_id' => 12,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            259 => 
            array (
                'id' => 260,
                'name' => 'CUYAPO',
                'province_id' => 12,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            260 => 
            array (
                'id' => 261,
            'name' => 'GABALDON (BITULOK & SABANI)',
                'province_id' => 12,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            261 => 
            array (
                'id' => 262,
                'name' => 'CITY OF GAPAN',
                'province_id' => 12,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            262 => 
            array (
                'id' => 263,
                'name' => 'GENERAL MAMERTO NATIVIDAD',
                'province_id' => 12,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            263 => 
            array (
                'id' => 264,
            'name' => 'GENERAL TINIO (PAPAYA)',
                'province_id' => 12,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            264 => 
            array (
                'id' => 265,
                'name' => 'GUIMBA',
                'province_id' => 12,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            265 => 
            array (
                'id' => 266,
                'name' => 'JAEN',
                'province_id' => 12,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            266 => 
            array (
                'id' => 267,
                'name' => 'LAUR',
                'province_id' => 12,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            267 => 
            array (
                'id' => 268,
                'name' => 'LICAB',
                'province_id' => 12,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            268 => 
            array (
                'id' => 269,
                'name' => 'LLANERA',
                'province_id' => 12,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            269 => 
            array (
                'id' => 270,
                'name' => 'LUPAO',
                'province_id' => 12,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            270 => 
            array (
                'id' => 271,
                'name' => 'SCIENCE CITY OF MUÑOZ',
                'province_id' => 12,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            271 => 
            array (
                'id' => 272,
                'name' => 'NAMPICUAN',
                'province_id' => 12,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            272 => 
            array (
                'id' => 273,
            'name' => 'PALAYAN CITY (Capital)',
                'province_id' => 12,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            273 => 
            array (
                'id' => 274,
                'name' => 'PANTABANGAN',
                'province_id' => 12,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            274 => 
            array (
                'id' => 275,
                'name' => 'PEÑARANDA',
                'province_id' => 12,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            275 => 
            array (
                'id' => 276,
                'name' => 'QUEZON',
                'province_id' => 12,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            276 => 
            array (
                'id' => 277,
                'name' => 'RIZAL',
                'province_id' => 12,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            277 => 
            array (
                'id' => 278,
                'name' => 'SAN ANTONIO',
                'province_id' => 12,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            278 => 
            array (
                'id' => 279,
                'name' => 'SAN ISIDRO',
                'province_id' => 12,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            279 => 
            array (
                'id' => 280,
                'name' => 'SAN JOSE CITY',
                'province_id' => 12,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            280 => 
            array (
                'id' => 281,
                'name' => 'SAN LEONARDO',
                'province_id' => 12,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            281 => 
            array (
                'id' => 282,
                'name' => 'SANTA ROSA',
                'province_id' => 12,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            282 => 
            array (
                'id' => 283,
                'name' => 'SANTO DOMINGO',
                'province_id' => 12,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            283 => 
            array (
                'id' => 284,
                'name' => 'TALAVERA',
                'province_id' => 12,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            284 => 
            array (
                'id' => 285,
                'name' => 'TALUGTUG',
                'province_id' => 12,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            285 => 
            array (
                'id' => 286,
                'name' => 'ZARAGOZA',
                'province_id' => 12,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            286 => 
            array (
                'id' => 287,
                'name' => 'ANGELES CITY',
                'province_id' => 13,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            287 => 
            array (
                'id' => 288,
                'name' => 'APALIT',
                'province_id' => 13,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            288 => 
            array (
                'id' => 289,
                'name' => 'ARAYAT',
                'province_id' => 13,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            289 => 
            array (
                'id' => 290,
                'name' => 'BACOLOR',
                'province_id' => 13,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            290 => 
            array (
                'id' => 291,
                'name' => 'CANDABA',
                'province_id' => 13,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            291 => 
            array (
                'id' => 292,
                'name' => 'FLORIDABLANCA',
                'province_id' => 13,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            292 => 
            array (
                'id' => 293,
                'name' => 'GUAGUA',
                'province_id' => 13,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            293 => 
            array (
                'id' => 294,
                'name' => 'LUBAO',
                'province_id' => 13,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            294 => 
            array (
                'id' => 295,
                'name' => 'MABALACAT CITY',
                'province_id' => 13,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            295 => 
            array (
                'id' => 296,
                'name' => 'MACABEBE',
                'province_id' => 13,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            296 => 
            array (
                'id' => 297,
                'name' => 'MAGALANG',
                'province_id' => 13,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            297 => 
            array (
                'id' => 298,
                'name' => 'MASANTOL',
                'province_id' => 13,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            298 => 
            array (
                'id' => 299,
                'name' => 'MEXICO',
                'province_id' => 13,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            299 => 
            array (
                'id' => 300,
                'name' => 'MINALIN',
                'province_id' => 13,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            300 => 
            array (
                'id' => 301,
                'name' => 'PORAC',
                'province_id' => 13,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            301 => 
            array (
                'id' => 302,
            'name' => 'CITY OF SAN FERNANDO (Capital)',
                'province_id' => 13,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            302 => 
            array (
                'id' => 303,
                'name' => 'SAN LUIS',
                'province_id' => 13,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            303 => 
            array (
                'id' => 304,
                'name' => 'SAN SIMON',
                'province_id' => 13,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            304 => 
            array (
                'id' => 305,
                'name' => 'SANTA ANA',
                'province_id' => 13,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            305 => 
            array (
                'id' => 306,
                'name' => 'SANTA RITA',
                'province_id' => 13,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            306 => 
            array (
                'id' => 307,
                'name' => 'SANTO TOMAS',
                'province_id' => 13,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            307 => 
            array (
                'id' => 308,
            'name' => 'SASMUAN (Sexmoan)',
                'province_id' => 13,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            308 => 
            array (
                'id' => 309,
                'name' => 'ANAO',
                'province_id' => 14,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            309 => 
            array (
                'id' => 310,
                'name' => 'BAMBAN',
                'province_id' => 14,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            310 => 
            array (
                'id' => 311,
                'name' => 'CAMILING',
                'province_id' => 14,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            311 => 
            array (
                'id' => 312,
                'name' => 'CAPAS',
                'province_id' => 14,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            312 => 
            array (
                'id' => 313,
                'name' => 'CONCEPCION',
                'province_id' => 14,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            313 => 
            array (
                'id' => 314,
                'name' => 'GERONA',
                'province_id' => 14,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            314 => 
            array (
                'id' => 315,
                'name' => 'LA PAZ',
                'province_id' => 14,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            315 => 
            array (
                'id' => 316,
                'name' => 'MAYANTOC',
                'province_id' => 14,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            316 => 
            array (
                'id' => 317,
                'name' => 'MONCADA',
                'province_id' => 14,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            317 => 
            array (
                'id' => 318,
                'name' => 'PANIQUI',
                'province_id' => 14,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            318 => 
            array (
                'id' => 319,
                'name' => 'PURA',
                'province_id' => 14,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            319 => 
            array (
                'id' => 320,
                'name' => 'RAMOS',
                'province_id' => 14,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            320 => 
            array (
                'id' => 321,
                'name' => 'SAN CLEMENTE',
                'province_id' => 14,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            321 => 
            array (
                'id' => 322,
                'name' => 'SAN MANUEL',
                'province_id' => 14,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            322 => 
            array (
                'id' => 323,
                'name' => 'SANTA IGNACIA',
                'province_id' => 14,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            323 => 
            array (
                'id' => 324,
            'name' => 'CITY OF TARLAC (Capital)',
                'province_id' => 14,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            324 => 
            array (
                'id' => 325,
                'name' => 'VICTORIA',
                'province_id' => 14,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            325 => 
            array (
                'id' => 326,
                'name' => 'SAN JOSE',
                'province_id' => 14,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            326 => 
            array (
                'id' => 327,
                'name' => 'BOTOLAN',
                'province_id' => 15,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            327 => 
            array (
                'id' => 328,
                'name' => 'CABANGAN',
                'province_id' => 15,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            328 => 
            array (
                'id' => 329,
                'name' => 'CANDELARIA',
                'province_id' => 15,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            329 => 
            array (
                'id' => 330,
                'name' => 'CASTILLEJOS',
                'province_id' => 15,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            330 => 
            array (
                'id' => 331,
            'name' => 'IBA (Capital)',
                'province_id' => 15,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            331 => 
            array (
                'id' => 332,
                'name' => 'MASINLOC',
                'province_id' => 15,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            332 => 
            array (
                'id' => 333,
                'name' => 'OLONGAPO CITY',
                'province_id' => 15,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            333 => 
            array (
                'id' => 334,
                'name' => 'PALAUIG',
                'province_id' => 15,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            334 => 
            array (
                'id' => 335,
                'name' => 'SAN ANTONIO',
                'province_id' => 15,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            335 => 
            array (
                'id' => 336,
                'name' => 'SAN FELIPE',
                'province_id' => 15,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            336 => 
            array (
                'id' => 337,
                'name' => 'SAN MARCELINO',
                'province_id' => 15,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            337 => 
            array (
                'id' => 338,
                'name' => 'SAN NARCISO',
                'province_id' => 15,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            338 => 
            array (
                'id' => 339,
                'name' => 'SANTA CRUZ',
                'province_id' => 15,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            339 => 
            array (
                'id' => 340,
                'name' => 'SUBIC',
                'province_id' => 15,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            340 => 
            array (
                'id' => 341,
            'name' => 'BALER (Capital)',
                'province_id' => 16,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            341 => 
            array (
                'id' => 342,
                'name' => 'CASIGURAN',
                'province_id' => 16,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            342 => 
            array (
                'id' => 343,
                'name' => 'DILASAG',
                'province_id' => 16,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            343 => 
            array (
                'id' => 344,
                'name' => 'DINALUNGAN',
                'province_id' => 16,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            344 => 
            array (
                'id' => 345,
                'name' => 'DINGALAN',
                'province_id' => 16,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            345 => 
            array (
                'id' => 346,
                'name' => 'DIPACULAO',
                'province_id' => 16,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            346 => 
            array (
                'id' => 347,
                'name' => 'MARIA AURORA',
                'province_id' => 16,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            347 => 
            array (
                'id' => 348,
                'name' => 'SAN LUIS',
                'province_id' => 16,
                'region_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            348 => 
            array (
                'id' => 349,
                'name' => 'AGONCILLO',
                'province_id' => 17,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            349 => 
            array (
                'id' => 350,
                'name' => 'ALITAGTAG',
                'province_id' => 17,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            350 => 
            array (
                'id' => 351,
                'name' => 'BALAYAN',
                'province_id' => 17,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            351 => 
            array (
                'id' => 352,
                'name' => 'BALETE',
                'province_id' => 17,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            352 => 
            array (
                'id' => 353,
            'name' => 'BATANGAS CITY (Capital)',
                'province_id' => 17,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            353 => 
            array (
                'id' => 354,
                'name' => 'BAUAN',
                'province_id' => 17,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            354 => 
            array (
                'id' => 355,
                'name' => 'CALACA',
                'province_id' => 17,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            355 => 
            array (
                'id' => 356,
                'name' => 'CALATAGAN',
                'province_id' => 17,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            356 => 
            array (
                'id' => 357,
                'name' => 'CUENCA',
                'province_id' => 17,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            357 => 
            array (
                'id' => 358,
                'name' => 'IBAAN',
                'province_id' => 17,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            358 => 
            array (
                'id' => 359,
                'name' => 'LAUREL',
                'province_id' => 17,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            359 => 
            array (
                'id' => 360,
                'name' => 'LEMERY',
                'province_id' => 17,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            360 => 
            array (
                'id' => 361,
                'name' => 'LIAN',
                'province_id' => 17,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            361 => 
            array (
                'id' => 362,
                'name' => 'LIPA CITY',
                'province_id' => 17,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            362 => 
            array (
                'id' => 363,
                'name' => 'LOBO',
                'province_id' => 17,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            363 => 
            array (
                'id' => 364,
                'name' => 'MABINI',
                'province_id' => 17,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            364 => 
            array (
                'id' => 365,
                'name' => 'MALVAR',
                'province_id' => 17,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            365 => 
            array (
                'id' => 366,
                'name' => 'MATAASNAKAHOY',
                'province_id' => 17,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            366 => 
            array (
                'id' => 367,
                'name' => 'NASUGBU',
                'province_id' => 17,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            367 => 
            array (
                'id' => 368,
                'name' => 'PADRE GARCIA',
                'province_id' => 17,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            368 => 
            array (
                'id' => 369,
                'name' => 'ROSARIO',
                'province_id' => 17,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            369 => 
            array (
                'id' => 370,
                'name' => 'SAN JOSE',
                'province_id' => 17,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            370 => 
            array (
                'id' => 371,
                'name' => 'SAN JUAN',
                'province_id' => 17,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            371 => 
            array (
                'id' => 372,
                'name' => 'SAN LUIS',
                'province_id' => 17,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            372 => 
            array (
                'id' => 373,
                'name' => 'SAN NICOLAS',
                'province_id' => 17,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            373 => 
            array (
                'id' => 374,
                'name' => 'SAN PASCUAL',
                'province_id' => 17,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            374 => 
            array (
                'id' => 375,
                'name' => 'SANTA TERESITA',
                'province_id' => 17,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            375 => 
            array (
                'id' => 376,
                'name' => 'SANTO TOMAS',
                'province_id' => 17,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            376 => 
            array (
                'id' => 377,
                'name' => 'TAAL',
                'province_id' => 17,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            377 => 
            array (
                'id' => 378,
                'name' => 'TALISAY',
                'province_id' => 17,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            378 => 
            array (
                'id' => 379,
                'name' => 'CITY OF TANAUAN',
                'province_id' => 17,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            379 => 
            array (
                'id' => 380,
                'name' => 'TAYSAN',
                'province_id' => 17,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            380 => 
            array (
                'id' => 381,
                'name' => 'TINGLOY',
                'province_id' => 17,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            381 => 
            array (
                'id' => 382,
                'name' => 'TUY',
                'province_id' => 17,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            382 => 
            array (
                'id' => 383,
                'name' => 'ALFONSO',
                'province_id' => 18,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            383 => 
            array (
                'id' => 384,
                'name' => 'AMADEO',
                'province_id' => 18,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            384 => 
            array (
                'id' => 385,
                'name' => 'BACOOR CITY',
                'province_id' => 18,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            385 => 
            array (
                'id' => 386,
                'name' => 'CARMONA',
                'province_id' => 18,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            386 => 
            array (
                'id' => 387,
                'name' => 'CAVITE CITY',
                'province_id' => 18,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            387 => 
            array (
                'id' => 388,
                'name' => 'CITY OF DASMARIÑAS',
                'province_id' => 18,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            388 => 
            array (
                'id' => 389,
                'name' => 'GENERAL EMILIO AGUINALDO',
                'province_id' => 18,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            389 => 
            array (
                'id' => 390,
                'name' => 'GENERAL TRIAS',
                'province_id' => 18,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            390 => 
            array (
                'id' => 391,
                'name' => 'IMUS CITY',
                'province_id' => 18,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            391 => 
            array (
                'id' => 392,
                'name' => 'INDANG',
                'province_id' => 18,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            392 => 
            array (
                'id' => 393,
                'name' => 'KAWIT',
                'province_id' => 18,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            393 => 
            array (
                'id' => 394,
                'name' => 'MAGALLANES',
                'province_id' => 18,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            394 => 
            array (
                'id' => 395,
                'name' => 'MARAGONDON',
                'province_id' => 18,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            395 => 
            array (
                'id' => 396,
            'name' => 'MENDEZ (MENDEZ-NUÑEZ)',
                'province_id' => 18,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            396 => 
            array (
                'id' => 397,
                'name' => 'NAIC',
                'province_id' => 18,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            397 => 
            array (
                'id' => 398,
                'name' => 'NOVELETA',
                'province_id' => 18,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            398 => 
            array (
                'id' => 399,
                'name' => 'ROSARIO',
                'province_id' => 18,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            399 => 
            array (
                'id' => 400,
                'name' => 'SILANG',
                'province_id' => 18,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            400 => 
            array (
                'id' => 401,
                'name' => 'TAGAYTAY CITY',
                'province_id' => 18,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            401 => 
            array (
                'id' => 402,
                'name' => 'TANZA',
                'province_id' => 18,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            402 => 
            array (
                'id' => 403,
                'name' => 'TERNATE',
                'province_id' => 18,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            403 => 
            array (
                'id' => 404,
            'name' => 'TRECE MARTIRES CITY (Capital)',
                'province_id' => 18,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            404 => 
            array (
                'id' => 405,
                'name' => 'GEN. MARIANO ALVAREZ',
                'province_id' => 18,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            405 => 
            array (
                'id' => 406,
                'name' => 'ALAMINOS',
                'province_id' => 19,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            406 => 
            array (
                'id' => 407,
                'name' => 'BAY',
                'province_id' => 19,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            407 => 
            array (
                'id' => 408,
                'name' => 'CITY OF BIÑAN',
                'province_id' => 19,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            408 => 
            array (
                'id' => 409,
                'name' => 'CABUYAO CITY',
                'province_id' => 19,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            409 => 
            array (
                'id' => 410,
                'name' => 'CITY OF CALAMBA',
                'province_id' => 19,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            410 => 
            array (
                'id' => 411,
                'name' => 'CALAUAN',
                'province_id' => 19,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            411 => 
            array (
                'id' => 412,
                'name' => 'CAVINTI',
                'province_id' => 19,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            412 => 
            array (
                'id' => 413,
                'name' => 'FAMY',
                'province_id' => 19,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            413 => 
            array (
                'id' => 414,
                'name' => 'KALAYAAN',
                'province_id' => 19,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            414 => 
            array (
                'id' => 415,
                'name' => 'LILIW',
                'province_id' => 19,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            415 => 
            array (
                'id' => 416,
                'name' => 'LOS BAÑOS',
                'province_id' => 19,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            416 => 
            array (
                'id' => 417,
                'name' => 'LUISIANA',
                'province_id' => 19,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            417 => 
            array (
                'id' => 418,
                'name' => 'LUMBAN',
                'province_id' => 19,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            418 => 
            array (
                'id' => 419,
                'name' => 'MABITAC',
                'province_id' => 19,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            419 => 
            array (
                'id' => 420,
                'name' => 'MAGDALENA',
                'province_id' => 19,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            420 => 
            array (
                'id' => 421,
                'name' => 'MAJAYJAY',
                'province_id' => 19,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            421 => 
            array (
                'id' => 422,
                'name' => 'NAGCARLAN',
                'province_id' => 19,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            422 => 
            array (
                'id' => 423,
                'name' => 'PAETE',
                'province_id' => 19,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            423 => 
            array (
                'id' => 424,
                'name' => 'PAGSANJAN',
                'province_id' => 19,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            424 => 
            array (
                'id' => 425,
                'name' => 'PAKIL',
                'province_id' => 19,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            425 => 
            array (
                'id' => 426,
                'name' => 'PANGIL',
                'province_id' => 19,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            426 => 
            array (
                'id' => 427,
                'name' => 'PILA',
                'province_id' => 19,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            427 => 
            array (
                'id' => 428,
                'name' => 'RIZAL',
                'province_id' => 19,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            428 => 
            array (
                'id' => 429,
                'name' => 'SAN PABLO CITY',
                'province_id' => 19,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            429 => 
            array (
                'id' => 430,
                'name' => 'CITY OF SAN PEDRO',
                'province_id' => 19,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            430 => 
            array (
                'id' => 431,
            'name' => 'SANTA CRUZ (Capital)',
                'province_id' => 19,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            431 => 
            array (
                'id' => 432,
                'name' => 'SANTA MARIA',
                'province_id' => 19,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            432 => 
            array (
                'id' => 433,
                'name' => 'CITY OF SANTA ROSA',
                'province_id' => 19,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            433 => 
            array (
                'id' => 434,
                'name' => 'SINILOAN',
                'province_id' => 19,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            434 => 
            array (
                'id' => 435,
                'name' => 'VICTORIA',
                'province_id' => 19,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            435 => 
            array (
                'id' => 436,
                'name' => 'AGDANGAN',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            436 => 
            array (
                'id' => 437,
                'name' => 'ALABAT',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            437 => 
            array (
                'id' => 438,
                'name' => 'ATIMONAN',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            438 => 
            array (
                'id' => 439,
                'name' => 'BUENAVISTA',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            439 => 
            array (
                'id' => 440,
                'name' => 'BURDEOS',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            440 => 
            array (
                'id' => 441,
                'name' => 'CALAUAG',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            441 => 
            array (
                'id' => 442,
                'name' => 'CANDELARIA',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            442 => 
            array (
                'id' => 443,
                'name' => 'CATANAUAN',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            443 => 
            array (
                'id' => 444,
                'name' => 'DOLORES',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            444 => 
            array (
                'id' => 445,
                'name' => 'GENERAL LUNA',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            445 => 
            array (
                'id' => 446,
                'name' => 'GENERAL NAKAR',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            446 => 
            array (
                'id' => 447,
                'name' => 'GUINAYANGAN',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            447 => 
            array (
                'id' => 448,
                'name' => 'GUMACA',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            448 => 
            array (
                'id' => 449,
                'name' => 'INFANTA',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            449 => 
            array (
                'id' => 450,
                'name' => 'JOMALIG',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            450 => 
            array (
                'id' => 451,
                'name' => 'LOPEZ',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            451 => 
            array (
                'id' => 452,
                'name' => 'LUCBAN',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            452 => 
            array (
                'id' => 453,
            'name' => 'LUCENA CITY (Capital)',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            453 => 
            array (
                'id' => 454,
                'name' => 'MACALELON',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            454 => 
            array (
                'id' => 455,
                'name' => 'MAUBAN',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            455 => 
            array (
                'id' => 456,
                'name' => 'MULANAY',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            456 => 
            array (
                'id' => 457,
                'name' => 'PADRE BURGOS',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            457 => 
            array (
                'id' => 458,
                'name' => 'PAGBILAO',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            458 => 
            array (
                'id' => 459,
                'name' => 'PANUKULAN',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            459 => 
            array (
                'id' => 460,
                'name' => 'PATNANUNGAN',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            460 => 
            array (
                'id' => 461,
                'name' => 'PEREZ',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            461 => 
            array (
                'id' => 462,
                'name' => 'PITOGO',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            462 => 
            array (
                'id' => 463,
                'name' => 'PLARIDEL',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            463 => 
            array (
                'id' => 464,
                'name' => 'POLILLO',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            464 => 
            array (
                'id' => 465,
                'name' => 'QUEZON',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            465 => 
            array (
                'id' => 466,
                'name' => 'REAL',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            466 => 
            array (
                'id' => 467,
                'name' => 'SAMPALOC',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            467 => 
            array (
                'id' => 468,
                'name' => 'SAN ANDRES',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            468 => 
            array (
                'id' => 469,
                'name' => 'SAN ANTONIO',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            469 => 
            array (
                'id' => 470,
            'name' => 'SAN FRANCISCO (AURORA)',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            470 => 
            array (
                'id' => 471,
                'name' => 'SAN NARCISO',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            471 => 
            array (
                'id' => 472,
                'name' => 'SARIAYA',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            472 => 
            array (
                'id' => 473,
                'name' => 'TAGKAWAYAN',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            473 => 
            array (
                'id' => 474,
                'name' => 'CITY OF TAYABAS',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            474 => 
            array (
                'id' => 475,
                'name' => 'TIAONG',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            475 => 
            array (
                'id' => 476,
                'name' => 'UNISAN',
                'province_id' => 20,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            476 => 
            array (
                'id' => 477,
                'name' => 'ANGONO',
                'province_id' => 21,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            477 => 
            array (
                'id' => 478,
                'name' => 'CITY OF ANTIPOLO',
                'province_id' => 21,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            478 => 
            array (
                'id' => 479,
                'name' => 'BARAS',
                'province_id' => 21,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            479 => 
            array (
                'id' => 480,
                'name' => 'BINANGONAN',
                'province_id' => 21,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            480 => 
            array (
                'id' => 481,
                'name' => 'CAINTA',
                'province_id' => 21,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            481 => 
            array (
                'id' => 482,
                'name' => 'CARDONA',
                'province_id' => 21,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            482 => 
            array (
                'id' => 483,
                'name' => 'JALA-JALA',
                'province_id' => 21,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            483 => 
            array (
                'id' => 484,
            'name' => 'RODRIGUEZ (MONTALBAN)',
                'province_id' => 21,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            484 => 
            array (
                'id' => 485,
                'name' => 'MORONG',
                'province_id' => 21,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            485 => 
            array (
                'id' => 486,
                'name' => 'PILILLA',
                'province_id' => 21,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            486 => 
            array (
                'id' => 487,
                'name' => 'SAN MATEO',
                'province_id' => 21,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            487 => 
            array (
                'id' => 488,
                'name' => 'TANAY',
                'province_id' => 21,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            488 => 
            array (
                'id' => 489,
                'name' => 'TAYTAY',
                'province_id' => 21,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            489 => 
            array (
                'id' => 490,
                'name' => 'TERESA',
                'province_id' => 21,
                'region_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            490 => 
            array (
                'id' => 491,
            'name' => 'BOAC (Capital)',
                'province_id' => 22,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            491 => 
            array (
                'id' => 492,
                'name' => 'BUENAVISTA',
                'province_id' => 22,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            492 => 
            array (
                'id' => 493,
                'name' => 'GASAN',
                'province_id' => 22,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            493 => 
            array (
                'id' => 494,
                'name' => 'MOGPOG',
                'province_id' => 22,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            494 => 
            array (
                'id' => 495,
                'name' => 'SANTA CRUZ',
                'province_id' => 22,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            495 => 
            array (
                'id' => 496,
                'name' => 'TORRIJOS',
                'province_id' => 22,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            496 => 
            array (
                'id' => 497,
                'name' => 'ABRA DE ILOG',
                'province_id' => 23,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            497 => 
            array (
                'id' => 498,
                'name' => 'CALINTAAN',
                'province_id' => 23,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            498 => 
            array (
                'id' => 499,
                'name' => 'LOOC',
                'province_id' => 23,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            499 => 
            array (
                'id' => 500,
                'name' => 'LUBANG',
                'province_id' => 23,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        \DB::table('cities')->insert(array (
            0 => 
            array (
                'id' => 501,
                'name' => 'MAGSAYSAY',
                'province_id' => 23,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 502,
            'name' => 'MAMBURAO (Capital)',
                'province_id' => 23,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 503,
                'name' => 'PALUAN',
                'province_id' => 23,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 504,
                'name' => 'RIZAL',
                'province_id' => 23,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 505,
                'name' => 'SABLAYAN',
                'province_id' => 23,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 506,
                'name' => 'SAN JOSE',
                'province_id' => 23,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 507,
                'name' => 'SANTA CRUZ',
                'province_id' => 23,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 508,
                'name' => 'BACO',
                'province_id' => 24,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 509,
                'name' => 'BANSUD',
                'province_id' => 24,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 510,
                'name' => 'BONGABONG',
                'province_id' => 24,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 511,
            'name' => 'BULALACAO (SAN PEDRO)',
                'province_id' => 24,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 512,
            'name' => 'CITY OF CALAPAN (Capital)',
                'province_id' => 24,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 513,
                'name' => 'GLORIA',
                'province_id' => 24,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => 514,
                'name' => 'MANSALAY',
                'province_id' => 24,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => 515,
                'name' => 'NAUJAN',
                'province_id' => 24,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => 516,
                'name' => 'PINAMALAYAN',
                'province_id' => 24,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'id' => 517,
                'name' => 'POLA',
                'province_id' => 24,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'id' => 518,
                'name' => 'PUERTO GALERA',
                'province_id' => 24,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'id' => 519,
                'name' => 'ROXAS',
                'province_id' => 24,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'id' => 520,
                'name' => 'SAN TEODORO',
                'province_id' => 24,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'id' => 521,
                'name' => 'SOCORRO',
                'province_id' => 24,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'id' => 522,
                'name' => 'VICTORIA',
                'province_id' => 24,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'id' => 523,
                'name' => 'ABORLAN',
                'province_id' => 25,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'id' => 524,
                'name' => 'AGUTAYA',
                'province_id' => 25,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'id' => 525,
                'name' => 'ARACELI',
                'province_id' => 25,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            25 => 
            array (
                'id' => 526,
                'name' => 'BALABAC',
                'province_id' => 25,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'id' => 527,
                'name' => 'BATARAZA',
                'province_id' => 25,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            27 => 
            array (
                'id' => 528,
                'name' => 'BROOKE\'S POINT',
                'province_id' => 25,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            28 => 
            array (
                'id' => 529,
                'name' => 'BUSUANGA',
                'province_id' => 25,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            29 => 
            array (
                'id' => 530,
                'name' => 'CAGAYANCILLO',
                'province_id' => 25,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            30 => 
            array (
                'id' => 531,
                'name' => 'CORON',
                'province_id' => 25,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            31 => 
            array (
                'id' => 532,
                'name' => 'CUYO',
                'province_id' => 25,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            32 => 
            array (
                'id' => 533,
                'name' => 'DUMARAN',
                'province_id' => 25,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            33 => 
            array (
                'id' => 534,
            'name' => 'EL NIDO (BACUIT)',
                'province_id' => 25,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            34 => 
            array (
                'id' => 535,
                'name' => 'LINAPACAN',
                'province_id' => 25,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            35 => 
            array (
                'id' => 536,
                'name' => 'MAGSAYSAY',
                'province_id' => 25,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            36 => 
            array (
                'id' => 537,
                'name' => 'NARRA',
                'province_id' => 25,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            37 => 
            array (
                'id' => 538,
            'name' => 'PUERTO PRINCESA CITY (Capital)',
                'province_id' => 25,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            38 => 
            array (
                'id' => 539,
                'name' => 'QUEZON',
                'province_id' => 25,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            39 => 
            array (
                'id' => 540,
                'name' => 'ROXAS',
                'province_id' => 25,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            40 => 
            array (
                'id' => 541,
                'name' => 'SAN VICENTE',
                'province_id' => 25,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            41 => 
            array (
                'id' => 542,
                'name' => 'TAYTAY',
                'province_id' => 25,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            42 => 
            array (
                'id' => 543,
                'name' => 'KALAYAAN',
                'province_id' => 25,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            43 => 
            array (
                'id' => 544,
                'name' => 'CULION',
                'province_id' => 25,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            44 => 
            array (
                'id' => 545,
            'name' => 'RIZAL (MARCOS)',
                'province_id' => 25,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            45 => 
            array (
                'id' => 546,
                'name' => 'SOFRONIO ESPAÑOLA',
                'province_id' => 25,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            46 => 
            array (
                'id' => 547,
                'name' => 'ALCANTARA',
                'province_id' => 26,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            47 => 
            array (
                'id' => 548,
                'name' => 'BANTON',
                'province_id' => 26,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            48 => 
            array (
                'id' => 549,
                'name' => 'CAJIDIOCAN',
                'province_id' => 26,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            49 => 
            array (
                'id' => 550,
                'name' => 'CALATRAVA',
                'province_id' => 26,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            50 => 
            array (
                'id' => 551,
                'name' => 'CONCEPCION',
                'province_id' => 26,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            51 => 
            array (
                'id' => 552,
                'name' => 'CORCUERA',
                'province_id' => 26,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            52 => 
            array (
                'id' => 553,
                'name' => 'LOOC',
                'province_id' => 26,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            53 => 
            array (
                'id' => 554,
                'name' => 'MAGDIWANG',
                'province_id' => 26,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            54 => 
            array (
                'id' => 555,
                'name' => 'ODIONGAN',
                'province_id' => 26,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            55 => 
            array (
                'id' => 556,
            'name' => 'ROMBLON (Capital)',
                'province_id' => 26,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            56 => 
            array (
                'id' => 557,
                'name' => 'SAN AGUSTIN',
                'province_id' => 26,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            57 => 
            array (
                'id' => 558,
                'name' => 'SAN ANDRES',
                'province_id' => 26,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            58 => 
            array (
                'id' => 559,
                'name' => 'SAN FERNANDO',
                'province_id' => 26,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            59 => 
            array (
                'id' => 560,
                'name' => 'SAN JOSE',
                'province_id' => 26,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            60 => 
            array (
                'id' => 561,
                'name' => 'SANTA FE',
                'province_id' => 26,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            61 => 
            array (
                'id' => 562,
                'name' => 'FERROL',
                'province_id' => 26,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            62 => 
            array (
                'id' => 563,
            'name' => 'SANTA MARIA (IMELDA)',
                'province_id' => 26,
                'region_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            63 => 
            array (
                'id' => 564,
                'name' => 'BACACAY',
                'province_id' => 27,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            64 => 
            array (
                'id' => 565,
                'name' => 'CAMALIG',
                'province_id' => 27,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            65 => 
            array (
                'id' => 566,
            'name' => 'DARAGA (LOCSIN)',
                'province_id' => 27,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            66 => 
            array (
                'id' => 567,
                'name' => 'GUINOBATAN',
                'province_id' => 27,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            67 => 
            array (
                'id' => 568,
                'name' => 'JOVELLAR',
                'province_id' => 27,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            68 => 
            array (
                'id' => 569,
            'name' => 'LEGAZPI CITY (Capital)',
                'province_id' => 27,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            69 => 
            array (
                'id' => 570,
                'name' => 'LIBON',
                'province_id' => 27,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            70 => 
            array (
                'id' => 571,
                'name' => 'CITY OF LIGAO',
                'province_id' => 27,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            71 => 
            array (
                'id' => 572,
                'name' => 'MALILIPOT',
                'province_id' => 27,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            72 => 
            array (
                'id' => 573,
                'name' => 'MALINAO',
                'province_id' => 27,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            73 => 
            array (
                'id' => 574,
                'name' => 'MANITO',
                'province_id' => 27,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            74 => 
            array (
                'id' => 575,
                'name' => 'OAS',
                'province_id' => 27,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            75 => 
            array (
                'id' => 576,
                'name' => 'PIO DURAN',
                'province_id' => 27,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            76 => 
            array (
                'id' => 577,
                'name' => 'POLANGUI',
                'province_id' => 27,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            77 => 
            array (
                'id' => 578,
                'name' => 'RAPU-RAPU',
                'province_id' => 27,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            78 => 
            array (
                'id' => 579,
            'name' => 'SANTO DOMINGO (LIBOG)',
                'province_id' => 27,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            79 => 
            array (
                'id' => 580,
                'name' => 'CITY OF TABACO',
                'province_id' => 27,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            80 => 
            array (
                'id' => 581,
                'name' => 'TIWI',
                'province_id' => 27,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            81 => 
            array (
                'id' => 582,
                'name' => 'BASUD',
                'province_id' => 28,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            82 => 
            array (
                'id' => 583,
                'name' => 'CAPALONGA',
                'province_id' => 28,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            83 => 
            array (
                'id' => 584,
            'name' => 'DAET (Capital)',
                'province_id' => 28,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            84 => 
            array (
                'id' => 585,
            'name' => 'SAN LORENZO RUIZ (IMELDA)',
                'province_id' => 28,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            85 => 
            array (
                'id' => 586,
                'name' => 'JOSE PANGANIBAN',
                'province_id' => 28,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            86 => 
            array (
                'id' => 587,
                'name' => 'LABO',
                'province_id' => 28,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            87 => 
            array (
                'id' => 588,
                'name' => 'MERCEDES',
                'province_id' => 28,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            88 => 
            array (
                'id' => 589,
                'name' => 'PARACALE',
                'province_id' => 28,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            89 => 
            array (
                'id' => 590,
                'name' => 'SAN VICENTE',
                'province_id' => 28,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            90 => 
            array (
                'id' => 591,
                'name' => 'SANTA ELENA',
                'province_id' => 28,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            91 => 
            array (
                'id' => 592,
                'name' => 'TALISAY',
                'province_id' => 28,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            92 => 
            array (
                'id' => 593,
                'name' => 'VINZONS',
                'province_id' => 28,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            93 => 
            array (
                'id' => 594,
                'name' => 'BAAO',
                'province_id' => 29,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            94 => 
            array (
                'id' => 595,
                'name' => 'BALATAN',
                'province_id' => 29,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            95 => 
            array (
                'id' => 596,
                'name' => 'BATO',
                'province_id' => 29,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            96 => 
            array (
                'id' => 597,
                'name' => 'BOMBON',
                'province_id' => 29,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            97 => 
            array (
                'id' => 598,
                'name' => 'BUHI',
                'province_id' => 29,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            98 => 
            array (
                'id' => 599,
                'name' => 'BULA',
                'province_id' => 29,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            99 => 
            array (
                'id' => 600,
                'name' => 'CABUSAO',
                'province_id' => 29,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            100 => 
            array (
                'id' => 601,
                'name' => 'CALABANGA',
                'province_id' => 29,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            101 => 
            array (
                'id' => 602,
                'name' => 'CAMALIGAN',
                'province_id' => 29,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            102 => 
            array (
                'id' => 603,
                'name' => 'CANAMAN',
                'province_id' => 29,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            103 => 
            array (
                'id' => 604,
                'name' => 'CARAMOAN',
                'province_id' => 29,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            104 => 
            array (
                'id' => 605,
                'name' => 'DEL GALLEGO',
                'province_id' => 29,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            105 => 
            array (
                'id' => 606,
                'name' => 'GAINZA',
                'province_id' => 29,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            106 => 
            array (
                'id' => 607,
                'name' => 'GARCHITORENA',
                'province_id' => 29,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            107 => 
            array (
                'id' => 608,
                'name' => 'GOA',
                'province_id' => 29,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            108 => 
            array (
                'id' => 609,
                'name' => 'IRIGA CITY',
                'province_id' => 29,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            109 => 
            array (
                'id' => 610,
                'name' => 'LAGONOY',
                'province_id' => 29,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            110 => 
            array (
                'id' => 611,
                'name' => 'LIBMANAN',
                'province_id' => 29,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            111 => 
            array (
                'id' => 612,
                'name' => 'LUPI',
                'province_id' => 29,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            112 => 
            array (
                'id' => 613,
                'name' => 'MAGARAO',
                'province_id' => 29,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            113 => 
            array (
                'id' => 614,
                'name' => 'MILAOR',
                'province_id' => 29,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            114 => 
            array (
                'id' => 615,
                'name' => 'MINALABAC',
                'province_id' => 29,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            115 => 
            array (
                'id' => 616,
                'name' => 'NABUA',
                'province_id' => 29,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            116 => 
            array (
                'id' => 617,
                'name' => 'NAGA CITY',
                'province_id' => 29,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            117 => 
            array (
                'id' => 618,
                'name' => 'OCAMPO',
                'province_id' => 29,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            118 => 
            array (
                'id' => 619,
                'name' => 'PAMPLONA',
                'province_id' => 29,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            119 => 
            array (
                'id' => 620,
                'name' => 'PASACAO',
                'province_id' => 29,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            120 => 
            array (
                'id' => 621,
            'name' => 'PILI (Capital)',
                'province_id' => 29,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            121 => 
            array (
                'id' => 622,
            'name' => 'PRESENTACION (PARUBCAN)',
                'province_id' => 29,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            122 => 
            array (
                'id' => 623,
                'name' => 'RAGAY',
                'province_id' => 29,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            123 => 
            array (
                'id' => 624,
                'name' => 'SAGÑAY',
                'province_id' => 29,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            124 => 
            array (
                'id' => 625,
                'name' => 'SAN FERNANDO',
                'province_id' => 29,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            125 => 
            array (
                'id' => 626,
                'name' => 'SAN JOSE',
                'province_id' => 29,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            126 => 
            array (
                'id' => 627,
                'name' => 'SIPOCOT',
                'province_id' => 29,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            127 => 
            array (
                'id' => 628,
                'name' => 'SIRUMA',
                'province_id' => 29,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            128 => 
            array (
                'id' => 629,
                'name' => 'TIGAON',
                'province_id' => 29,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            129 => 
            array (
                'id' => 630,
                'name' => 'TINAMBAC',
                'province_id' => 29,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            130 => 
            array (
                'id' => 631,
                'name' => 'BAGAMANOC',
                'province_id' => 30,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            131 => 
            array (
                'id' => 632,
                'name' => 'BARAS',
                'province_id' => 30,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            132 => 
            array (
                'id' => 633,
                'name' => 'BATO',
                'province_id' => 30,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            133 => 
            array (
                'id' => 634,
                'name' => 'CARAMORAN',
                'province_id' => 30,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            134 => 
            array (
                'id' => 635,
                'name' => 'GIGMOTO',
                'province_id' => 30,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            135 => 
            array (
                'id' => 636,
                'name' => 'PANDAN',
                'province_id' => 30,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            136 => 
            array (
                'id' => 637,
            'name' => 'PANGANIBAN (PAYO)',
                'province_id' => 30,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            137 => 
            array (
                'id' => 638,
            'name' => 'SAN ANDRES (CALOLBON)',
                'province_id' => 30,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            138 => 
            array (
                'id' => 639,
                'name' => 'SAN MIGUEL',
                'province_id' => 30,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            139 => 
            array (
                'id' => 640,
                'name' => 'VIGA',
                'province_id' => 30,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            140 => 
            array (
                'id' => 641,
            'name' => 'VIRAC (Capital)',
                'province_id' => 30,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            141 => 
            array (
                'id' => 642,
                'name' => 'AROROY',
                'province_id' => 31,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            142 => 
            array (
                'id' => 643,
                'name' => 'BALENO',
                'province_id' => 31,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            143 => 
            array (
                'id' => 644,
                'name' => 'BALUD',
                'province_id' => 31,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            144 => 
            array (
                'id' => 645,
                'name' => 'BATUAN',
                'province_id' => 31,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            145 => 
            array (
                'id' => 646,
                'name' => 'CATAINGAN',
                'province_id' => 31,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            146 => 
            array (
                'id' => 647,
                'name' => 'CAWAYAN',
                'province_id' => 31,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            147 => 
            array (
                'id' => 648,
                'name' => 'CLAVERIA',
                'province_id' => 31,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            148 => 
            array (
                'id' => 649,
                'name' => 'DIMASALANG',
                'province_id' => 31,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            149 => 
            array (
                'id' => 650,
                'name' => 'ESPERANZA',
                'province_id' => 31,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            150 => 
            array (
                'id' => 651,
                'name' => 'MANDAON',
                'province_id' => 31,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            151 => 
            array (
                'id' => 652,
            'name' => 'CITY OF MASBATE (Capital)',
                'province_id' => 31,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            152 => 
            array (
                'id' => 653,
                'name' => 'MILAGROS',
                'province_id' => 31,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            153 => 
            array (
                'id' => 654,
                'name' => 'MOBO',
                'province_id' => 31,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            154 => 
            array (
                'id' => 655,
                'name' => 'MONREAL',
                'province_id' => 31,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            155 => 
            array (
                'id' => 656,
                'name' => 'PALANAS',
                'province_id' => 31,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            156 => 
            array (
                'id' => 657,
            'name' => 'PIO V. CORPUZ (LIMBUHAN)',
                'province_id' => 31,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            157 => 
            array (
                'id' => 658,
                'name' => 'PLACER',
                'province_id' => 31,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            158 => 
            array (
                'id' => 659,
                'name' => 'SAN FERNANDO',
                'province_id' => 31,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            159 => 
            array (
                'id' => 660,
                'name' => 'SAN JACINTO',
                'province_id' => 31,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            160 => 
            array (
                'id' => 661,
                'name' => 'SAN PASCUAL',
                'province_id' => 31,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            161 => 
            array (
                'id' => 662,
                'name' => 'USON',
                'province_id' => 31,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            162 => 
            array (
                'id' => 663,
                'name' => 'BARCELONA',
                'province_id' => 32,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            163 => 
            array (
                'id' => 664,
                'name' => 'BULAN',
                'province_id' => 32,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            164 => 
            array (
                'id' => 665,
                'name' => 'BULUSAN',
                'province_id' => 32,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            165 => 
            array (
                'id' => 666,
                'name' => 'CASIGURAN',
                'province_id' => 32,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            166 => 
            array (
                'id' => 667,
                'name' => 'CASTILLA',
                'province_id' => 32,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            167 => 
            array (
                'id' => 668,
                'name' => 'DONSOL',
                'province_id' => 32,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            168 => 
            array (
                'id' => 669,
                'name' => 'GUBAT',
                'province_id' => 32,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            169 => 
            array (
                'id' => 670,
                'name' => 'IROSIN',
                'province_id' => 32,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            170 => 
            array (
                'id' => 671,
                'name' => 'JUBAN',
                'province_id' => 32,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            171 => 
            array (
                'id' => 672,
                'name' => 'MAGALLANES',
                'province_id' => 32,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            172 => 
            array (
                'id' => 673,
                'name' => 'MATNOG',
                'province_id' => 32,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            173 => 
            array (
                'id' => 674,
                'name' => 'PILAR',
                'province_id' => 32,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            174 => 
            array (
                'id' => 675,
                'name' => 'PRIETO DIAZ',
                'province_id' => 32,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            175 => 
            array (
                'id' => 676,
                'name' => 'SANTA MAGDALENA',
                'province_id' => 32,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            176 => 
            array (
                'id' => 677,
            'name' => 'CITY OF SORSOGON (Capital)',
                'province_id' => 32,
                'region_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            177 => 
            array (
                'id' => 678,
                'name' => 'ALTAVAS',
                'province_id' => 33,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            178 => 
            array (
                'id' => 679,
                'name' => 'BALETE',
                'province_id' => 33,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            179 => 
            array (
                'id' => 680,
                'name' => 'BANGA',
                'province_id' => 33,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            180 => 
            array (
                'id' => 681,
                'name' => 'BATAN',
                'province_id' => 33,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            181 => 
            array (
                'id' => 682,
                'name' => 'BURUANGA',
                'province_id' => 33,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            182 => 
            array (
                'id' => 683,
                'name' => 'IBAJAY',
                'province_id' => 33,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            183 => 
            array (
                'id' => 684,
            'name' => 'KALIBO (Capital)',
                'province_id' => 33,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            184 => 
            array (
                'id' => 685,
                'name' => 'LEZO',
                'province_id' => 33,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            185 => 
            array (
                'id' => 686,
                'name' => 'LIBACAO',
                'province_id' => 33,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            186 => 
            array (
                'id' => 687,
                'name' => 'MADALAG',
                'province_id' => 33,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            187 => 
            array (
                'id' => 688,
                'name' => 'MAKATO',
                'province_id' => 33,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            188 => 
            array (
                'id' => 689,
                'name' => 'MALAY',
                'province_id' => 33,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            189 => 
            array (
                'id' => 690,
                'name' => 'MALINAO',
                'province_id' => 33,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            190 => 
            array (
                'id' => 691,
                'name' => 'NABAS',
                'province_id' => 33,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            191 => 
            array (
                'id' => 692,
                'name' => 'NEW WASHINGTON',
                'province_id' => 33,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            192 => 
            array (
                'id' => 693,
                'name' => 'NUMANCIA',
                'province_id' => 33,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            193 => 
            array (
                'id' => 694,
                'name' => 'TANGALAN',
                'province_id' => 33,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            194 => 
            array (
                'id' => 695,
                'name' => 'ANINI-Y',
                'province_id' => 34,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            195 => 
            array (
                'id' => 696,
                'name' => 'BARBAZA',
                'province_id' => 34,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            196 => 
            array (
                'id' => 697,
                'name' => 'BELISON',
                'province_id' => 34,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            197 => 
            array (
                'id' => 698,
                'name' => 'BUGASONG',
                'province_id' => 34,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            198 => 
            array (
                'id' => 699,
                'name' => 'CALUYA',
                'province_id' => 34,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            199 => 
            array (
                'id' => 700,
                'name' => 'CULASI',
                'province_id' => 34,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            200 => 
            array (
                'id' => 701,
            'name' => 'TOBIAS FORNIER (DAO)',
                'province_id' => 34,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            201 => 
            array (
                'id' => 702,
                'name' => 'HAMTIC',
                'province_id' => 34,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            202 => 
            array (
                'id' => 703,
                'name' => 'LAUA-AN',
                'province_id' => 34,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            203 => 
            array (
                'id' => 704,
                'name' => 'LIBERTAD',
                'province_id' => 34,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            204 => 
            array (
                'id' => 705,
                'name' => 'PANDAN',
                'province_id' => 34,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            205 => 
            array (
                'id' => 706,
                'name' => 'PATNONGON',
                'province_id' => 34,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            206 => 
            array (
                'id' => 707,
            'name' => 'SAN JOSE (Capital)',
                'province_id' => 34,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            207 => 
            array (
                'id' => 708,
                'name' => 'SAN REMIGIO',
                'province_id' => 34,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            208 => 
            array (
                'id' => 709,
                'name' => 'SEBASTE',
                'province_id' => 34,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            209 => 
            array (
                'id' => 710,
                'name' => 'SIBALOM',
                'province_id' => 34,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            210 => 
            array (
                'id' => 711,
                'name' => 'TIBIAO',
                'province_id' => 34,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            211 => 
            array (
                'id' => 712,
                'name' => 'VALDERRAMA',
                'province_id' => 34,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            212 => 
            array (
                'id' => 713,
                'name' => 'CUARTERO',
                'province_id' => 35,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            213 => 
            array (
                'id' => 714,
                'name' => 'DAO',
                'province_id' => 35,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            214 => 
            array (
                'id' => 715,
                'name' => 'DUMALAG',
                'province_id' => 35,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            215 => 
            array (
                'id' => 716,
                'name' => 'DUMARAO',
                'province_id' => 35,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            216 => 
            array (
                'id' => 717,
                'name' => 'IVISAN',
                'province_id' => 35,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            217 => 
            array (
                'id' => 718,
                'name' => 'JAMINDAN',
                'province_id' => 35,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            218 => 
            array (
                'id' => 719,
                'name' => 'MA-AYON',
                'province_id' => 35,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            219 => 
            array (
                'id' => 720,
                'name' => 'MAMBUSAO',
                'province_id' => 35,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            220 => 
            array (
                'id' => 721,
                'name' => 'PANAY',
                'province_id' => 35,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            221 => 
            array (
                'id' => 722,
                'name' => 'PANITAN',
                'province_id' => 35,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            222 => 
            array (
                'id' => 723,
                'name' => 'PILAR',
                'province_id' => 35,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            223 => 
            array (
                'id' => 724,
                'name' => 'PONTEVEDRA',
                'province_id' => 35,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            224 => 
            array (
                'id' => 725,
                'name' => 'PRESIDENT ROXAS',
                'province_id' => 35,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            225 => 
            array (
                'id' => 726,
            'name' => 'ROXAS CITY (Capital)',
                'province_id' => 35,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            226 => 
            array (
                'id' => 727,
                'name' => 'SAPI-AN',
                'province_id' => 35,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            227 => 
            array (
                'id' => 728,
                'name' => 'SIGMA',
                'province_id' => 35,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            228 => 
            array (
                'id' => 729,
                'name' => 'TAPAZ',
                'province_id' => 35,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            229 => 
            array (
                'id' => 730,
                'name' => 'AJUY',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            230 => 
            array (
                'id' => 731,
                'name' => 'ALIMODIAN',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            231 => 
            array (
                'id' => 732,
                'name' => 'ANILAO',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            232 => 
            array (
                'id' => 733,
                'name' => 'BADIANGAN',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            233 => 
            array (
                'id' => 734,
                'name' => 'BALASAN',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            234 => 
            array (
                'id' => 735,
                'name' => 'BANATE',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            235 => 
            array (
                'id' => 736,
                'name' => 'BAROTAC NUEVO',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            236 => 
            array (
                'id' => 737,
                'name' => 'BAROTAC VIEJO',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            237 => 
            array (
                'id' => 738,
                'name' => 'BATAD',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            238 => 
            array (
                'id' => 739,
                'name' => 'BINGAWAN',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            239 => 
            array (
                'id' => 740,
                'name' => 'CABATUAN',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            240 => 
            array (
                'id' => 741,
                'name' => 'CALINOG',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            241 => 
            array (
                'id' => 742,
                'name' => 'CARLES',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            242 => 
            array (
                'id' => 743,
                'name' => 'CONCEPCION',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            243 => 
            array (
                'id' => 744,
                'name' => 'DINGLE',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            244 => 
            array (
                'id' => 745,
                'name' => 'DUEÑAS',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            245 => 
            array (
                'id' => 746,
                'name' => 'DUMANGAS',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            246 => 
            array (
                'id' => 747,
                'name' => 'ESTANCIA',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            247 => 
            array (
                'id' => 748,
                'name' => 'GUIMBAL',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            248 => 
            array (
                'id' => 749,
                'name' => 'IGBARAS',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            249 => 
            array (
                'id' => 750,
            'name' => 'ILOILO CITY (Capital)',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            250 => 
            array (
                'id' => 751,
                'name' => 'JANIUAY',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            251 => 
            array (
                'id' => 752,
                'name' => 'LAMBUNAO',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            252 => 
            array (
                'id' => 753,
                'name' => 'LEGANES',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            253 => 
            array (
                'id' => 754,
                'name' => 'LEMERY',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            254 => 
            array (
                'id' => 755,
                'name' => 'LEON',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            255 => 
            array (
                'id' => 756,
                'name' => 'MAASIN',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            256 => 
            array (
                'id' => 757,
                'name' => 'MIAGAO',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            257 => 
            array (
                'id' => 758,
                'name' => 'MINA',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            258 => 
            array (
                'id' => 759,
                'name' => 'NEW LUCENA',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            259 => 
            array (
                'id' => 760,
                'name' => 'OTON',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            260 => 
            array (
                'id' => 761,
                'name' => 'CITY OF PASSI',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            261 => 
            array (
                'id' => 762,
                'name' => 'PAVIA',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            262 => 
            array (
                'id' => 763,
                'name' => 'POTOTAN',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            263 => 
            array (
                'id' => 764,
                'name' => 'SAN DIONISIO',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            264 => 
            array (
                'id' => 765,
                'name' => 'SAN ENRIQUE',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            265 => 
            array (
                'id' => 766,
                'name' => 'SAN JOAQUIN',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            266 => 
            array (
                'id' => 767,
                'name' => 'SAN MIGUEL',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            267 => 
            array (
                'id' => 768,
                'name' => 'SAN RAFAEL',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            268 => 
            array (
                'id' => 769,
                'name' => 'SANTA BARBARA',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            269 => 
            array (
                'id' => 770,
                'name' => 'SARA',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            270 => 
            array (
                'id' => 771,
                'name' => 'TIGBAUAN',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            271 => 
            array (
                'id' => 772,
                'name' => 'TUBUNGAN',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            272 => 
            array (
                'id' => 773,
                'name' => 'ZARRAGA',
                'province_id' => 36,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            273 => 
            array (
                'id' => 774,
            'name' => 'BACOLOD CITY (Capital)',
                'province_id' => 37,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            274 => 
            array (
                'id' => 775,
                'name' => 'BAGO CITY',
                'province_id' => 37,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            275 => 
            array (
                'id' => 776,
                'name' => 'BINALBAGAN',
                'province_id' => 37,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            276 => 
            array (
                'id' => 777,
                'name' => 'CADIZ CITY',
                'province_id' => 37,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            277 => 
            array (
                'id' => 778,
                'name' => 'CALATRAVA',
                'province_id' => 37,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            278 => 
            array (
                'id' => 779,
                'name' => 'CANDONI',
                'province_id' => 37,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            279 => 
            array (
                'id' => 780,
                'name' => 'CAUAYAN',
                'province_id' => 37,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            280 => 
            array (
                'id' => 781,
            'name' => 'ENRIQUE B. MAGALONA (SARAVIA)',
                'province_id' => 37,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            281 => 
            array (
                'id' => 782,
                'name' => 'CITY OF ESCALANTE',
                'province_id' => 37,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            282 => 
            array (
                'id' => 783,
                'name' => 'CITY OF HIMAMAYLAN',
                'province_id' => 37,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            283 => 
            array (
                'id' => 784,
                'name' => 'HINIGARAN',
                'province_id' => 37,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            284 => 
            array (
                'id' => 785,
            'name' => 'HINOBA-AN (ASIA)',
                'province_id' => 37,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            285 => 
            array (
                'id' => 786,
                'name' => 'ILOG',
                'province_id' => 37,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            286 => 
            array (
                'id' => 787,
                'name' => 'ISABELA',
                'province_id' => 37,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            287 => 
            array (
                'id' => 788,
                'name' => 'CITY OF KABANKALAN',
                'province_id' => 37,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            288 => 
            array (
                'id' => 789,
                'name' => 'LA CARLOTA CITY',
                'province_id' => 37,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            289 => 
            array (
                'id' => 790,
                'name' => 'LA CASTELLANA',
                'province_id' => 37,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            290 => 
            array (
                'id' => 791,
                'name' => 'MANAPLA',
                'province_id' => 37,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            291 => 
            array (
                'id' => 792,
            'name' => 'MOISES PADILLA (MAGALLON)',
                'province_id' => 37,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            292 => 
            array (
                'id' => 793,
                'name' => 'MURCIA',
                'province_id' => 37,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            293 => 
            array (
                'id' => 794,
                'name' => 'PONTEVEDRA',
                'province_id' => 37,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            294 => 
            array (
                'id' => 795,
                'name' => 'PULUPANDAN',
                'province_id' => 37,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            295 => 
            array (
                'id' => 796,
                'name' => 'SAGAY CITY',
                'province_id' => 37,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            296 => 
            array (
                'id' => 797,
                'name' => 'SAN CARLOS CITY',
                'province_id' => 37,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            297 => 
            array (
                'id' => 798,
                'name' => 'SAN ENRIQUE',
                'province_id' => 37,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            298 => 
            array (
                'id' => 799,
                'name' => 'SILAY CITY',
                'province_id' => 37,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            299 => 
            array (
                'id' => 800,
                'name' => 'CITY OF SIPALAY',
                'province_id' => 37,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            300 => 
            array (
                'id' => 801,
                'name' => 'CITY OF TALISAY',
                'province_id' => 37,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            301 => 
            array (
                'id' => 802,
                'name' => 'TOBOSO',
                'province_id' => 37,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            302 => 
            array (
                'id' => 803,
                'name' => 'VALLADOLID',
                'province_id' => 37,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            303 => 
            array (
                'id' => 804,
                'name' => 'CITY OF VICTORIAS',
                'province_id' => 37,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            304 => 
            array (
                'id' => 805,
                'name' => 'SALVADOR BENEDICTO',
                'province_id' => 37,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            305 => 
            array (
                'id' => 806,
                'name' => 'BUENAVISTA',
                'province_id' => 38,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            306 => 
            array (
                'id' => 807,
            'name' => 'JORDAN (Capital)',
                'province_id' => 38,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            307 => 
            array (
                'id' => 808,
                'name' => 'NUEVA VALENCIA',
                'province_id' => 38,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            308 => 
            array (
                'id' => 809,
                'name' => 'SAN LORENZO',
                'province_id' => 38,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            309 => 
            array (
                'id' => 810,
                'name' => 'SIBUNAG',
                'province_id' => 38,
                'region_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            310 => 
            array (
                'id' => 811,
                'name' => 'ALBURQUERQUE',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            311 => 
            array (
                'id' => 812,
                'name' => 'ALICIA',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            312 => 
            array (
                'id' => 813,
                'name' => 'ANDA',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            313 => 
            array (
                'id' => 814,
                'name' => 'ANTEQUERA',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            314 => 
            array (
                'id' => 815,
                'name' => 'BACLAYON',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            315 => 
            array (
                'id' => 816,
                'name' => 'BALILIHAN',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            316 => 
            array (
                'id' => 817,
                'name' => 'BATUAN',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            317 => 
            array (
                'id' => 818,
                'name' => 'BILAR',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            318 => 
            array (
                'id' => 819,
                'name' => 'BUENAVISTA',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            319 => 
            array (
                'id' => 820,
                'name' => 'CALAPE',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            320 => 
            array (
                'id' => 821,
                'name' => 'CANDIJAY',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            321 => 
            array (
                'id' => 822,
                'name' => 'CARMEN',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            322 => 
            array (
                'id' => 823,
                'name' => 'CATIGBIAN',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            323 => 
            array (
                'id' => 824,
                'name' => 'CLARIN',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            324 => 
            array (
                'id' => 825,
                'name' => 'CORELLA',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            325 => 
            array (
                'id' => 826,
                'name' => 'CORTES',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            326 => 
            array (
                'id' => 827,
                'name' => 'DAGOHOY',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            327 => 
            array (
                'id' => 828,
                'name' => 'DANAO',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            328 => 
            array (
                'id' => 829,
                'name' => 'DAUIS',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            329 => 
            array (
                'id' => 830,
                'name' => 'DIMIAO',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            330 => 
            array (
                'id' => 831,
                'name' => 'DUERO',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            331 => 
            array (
                'id' => 832,
                'name' => 'GARCIA HERNANDEZ',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            332 => 
            array (
                'id' => 833,
                'name' => 'GUINDULMAN',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            333 => 
            array (
                'id' => 834,
                'name' => 'INABANGA',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            334 => 
            array (
                'id' => 835,
                'name' => 'JAGNA',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            335 => 
            array (
                'id' => 836,
                'name' => 'JETAFE',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            336 => 
            array (
                'id' => 837,
                'name' => 'LILA',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            337 => 
            array (
                'id' => 838,
                'name' => 'LOAY',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            338 => 
            array (
                'id' => 839,
                'name' => 'LOBOC',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            339 => 
            array (
                'id' => 840,
                'name' => 'LOON',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            340 => 
            array (
                'id' => 841,
                'name' => 'MABINI',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            341 => 
            array (
                'id' => 842,
                'name' => 'MARIBOJOC',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            342 => 
            array (
                'id' => 843,
                'name' => 'PANGLAO',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            343 => 
            array (
                'id' => 844,
                'name' => 'PILAR',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            344 => 
            array (
                'id' => 845,
            'name' => 'PRES. CARLOS P. GARCIA (PITOGO)',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            345 => 
            array (
                'id' => 846,
            'name' => 'SAGBAYAN (BORJA)',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            346 => 
            array (
                'id' => 847,
                'name' => 'SAN ISIDRO',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            347 => 
            array (
                'id' => 848,
                'name' => 'SAN MIGUEL',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            348 => 
            array (
                'id' => 849,
                'name' => 'SEVILLA',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            349 => 
            array (
                'id' => 850,
                'name' => 'SIERRA BULLONES',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            350 => 
            array (
                'id' => 851,
                'name' => 'SIKATUNA',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            351 => 
            array (
                'id' => 852,
            'name' => 'TAGBILARAN CITY (Capital)',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            352 => 
            array (
                'id' => 853,
                'name' => 'TALIBON',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            353 => 
            array (
                'id' => 854,
                'name' => 'TRINIDAD',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            354 => 
            array (
                'id' => 855,
                'name' => 'TUBIGON',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            355 => 
            array (
                'id' => 856,
                'name' => 'UBAY',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            356 => 
            array (
                'id' => 857,
                'name' => 'VALENCIA',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            357 => 
            array (
                'id' => 858,
                'name' => 'BIEN UNIDO',
                'province_id' => 39,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            358 => 
            array (
                'id' => 859,
                'name' => 'ALCANTARA',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            359 => 
            array (
                'id' => 860,
                'name' => 'ALCOY',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            360 => 
            array (
                'id' => 861,
                'name' => 'ALEGRIA',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            361 => 
            array (
                'id' => 862,
                'name' => 'ALOGUINSAN',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            362 => 
            array (
                'id' => 863,
                'name' => 'ARGAO',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            363 => 
            array (
                'id' => 864,
                'name' => 'ASTURIAS',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            364 => 
            array (
                'id' => 865,
                'name' => 'BADIAN',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            365 => 
            array (
                'id' => 866,
                'name' => 'BALAMBAN',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            366 => 
            array (
                'id' => 867,
                'name' => 'BANTAYAN',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            367 => 
            array (
                'id' => 868,
                'name' => 'BARILI',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            368 => 
            array (
                'id' => 869,
                'name' => 'CITY OF BOGO',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            369 => 
            array (
                'id' => 870,
                'name' => 'BOLJOON',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            370 => 
            array (
                'id' => 871,
                'name' => 'BORBON',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            371 => 
            array (
                'id' => 872,
                'name' => 'CITY OF CARCAR',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            372 => 
            array (
                'id' => 873,
                'name' => 'CARMEN',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            373 => 
            array (
                'id' => 874,
                'name' => 'CATMON',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            374 => 
            array (
                'id' => 875,
            'name' => 'CEBU CITY (Capital)',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            375 => 
            array (
                'id' => 876,
                'name' => 'COMPOSTELA',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            376 => 
            array (
                'id' => 877,
                'name' => 'CONSOLACION',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            377 => 
            array (
                'id' => 878,
                'name' => 'CORDOVA',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            378 => 
            array (
                'id' => 879,
                'name' => 'DAANBANTAYAN',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            379 => 
            array (
                'id' => 880,
                'name' => 'DALAGUETE',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            380 => 
            array (
                'id' => 881,
                'name' => 'DANAO CITY',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            381 => 
            array (
                'id' => 882,
                'name' => 'DUMANJUG',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            382 => 
            array (
                'id' => 883,
                'name' => 'GINATILAN',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            383 => 
            array (
                'id' => 884,
            'name' => 'LAPU-LAPU CITY (OPON)',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            384 => 
            array (
                'id' => 885,
                'name' => 'LILOAN',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            385 => 
            array (
                'id' => 886,
                'name' => 'MADRIDEJOS',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            386 => 
            array (
                'id' => 887,
                'name' => 'MALABUYOC',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            387 => 
            array (
                'id' => 888,
                'name' => 'MANDAUE CITY',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            388 => 
            array (
                'id' => 889,
                'name' => 'MEDELLIN',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            389 => 
            array (
                'id' => 890,
                'name' => 'MINGLANILLA',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            390 => 
            array (
                'id' => 891,
                'name' => 'MOALBOAL',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            391 => 
            array (
                'id' => 892,
                'name' => 'CITY OF NAGA',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            392 => 
            array (
                'id' => 893,
                'name' => 'OSLOB',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            393 => 
            array (
                'id' => 894,
                'name' => 'PILAR',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            394 => 
            array (
                'id' => 895,
                'name' => 'PINAMUNGAHAN',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            395 => 
            array (
                'id' => 896,
                'name' => 'PORO',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            396 => 
            array (
                'id' => 897,
                'name' => 'RONDA',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            397 => 
            array (
                'id' => 898,
                'name' => 'SAMBOAN',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            398 => 
            array (
                'id' => 899,
                'name' => 'SAN FERNANDO',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            399 => 
            array (
                'id' => 900,
                'name' => 'SAN FRANCISCO',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            400 => 
            array (
                'id' => 901,
                'name' => 'SAN REMIGIO',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            401 => 
            array (
                'id' => 902,
                'name' => 'SANTA FE',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            402 => 
            array (
                'id' => 903,
                'name' => 'SANTANDER',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            403 => 
            array (
                'id' => 904,
                'name' => 'SIBONGA',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            404 => 
            array (
                'id' => 905,
                'name' => 'SOGOD',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            405 => 
            array (
                'id' => 906,
                'name' => 'TABOGON',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            406 => 
            array (
                'id' => 907,
                'name' => 'TABUELAN',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            407 => 
            array (
                'id' => 908,
                'name' => 'CITY OF TALISAY',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            408 => 
            array (
                'id' => 909,
                'name' => 'TOLEDO CITY',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            409 => 
            array (
                'id' => 910,
                'name' => 'TUBURAN',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            410 => 
            array (
                'id' => 911,
                'name' => 'TUDELA',
                'province_id' => 40,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            411 => 
            array (
                'id' => 912,
            'name' => 'AMLAN (AYUQUITAN)',
                'province_id' => 41,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            412 => 
            array (
                'id' => 913,
                'name' => 'AYUNGON',
                'province_id' => 41,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            413 => 
            array (
                'id' => 914,
                'name' => 'BACONG',
                'province_id' => 41,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            414 => 
            array (
                'id' => 915,
                'name' => 'BAIS CITY',
                'province_id' => 41,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            415 => 
            array (
                'id' => 916,
                'name' => 'BASAY',
                'province_id' => 41,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            416 => 
            array (
                'id' => 917,
            'name' => 'CITY OF BAYAWAN (TULONG)',
                'province_id' => 41,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            417 => 
            array (
                'id' => 918,
            'name' => 'BINDOY (PAYABON)',
                'province_id' => 41,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            418 => 
            array (
                'id' => 919,
                'name' => 'CANLAON CITY',
                'province_id' => 41,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            419 => 
            array (
                'id' => 920,
                'name' => 'DAUIN',
                'province_id' => 41,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            420 => 
            array (
                'id' => 921,
            'name' => 'DUMAGUETE CITY (Capital)',
                'province_id' => 41,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            421 => 
            array (
                'id' => 922,
                'name' => 'CITY OF GUIHULNGAN',
                'province_id' => 41,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            422 => 
            array (
                'id' => 923,
                'name' => 'JIMALALUD',
                'province_id' => 41,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            423 => 
            array (
                'id' => 924,
                'name' => 'LA LIBERTAD',
                'province_id' => 41,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            424 => 
            array (
                'id' => 925,
                'name' => 'MABINAY',
                'province_id' => 41,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            425 => 
            array (
                'id' => 926,
                'name' => 'MANJUYOD',
                'province_id' => 41,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            426 => 
            array (
                'id' => 927,
                'name' => 'PAMPLONA',
                'province_id' => 41,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            427 => 
            array (
                'id' => 928,
                'name' => 'SAN JOSE',
                'province_id' => 41,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            428 => 
            array (
                'id' => 929,
                'name' => 'SANTA CATALINA',
                'province_id' => 41,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            429 => 
            array (
                'id' => 930,
                'name' => 'SIATON',
                'province_id' => 41,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            430 => 
            array (
                'id' => 931,
                'name' => 'SIBULAN',
                'province_id' => 41,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            431 => 
            array (
                'id' => 932,
                'name' => 'CITY OF TANJAY',
                'province_id' => 41,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            432 => 
            array (
                'id' => 933,
                'name' => 'TAYASAN',
                'province_id' => 41,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            433 => 
            array (
                'id' => 934,
            'name' => 'VALENCIA (LUZURRIAGA)',
                'province_id' => 41,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            434 => 
            array (
                'id' => 935,
                'name' => 'VALLEHERMOSO',
                'province_id' => 41,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            435 => 
            array (
                'id' => 936,
                'name' => 'ZAMBOANGUITA',
                'province_id' => 41,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            436 => 
            array (
                'id' => 937,
                'name' => 'ENRIQUE VILLANUEVA',
                'province_id' => 42,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            437 => 
            array (
                'id' => 938,
                'name' => 'LARENA',
                'province_id' => 42,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            438 => 
            array (
                'id' => 939,
                'name' => 'LAZI',
                'province_id' => 42,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            439 => 
            array (
                'id' => 940,
                'name' => 'MARIA',
                'province_id' => 42,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            440 => 
            array (
                'id' => 941,
                'name' => 'SAN JUAN',
                'province_id' => 42,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            441 => 
            array (
                'id' => 942,
            'name' => 'SIQUIJOR (Capital)',
                'province_id' => 42,
                'region_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            442 => 
            array (
                'id' => 943,
                'name' => 'ARTECHE',
                'province_id' => 43,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            443 => 
            array (
                'id' => 944,
                'name' => 'BALANGIGA',
                'province_id' => 43,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            444 => 
            array (
                'id' => 945,
                'name' => 'BALANGKAYAN',
                'province_id' => 43,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            445 => 
            array (
                'id' => 946,
            'name' => 'CITY OF BORONGAN (Capital)',
                'province_id' => 43,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            446 => 
            array (
                'id' => 947,
                'name' => 'CAN-AVID',
                'province_id' => 43,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            447 => 
            array (
                'id' => 948,
                'name' => 'DOLORES',
                'province_id' => 43,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            448 => 
            array (
                'id' => 949,
                'name' => 'GENERAL MACARTHUR',
                'province_id' => 43,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            449 => 
            array (
                'id' => 950,
                'name' => 'GIPORLOS',
                'province_id' => 43,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            450 => 
            array (
                'id' => 951,
                'name' => 'GUIUAN',
                'province_id' => 43,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            451 => 
            array (
                'id' => 952,
                'name' => 'HERNANI',
                'province_id' => 43,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            452 => 
            array (
                'id' => 953,
                'name' => 'JIPAPAD',
                'province_id' => 43,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            453 => 
            array (
                'id' => 954,
                'name' => 'LAWAAN',
                'province_id' => 43,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            454 => 
            array (
                'id' => 955,
                'name' => 'LLORENTE',
                'province_id' => 43,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            455 => 
            array (
                'id' => 956,
                'name' => 'MASLOG',
                'province_id' => 43,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            456 => 
            array (
                'id' => 957,
                'name' => 'MAYDOLONG',
                'province_id' => 43,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            457 => 
            array (
                'id' => 958,
                'name' => 'MERCEDES',
                'province_id' => 43,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            458 => 
            array (
                'id' => 959,
                'name' => 'ORAS',
                'province_id' => 43,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            459 => 
            array (
                'id' => 960,
                'name' => 'QUINAPONDAN',
                'province_id' => 43,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            460 => 
            array (
                'id' => 961,
                'name' => 'SALCEDO',
                'province_id' => 43,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            461 => 
            array (
                'id' => 962,
                'name' => 'SAN JULIAN',
                'province_id' => 43,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            462 => 
            array (
                'id' => 963,
                'name' => 'SAN POLICARPO',
                'province_id' => 43,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            463 => 
            array (
                'id' => 964,
                'name' => 'SULAT',
                'province_id' => 43,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            464 => 
            array (
                'id' => 965,
                'name' => 'TAFT',
                'province_id' => 43,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            465 => 
            array (
                'id' => 966,
                'name' => 'ABUYOG',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            466 => 
            array (
                'id' => 967,
                'name' => 'ALANGALANG',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            467 => 
            array (
                'id' => 968,
                'name' => 'ALBUERA',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            468 => 
            array (
                'id' => 969,
                'name' => 'BABATNGON',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            469 => 
            array (
                'id' => 970,
                'name' => 'BARUGO',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            470 => 
            array (
                'id' => 971,
                'name' => 'BATO',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            471 => 
            array (
                'id' => 972,
                'name' => 'CITY OF BAYBAY',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            472 => 
            array (
                'id' => 973,
                'name' => 'BURAUEN',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            473 => 
            array (
                'id' => 974,
                'name' => 'CALUBIAN',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            474 => 
            array (
                'id' => 975,
                'name' => 'CAPOOCAN',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            475 => 
            array (
                'id' => 976,
                'name' => 'CARIGARA',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            476 => 
            array (
                'id' => 977,
                'name' => 'DAGAMI',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            477 => 
            array (
                'id' => 978,
                'name' => 'DULAG',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            478 => 
            array (
                'id' => 979,
                'name' => 'HILONGOS',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            479 => 
            array (
                'id' => 980,
                'name' => 'HINDANG',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            480 => 
            array (
                'id' => 981,
                'name' => 'INOPACAN',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            481 => 
            array (
                'id' => 982,
                'name' => 'ISABEL',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            482 => 
            array (
                'id' => 983,
                'name' => 'JARO',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            483 => 
            array (
                'id' => 984,
            'name' => 'JAVIER (BUGHO)',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            484 => 
            array (
                'id' => 985,
                'name' => 'JULITA',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            485 => 
            array (
                'id' => 986,
                'name' => 'KANANGA',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            486 => 
            array (
                'id' => 987,
                'name' => 'LA PAZ',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            487 => 
            array (
                'id' => 988,
                'name' => 'LEYTE',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            488 => 
            array (
                'id' => 989,
                'name' => 'MACARTHUR',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            489 => 
            array (
                'id' => 990,
                'name' => 'MAHAPLAG',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            490 => 
            array (
                'id' => 991,
                'name' => 'MATAG-OB',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            491 => 
            array (
                'id' => 992,
                'name' => 'MATALOM',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            492 => 
            array (
                'id' => 993,
                'name' => 'MAYORGA',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            493 => 
            array (
                'id' => 994,
                'name' => 'MERIDA',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            494 => 
            array (
                'id' => 995,
                'name' => 'ORMOC CITY',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            495 => 
            array (
                'id' => 996,
                'name' => 'PALO',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            496 => 
            array (
                'id' => 997,
                'name' => 'PALOMPON',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            497 => 
            array (
                'id' => 998,
                'name' => 'PASTRANA',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            498 => 
            array (
                'id' => 999,
                'name' => 'SAN ISIDRO',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            499 => 
            array (
                'id' => 1000,
                'name' => 'SAN MIGUEL',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        \DB::table('cities')->insert(array (
            0 => 
            array (
                'id' => 1001,
                'name' => 'SANTA FE',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 1002,
                'name' => 'TABANGO',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 1003,
                'name' => 'TABONTABON',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 1004,
            'name' => 'TACLOBAN CITY (Capital)',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 1005,
                'name' => 'TANAUAN',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 1006,
                'name' => 'TOLOSA',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 1007,
                'name' => 'TUNGA',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 1008,
                'name' => 'VILLABA',
                'province_id' => 44,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 1009,
                'name' => 'ALLEN',
                'province_id' => 45,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 1010,
                'name' => 'BIRI',
                'province_id' => 45,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 1011,
                'name' => 'BOBON',
                'province_id' => 45,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 1012,
                'name' => 'CAPUL',
                'province_id' => 45,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 1013,
            'name' => 'CATARMAN (Capital)',
                'province_id' => 45,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => 1014,
                'name' => 'CATUBIG',
                'province_id' => 45,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => 1015,
                'name' => 'GAMAY',
                'province_id' => 45,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => 1016,
                'name' => 'LAOANG',
                'province_id' => 45,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'id' => 1017,
                'name' => 'LAPINIG',
                'province_id' => 45,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'id' => 1018,
                'name' => 'LAS NAVAS',
                'province_id' => 45,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'id' => 1019,
                'name' => 'LAVEZARES',
                'province_id' => 45,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'id' => 1020,
                'name' => 'MAPANAS',
                'province_id' => 45,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'id' => 1021,
                'name' => 'MONDRAGON',
                'province_id' => 45,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'id' => 1022,
                'name' => 'PALAPAG',
                'province_id' => 45,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'id' => 1023,
                'name' => 'PAMBUJAN',
                'province_id' => 45,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'id' => 1024,
                'name' => 'ROSARIO',
                'province_id' => 45,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'id' => 1025,
                'name' => 'SAN ANTONIO',
                'province_id' => 45,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            25 => 
            array (
                'id' => 1026,
                'name' => 'SAN ISIDRO',
                'province_id' => 45,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'id' => 1027,
                'name' => 'SAN JOSE',
                'province_id' => 45,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            27 => 
            array (
                'id' => 1028,
                'name' => 'SAN ROQUE',
                'province_id' => 45,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            28 => 
            array (
                'id' => 1029,
                'name' => 'SAN VICENTE',
                'province_id' => 45,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            29 => 
            array (
                'id' => 1030,
                'name' => 'SILVINO LOBOS',
                'province_id' => 45,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            30 => 
            array (
                'id' => 1031,
                'name' => 'VICTORIA',
                'province_id' => 45,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            31 => 
            array (
                'id' => 1032,
                'name' => 'LOPE DE VEGA',
                'province_id' => 45,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            32 => 
            array (
                'id' => 1033,
                'name' => 'ALMAGRO',
                'province_id' => 46,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            33 => 
            array (
                'id' => 1034,
                'name' => 'BASEY',
                'province_id' => 46,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            34 => 
            array (
                'id' => 1035,
                'name' => 'CALBAYOG CITY',
                'province_id' => 46,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            35 => 
            array (
                'id' => 1036,
                'name' => 'CALBIGA',
                'province_id' => 46,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            36 => 
            array (
                'id' => 1037,
            'name' => 'CITY OF CATBALOGAN (Capital)',
                'province_id' => 46,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            37 => 
            array (
                'id' => 1038,
                'name' => 'DARAM',
                'province_id' => 46,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            38 => 
            array (
                'id' => 1039,
                'name' => 'GANDARA',
                'province_id' => 46,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            39 => 
            array (
                'id' => 1040,
                'name' => 'HINABANGAN',
                'province_id' => 46,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            40 => 
            array (
                'id' => 1041,
                'name' => 'JIABONG',
                'province_id' => 46,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            41 => 
            array (
                'id' => 1042,
                'name' => 'MARABUT',
                'province_id' => 46,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            42 => 
            array (
                'id' => 1043,
                'name' => 'MATUGUINAO',
                'province_id' => 46,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            43 => 
            array (
                'id' => 1044,
                'name' => 'MOTIONG',
                'province_id' => 46,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            44 => 
            array (
                'id' => 1045,
                'name' => 'PINABACDAO',
                'province_id' => 46,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            45 => 
            array (
                'id' => 1046,
                'name' => 'SAN JOSE DE BUAN',
                'province_id' => 46,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            46 => 
            array (
                'id' => 1047,
                'name' => 'SAN SEBASTIAN',
                'province_id' => 46,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            47 => 
            array (
                'id' => 1048,
                'name' => 'SANTA MARGARITA',
                'province_id' => 46,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            48 => 
            array (
                'id' => 1049,
                'name' => 'SANTA RITA',
                'province_id' => 46,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            49 => 
            array (
                'id' => 1050,
                'name' => 'SANTO NIÑO',
                'province_id' => 46,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            50 => 
            array (
                'id' => 1051,
                'name' => 'TALALORA',
                'province_id' => 46,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            51 => 
            array (
                'id' => 1052,
                'name' => 'TARANGNAN',
                'province_id' => 46,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            52 => 
            array (
                'id' => 1053,
                'name' => 'VILLAREAL',
                'province_id' => 46,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            53 => 
            array (
                'id' => 1054,
            'name' => 'PARANAS (WRIGHT)',
                'province_id' => 46,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            54 => 
            array (
                'id' => 1055,
                'name' => 'ZUMARRAGA',
                'province_id' => 46,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            55 => 
            array (
                'id' => 1056,
                'name' => 'TAGAPUL-AN',
                'province_id' => 46,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            56 => 
            array (
                'id' => 1057,
                'name' => 'SAN JORGE',
                'province_id' => 46,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            57 => 
            array (
                'id' => 1058,
                'name' => 'PAGSANGHAN',
                'province_id' => 46,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            58 => 
            array (
                'id' => 1059,
                'name' => 'ANAHAWAN',
                'province_id' => 47,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            59 => 
            array (
                'id' => 1060,
                'name' => 'BONTOC',
                'province_id' => 47,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            60 => 
            array (
                'id' => 1061,
                'name' => 'HINUNANGAN',
                'province_id' => 47,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            61 => 
            array (
                'id' => 1062,
                'name' => 'HINUNDAYAN',
                'province_id' => 47,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            62 => 
            array (
                'id' => 1063,
                'name' => 'LIBAGON',
                'province_id' => 47,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            63 => 
            array (
                'id' => 1064,
                'name' => 'LILOAN',
                'province_id' => 47,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            64 => 
            array (
                'id' => 1065,
            'name' => 'CITY OF MAASIN (Capital)',
                'province_id' => 47,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            65 => 
            array (
                'id' => 1066,
                'name' => 'MACROHON',
                'province_id' => 47,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            66 => 
            array (
                'id' => 1067,
                'name' => 'MALITBOG',
                'province_id' => 47,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            67 => 
            array (
                'id' => 1068,
                'name' => 'PADRE BURGOS',
                'province_id' => 47,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            68 => 
            array (
                'id' => 1069,
                'name' => 'PINTUYAN',
                'province_id' => 47,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            69 => 
            array (
                'id' => 1070,
                'name' => 'SAINT BERNARD',
                'province_id' => 47,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            70 => 
            array (
                'id' => 1071,
                'name' => 'SAN FRANCISCO',
                'province_id' => 47,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            71 => 
            array (
                'id' => 1072,
            'name' => 'SAN JUAN (CABALIAN)',
                'province_id' => 47,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            72 => 
            array (
                'id' => 1073,
                'name' => 'SAN RICARDO',
                'province_id' => 47,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            73 => 
            array (
                'id' => 1074,
                'name' => 'SILAGO',
                'province_id' => 47,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            74 => 
            array (
                'id' => 1075,
                'name' => 'SOGOD',
                'province_id' => 47,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            75 => 
            array (
                'id' => 1076,
                'name' => 'TOMAS OPPUS',
                'province_id' => 47,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            76 => 
            array (
                'id' => 1077,
                'name' => 'LIMASAWA',
                'province_id' => 47,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            77 => 
            array (
                'id' => 1078,
                'name' => 'ALMERIA',
                'province_id' => 48,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            78 => 
            array (
                'id' => 1079,
                'name' => 'BILIRAN',
                'province_id' => 48,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            79 => 
            array (
                'id' => 1080,
                'name' => 'CABUCGAYAN',
                'province_id' => 48,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            80 => 
            array (
                'id' => 1081,
                'name' => 'CAIBIRAN',
                'province_id' => 48,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            81 => 
            array (
                'id' => 1082,
                'name' => 'CULABA',
                'province_id' => 48,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            82 => 
            array (
                'id' => 1083,
                'name' => 'KAWAYAN',
                'province_id' => 48,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            83 => 
            array (
                'id' => 1084,
                'name' => 'MARIPIPI',
                'province_id' => 48,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            84 => 
            array (
                'id' => 1085,
            'name' => 'NAVAL (Capital)',
                'province_id' => 48,
                'region_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            85 => 
            array (
                'id' => 1086,
                'name' => 'DAPITAN CITY',
                'province_id' => 49,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            86 => 
            array (
                'id' => 1087,
            'name' => 'DIPOLOG CITY (Capital)',
                'province_id' => 49,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            87 => 
            array (
                'id' => 1088,
                'name' => 'KATIPUNAN',
                'province_id' => 49,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            88 => 
            array (
                'id' => 1089,
                'name' => 'LA LIBERTAD',
                'province_id' => 49,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            89 => 
            array (
                'id' => 1090,
                'name' => 'LABASON',
                'province_id' => 49,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            90 => 
            array (
                'id' => 1091,
                'name' => 'LILOY',
                'province_id' => 49,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            91 => 
            array (
                'id' => 1092,
                'name' => 'MANUKAN',
                'province_id' => 49,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            92 => 
            array (
                'id' => 1093,
                'name' => 'MUTIA',
                'province_id' => 49,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            93 => 
            array (
                'id' => 1094,
            'name' => 'PIÑAN (NEW PIÑAN)',
                'province_id' => 49,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            94 => 
            array (
                'id' => 1095,
                'name' => 'POLANCO',
                'province_id' => 49,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            95 => 
            array (
                'id' => 1096,
                'name' => 'PRES. MANUEL A. ROXAS',
                'province_id' => 49,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            96 => 
            array (
                'id' => 1097,
                'name' => 'RIZAL',
                'province_id' => 49,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            97 => 
            array (
                'id' => 1098,
                'name' => 'SALUG',
                'province_id' => 49,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            98 => 
            array (
                'id' => 1099,
                'name' => 'SERGIO OSMEÑA SR.',
                'province_id' => 49,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            99 => 
            array (
                'id' => 1100,
                'name' => 'SIAYAN',
                'province_id' => 49,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            100 => 
            array (
                'id' => 1101,
                'name' => 'SIBUCO',
                'province_id' => 49,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            101 => 
            array (
                'id' => 1102,
                'name' => 'SIBUTAD',
                'province_id' => 49,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            102 => 
            array (
                'id' => 1103,
                'name' => 'SINDANGAN',
                'province_id' => 49,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            103 => 
            array (
                'id' => 1104,
                'name' => 'SIOCON',
                'province_id' => 49,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            104 => 
            array (
                'id' => 1105,
                'name' => 'SIRAWAI',
                'province_id' => 49,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            105 => 
            array (
                'id' => 1106,
                'name' => 'TAMPILISAN',
                'province_id' => 49,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            106 => 
            array (
                'id' => 1107,
            'name' => 'JOSE DALMAN (PONOT)',
                'province_id' => 49,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            107 => 
            array (
                'id' => 1108,
                'name' => 'GUTALAC',
                'province_id' => 49,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            108 => 
            array (
                'id' => 1109,
                'name' => 'BALIGUIAN',
                'province_id' => 49,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            109 => 
            array (
                'id' => 1110,
                'name' => 'GODOD',
                'province_id' => 49,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            110 => 
            array (
                'id' => 1111,
            'name' => 'BACUNGAN (Leon T. Postigo)',
                'province_id' => 49,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            111 => 
            array (
                'id' => 1112,
                'name' => 'KALAWIT',
                'province_id' => 49,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            112 => 
            array (
                'id' => 1113,
                'name' => 'AURORA',
                'province_id' => 50,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            113 => 
            array (
                'id' => 1114,
                'name' => 'BAYOG',
                'province_id' => 50,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            114 => 
            array (
                'id' => 1115,
                'name' => 'DIMATALING',
                'province_id' => 50,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            115 => 
            array (
                'id' => 1116,
                'name' => 'DINAS',
                'province_id' => 50,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            116 => 
            array (
                'id' => 1117,
                'name' => 'DUMALINAO',
                'province_id' => 50,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            117 => 
            array (
                'id' => 1118,
                'name' => 'DUMINGAG',
                'province_id' => 50,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            118 => 
            array (
                'id' => 1119,
                'name' => 'KUMALARANG',
                'province_id' => 50,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            119 => 
            array (
                'id' => 1120,
                'name' => 'LABANGAN',
                'province_id' => 50,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            120 => 
            array (
                'id' => 1121,
                'name' => 'LAPUYAN',
                'province_id' => 50,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            121 => 
            array (
                'id' => 1122,
                'name' => 'MAHAYAG',
                'province_id' => 50,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            122 => 
            array (
                'id' => 1123,
                'name' => 'MARGOSATUBIG',
                'province_id' => 50,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            123 => 
            array (
                'id' => 1124,
                'name' => 'MIDSALIP',
                'province_id' => 50,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            124 => 
            array (
                'id' => 1125,
                'name' => 'MOLAVE',
                'province_id' => 50,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            125 => 
            array (
                'id' => 1126,
            'name' => 'PAGADIAN CITY (Capital)',
                'province_id' => 50,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            126 => 
            array (
                'id' => 1127,
            'name' => 'RAMON MAGSAYSAY (LIARGO)',
                'province_id' => 50,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            127 => 
            array (
                'id' => 1128,
                'name' => 'SAN MIGUEL',
                'province_id' => 50,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            128 => 
            array (
                'id' => 1129,
                'name' => 'SAN PABLO',
                'province_id' => 50,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            129 => 
            array (
                'id' => 1130,
                'name' => 'TABINA',
                'province_id' => 50,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            130 => 
            array (
                'id' => 1131,
                'name' => 'TAMBULIG',
                'province_id' => 50,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            131 => 
            array (
                'id' => 1132,
                'name' => 'TUKURAN',
                'province_id' => 50,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            132 => 
            array (
                'id' => 1133,
                'name' => 'ZAMBOANGA CITY',
                'province_id' => 50,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            133 => 
            array (
                'id' => 1134,
                'name' => 'LAKEWOOD',
                'province_id' => 50,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            134 => 
            array (
                'id' => 1135,
                'name' => 'JOSEFINA',
                'province_id' => 50,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            135 => 
            array (
                'id' => 1136,
                'name' => 'PITOGO',
                'province_id' => 50,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            136 => 
            array (
                'id' => 1137,
            'name' => 'SOMINOT (DON MARIANO MARCOS)',
                'province_id' => 50,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            137 => 
            array (
                'id' => 1138,
                'name' => 'VINCENZO A. SAGUN',
                'province_id' => 50,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            138 => 
            array (
                'id' => 1139,
                'name' => 'GUIPOS',
                'province_id' => 50,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            139 => 
            array (
                'id' => 1140,
                'name' => 'TIGBAO',
                'province_id' => 50,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            140 => 
            array (
                'id' => 1141,
                'name' => 'ALICIA',
                'province_id' => 51,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            141 => 
            array (
                'id' => 1142,
                'name' => 'BUUG',
                'province_id' => 51,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            142 => 
            array (
                'id' => 1143,
                'name' => 'DIPLAHAN',
                'province_id' => 51,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            143 => 
            array (
                'id' => 1144,
                'name' => 'IMELDA',
                'province_id' => 51,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            144 => 
            array (
                'id' => 1145,
            'name' => 'IPIL (Capital)',
                'province_id' => 51,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            145 => 
            array (
                'id' => 1146,
                'name' => 'KABASALAN',
                'province_id' => 51,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            146 => 
            array (
                'id' => 1147,
                'name' => 'MABUHAY',
                'province_id' => 51,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            147 => 
            array (
                'id' => 1148,
                'name' => 'MALANGAS',
                'province_id' => 51,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            148 => 
            array (
                'id' => 1149,
                'name' => 'NAGA',
                'province_id' => 51,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            149 => 
            array (
                'id' => 1150,
                'name' => 'OLUTANGA',
                'province_id' => 51,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            150 => 
            array (
                'id' => 1151,
                'name' => 'PAYAO',
                'province_id' => 51,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            151 => 
            array (
                'id' => 1152,
                'name' => 'ROSELLER LIM',
                'province_id' => 51,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            152 => 
            array (
                'id' => 1153,
                'name' => 'SIAY',
                'province_id' => 51,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            153 => 
            array (
                'id' => 1154,
                'name' => 'TALUSAN',
                'province_id' => 51,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            154 => 
            array (
                'id' => 1155,
                'name' => 'TITAY',
                'province_id' => 51,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            155 => 
            array (
                'id' => 1156,
                'name' => 'TUNGAWAN',
                'province_id' => 51,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            156 => 
            array (
                'id' => 1157,
                'name' => 'CITY OF ISABELA',
                'province_id' => 52,
                'region_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            157 => 
            array (
                'id' => 1158,
                'name' => 'BAUNGON',
                'province_id' => 53,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            158 => 
            array (
                'id' => 1159,
                'name' => 'DAMULOG',
                'province_id' => 53,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            159 => 
            array (
                'id' => 1160,
                'name' => 'DANGCAGAN',
                'province_id' => 53,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            160 => 
            array (
                'id' => 1161,
                'name' => 'DON CARLOS',
                'province_id' => 53,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            161 => 
            array (
                'id' => 1162,
                'name' => 'IMPASUG-ONG',
                'province_id' => 53,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            162 => 
            array (
                'id' => 1163,
                'name' => 'KADINGILAN',
                'province_id' => 53,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            163 => 
            array (
                'id' => 1164,
                'name' => 'KALILANGAN',
                'province_id' => 53,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            164 => 
            array (
                'id' => 1165,
                'name' => 'KIBAWE',
                'province_id' => 53,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            165 => 
            array (
                'id' => 1166,
                'name' => 'KITAOTAO',
                'province_id' => 53,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            166 => 
            array (
                'id' => 1167,
                'name' => 'LANTAPAN',
                'province_id' => 53,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            167 => 
            array (
                'id' => 1168,
                'name' => 'LIBONA',
                'province_id' => 53,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            168 => 
            array (
                'id' => 1169,
            'name' => 'CITY OF MALAYBALAY (Capital)',
                'province_id' => 53,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            169 => 
            array (
                'id' => 1170,
                'name' => 'MALITBOG',
                'province_id' => 53,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            170 => 
            array (
                'id' => 1171,
                'name' => 'MANOLO FORTICH',
                'province_id' => 53,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            171 => 
            array (
                'id' => 1172,
                'name' => 'MARAMAG',
                'province_id' => 53,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            172 => 
            array (
                'id' => 1173,
                'name' => 'PANGANTUCAN',
                'province_id' => 53,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            173 => 
            array (
                'id' => 1174,
                'name' => 'QUEZON',
                'province_id' => 53,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            174 => 
            array (
                'id' => 1175,
                'name' => 'SAN FERNANDO',
                'province_id' => 53,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            175 => 
            array (
                'id' => 1176,
                'name' => 'SUMILAO',
                'province_id' => 53,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            176 => 
            array (
                'id' => 1177,
                'name' => 'TALAKAG',
                'province_id' => 53,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            177 => 
            array (
                'id' => 1178,
                'name' => 'CITY OF VALENCIA',
                'province_id' => 53,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            178 => 
            array (
                'id' => 1179,
                'name' => 'CABANGLASAN',
                'province_id' => 53,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            179 => 
            array (
                'id' => 1180,
                'name' => 'CATARMAN',
                'province_id' => 54,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            180 => 
            array (
                'id' => 1181,
                'name' => 'GUINSILIBAN',
                'province_id' => 54,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            181 => 
            array (
                'id' => 1182,
                'name' => 'MAHINOG',
                'province_id' => 54,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            182 => 
            array (
                'id' => 1183,
            'name' => 'MAMBAJAO (Capital)',
                'province_id' => 54,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            183 => 
            array (
                'id' => 1184,
                'name' => 'SAGAY',
                'province_id' => 54,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            184 => 
            array (
                'id' => 1185,
                'name' => 'BACOLOD',
                'province_id' => 55,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            185 => 
            array (
                'id' => 1186,
                'name' => 'BALOI',
                'province_id' => 55,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            186 => 
            array (
                'id' => 1187,
                'name' => 'BAROY',
                'province_id' => 55,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            187 => 
            array (
                'id' => 1188,
                'name' => 'ILIGAN CITY',
                'province_id' => 55,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            188 => 
            array (
                'id' => 1189,
                'name' => 'KAPATAGAN',
                'province_id' => 55,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            189 => 
            array (
                'id' => 1190,
            'name' => 'SULTAN NAGA DIMAPORO (KAROMATAN)',
                'province_id' => 55,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            190 => 
            array (
                'id' => 1191,
                'name' => 'KAUSWAGAN',
                'province_id' => 55,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            191 => 
            array (
                'id' => 1192,
                'name' => 'KOLAMBUGAN',
                'province_id' => 55,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            192 => 
            array (
                'id' => 1193,
                'name' => 'LALA',
                'province_id' => 55,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            193 => 
            array (
                'id' => 1194,
                'name' => 'LINAMON',
                'province_id' => 55,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            194 => 
            array (
                'id' => 1195,
                'name' => 'MAGSAYSAY',
                'province_id' => 55,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            195 => 
            array (
                'id' => 1196,
                'name' => 'MAIGO',
                'province_id' => 55,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            196 => 
            array (
                'id' => 1197,
                'name' => 'MATUNGAO',
                'province_id' => 55,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            197 => 
            array (
                'id' => 1198,
                'name' => 'MUNAI',
                'province_id' => 55,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            198 => 
            array (
                'id' => 1199,
                'name' => 'NUNUNGAN',
                'province_id' => 55,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            199 => 
            array (
                'id' => 1200,
                'name' => 'PANTAO RAGAT',
                'province_id' => 55,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            200 => 
            array (
                'id' => 1201,
                'name' => 'POONA PIAGAPO',
                'province_id' => 55,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            201 => 
            array (
                'id' => 1202,
                'name' => 'SALVADOR',
                'province_id' => 55,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            202 => 
            array (
                'id' => 1203,
                'name' => 'SAPAD',
                'province_id' => 55,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            203 => 
            array (
                'id' => 1204,
                'name' => 'TAGOLOAN',
                'province_id' => 55,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            204 => 
            array (
                'id' => 1205,
                'name' => 'TANGCAL',
                'province_id' => 55,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            205 => 
            array (
                'id' => 1206,
            'name' => 'TUBOD (Capital)',
                'province_id' => 55,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            206 => 
            array (
                'id' => 1207,
                'name' => 'PANTAR',
                'province_id' => 55,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            207 => 
            array (
                'id' => 1208,
                'name' => 'ALORAN',
                'province_id' => 56,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            208 => 
            array (
                'id' => 1209,
                'name' => 'BALIANGAO',
                'province_id' => 56,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            209 => 
            array (
                'id' => 1210,
                'name' => 'BONIFACIO',
                'province_id' => 56,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            210 => 
            array (
                'id' => 1211,
                'name' => 'CALAMBA',
                'province_id' => 56,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            211 => 
            array (
                'id' => 1212,
                'name' => 'CLARIN',
                'province_id' => 56,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            212 => 
            array (
                'id' => 1213,
                'name' => 'CONCEPCION',
                'province_id' => 56,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            213 => 
            array (
                'id' => 1214,
                'name' => 'JIMENEZ',
                'province_id' => 56,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            214 => 
            array (
                'id' => 1215,
                'name' => 'LOPEZ JAENA',
                'province_id' => 56,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            215 => 
            array (
                'id' => 1216,
            'name' => 'OROQUIETA CITY (Capital)',
                'province_id' => 56,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            216 => 
            array (
                'id' => 1217,
                'name' => 'OZAMIS CITY',
                'province_id' => 56,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            217 => 
            array (
                'id' => 1218,
                'name' => 'PANAON',
                'province_id' => 56,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            218 => 
            array (
                'id' => 1219,
                'name' => 'PLARIDEL',
                'province_id' => 56,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            219 => 
            array (
                'id' => 1220,
                'name' => 'SAPANG DALAGA',
                'province_id' => 56,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            220 => 
            array (
                'id' => 1221,
                'name' => 'SINACABAN',
                'province_id' => 56,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            221 => 
            array (
                'id' => 1222,
                'name' => 'TANGUB CITY',
                'province_id' => 56,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            222 => 
            array (
                'id' => 1223,
                'name' => 'TUDELA',
                'province_id' => 56,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            223 => 
            array (
                'id' => 1224,
            'name' => 'DON VICTORIANO CHIONGBIAN  (DON MARIANO MARCOS)',
                'province_id' => 56,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            224 => 
            array (
                'id' => 1225,
                'name' => 'ALUBIJID',
                'province_id' => 57,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            225 => 
            array (
                'id' => 1226,
                'name' => 'BALINGASAG',
                'province_id' => 57,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            226 => 
            array (
                'id' => 1227,
                'name' => 'BALINGOAN',
                'province_id' => 57,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            227 => 
            array (
                'id' => 1228,
                'name' => 'BINUANGAN',
                'province_id' => 57,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            228 => 
            array (
                'id' => 1229,
            'name' => 'CAGAYAN DE ORO CITY (Capital)',
                'province_id' => 57,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            229 => 
            array (
                'id' => 1230,
                'name' => 'CLAVERIA',
                'province_id' => 57,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            230 => 
            array (
                'id' => 1231,
                'name' => 'CITY OF EL SALVADOR',
                'province_id' => 57,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            231 => 
            array (
                'id' => 1232,
                'name' => 'GINGOOG CITY',
                'province_id' => 57,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            232 => 
            array (
                'id' => 1233,
                'name' => 'GITAGUM',
                'province_id' => 57,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            233 => 
            array (
                'id' => 1234,
                'name' => 'INITAO',
                'province_id' => 57,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            234 => 
            array (
                'id' => 1235,
                'name' => 'JASAAN',
                'province_id' => 57,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            235 => 
            array (
                'id' => 1236,
                'name' => 'KINOGUITAN',
                'province_id' => 57,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            236 => 
            array (
                'id' => 1237,
                'name' => 'LAGONGLONG',
                'province_id' => 57,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            237 => 
            array (
                'id' => 1238,
                'name' => 'LAGUINDINGAN',
                'province_id' => 57,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            238 => 
            array (
                'id' => 1239,
                'name' => 'LIBERTAD',
                'province_id' => 57,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            239 => 
            array (
                'id' => 1240,
                'name' => 'LUGAIT',
                'province_id' => 57,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            240 => 
            array (
                'id' => 1241,
            'name' => 'MAGSAYSAY (LINUGOS)',
                'province_id' => 57,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            241 => 
            array (
                'id' => 1242,
                'name' => 'MANTICAO',
                'province_id' => 57,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            242 => 
            array (
                'id' => 1243,
                'name' => 'MEDINA',
                'province_id' => 57,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            243 => 
            array (
                'id' => 1244,
                'name' => 'NAAWAN',
                'province_id' => 57,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            244 => 
            array (
                'id' => 1245,
                'name' => 'OPOL',
                'province_id' => 57,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            245 => 
            array (
                'id' => 1246,
                'name' => 'SALAY',
                'province_id' => 57,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            246 => 
            array (
                'id' => 1247,
                'name' => 'SUGBONGCOGON',
                'province_id' => 57,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            247 => 
            array (
                'id' => 1248,
                'name' => 'TAGOLOAN',
                'province_id' => 57,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            248 => 
            array (
                'id' => 1249,
                'name' => 'TALISAYAN',
                'province_id' => 57,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            249 => 
            array (
                'id' => 1250,
                'name' => 'VILLANUEVA',
                'province_id' => 57,
                'region_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            250 => 
            array (
                'id' => 1251,
            'name' => 'ASUNCION (SAUG)',
                'province_id' => 58,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            251 => 
            array (
                'id' => 1252,
                'name' => 'CARMEN',
                'province_id' => 58,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            252 => 
            array (
                'id' => 1253,
                'name' => 'KAPALONG',
                'province_id' => 58,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            253 => 
            array (
                'id' => 1254,
                'name' => 'NEW CORELLA',
                'province_id' => 58,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            254 => 
            array (
                'id' => 1255,
                'name' => 'CITY OF PANABO',
                'province_id' => 58,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            255 => 
            array (
                'id' => 1256,
                'name' => 'ISLAND GARDEN CITY OF SAMAL',
                'province_id' => 58,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            256 => 
            array (
                'id' => 1257,
                'name' => 'SANTO TOMAS',
                'province_id' => 58,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            257 => 
            array (
                'id' => 1258,
            'name' => 'CITY OF TAGUM (Capital)',
                'province_id' => 58,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            258 => 
            array (
                'id' => 1259,
                'name' => 'TALAINGOD',
                'province_id' => 58,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            259 => 
            array (
                'id' => 1260,
                'name' => 'BRAULIO E. DUJALI',
                'province_id' => 58,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            260 => 
            array (
                'id' => 1261,
                'name' => 'SAN ISIDRO',
                'province_id' => 58,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            261 => 
            array (
                'id' => 1262,
                'name' => 'BANSALAN',
                'province_id' => 59,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            262 => 
            array (
                'id' => 1263,
                'name' => 'DAVAO CITY',
                'province_id' => 59,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            263 => 
            array (
                'id' => 1264,
            'name' => 'CITY OF DIGOS (Capital)',
                'province_id' => 59,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            264 => 
            array (
                'id' => 1265,
                'name' => 'HAGONOY',
                'province_id' => 59,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            265 => 
            array (
                'id' => 1266,
                'name' => 'KIBLAWAN',
                'province_id' => 59,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            266 => 
            array (
                'id' => 1267,
                'name' => 'MAGSAYSAY',
                'province_id' => 59,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            267 => 
            array (
                'id' => 1268,
                'name' => 'MALALAG',
                'province_id' => 59,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            268 => 
            array (
                'id' => 1269,
                'name' => 'MATANAO',
                'province_id' => 59,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            269 => 
            array (
                'id' => 1270,
                'name' => 'PADADA',
                'province_id' => 59,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            270 => 
            array (
                'id' => 1271,
                'name' => 'SANTA CRUZ',
                'province_id' => 59,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            271 => 
            array (
                'id' => 1272,
                'name' => 'SULOP',
                'province_id' => 59,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            272 => 
            array (
                'id' => 1273,
                'name' => 'BAGANGA',
                'province_id' => 60,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            273 => 
            array (
                'id' => 1274,
                'name' => 'BANAYBANAY',
                'province_id' => 60,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            274 => 
            array (
                'id' => 1275,
                'name' => 'BOSTON',
                'province_id' => 60,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            275 => 
            array (
                'id' => 1276,
                'name' => 'CARAGA',
                'province_id' => 60,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            276 => 
            array (
                'id' => 1277,
                'name' => 'CATEEL',
                'province_id' => 60,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            277 => 
            array (
                'id' => 1278,
                'name' => 'GOVERNOR GENEROSO',
                'province_id' => 60,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            278 => 
            array (
                'id' => 1279,
                'name' => 'LUPON',
                'province_id' => 60,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            279 => 
            array (
                'id' => 1280,
                'name' => 'MANAY',
                'province_id' => 60,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            280 => 
            array (
                'id' => 1281,
            'name' => 'CITY OF MATI (Capital)',
                'province_id' => 60,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            281 => 
            array (
                'id' => 1282,
                'name' => 'SAN ISIDRO',
                'province_id' => 60,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            282 => 
            array (
                'id' => 1283,
                'name' => 'TARRAGONA',
                'province_id' => 60,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            283 => 
            array (
                'id' => 1284,
                'name' => 'COMPOSTELA',
                'province_id' => 61,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            284 => 
            array (
                'id' => 1285,
            'name' => 'LAAK (SAN VICENTE)',
                'province_id' => 61,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            285 => 
            array (
                'id' => 1286,
            'name' => 'MABINI (DOÑA ALICIA)',
                'province_id' => 61,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            286 => 
            array (
                'id' => 1287,
                'name' => 'MACO',
                'province_id' => 61,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            287 => 
            array (
                'id' => 1288,
            'name' => 'MARAGUSAN (SAN MARIANO)',
                'province_id' => 61,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            288 => 
            array (
                'id' => 1289,
                'name' => 'MAWAB',
                'province_id' => 61,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            289 => 
            array (
                'id' => 1290,
                'name' => 'MONKAYO',
                'province_id' => 61,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            290 => 
            array (
                'id' => 1291,
                'name' => 'MONTEVISTA',
                'province_id' => 61,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            291 => 
            array (
                'id' => 1292,
            'name' => 'NABUNTURAN (Capital)',
                'province_id' => 61,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            292 => 
            array (
                'id' => 1293,
                'name' => 'NEW BATAAN',
                'province_id' => 61,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            293 => 
            array (
                'id' => 1294,
                'name' => 'PANTUKAN',
                'province_id' => 61,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            294 => 
            array (
                'id' => 1295,
                'name' => 'DON MARCELINO',
                'province_id' => 62,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            295 => 
            array (
                'id' => 1296,
            'name' => 'JOSE ABAD SANTOS (TRINIDAD)',
                'province_id' => 62,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            296 => 
            array (
                'id' => 1297,
                'name' => 'MALITA',
                'province_id' => 62,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            297 => 
            array (
                'id' => 1298,
                'name' => 'SANTA MARIA',
                'province_id' => 62,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            298 => 
            array (
                'id' => 1299,
                'name' => 'SARANGANI',
                'province_id' => 62,
                'region_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            299 => 
            array (
                'id' => 1300,
                'name' => 'ALAMADA',
                'province_id' => 63,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            300 => 
            array (
                'id' => 1301,
                'name' => 'CARMEN',
                'province_id' => 63,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            301 => 
            array (
                'id' => 1302,
                'name' => 'KABACAN',
                'province_id' => 63,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            302 => 
            array (
                'id' => 1303,
            'name' => 'CITY OF KIDAPAWAN (Capital)',
                'province_id' => 63,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            303 => 
            array (
                'id' => 1304,
                'name' => 'LIBUNGAN',
                'province_id' => 63,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            304 => 
            array (
                'id' => 1305,
                'name' => 'MAGPET',
                'province_id' => 63,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            305 => 
            array (
                'id' => 1306,
                'name' => 'MAKILALA',
                'province_id' => 63,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            306 => 
            array (
                'id' => 1307,
                'name' => 'MATALAM',
                'province_id' => 63,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            307 => 
            array (
                'id' => 1308,
                'name' => 'MIDSAYAP',
                'province_id' => 63,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            308 => 
            array (
                'id' => 1309,
                'name' => 'M\'LANG',
                'province_id' => 63,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            309 => 
            array (
                'id' => 1310,
                'name' => 'PIGKAWAYAN',
                'province_id' => 63,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            310 => 
            array (
                'id' => 1311,
                'name' => 'PIKIT',
                'province_id' => 63,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            311 => 
            array (
                'id' => 1312,
                'name' => 'PRESIDENT ROXAS',
                'province_id' => 63,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            312 => 
            array (
                'id' => 1313,
                'name' => 'TULUNAN',
                'province_id' => 63,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            313 => 
            array (
                'id' => 1314,
                'name' => 'ANTIPAS',
                'province_id' => 63,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            314 => 
            array (
                'id' => 1315,
                'name' => 'BANISILAN',
                'province_id' => 63,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            315 => 
            array (
                'id' => 1316,
                'name' => 'ALEOSAN',
                'province_id' => 63,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            316 => 
            array (
                'id' => 1317,
                'name' => 'ARAKAN',
                'province_id' => 63,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            317 => 
            array (
                'id' => 1318,
                'name' => 'BANGA',
                'province_id' => 64,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            318 => 
            array (
                'id' => 1319,
            'name' => 'GENERAL SANTOS CITY (DADIANGAS)',
                'province_id' => 64,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            319 => 
            array (
                'id' => 1320,
            'name' => 'CITY OF KORONADAL (Capital)',
                'province_id' => 64,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            320 => 
            array (
                'id' => 1321,
                'name' => 'NORALA',
                'province_id' => 64,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            321 => 
            array (
                'id' => 1322,
                'name' => 'POLOMOLOK',
                'province_id' => 64,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            322 => 
            array (
                'id' => 1323,
                'name' => 'SURALLAH',
                'province_id' => 64,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            323 => 
            array (
                'id' => 1324,
                'name' => 'TAMPAKAN',
                'province_id' => 64,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            324 => 
            array (
                'id' => 1325,
                'name' => 'TANTANGAN',
                'province_id' => 64,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            325 => 
            array (
                'id' => 1326,
                'name' => 'T\'BOLI',
                'province_id' => 64,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            326 => 
            array (
                'id' => 1327,
                'name' => 'TUPI',
                'province_id' => 64,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            327 => 
            array (
                'id' => 1328,
                'name' => 'SANTO NIÑO',
                'province_id' => 64,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            328 => 
            array (
                'id' => 1329,
                'name' => 'LAKE SEBU',
                'province_id' => 64,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            329 => 
            array (
                'id' => 1330,
                'name' => 'BAGUMBAYAN',
                'province_id' => 65,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            330 => 
            array (
                'id' => 1331,
                'name' => 'COLUMBIO',
                'province_id' => 65,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            331 => 
            array (
                'id' => 1332,
                'name' => 'ESPERANZA',
                'province_id' => 65,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            332 => 
            array (
                'id' => 1333,
            'name' => 'ISULAN (Capital)',
                'province_id' => 65,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            333 => 
            array (
                'id' => 1334,
                'name' => 'KALAMANSIG',
                'province_id' => 65,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            334 => 
            array (
                'id' => 1335,
                'name' => 'LEBAK',
                'province_id' => 65,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            335 => 
            array (
                'id' => 1336,
                'name' => 'LUTAYAN',
                'province_id' => 65,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            336 => 
            array (
                'id' => 1337,
            'name' => 'LAMBAYONG (MARIANO MARCOS)',
                'province_id' => 65,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            337 => 
            array (
                'id' => 1338,
                'name' => 'PALIMBANG',
                'province_id' => 65,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            338 => 
            array (
                'id' => 1339,
                'name' => 'PRESIDENT QUIRINO',
                'province_id' => 65,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            339 => 
            array (
                'id' => 1340,
                'name' => 'CITY OF TACURONG',
                'province_id' => 65,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            340 => 
            array (
                'id' => 1341,
                'name' => 'SEN. NINOY AQUINO',
                'province_id' => 65,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            341 => 
            array (
                'id' => 1342,
            'name' => 'ALABEL (Capital)',
                'province_id' => 66,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            342 => 
            array (
                'id' => 1343,
                'name' => 'GLAN',
                'province_id' => 66,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            343 => 
            array (
                'id' => 1344,
                'name' => 'KIAMBA',
                'province_id' => 66,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            344 => 
            array (
                'id' => 1345,
                'name' => 'MAASIM',
                'province_id' => 66,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            345 => 
            array (
                'id' => 1346,
                'name' => 'MAITUM',
                'province_id' => 66,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            346 => 
            array (
                'id' => 1347,
                'name' => 'MALAPATAN',
                'province_id' => 66,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            347 => 
            array (
                'id' => 1348,
                'name' => 'MALUNGON',
                'province_id' => 66,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            348 => 
            array (
                'id' => 1349,
                'name' => 'COTABATO CITY',
                'province_id' => 67,
                'region_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            349 => 
            array (
                'id' => 1350,
                'name' => 'TONDO I / II',
                'province_id' => 68,
                'region_id' => 14,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            350 => 
            array (
                'id' => 1351,
                'name' => 'BINONDO',
                'province_id' => 68,
                'region_id' => 14,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            351 => 
            array (
                'id' => 1352,
                'name' => 'QUIAPO',
                'province_id' => 68,
                'region_id' => 14,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            352 => 
            array (
                'id' => 1353,
                'name' => 'SAN NICOLAS',
                'province_id' => 68,
                'region_id' => 14,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            353 => 
            array (
                'id' => 1354,
                'name' => 'SANTA CRUZ',
                'province_id' => 68,
                'region_id' => 14,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            354 => 
            array (
                'id' => 1355,
                'name' => 'SAMPALOC',
                'province_id' => 68,
                'region_id' => 14,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            355 => 
            array (
                'id' => 1356,
                'name' => 'SAN MIGUEL',
                'province_id' => 68,
                'region_id' => 14,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            356 => 
            array (
                'id' => 1357,
                'name' => 'ERMITA',
                'province_id' => 68,
                'region_id' => 14,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            357 => 
            array (
                'id' => 1358,
                'name' => 'INTRAMUROS',
                'province_id' => 68,
                'region_id' => 14,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            358 => 
            array (
                'id' => 1359,
                'name' => 'MALATE',
                'province_id' => 68,
                'region_id' => 14,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            359 => 
            array (
                'id' => 1360,
                'name' => 'PACO',
                'province_id' => 68,
                'region_id' => 14,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            360 => 
            array (
                'id' => 1361,
                'name' => 'PANDACAN',
                'province_id' => 68,
                'region_id' => 14,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            361 => 
            array (
                'id' => 1362,
                'name' => 'PORT AREA',
                'province_id' => 68,
                'region_id' => 14,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            362 => 
            array (
                'id' => 1363,
                'name' => 'SANTA ANA',
                'province_id' => 68,
                'region_id' => 14,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            363 => 
            array (
                'id' => 1364,
                'name' => 'CITY OF MANDALUYONG',
                'province_id' => 70,
                'region_id' => 14,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            364 => 
            array (
                'id' => 1365,
                'name' => 'CITY OF MARIKINA',
                'province_id' => 70,
                'region_id' => 14,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            365 => 
            array (
                'id' => 1366,
                'name' => 'CITY OF PASIG',
                'province_id' => 70,
                'region_id' => 14,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            366 => 
            array (
                'id' => 1367,
                'name' => 'QUEZON CITY',
                'province_id' => 70,
                'region_id' => 14,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            367 => 
            array (
                'id' => 1368,
                'name' => 'CITY OF SAN JUAN',
                'province_id' => 70,
                'region_id' => 14,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            368 => 
            array (
                'id' => 1369,
                'name' => 'CALOOCAN CITY',
                'province_id' => 71,
                'region_id' => 14,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            369 => 
            array (
                'id' => 1370,
                'name' => 'CITY OF MALABON',
                'province_id' => 71,
                'region_id' => 14,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            370 => 
            array (
                'id' => 1371,
                'name' => 'CITY OF NAVOTAS',
                'province_id' => 71,
                'region_id' => 14,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            371 => 
            array (
                'id' => 1372,
                'name' => 'CITY OF VALENZUELA',
                'province_id' => 71,
                'region_id' => 14,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            372 => 
            array (
                'id' => 1373,
                'name' => 'CITY OF LAS PIÑAS',
                'province_id' => 72,
                'region_id' => 14,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            373 => 
            array (
                'id' => 1374,
                'name' => 'CITY OF MAKATI',
                'province_id' => 72,
                'region_id' => 14,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            374 => 
            array (
                'id' => 1375,
                'name' => 'CITY OF MUNTINLUPA',
                'province_id' => 72,
                'region_id' => 14,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            375 => 
            array (
                'id' => 1376,
                'name' => 'CITY OF PARAÑAQUE',
                'province_id' => 72,
                'region_id' => 14,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            376 => 
            array (
                'id' => 1377,
                'name' => 'PASAY CITY',
                'province_id' => 72,
                'region_id' => 14,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            377 => 
            array (
                'id' => 1378,
                'name' => 'PATEROS',
                'province_id' => 72,
                'region_id' => 14,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            378 => 
            array (
                'id' => 1379,
                'name' => 'TAGUIG CITY',
                'province_id' => 72,
                'region_id' => 14,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            379 => 
            array (
                'id' => 1380,
            'name' => 'BANGUED (Capital)',
                'province_id' => 73,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            380 => 
            array (
                'id' => 1381,
                'name' => 'BOLINEY',
                'province_id' => 73,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            381 => 
            array (
                'id' => 1382,
                'name' => 'BUCAY',
                'province_id' => 73,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            382 => 
            array (
                'id' => 1383,
                'name' => 'BUCLOC',
                'province_id' => 73,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            383 => 
            array (
                'id' => 1384,
                'name' => 'DAGUIOMAN',
                'province_id' => 73,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            384 => 
            array (
                'id' => 1385,
                'name' => 'DANGLAS',
                'province_id' => 73,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            385 => 
            array (
                'id' => 1386,
                'name' => 'DOLORES',
                'province_id' => 73,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            386 => 
            array (
                'id' => 1387,
                'name' => 'LA PAZ',
                'province_id' => 73,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            387 => 
            array (
                'id' => 1388,
                'name' => 'LACUB',
                'province_id' => 73,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            388 => 
            array (
                'id' => 1389,
                'name' => 'LAGANGILANG',
                'province_id' => 73,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            389 => 
            array (
                'id' => 1390,
                'name' => 'LAGAYAN',
                'province_id' => 73,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            390 => 
            array (
                'id' => 1391,
                'name' => 'LANGIDEN',
                'province_id' => 73,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            391 => 
            array (
                'id' => 1392,
            'name' => 'LICUAN-BAAY (LICUAN)',
                'province_id' => 73,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            392 => 
            array (
                'id' => 1393,
                'name' => 'LUBA',
                'province_id' => 73,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            393 => 
            array (
                'id' => 1394,
                'name' => 'MALIBCONG',
                'province_id' => 73,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            394 => 
            array (
                'id' => 1395,
                'name' => 'MANABO',
                'province_id' => 73,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            395 => 
            array (
                'id' => 1396,
                'name' => 'PEÑARRUBIA',
                'province_id' => 73,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            396 => 
            array (
                'id' => 1397,
                'name' => 'PIDIGAN',
                'province_id' => 73,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            397 => 
            array (
                'id' => 1398,
                'name' => 'PILAR',
                'province_id' => 73,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            398 => 
            array (
                'id' => 1399,
                'name' => 'SALLAPADAN',
                'province_id' => 73,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            399 => 
            array (
                'id' => 1400,
                'name' => 'SAN ISIDRO',
                'province_id' => 73,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            400 => 
            array (
                'id' => 1401,
                'name' => 'SAN JUAN',
                'province_id' => 73,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            401 => 
            array (
                'id' => 1402,
                'name' => 'SAN QUINTIN',
                'province_id' => 73,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            402 => 
            array (
                'id' => 1403,
                'name' => 'TAYUM',
                'province_id' => 73,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            403 => 
            array (
                'id' => 1404,
                'name' => 'TINEG',
                'province_id' => 73,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            404 => 
            array (
                'id' => 1405,
                'name' => 'TUBO',
                'province_id' => 73,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            405 => 
            array (
                'id' => 1406,
                'name' => 'VILLAVICIOSA',
                'province_id' => 73,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            406 => 
            array (
                'id' => 1407,
                'name' => 'ATOK',
                'province_id' => 74,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            407 => 
            array (
                'id' => 1408,
                'name' => 'BAGUIO CITY',
                'province_id' => 74,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            408 => 
            array (
                'id' => 1409,
                'name' => 'BAKUN',
                'province_id' => 74,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            409 => 
            array (
                'id' => 1410,
                'name' => 'BOKOD',
                'province_id' => 74,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            410 => 
            array (
                'id' => 1411,
                'name' => 'BUGUIAS',
                'province_id' => 74,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            411 => 
            array (
                'id' => 1412,
                'name' => 'ITOGON',
                'province_id' => 74,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            412 => 
            array (
                'id' => 1413,
                'name' => 'KABAYAN',
                'province_id' => 74,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            413 => 
            array (
                'id' => 1414,
                'name' => 'KAPANGAN',
                'province_id' => 74,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            414 => 
            array (
                'id' => 1415,
                'name' => 'KIBUNGAN',
                'province_id' => 74,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            415 => 
            array (
                'id' => 1416,
            'name' => 'LA TRINIDAD (Capital)',
                'province_id' => 74,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            416 => 
            array (
                'id' => 1417,
                'name' => 'MANKAYAN',
                'province_id' => 74,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            417 => 
            array (
                'id' => 1418,
                'name' => 'SABLAN',
                'province_id' => 74,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            418 => 
            array (
                'id' => 1419,
                'name' => 'TUBA',
                'province_id' => 74,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            419 => 
            array (
                'id' => 1420,
                'name' => 'TUBLAY',
                'province_id' => 74,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            420 => 
            array (
                'id' => 1421,
                'name' => 'BANAUE',
                'province_id' => 75,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            421 => 
            array (
                'id' => 1422,
                'name' => 'HUNGDUAN',
                'province_id' => 75,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            422 => 
            array (
                'id' => 1423,
                'name' => 'KIANGAN',
                'province_id' => 75,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            423 => 
            array (
                'id' => 1424,
            'name' => 'LAGAWE (Capital)',
                'province_id' => 75,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            424 => 
            array (
                'id' => 1425,
                'name' => 'LAMUT',
                'province_id' => 75,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            425 => 
            array (
                'id' => 1426,
                'name' => 'MAYOYAO',
                'province_id' => 75,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            426 => 
            array (
                'id' => 1427,
            'name' => 'ALFONSO LISTA (POTIA)',
                'province_id' => 75,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            427 => 
            array (
                'id' => 1428,
                'name' => 'AGUINALDO',
                'province_id' => 75,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            428 => 
            array (
                'id' => 1429,
                'name' => 'HINGYON',
                'province_id' => 75,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            429 => 
            array (
                'id' => 1430,
                'name' => 'TINOC',
                'province_id' => 75,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            430 => 
            array (
                'id' => 1431,
                'name' => 'ASIPULO',
                'province_id' => 75,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            431 => 
            array (
                'id' => 1432,
                'name' => 'BALBALAN',
                'province_id' => 76,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            432 => 
            array (
                'id' => 1433,
                'name' => 'LUBUAGAN',
                'province_id' => 76,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            433 => 
            array (
                'id' => 1434,
                'name' => 'PASIL',
                'province_id' => 76,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            434 => 
            array (
                'id' => 1435,
                'name' => 'PINUKPUK',
                'province_id' => 76,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            435 => 
            array (
                'id' => 1436,
            'name' => 'RIZAL (LIWAN)',
                'province_id' => 76,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            436 => 
            array (
                'id' => 1437,
            'name' => 'CITY OF TABUK (Capital)',
                'province_id' => 76,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            437 => 
            array (
                'id' => 1438,
                'name' => 'TANUDAN',
                'province_id' => 76,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            438 => 
            array (
                'id' => 1439,
                'name' => 'TINGLAYAN',
                'province_id' => 76,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            439 => 
            array (
                'id' => 1440,
                'name' => 'BARLIG',
                'province_id' => 77,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            440 => 
            array (
                'id' => 1441,
                'name' => 'BAUKO',
                'province_id' => 77,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            441 => 
            array (
                'id' => 1442,
                'name' => 'BESAO',
                'province_id' => 77,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            442 => 
            array (
                'id' => 1443,
            'name' => 'BONTOC (Capital)',
                'province_id' => 77,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            443 => 
            array (
                'id' => 1444,
                'name' => 'NATONIN',
                'province_id' => 77,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            444 => 
            array (
                'id' => 1445,
                'name' => 'PARACELIS',
                'province_id' => 77,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            445 => 
            array (
                'id' => 1446,
                'name' => 'SABANGAN',
                'province_id' => 77,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            446 => 
            array (
                'id' => 1447,
                'name' => 'SADANGA',
                'province_id' => 77,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            447 => 
            array (
                'id' => 1448,
                'name' => 'SAGADA',
                'province_id' => 77,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            448 => 
            array (
                'id' => 1449,
                'name' => 'TADIAN',
                'province_id' => 77,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            449 => 
            array (
                'id' => 1450,
            'name' => 'CALANASAN (BAYAG)',
                'province_id' => 78,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            450 => 
            array (
                'id' => 1451,
                'name' => 'CONNER',
                'province_id' => 78,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            451 => 
            array (
                'id' => 1452,
                'name' => 'FLORA',
                'province_id' => 78,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            452 => 
            array (
                'id' => 1453,
            'name' => 'KABUGAO (Capital)',
                'province_id' => 78,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            453 => 
            array (
                'id' => 1454,
                'name' => 'LUNA',
                'province_id' => 78,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            454 => 
            array (
                'id' => 1455,
                'name' => 'PUDTOL',
                'province_id' => 78,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            455 => 
            array (
                'id' => 1456,
                'name' => 'SANTA MARCELA',
                'province_id' => 78,
                'region_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            456 => 
            array (
                'id' => 1457,
                'name' => 'CITY OF LAMITAN',
                'province_id' => 79,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            457 => 
            array (
                'id' => 1458,
                'name' => 'LANTAWAN',
                'province_id' => 79,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            458 => 
            array (
                'id' => 1459,
                'name' => 'MALUSO',
                'province_id' => 79,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            459 => 
            array (
                'id' => 1460,
                'name' => 'SUMISIP',
                'province_id' => 79,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            460 => 
            array (
                'id' => 1461,
                'name' => 'TIPO-TIPO',
                'province_id' => 79,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            461 => 
            array (
                'id' => 1462,
                'name' => 'TUBURAN',
                'province_id' => 79,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            462 => 
            array (
                'id' => 1463,
                'name' => 'AKBAR',
                'province_id' => 79,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            463 => 
            array (
                'id' => 1464,
                'name' => 'AL-BARKA',
                'province_id' => 79,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            464 => 
            array (
                'id' => 1465,
                'name' => 'HADJI MOHAMMAD AJUL',
                'province_id' => 79,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            465 => 
            array (
                'id' => 1466,
                'name' => 'UNGKAYA PUKAN',
                'province_id' => 79,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            466 => 
            array (
                'id' => 1467,
                'name' => 'HADJI MUHTAMAD',
                'province_id' => 79,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            467 => 
            array (
                'id' => 1468,
                'name' => 'TABUAN-LASA',
                'province_id' => 79,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            468 => 
            array (
                'id' => 1469,
            'name' => 'BACOLOD-KALAWI (BACOLOD GRANDE)',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            469 => 
            array (
                'id' => 1470,
                'name' => 'BALABAGAN',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            470 => 
            array (
                'id' => 1471,
            'name' => 'BALINDONG (WATU)',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            471 => 
            array (
                'id' => 1472,
                'name' => 'BAYANG',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            472 => 
            array (
                'id' => 1473,
                'name' => 'BINIDAYAN',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            473 => 
            array (
                'id' => 1474,
                'name' => 'BUBONG',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            474 => 
            array (
                'id' => 1475,
                'name' => 'BUTIG',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            475 => 
            array (
                'id' => 1476,
                'name' => 'GANASSI',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            476 => 
            array (
                'id' => 1477,
                'name' => 'KAPAI',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            477 => 
            array (
                'id' => 1478,
            'name' => 'LUMBA-BAYABAO (MAGUING)',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            478 => 
            array (
                'id' => 1479,
                'name' => 'LUMBATAN',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            479 => 
            array (
                'id' => 1480,
                'name' => 'MADALUM',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            480 => 
            array (
                'id' => 1481,
                'name' => 'MADAMBA',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            481 => 
            array (
                'id' => 1482,
                'name' => 'MALABANG',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            482 => 
            array (
                'id' => 1483,
                'name' => 'MARANTAO',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            483 => 
            array (
                'id' => 1484,
            'name' => 'MARAWI CITY (Capital)',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            484 => 
            array (
                'id' => 1485,
                'name' => 'MASIU',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            485 => 
            array (
                'id' => 1486,
                'name' => 'MULONDO',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            486 => 
            array (
                'id' => 1487,
            'name' => 'PAGAYAWAN (TATARIKAN)',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            487 => 
            array (
                'id' => 1488,
                'name' => 'PIAGAPO',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            488 => 
            array (
                'id' => 1489,
            'name' => 'POONA BAYABAO (GATA)',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            489 => 
            array (
                'id' => 1490,
                'name' => 'PUALAS',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            490 => 
            array (
                'id' => 1491,
                'name' => 'DITSAAN-RAMAIN',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            491 => 
            array (
                'id' => 1492,
                'name' => 'SAGUIARAN',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            492 => 
            array (
                'id' => 1493,
                'name' => 'TAMPARAN',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            493 => 
            array (
                'id' => 1494,
                'name' => 'TARAKA',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            494 => 
            array (
                'id' => 1495,
                'name' => 'TUBARAN',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            495 => 
            array (
                'id' => 1496,
                'name' => 'TUGAYA',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            496 => 
            array (
                'id' => 1497,
                'name' => 'WAO',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            497 => 
            array (
                'id' => 1498,
                'name' => 'MAROGONG',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            498 => 
            array (
                'id' => 1499,
                'name' => 'CALANOGAS',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            499 => 
            array (
                'id' => 1500,
                'name' => 'BUADIPOSO-BUNTONG',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        \DB::table('cities')->insert(array (
            0 => 
            array (
                'id' => 1501,
                'name' => 'MAGUING',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 1502,
            'name' => 'PICONG (SULTAN GUMANDER)',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 1503,
                'name' => 'LUMBAYANAGUE',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 1504,
                'name' => 'BUMBARAN',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 1505,
                'name' => 'TAGOLOAN II',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 1506,
                'name' => 'KAPATAGAN',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 1507,
                'name' => 'SULTAN DUMALONDONG',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 1508,
                'name' => 'LUMBACA-UNAYAN',
                'province_id' => 80,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 1509,
                'name' => 'AMPATUAN',
                'province_id' => 81,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 1510,
                'name' => 'BULDON',
                'province_id' => 81,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 1511,
                'name' => 'BULUAN',
                'province_id' => 81,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 1512,
                'name' => 'DATU PAGLAS',
                'province_id' => 81,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 1513,
                'name' => 'DATU PIANG',
                'province_id' => 81,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => 1514,
            'name' => 'DATU ODIN SINSUAT (DINAIG)',
                'province_id' => 81,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => 1515,
            'name' => 'SHARIFF AGUAK (MAGANOY) (Capital)',
                'province_id' => 81,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => 1516,
                'name' => 'MATANOG',
                'province_id' => 81,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'id' => 1517,
                'name' => 'PAGALUNGAN',
                'province_id' => 81,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'id' => 1518,
                'name' => 'PARANG',
                'province_id' => 81,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'id' => 1519,
            'name' => 'SULTAN KUDARAT (NULING)',
                'province_id' => 81,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'id' => 1520,
            'name' => 'SULTAN SA BARONGIS (LAMBAYONG)',
                'province_id' => 81,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'id' => 1521,
            'name' => 'KABUNTALAN (TUMBAO)',
                'province_id' => 81,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'id' => 1522,
                'name' => 'UPI',
                'province_id' => 81,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'id' => 1523,
                'name' => 'TALAYAN',
                'province_id' => 81,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'id' => 1524,
                'name' => 'SOUTH UPI',
                'province_id' => 81,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'id' => 1525,
                'name' => 'BARIRA',
                'province_id' => 81,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            25 => 
            array (
                'id' => 1526,
                'name' => 'GEN. S. K. PENDATUN',
                'province_id' => 81,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'id' => 1527,
                'name' => 'MAMASAPANO',
                'province_id' => 81,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            27 => 
            array (
                'id' => 1528,
                'name' => 'TALITAY',
                'province_id' => 81,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            28 => 
            array (
                'id' => 1529,
                'name' => 'PAGAGAWAN',
                'province_id' => 81,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            29 => 
            array (
                'id' => 1530,
                'name' => 'PAGLAT',
                'province_id' => 81,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            30 => 
            array (
                'id' => 1531,
                'name' => 'SULTAN MASTURA',
                'province_id' => 81,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            31 => 
            array (
                'id' => 1532,
                'name' => 'GUINDULUNGAN',
                'province_id' => 81,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            32 => 
            array (
                'id' => 1533,
                'name' => 'DATU SAUDI-AMPATUAN',
                'province_id' => 81,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            33 => 
            array (
                'id' => 1534,
                'name' => 'DATU UNSAY',
                'province_id' => 81,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            34 => 
            array (
                'id' => 1535,
                'name' => 'DATU ABDULLAH SANGKI',
                'province_id' => 81,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            35 => 
            array (
                'id' => 1536,
                'name' => 'RAJAH BUAYAN',
                'province_id' => 81,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            36 => 
            array (
                'id' => 1537,
                'name' => 'DATU BLAH T. SINSUAT',
                'province_id' => 81,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            37 => 
            array (
                'id' => 1538,
                'name' => 'DATU ANGGAL MIDTIMBANG',
                'province_id' => 81,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            38 => 
            array (
                'id' => 1539,
                'name' => 'MANGUDADATU',
                'province_id' => 81,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            39 => 
            array (
                'id' => 1540,
                'name' => 'PANDAG',
                'province_id' => 81,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            40 => 
            array (
                'id' => 1541,
                'name' => 'NORTHERN KABUNTALAN',
                'province_id' => 81,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            41 => 
            array (
                'id' => 1542,
                'name' => 'DATU HOFFER AMPATUAN',
                'province_id' => 81,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            42 => 
            array (
                'id' => 1543,
                'name' => 'DATU SALIBO',
                'province_id' => 81,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            43 => 
            array (
                'id' => 1544,
                'name' => 'SHARIFF SAYDONA MUSTAPHA',
                'province_id' => 81,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            44 => 
            array (
                'id' => 1545,
                'name' => 'INDANAN',
                'province_id' => 82,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            45 => 
            array (
                'id' => 1546,
            'name' => 'JOLO (Capital)',
                'province_id' => 82,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            46 => 
            array (
                'id' => 1547,
                'name' => 'KALINGALAN CALUANG',
                'province_id' => 82,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            47 => 
            array (
                'id' => 1548,
                'name' => 'LUUK',
                'province_id' => 82,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            48 => 
            array (
                'id' => 1549,
                'name' => 'MAIMBUNG',
                'province_id' => 82,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            49 => 
            array (
                'id' => 1550,
            'name' => 'HADJI PANGLIMA TAHIL (MARUNGGAS)',
                'province_id' => 82,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            50 => 
            array (
                'id' => 1551,
                'name' => 'OLD PANAMAO',
                'province_id' => 82,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            51 => 
            array (
                'id' => 1552,
                'name' => 'PANGUTARAN',
                'province_id' => 82,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            52 => 
            array (
                'id' => 1553,
                'name' => 'PARANG',
                'province_id' => 82,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            53 => 
            array (
                'id' => 1554,
                'name' => 'PATA',
                'province_id' => 82,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            54 => 
            array (
                'id' => 1555,
                'name' => 'PATIKUL',
                'province_id' => 82,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            55 => 
            array (
                'id' => 1556,
                'name' => 'SIASI',
                'province_id' => 82,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            56 => 
            array (
                'id' => 1557,
                'name' => 'TALIPAO',
                'province_id' => 82,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            57 => 
            array (
                'id' => 1558,
                'name' => 'TAPUL',
                'province_id' => 82,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            58 => 
            array (
                'id' => 1559,
                'name' => 'TONGKIL',
                'province_id' => 82,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            59 => 
            array (
                'id' => 1560,
            'name' => 'PANGLIMA ESTINO (NEW PANAMAO)',
                'province_id' => 82,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            60 => 
            array (
                'id' => 1561,
                'name' => 'LUGUS',
                'province_id' => 82,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            61 => 
            array (
                'id' => 1562,
                'name' => 'PANDAMI',
                'province_id' => 82,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            62 => 
            array (
                'id' => 1563,
                'name' => 'OMAR',
                'province_id' => 82,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            63 => 
            array (
                'id' => 1564,
            'name' => 'PANGLIMA SUGALA (BALIMBING)',
                'province_id' => 83,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            64 => 
            array (
                'id' => 1565,
            'name' => 'BONGAO (Capital)',
                'province_id' => 83,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            65 => 
            array (
                'id' => 1566,
            'name' => 'MAPUN (CAGAYAN DE TAWI-TAWI)',
                'province_id' => 83,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            66 => 
            array (
                'id' => 1567,
                'name' => 'SIMUNUL',
                'province_id' => 83,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            67 => 
            array (
                'id' => 1568,
                'name' => 'SITANGKAI',
                'province_id' => 83,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            68 => 
            array (
                'id' => 1569,
                'name' => 'SOUTH UBIAN',
                'province_id' => 83,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            69 => 
            array (
                'id' => 1570,
                'name' => 'TANDUBAS',
                'province_id' => 83,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            70 => 
            array (
                'id' => 1571,
                'name' => 'TURTLE ISLANDS',
                'province_id' => 83,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            71 => 
            array (
                'id' => 1572,
                'name' => 'LANGUYAN',
                'province_id' => 83,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            72 => 
            array (
                'id' => 1573,
                'name' => 'SAPA-SAPA',
                'province_id' => 83,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            73 => 
            array (
                'id' => 1574,
                'name' => 'SIBUTU',
                'province_id' => 83,
                'region_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            74 => 
            array (
                'id' => 1575,
                'name' => 'BUENAVISTA',
                'province_id' => 84,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            75 => 
            array (
                'id' => 1576,
            'name' => 'BUTUAN CITY (Capital)',
                'province_id' => 84,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            76 => 
            array (
                'id' => 1577,
                'name' => 'CITY OF CABADBARAN',
                'province_id' => 84,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            77 => 
            array (
                'id' => 1578,
                'name' => 'CARMEN',
                'province_id' => 84,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            78 => 
            array (
                'id' => 1579,
                'name' => 'JABONGA',
                'province_id' => 84,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            79 => 
            array (
                'id' => 1580,
                'name' => 'KITCHARAO',
                'province_id' => 84,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            80 => 
            array (
                'id' => 1581,
                'name' => 'LAS NIEVES',
                'province_id' => 84,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            81 => 
            array (
                'id' => 1582,
                'name' => 'MAGALLANES',
                'province_id' => 84,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            82 => 
            array (
                'id' => 1583,
                'name' => 'NASIPIT',
                'province_id' => 84,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            83 => 
            array (
                'id' => 1584,
                'name' => 'SANTIAGO',
                'province_id' => 84,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            84 => 
            array (
                'id' => 1585,
                'name' => 'TUBAY',
                'province_id' => 84,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            85 => 
            array (
                'id' => 1586,
                'name' => 'REMEDIOS T. ROMUALDEZ',
                'province_id' => 84,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            86 => 
            array (
                'id' => 1587,
                'name' => 'CITY OF BAYUGAN',
                'province_id' => 85,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            87 => 
            array (
                'id' => 1588,
                'name' => 'BUNAWAN',
                'province_id' => 85,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            88 => 
            array (
                'id' => 1589,
                'name' => 'ESPERANZA',
                'province_id' => 85,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            89 => 
            array (
                'id' => 1590,
                'name' => 'LA PAZ',
                'province_id' => 85,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            90 => 
            array (
                'id' => 1591,
                'name' => 'LORETO',
                'province_id' => 85,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            91 => 
            array (
                'id' => 1592,
            'name' => 'PROSPERIDAD (Capital)',
                'province_id' => 85,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            92 => 
            array (
                'id' => 1593,
                'name' => 'ROSARIO',
                'province_id' => 85,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            93 => 
            array (
                'id' => 1594,
                'name' => 'SAN FRANCISCO',
                'province_id' => 85,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            94 => 
            array (
                'id' => 1595,
                'name' => 'SAN LUIS',
                'province_id' => 85,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            95 => 
            array (
                'id' => 1596,
                'name' => 'SANTA JOSEFA',
                'province_id' => 85,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            96 => 
            array (
                'id' => 1597,
                'name' => 'TALACOGON',
                'province_id' => 85,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            97 => 
            array (
                'id' => 1598,
                'name' => 'TRENTO',
                'province_id' => 85,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            98 => 
            array (
                'id' => 1599,
                'name' => 'VERUELA',
                'province_id' => 85,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            99 => 
            array (
                'id' => 1600,
                'name' => 'SIBAGAT',
                'province_id' => 85,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            100 => 
            array (
                'id' => 1601,
                'name' => 'ALEGRIA',
                'province_id' => 86,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            101 => 
            array (
                'id' => 1602,
                'name' => 'BACUAG',
                'province_id' => 86,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            102 => 
            array (
                'id' => 1603,
                'name' => 'BURGOS',
                'province_id' => 86,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            103 => 
            array (
                'id' => 1604,
                'name' => 'CLAVER',
                'province_id' => 86,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            104 => 
            array (
                'id' => 1605,
                'name' => 'DAPA',
                'province_id' => 86,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            105 => 
            array (
                'id' => 1606,
                'name' => 'DEL CARMEN',
                'province_id' => 86,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            106 => 
            array (
                'id' => 1607,
                'name' => 'GENERAL LUNA',
                'province_id' => 86,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            107 => 
            array (
                'id' => 1608,
                'name' => 'GIGAQUIT',
                'province_id' => 86,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            108 => 
            array (
                'id' => 1609,
                'name' => 'MAINIT',
                'province_id' => 86,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            109 => 
            array (
                'id' => 1610,
                'name' => 'MALIMONO',
                'province_id' => 86,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            110 => 
            array (
                'id' => 1611,
                'name' => 'PILAR',
                'province_id' => 86,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            111 => 
            array (
                'id' => 1612,
                'name' => 'PLACER',
                'province_id' => 86,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            112 => 
            array (
                'id' => 1613,
                'name' => 'SAN BENITO',
                'province_id' => 86,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            113 => 
            array (
                'id' => 1614,
            'name' => 'SAN FRANCISCO (ANAO-AON)',
                'province_id' => 86,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            114 => 
            array (
                'id' => 1615,
                'name' => 'SAN ISIDRO',
                'province_id' => 86,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            115 => 
            array (
                'id' => 1616,
            'name' => 'SANTA MONICA (SAPAO)',
                'province_id' => 86,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            116 => 
            array (
                'id' => 1617,
                'name' => 'SISON',
                'province_id' => 86,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            117 => 
            array (
                'id' => 1618,
                'name' => 'SOCORRO',
                'province_id' => 86,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            118 => 
            array (
                'id' => 1619,
            'name' => 'SURIGAO CITY (Capital)',
                'province_id' => 86,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            119 => 
            array (
                'id' => 1620,
                'name' => 'TAGANA-AN',
                'province_id' => 86,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            120 => 
            array (
                'id' => 1621,
                'name' => 'TUBOD',
                'province_id' => 86,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            121 => 
            array (
                'id' => 1622,
                'name' => 'BAROBO',
                'province_id' => 87,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            122 => 
            array (
                'id' => 1623,
                'name' => 'BAYABAS',
                'province_id' => 87,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            123 => 
            array (
                'id' => 1624,
                'name' => 'CITY OF BISLIG',
                'province_id' => 87,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            124 => 
            array (
                'id' => 1625,
                'name' => 'CAGWAIT',
                'province_id' => 87,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            125 => 
            array (
                'id' => 1626,
                'name' => 'CANTILAN',
                'province_id' => 87,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            126 => 
            array (
                'id' => 1627,
                'name' => 'CARMEN',
                'province_id' => 87,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            127 => 
            array (
                'id' => 1628,
                'name' => 'CARRASCAL',
                'province_id' => 87,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            128 => 
            array (
                'id' => 1629,
                'name' => 'CORTES',
                'province_id' => 87,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            129 => 
            array (
                'id' => 1630,
                'name' => 'HINATUAN',
                'province_id' => 87,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            130 => 
            array (
                'id' => 1631,
                'name' => 'LANUZA',
                'province_id' => 87,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            131 => 
            array (
                'id' => 1632,
                'name' => 'LIANGA',
                'province_id' => 87,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            132 => 
            array (
                'id' => 1633,
                'name' => 'LINGIG',
                'province_id' => 87,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            133 => 
            array (
                'id' => 1634,
                'name' => 'MADRID',
                'province_id' => 87,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            134 => 
            array (
                'id' => 1635,
                'name' => 'MARIHATAG',
                'province_id' => 87,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            135 => 
            array (
                'id' => 1636,
                'name' => 'SAN AGUSTIN',
                'province_id' => 87,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            136 => 
            array (
                'id' => 1637,
                'name' => 'SAN MIGUEL',
                'province_id' => 87,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            137 => 
            array (
                'id' => 1638,
                'name' => 'TAGBINA',
                'province_id' => 87,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            138 => 
            array (
                'id' => 1639,
                'name' => 'TAGO',
                'province_id' => 87,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            139 => 
            array (
                'id' => 1640,
            'name' => 'CITY OF TANDAG (Capital)',
                'province_id' => 87,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            140 => 
            array (
                'id' => 1641,
            'name' => 'BASILISA (RIZAL)',
                'province_id' => 88,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            141 => 
            array (
                'id' => 1642,
                'name' => 'CAGDIANAO',
                'province_id' => 88,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            142 => 
            array (
                'id' => 1643,
                'name' => 'DINAGAT',
                'province_id' => 88,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            143 => 
            array (
                'id' => 1644,
            'name' => 'LIBJO (ALBOR)',
                'province_id' => 88,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            144 => 
            array (
                'id' => 1645,
                'name' => 'LORETO',
                'province_id' => 88,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            145 => 
            array (
                'id' => 1646,
            'name' => 'SAN JOSE (Capital)',
                'province_id' => 88,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            146 => 
            array (
                'id' => 1647,
                'name' => 'TUBAJON',
                'province_id' => 88,
                'region_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}