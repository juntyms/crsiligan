<?php

namespace Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        //$this->call(CityTableSeeder::class);
        //$this->call(BarangayTableSeeder::class);
        $this->call(ReligionTableSeeder::class);
        $this->call(AffiliationTableSeeder::class);
        $this->call(CivilstatusTableSeeder::class);
        $this->call(DesignationTableSeeder::class);
        $this->call(NationalityTableSeeder::class);
        $this->call(SacramentTableSeeder::class);
        $this->call(DioceseTableSeeder::class);

        $this->call(UserTableSeeder::class);
        $this->call(CitizenshipTableSeeder::class);

        $this->call(RegionsTableSeeder::class);
        $this->call(ProvincesTableSeeder::class);
        $this->call(CitiesTableSeeder::class);
        $this->call(BarangaysTableSeeder::class);
        $this->call(RelationshipTableSeeder::class);

        $this->call(ParishTableSeeder::class);
        $this->call(PriestTableSeeder::class);

        Model::reguard();
    }
}