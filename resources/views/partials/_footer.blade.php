<!-- partial:partials/_footer.html -->
<footer class="footer">
    <div class="d-sm-flex justify-content-center justify-content-sm-between">
        <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright ©{{ now()->year }}<a href="http://www.vgtechph.com" target="_blank">&reg; Vangard Technology Solutions Philippines &trade;</a>. All rights reserved.</span>
        <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Church Record System <i class="mdi mdi-heart text-danger"></i></span>
    </div>
</footer>
<!-- partial -->
