<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">{{ config('app.name', 'Church Database System') }}</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="{{ Helper::setActive('home') }}"><a href="{{ URL::route('home') }}"><i class="fa fa-home"></i> Home</a></li>        
        <li class="dropdown  {{ Helper::setActive('baptismal.show') }}  {{ Helper::setActive('baptismal.create') }}">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-file" aria-hidden="true"></i> Baptismal <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="{{ URL::route('baptismal.show') }}"><i class="fa fa-list" aria-hidden="true"></i> List Baptismal</a></li>  
          <li><a href="{{ URL::route('baptismal.create') }}"><i class="fa fa-plus-square" aria-hidden="true"></i> New Baptismal</a></li>             
        </ul>
      </li>
      <li class="dropdown  {{ Helper::setActive('confirmation.create') }} ">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-files-o" aria-hidden="true"></i> Confirmation <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="{!! URL::route('confirmation.show') !!}"><i class="fa fa-list" aria-hidden="true"></i> List Confirmation</a></li>
          <li><a href="{!! URL::route('confirmation.create') !!}"><i class="fa fa-plus-square" aria-hidden="true"></i> New Confirmation</a></li>
        </ul>
      </li>      
      <li class="dropdown  {{ Helper::setActive('death.create') }} ">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-file-text-o" aria-hidden="true"></i> Death <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="{!! URL::route('death.show') !!}"><i class="fa fa-list" aria-hidden="true"></i> List Death</a></li>
          <li><a href="{!! URL::route('death.create') !!}"><i class="fa fa-plus-square" aria-hidden="true"></i> New Record</a></li>
        </ul>
      </li>
          @if (Auth::check())
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-user"></i> {{ Auth::user()->firstname }} {{ Auth::user()->middlename}} {{ Auth::user()->lastname }} <span  class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                  @if (Auth::user()->isAdmin == 1)
                  <li><a href="{!! URL::route('city.index') !!}"> <i class="fa fa-university" aria-hidden="true"></i> City</a></li>
                  <li><a href="{!! URL::route('priest.index') !!}"> <i class="fa fa-user-secret" aria-hidden="true"></i> Priest</a></li>
                  <li><a href="{!! URL::route('cemetery.index') !!}"> <i class="fa fa-heart" aria-hidden="true"></i> Cemetery</a></li>
                  <li><a href="{!! URL::route('users.index') !!}"><i class="fa fa-users" aria-hidden="true"></i> List Users</a></li>
                  @endif
                  <li><a href="{!! URL::route('user.changepassword') !!}"><i class="fa fa-key" aria-hidden="true"></i> Change Password</a></li>
                  <li><a href="{{ URL::route('user.logout') }}"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a></li>
                </ul>
            </li>
          @else 
            <li><a href="{{ URL::route('user.getlogin') }}">Login</a></li>    
          @endif
      </ul>

    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>