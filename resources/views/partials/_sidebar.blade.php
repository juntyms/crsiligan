<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item nav-profile">
            <a href="#" class="nav-link">
                <div class="nav-profile-image">
                    <img src="{!! asset('purple/images/faces-clipart/pic-4.png') !!}" alt="profile">
                    <span class="login-status online"></span>
                    <!--change to offline or busy as needed-->
                </div>
                <div class="nav-profile-text d-flex flex-column">
                    <span class="font-weight-bold mb-2">{{ Auth::user()->firstname }} {{ Auth::user()->middlename}} {{ Auth::user()->lastname }}</span>
                    <span class="text-secondary text-small">
                        @if (Auth::user()->superAdmin == 1)
                        Super User <br />
                        @endif

                        @if (Auth::user()->isAdmin == 1)
                        System Administrator <br />
                        @endif

                        @if (Auth::user()->isSecretary == 1)
                        Parish Secretary
                        @endif
                    </span>
                </div>
                <i class="mdi mdi-bookmark-check text-success nav-profile-badge"></i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ URL::route('home') }}">
                <span class="menu-title">Dashboard</span>
                <i class="mdi mdi-home menu-icon"></i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#baptismal-page" aria-expanded="false" aria-controls="baptismal-page">
                <span class="menu-title">Baptismal</span>
                <i class="menu-arrow"></i>
                <i class="mdi mdi-baby menu-icon"></i>
            </a>
            <div class="collapse" id="baptismal-page">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="{!! URL::route('baptismal.create') !!}"> Create New Record </a></li>
                    <li class="nav-item"> <a class="nav-link" href="{!! URL::route('baptismal.search') !!}"> Search Record </a></li>
                    <li class="nav-item"> <a class="nav-link" href="{!! URL::route('baptismal.show') !!}"> Display All Records </a></li>
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#confirmation-page" aria-expanded="false" aria-controls="confirmation-page">
                <span class="menu-title">Confirmation</span>
                <i class="menu-arrow"></i>
                <i class="mdi mdi-account-check menu-icon"></i>
            </a>
            <div class="collapse" id="confirmation-page">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="{!! URL::route('confirmation.create') !!}"> Create New Record</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{!! URL::route('confirmation.search') !!}"> Search Record </a></li>
                    <li class="nav-item"> <a class="nav-link" href="{!! URL::route('confirmation.show') !!}"> Display All Records</a></li>
                </ul>
            </div>
        </li>
        <div class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#marriage-page" aria-expanded="false" aria-controls="marriage-page">
                <span class="menu-title">Marriage Record</span>
                <i class="menu-arrow"></i>
                <i class="mdi mdi-church menu-icon"></i>
            </a>
            <div class="collapse" id="marriage-page">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="{!! URL::route('marriage.create') !!}"> Create New Record</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{!! URL::route('marriage.search') !!}"> Search Record</a></li>
                </ul>
            </div>
        </div>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#death-page" aria-expanded="false" aria-controls="death-page">
                <span class="menu-title">Death Record</span>
                <i class="menu-arrow"></i>
                <i class="mdi mdi-church menu-icon"></i>
            </a>
            <div class="collapse" id="death-page">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="{!! URL::route('death.create') !!}"> Create New Record</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{!! URL::route('death.search') !!}"> Search Record</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{!! URL::route('death.show') !!}"> Display All Records</a></li>
                </ul>
            </div>
        </li>

        <li class="nav-item sidebar-actions">
            <span class="nav-link">
                <div class="border-bottom">
                    <h6 class="font-weight-normal mb-3">Settings</h6>
                </div>
                <div class="mt-4">
                    <div class="border-bottom">
                        <ul class="gradient-bullet-list mt-4">
                            <li><a href="{!! URL::route('city.index') !!}">City</a></li>
                            <li><a href="{!! URL::route('parish.index') !!}">Parish</a></li>
                            <li><a href="{!! URL::route('priest.index') !!}">Priest</a></li>
                            <li><a href="{!! URL::route('cemetery.index') !!}">Cemetery</a></li>
                            <li><a href="{!! URL::route('relationship.index') !!}">Relationship</a></li>
                        </ul>
                        @if ((Auth::user()->isAdmin == 1) || (Auth::user()->superAdmin == 1))
                        <ul class="gradient-bullet-list mt-4">
                            <li><a href="{!! URL::route('users.index') !!}">User List</a></li>
                            <li><a href="{!! URL::route('diocese.index') !!}">Diocese</a></li>
                            <li><a href="{!! URL::route('affiliation.index') !!}">Affiliations</a></li>
                            <li><a href="{!! URL::route('designation.index') !!}">Designations</a></li>
                        </ul>
                        @endif

                    </div>

                </div>
            </span>
        </li>
    </ul>
</nav>
