<footer class="footer">
    <div class="container">
        <p class="text-muted">&copy {{ now()->year }} - VGTech <a href="#"> www.vgtech.ph </a></p>
    </div>
</footer>
