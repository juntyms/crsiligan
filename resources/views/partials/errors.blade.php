@if (count($errors) > 0)
<div class="container">
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>  
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>  
    </div>
</div>
@endif

@if (Session::has('success'))    
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <p>{{ Session::get('success') }}</p>      
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>            
@endif

@if (Session::has('info'))    
    <div class="alert alert-info alert-dismissible fade show" role="alert">
        <p>{{ Session::get('info') }}</p>      
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>            
@endif

@if (Session::has('danger'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
        <p>{{ Session::get('danger') }}</p>      
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>   
@endif