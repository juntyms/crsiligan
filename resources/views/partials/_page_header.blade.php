<div class="page-header">
    <h3 class="page-title">
        <span class="page-title-icon bg-gradient-primary text-white mr-2">
        <i class="{!! $icon !!}"></i>                 
        </span>
        {!! $title !!}
    </h3>            
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">{!! $item1 !!}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{!! $item2 !!}</li>
        </ol>
    </nav>
</div>