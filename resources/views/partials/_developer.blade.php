<!-- Modal -->
<div class="modal fade" id="developerModal" tabindex="-1" role="dialog" aria-labelledby="developerModalTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content bg-info">
      <div class="modal-header">
        <h5 class="modal-title" id="developerModalTitle">Vangard Technology Solutions Philippines</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h3>Contact Us</h3>        
        <div class="card-deck">
            <div class="card bg-info" style="width: 18rem;">
                <img class="card-img-top rounded-circle" src="{!! asset('purple/images/faces/junn.jpg') !!}"  alt="lead developer">
                <div class="card-body">
                <h5 class="card-title">Junn Eric T. Timoteo</h5>
                <p class="card-text">juntyms@gmail.com</p>        
                </div>
                <div class="card-footer bg-secondary">
                <small class="text-white">Lead Developer</small>
                </div>
            </div>
            <div class="card bg-info" style="width: 18rem;">
                <img class="card-img-top rounded-circle" src="{!! asset('purple/images/faces/ben.jpg') !!}"  alt="Systems Analyst">
                <div class="card-body">
                <h5 class="card-title">Rubin E. Alce</h5>
                <p class="card-text">alceben@gmail.com</p>
                </div>
                <div class="card-footer bg-secondary">
                <small class="text-white">Systems Analyst</small>
                </div>
            </div>
            
        </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-gradient-secondary" data-dismiss="modal">Close</button>        
      </div>
    </div>
  </div>
</div>