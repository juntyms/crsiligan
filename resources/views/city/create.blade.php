@extends('mainlayout')

@section('maincontent')

@include('partials._page_header',['icon'=>'mdi mdi-baby','title'=>'City Record','item1'=>'City','item2'=>'Create New Record']) 

  <div class="card">
    <div class="card-header">Add City
    </div>
    <div class="card-body">
      {!! Form::open(['action'=>'CityController@save', 'class'=>'form-horizontal']) !!}
          @include('city._form',['submitButtonText'=>'Save'])

      {!! Form::close() !!}
    </div>
  </div>
@endsection