@extends('mainlayout')

@section('maincontent')

@include('partials._page_header',['icon'=>'mdi mdi-baby','title'=>'City Record','item1'=>'City','item2'=>'Show All Record']) 

<div class="card">
	<div class="card-header">City List</div>
	<div class="card-body">
	<p>
	<a href="{!! URL::route('city.create') !!}" class="btn btn-primary"> <i class="mdi mdi-library-plus d-block mb-1"></i> Add New City</a>
	</p>
		<table class="table">
			<tr>
				<th>SN</th>
				<th>City Name</th>
				<th></th>
			</tr>
		@foreach($cities as $city)
			<tr>
				<td>{!! ++$sn !!}</td>
				<td>{!! $city->name !!}</td>
				<td><a href="{{ URL::route('city.edit',[$city->id]) }}" class="btn btn-primary btn-sm"> <i class="mdi mdi-tooltip-edit"></i> Edit</a>  
					<a href="{!! URL::route('barangay.index', $city->id) !!}" class="btn btn-warning btn-sm"> <i class="mdi mdi-playlist-plus"></i> List Barangays</a></td>
			</tr>
		@endforeach
		</table>
	</div>
</div>



@endsection