@extends('mainlayout')

@section('maincontent')

@include('partials._page_header',['icon'=>'mdi mdi-baby','title'=>'City Record','item1'=>'City','item2'=>'Edit Record']) 
  
  <div class="card">
    <div class="card-header">Edit City</div>
    <div class="card-body">
      {!! Form::model($city,['action'=>['CityController@update',$city->id],'method'=>'PATCH', 'class'=>'form-horizontal']) !!}
          @include('city._form',['submitButtonText'=>'Update'])
      {!! Form::close() !!}
    </div>
  </div>
@endsection