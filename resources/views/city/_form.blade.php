<div class="form-group">
	{!! Form::label('cityname','City Name') !!}
	{!! Form::text('name',null,['class'=>'form-control']) !!}  	
</div>
<div class="col-sm-offset-2 col-sm-10">
	{!! Form::button('<i class="glyphicon glyphicon-floppy-save"></i> '. $submitButtonText, array('type' => 'submit', 'class' => 'btn btn-primary btn-sm ')) !!}
	<a href="{!! URL::route('city.index') !!}" class="btn btn-info btn-sm"> Cancel</a>
</div>


