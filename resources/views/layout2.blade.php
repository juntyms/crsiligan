<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>{{ config('app.name', 'Church Database System') }}</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="{!! asset('purple/vendors/iconfonts/mdi/css/materialdesignicons.min.css') !!}">
  <link rel="stylesheet" href="{!! asset('purple/vendors/css/vendor.bundle.base.css') !!}">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="{!! asset('purple/css/style.css') !!}">
  <!-- endinject -->
  <link rel="shortcut icon" href="{!! asset('purple/images/favicon.png') !!}" />
  @yield('css')
</head>
<body>	
	@yield('content')
	
  <script src="{!! asset('js/jquery-3.4.1.min.js') !!}"></script>
  <script src="{!! asset('purple/js/bootstrap.min.js') !!}"></script>  
  <!-- plugins:js -->
  <script src="{!! asset('purple/vendors/js/vendor.bundle.base.js') !!}"></script>
  <script src="{!! asset('purple/vendors/js/vendor.bundle.addons.js') !!}"></script>
  <!-- endinject -->
  <!-- inject:js -->
  <script src="{!! asset('purple/js/off-canvas.js') !!}"></script>
  <script src="{!! asset('purple/js/misc.js') !!}"></script>
  <!-- endinject -->
<!-- JavaScripts -->
  
  @yield('script')


</body>

</html>