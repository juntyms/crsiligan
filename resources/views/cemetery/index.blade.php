@extends('mainlayout')

@section('maincontent')

@include('partials._page_header',['icon'=>'mdi mdi-baby','title'=>'Cementery Record','item1'=>'Cementery','item2'=>'Show All Record']) 

<div class="card">
	<div class="card-header"> Cemetery Record</div>
	<div class="card-body">
	<p>
	<a href="{!! URL::route('cemetery.new') !!}" class="btn btn-primary"> <i class="mdi mdi-bookmark-plus d-block mb-1"></i>New</a>
	</p>
	<table class="table table-bordered">
		<tr class="table-info">
			<th>SN</th>
			<th>Name</th>
			<th></th>
		</tr>	
		@php 
			$sn = 1;
		@endphp
		@foreach($cemeterys as $cemetery)
		<tr>
			<td>{!! $sn++ !!}</td>
			<td>{!! $cemetery->name !!}</td>
			<td><a href="{!! URL::route('cemetery.edit',$cemetery->id) !!}" class="btn btn-sm btn-success"> <i class="mdi mdi-table-edit"></i> Edit</a></td>
		</tr>
		@endforeach
	</table>
	</div>
</div>
	



@endsection