@extends('mainlayout')

@section('maincontent')

@include('partials._page_header',['icon'=>'mdi mdi-baby','title'=>'Cementery Record','item1'=>'Cementery','item2'=>'Create New Record']) 

<div class="card">
	<div class="card-header">New Cemetery Record</div>
	<div class="card-body">
	{!! Form::open(['route'=>'cemetery.save','class'=>'form-horizontal']) !!}
		@include('cemetery._form',['buttonText'=>' Save'])
	{!! Form::close() !!}
	</div>
</div>
	

@endsection