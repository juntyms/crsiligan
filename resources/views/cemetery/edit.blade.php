@extends('mainlayout')

@section('maincontent')

@include('partials._page_header',['icon'=>'mdi mdi-baby','title'=>'Cementery Record','item1'=>'Cementery','item2'=>'Create New Record']) 

<div class="card">
	<div class="card-header">Edit Cemetery Record</div>
	<div class="card-body">
	{!! Form::model($cemetery,['route'=>['cemetery.update',$cemetery->id],'class'=>'form-horizontal']) !!}
		@include('cemetery._form',['buttonText'=>' Update'])
	{!! Form::close() !!}
	</div>
</div>
	

@endsection