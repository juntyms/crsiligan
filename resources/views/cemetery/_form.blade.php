<div class="form-group">
    <label for="name">Name</label>			
        {!! Form::text('name', null, ['class'=>'form-control','required']) !!}			
</div>
<div class="form-group">			
        <button class="btn btn-primary"><i class="mdi mdi-floppy"></i> {!! $buttonText !!}</button>			
</div>