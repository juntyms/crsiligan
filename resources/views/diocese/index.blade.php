@extends('mainlayout')

@section('maincontent')
        
        @include('partials._page_header',['icon'=>'mdi mdi-baby','title'=>'Diocese Record','item1'=>'Diocese','item2'=>'Create New Record'])      

        <div class="card">
          <div class="card-header"> New Record</div>
            <div class="card-body">
            <a href="{!! URL::route('diocese.create') !!}" class="btn btn-primary"> <i class="mdi mdi-library-plus d-block mb-1"></i> Add New Diocese</a>
                <table class="table table-bordered">
                    <tr>
                        <th>SN</th>
                        <th>Name</th>
                        <th></th>
                    </tr>
                    @php 
                        $sn = 1;
                    @endphp
                    @foreach($dioceses as $diocese)
                    <tr>
                        <td>{!! $sn++ !!}</td>
                        <td>{!! $diocese->name !!}</td>
                        <td><a href="{!! URL::route('diocese.edit',$diocese->id) !!}"> Edit</a></td>
                    </tr>
                    @endforeach
                </table>
            </div>   	
	    </div>
@endsection
