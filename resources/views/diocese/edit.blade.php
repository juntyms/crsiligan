@extends('mainlayout')

@section('maincontent')
        
        @include('partials._page_header',['icon'=>'mdi mdi-baby','title'=>'Diocese Record','item1'=>'Diocese','item2'=>'Edit Record'])      

        <div class="card">
          <div class="card-header"> Edit Record</div>
            <div class="card-body">
            {!! Form::model($diocese,['route'=>['diocese.update',$diocese->id]]) !!}	            	    	
	        	@include('diocese._form')

	        	<div class="pull-right">    			
            		<button type="submit" class="btn btn-gradient-info btn-icon-text"><i class="mdi mdi-floppy"></i> Update</button>            	            		
          		</div>
                  <br />
		        {!! Form::close() !!}	     
            </div>   	
	    </div>
@endsection
