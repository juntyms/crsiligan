@extends('mainlayout')

@section('maincontent')

@include('partials._page_header',['icon'=>'mdi mdi-church','title'=>'Parish Record','item1'=>'Parish','item2'=>'Edit
Record'])

<div class="card">
    <div class="card-header">Edit Parish
    </div>
    <div class="card-body">
        {!! Form::model($parish,['route'=>['parish.update',$parish->id],'method'=>'PATCH', 'class'=>'form-horizontal'])
        !!}

        @include('parish._form',['submitButtonText'=>'Update'])
        {!! Form::close() !!}
    </div>
</div>

@endsection