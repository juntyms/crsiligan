<div class="form-group">
	<label for="diocese">Diocese</label>
	{!! Form::select('diocese_id',$dioceses, null, ['class'=>'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('city','City', ['class'=>'col-sm-2 control-label']) !!}	
		{!! Form::select('city_id',$city,null,['class'=>'form-control']) !!}	
</div>
<div class="form-group">
	{!! Form::label('parish','Parish', ['class'=>'col-sm-2 control-label']) !!}	
		{!! Form::text('name',null,['class'=>'form-control']) !!}
		@if ($errors->has('name'))<small class="text-danger">{{ $errors->first('name') }}</small> @endif	
</div>
<div class="form-group">
	{!! Form::label('street','Parish Street Address', ['class'=>'col-sm-2 control-label']) !!}	
		{!! Form::text('address',null,['class'=>'form-control']) !!}
		@if ($errors->has('street'))<small class="text-danger">{{ $errors->first('street') }}</small> @endif	
</div>

<div class="form-group">
	<button class="btn btn-gradient-primary btn-sm"><i class="mdi mdi-floppy mdi-18px"></i> {!! $submitButtonText !!}</button>
	<a href="{!! URL::route('parish.index') !!}" class="btn btn-gradient-secondary btn-sm"> <i class="mdi mdi-close-octagon-outline mdi-18px"></i> Cancel</a>
</div>