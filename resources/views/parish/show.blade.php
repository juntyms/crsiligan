@extends('mainlayout')

@section('maincontent')

@include('partials._page_header',['icon'=>'mdi mdi-church','title'=>'Parish Record','item1'=>'Parish','item2'=>'Show All Record']) 

<div class="card">
	<div class="card-header">Show All Parish</div>
	<div class="card-body">
	<p>
		<a href="{!! URL::route('parish.create') !!}" class="btn btn-gradient-primary"><i class="mdi mdi-book-plus d-block mb-1"></i> Add Parish</a>
	</p>
	<table class="table table-bordered" id="parishTable">
		<thead>
		<tr class="table-primary">
			<th>SN</th>
			<th>Diocese</th>			
			<th>Parish</th>
			<th>City</th>
			<th>Address</th>
			<th></th>
		</tr>
		</thead>
		<tbody>
	@foreach($parishes as $parish)
		<tr>
			<td>{!! ++$sn !!}</td>
			<td>{!! $parish->diocese->name !!}</td>
			<td>{!! $parish->name !!}</td>
			<td>{!! $parish->city->name !!}</td>
			<td>{!! $parish->address !!}</td>
			<td><a href="{{ URL::route('parish.edit',[$parish->id]) }}" class="btn btn-sm btn-gradient-info"><i class="mdi mdi-table-edit"></i> Edit</a></td>
		</tr>
	@endforeach
		</tbody>
	</table>
	</div>
</div>
@endsection

@section('script')
	<script>
	$(document).ready( function () {
    	$('#parishTable').DataTable();
	} );
	</script>
@endsection