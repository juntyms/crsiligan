@extends('mainlayout')

@section('maincontent')

@include('partials._page_header',['icon'=>'mdi mdi-church','title'=>'Parish Record','item1'=>'Parish','item2'=>'Create
New Record'])

<div class="card">
    <div class="card-header">Add Parish
    </div>
    <div class="card-body">
        {!! Form::open(['route'=>'parish.save', 'class'=>'form-horizontal']) !!}
        @include('parish._form',['submitButtonText'=>'Save'])
        {!! Form::close() !!}
    </div>
</div>

@endsection