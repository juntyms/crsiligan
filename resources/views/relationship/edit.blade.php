@extends('mainlayout')

@section('maincontent')

@include('partials._page_header',['icon'=>'mdi mdi-church','title'=>'Relationship Record','item1'=>'Priest','item2'=>'Edit Record']) 

<div class="card">
    <div class="card-header"> Edit Relationship Record</div>
    <div class="card-body">
        {!! Form::model($relationship,['route'=>['relationship.update',$relationship->id]]) !!}
            @include('relationship._form',['buttonText'=>' Update'])
        {!! Form::close() !!}
    </div>
</div>

@endsection