<div class="col-md-6 offset-md-3">
<blockquote class="blockquote">
    <div class="form-group">
        <label for=""Name>Relationship</label>
        {!! Form::text('name', null, ['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        <button class="btn btn-primary"> <i class="mdi mdi-floppy"></i> {!! $buttonText !!}</button>
    </div>
</blockquote>
</div>