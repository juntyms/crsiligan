@extends('mainlayout')

@section('maincontent')

@include('partials._page_header',['icon'=>'mdi mdi-church','title'=>'Relationship Record','item1'=>'Priest','item2'=>'Add New Record']) 
    <div class="card">
        <div class="card-header"> New Relationship Record</div>
        <div class="card-body">
            {!! Form::open(['route'=>'relationship.save']) !!}
                @include('relationship._form',['buttonText'=>'Save'])
            {!! Form::close() !!}
        </div>
    </div>
@endsection