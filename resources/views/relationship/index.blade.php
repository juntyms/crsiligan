@extends('mainlayout')

@section('maincontent')

@include('partials._page_header',['icon'=>'mdi mdi-church','title'=>'Relationship Record','item1'=>'Priest','item2'=>'Show All Record']) 

<div class="card">
    <div class="card-header">Relationship Records</div>
    <div class="card-body">
        <p>
        <a href="{!! URL::route('relationship.create') !!}" class="btn btn-primary"> <i class="mdi mdi-library-plus d-block mb-1"></i> New Record</a>
        </p>
        <table class="table table-bordered">
            <tr class="table-info">
                <th>SN</th> 
                <th>Relationship Name</th>
                <th></th>
            </tr>
            @php 
                $sn=1;
            @endphp
            @foreach($relationships as $relationship)
            <tr>
                <td>{!! $sn++ !!}</td>
                <td>{!! $relationship->name !!}</td>
                <td><a href="{!! URL::route('relationship.edit',$relationship->id) !!}" class="btn btn-sm btn-gradient-warning"> Edit</a></td>
            </tr>
            @endforeach
        </table>
    </div>
</div>
@endsection