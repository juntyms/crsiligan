@extends('mainlayout')

@section('maincontent')

@include('partials._page_header',['icon'=>'mdi mdi-baby','title'=>'Barangay Record','item1'=>'Barangay','item2'=>'Edit']) 
	
	<div class="card">
	  <div class="card-header">Barangay Edit
	  </div>
	  <div class="card-body">	 	
        <div class="col-md-6">
	  	{!! Form::model($barangay,['route'=>['barangay.update', $barangay->id]]) !!}
            <div class="form-group">
                <label for="Barangay">Name</label>
                {!! Form::text('name',null, ['class'=>'form-control']) !!}
            </div>
            <button class="btn btn-sm btn-success"> Update</button>
        {!! Form::close() !!}
        </div>  		
	  </div>
	</div>
@endsection