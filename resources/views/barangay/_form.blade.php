
<div class="form-group">
	{!! Form::label('city_id','City') !!}	
		{!! Form::select('city_id',$cities,$cityid,['class'=>'form-control','id'=>'city_id']) !!}	
</div>

<div class="form-group">
	{!! Form::label('barangay','Barangay') !!}	
		{!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Barangay name']) !!}
		@if ($errors->has('name'))<small class="text-danger">{{ $errors->first('name') }}</small> @endif	
</div>
<div class="form-group">
	<button class="btn btn-primary"> <i class="mdi mdi-floppy"></i> {!! $submitButtonText !!}</button>
</div>
