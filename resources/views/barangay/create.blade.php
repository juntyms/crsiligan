@extends('mainlayout')

@section('maincontent')

@include('partials._page_header',['icon'=>'mdi mdi-baby','title'=>'Barangay Record','item1'=>'Barangay','item2'=>'Create
New Record'])

<div class="card">
    <div class="card-header">Create Barangay
    </div>
    <div class="card-body">
        {!! Form::open(['route'=>'barangay.save','method'=>'POST']) !!}
        @include('barangay._form',['submitButtonText' => 'Save'])
        {!! Form::close() !!}
    </div>
</div>

@endsection