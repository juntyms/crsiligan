@extends('mainlayout')

@section('maincontent')

@include('partials._page_header',['icon'=>'mdi mdi-baby','title'=>'Barangay Record','item1'=>'Barangay','item2'=>'Show All Record']) 
	<p>
	<a href="{!! URL::route('city.index') !!}" class="btn btn-info btn-sm"> List City</a>
	<a href="{!! URL::route('barangay.create',$city->id) !!}" class="btn btn-primary btn-sm"> Add Barangay</a> 		   	
	</p>
	<div class="card">
	  <div class="card-header">Barangay List
	  </div>
	  <div class="card-body">	 	  		
	  		<table class="table table-bordered">
			  	<tr>
					<th>City</th>  
					<th>Barangay</th>
					<th></th>
				</tr>
	   		@foreach($city->barangay as $barangay)
				<tr>
					<td>{!! $barangay->city->name !!}</td>
					<td>{!! $barangay->name !!}</td>
					<td><a href="{!! URL::route('barangay.edit', $barangay->id) !!}">Edit</a></td>
				</tr>
	   		@endforeach
	   		</table>
	  </div>
	</div>
@endsection