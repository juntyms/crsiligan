@extends('mainlayout')

@section('maincontent')
        
        @include('partials._page_header',['icon'=>'mdi mdi-account-card-details','title'=>'Affiliation Record','item1'=>'Affiliation','item2'=>'Edit']) 
        <div class="card">
            <div class="card-header">
                Edit Affiliations
            </div>
            <div class="card-body">
                {!! Form::model($affiliation,['route'=>['affiliation.update',$affiliation->id]]) !!}
                    <div class="form-group">
                        <label for="affiliation">Affiliation</label>
                        {!! Form::text('affiliation',null, ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        <button class="btn btn-sm btn-gradient-info"> Update</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
     

@endsection