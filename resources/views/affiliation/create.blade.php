@extends('mainlayout')

@section('maincontent')
        
        @include('partials._page_header',['icon'=>'mdi mdi-account-card-details','title'=>'Affiliation Record','item1'=>'Affiliation','item2'=>'New']) 
        <div class="card">
            <div class="card-header">
                Create New Affiliations
            </div>
            <div class="card-body">
                {!! Form::open(['route'=>'affiliation.save']) !!}
                    <div class="form-group">
                        <label for="affiliation">Affiliation</label>
                        {!! Form::text('affiliation',null, ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        <button class="btn btn-sm btn-gradient-info"> Save</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
     

@endsection