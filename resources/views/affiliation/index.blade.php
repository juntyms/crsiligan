@extends('mainlayout')

@section('maincontent')
        
        @include('partials._page_header',['icon'=>'mdi mdi-account-card-details','title'=>'Affiliation Record','item1'=>'Affiliation','item2'=>'Show All']) 
        <div class="card">
            <div class="card-header">
                Affiliations
            </div>
            <div class="card-body">
                <p>
                    <a href="{!! URL::route('affiliation.create') !!}" class="btn btn-gradient-primary"><i class="mdi mdi-book-plus d-block mb-1"></i> Add Affiliation</a>
                </p>
                <table class="table table-bordered">
                    <thead>
                        <tr class="bg-success">
                            <th>SN</th>
                            <th>Affiliation</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $sn=1; ?>
                        @foreach($affiliations as $affiliation)
                        <tr>
                            <td>{!! $sn++ !!}</td>
                            <td>{!! $affiliation->affiliation !!}</td>       
                            <td><a href="{!! URL::route('affiliation.edit', $affiliation->id) !!}">Edit</a></td>                    
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
     

@endsection