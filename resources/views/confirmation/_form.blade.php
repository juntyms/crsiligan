<div class="row">
  <div class="col-sm-6">  
    <blockquote class="blockquote">
      <div class="form-group">
        <label for="fname">First Name <sup class="text-danger">*</sup></label>                                                               
            {!! Form::text('fname',null,['placeholder'=>'Pangalan sa confirmahan','class'=>'form-control']) !!}
            @if ($errors->has('fname'))<small class="text-danger">{{ $errors->first('fname') }}</small> @endif                                
      </div>
      <div class="form-group">
        <label for="mname">Middle Name <sup class="text-danger">*</sup></label>              
            {!! Form::text('mname', null, ['placeholder'=>'Apilyedo sa inahan sa dalaga pa', 'class'=>'form-control']) !!}  
            @if ($errors->has('mname'))<small class="text-danger">{{ $errors->first('mname') }}</small> @endif                                              
      </div>
      <div class="form-group">
        <label for="lname">Last Name <sup class="text-danger">*</sup></label>            
          {!! Form::text('lname', null, ['placeholder'=>'Legal nga Apilyedo sa confirmahan', 'class'=>'form-control']) !!}   
          @if ($errors->has('lname'))<small class="text-danger">{{ $errors->first('lname') }}</small> @endif                                       
      </div>     
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="gender">Gender <sup class="text-danger">*</sup></label>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-check">
                    <label for="Male" class="form-check-label">
                      {!! Form::radio('gender','M', ['class'=>'form-check-input']) !!} Male
                    </label>        
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-check">
                    <label for="Female" class="form-check-label">
                    {!! Form::radio('gender','F', ['class'=>'form-check-input']) !!} Female                           
                    </label>                
                  </div>                
                </div>           
              </div>                                           
              @if ($errors->has('gender'))<small class="text-danger">{{ $errors->first('gender') }}</small> @endif
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="inputdob">Date of Birth <sup class="text-danger">*</sup></label>
                {!! Form::date('dob',null,['id'=>'datepicker-dob','class'=>'form-control','placeholder'=>'Date of Birth','required']) !!}
                @if ($errors->has('dob'))<small class="text-danger">{{ $errors->first('dob') }}</small> @endif                            
          </div>
        </div>
      </div>     
      <div class="form-group">
        <div class="row">
          <div class="col-sm-9">
          <label for="doc">Date of Confirmation <sup class="text-danger">*</sup></label>    
            {!! Form::date('dofconfirmation',null,['id'=>'datepicker-doc','class'=>'form-control','placeholder'=>'Confirmation Date']) !!}                                
            @if ($errors->has('dofconfirmation'))<small class="text-danger">{{ $errors->first('dofconfirmation') }}</small> @endif
          </div>                            
        <!--Age will display automatically after entering date of birth and date of confirmation -->
        <div class="col-sm-3">
          <label for="dob">Age <sup class="text-danger">*</sup></label>        
            {!! Form::text('age',null,['id'=>'age','class'=>'form-control', 'readonly']) !!}
          </div>
        </div>
      </div>    
      <div class="form-group">
          <label for="conby">Episcopal Minister <sup class="text-danger">*</sup></label>        
          <div class="input-group">
            {!! Form::select('episcopalMinister_id',$priests,null,['class'=>'form-control','id'=>'episcopal','placeholder'=>'Select Episcopal Minister']) !!}  
            <div class="input-group-append">
              <!-- Button trigger modal -->
              <a href="#" id="reload_coneminister" class="btn btn-sm btn-success"><i class="mdi mdi-cached"></i></a>
              <button type="button" class="btn btn-gradient-primary btn-sm" data-toggle="modal" data-target="#episcopalModal">
              <i class="mdi mdi-account-plus"></i> Add
              </button>              
            </div>
          </div>
                @if ($errors->has('episcopalMinister_id'))<small class="text-danger">{{ $errors->first('episcopalMinister_id') }}</small> @endif                                                      
      </div>                      
    </blockquote>
    <h5>Father's Information</h5>
    <blockquote class="blockquote">
      <div class="form-group">
          <label for="fa-fname">First Name </label>        
              {!! Form::text('fatherFn', null, ['class'=>'form-control', 'placeholder'=>'Pangalan sa amahan']) !!}                                          
      </div>
      <div class="form-group">
          <label for="fa-midname">Middle Name</label>        
              {!! Form::text('fatherMn', null, ['class'=>'form-control','placeholder'=>'Middle Name sa amahan']) !!}                                          
      </div>
      <div class="form-group">
          <label for="fa-lname">Last Name</label>        
            {!! Form::text('fatherLn', null, ['class'=>'form-control','placeholder'=>'Apilyedo sa amahan']) !!}                                        
      </div>
    </blockquote>
    <h5>Mother's Information</h5>
    <blockquote class="blockquote">
      <div class="form-group">
        <label for="mo-fname">First Name <sup class="text-danger">*</sup></label>                            
            {!! Form::text('motherFn',null, ['placeholder'=>'Pangalan sa inahan','class'=>'form-control']) !!}  
            @if ($errors->has('motherFn'))<small class="text-danger">{{ $errors->first('motherFn') }}</small> @endif                                                          
      </div>
      <div class="form-group">
        <label for="mo-midname">Maiden Name <sup class="text-danger">*</sup></label>                            
            {!! Form::text('motherMn',null, ['placeholder'=>'Apilyedo sa inahan sa dalaga pa','class'=>'form-control']) !!}  
            @if ($errors->has('motherMn'))<small class="text-danger">{{ $errors->first('motherMn') }}</small> @endif                                                          
      </div>
      <div class="form-group">
        <label for="mo-lname">Last Name <sup class="text-danger">*</sup></label>                            
            {!! Form::text('motherLn', null, ['placeholder'=>'Legal nga apilyedo sa inahan', 'class'=>'form-control']) !!}  
            @if ($errors->has('motherLn'))<small class="text-danger">{{ $errors->first('motherLn') }}</small> @endif                                                          
      </div>      
    </blockquote>
  </div> <!-- ./col-md-6 -->
  <div class="col-sm-6">
    <h5>Place of Confirmation</h5>
    <blockquote class="blockquote">
      <div class="form-group">
        <label for="placebap">City <sup class="text-danger">*</sup></label>
        <div class="input-group">
          {!! Form::select('placeCity_id',$cities,null,['class'=>'form-control','id'=>'confirmation_city','placeholder'=>'Select City']) !!}                            
          <div class="input-group-append">
            <button type="button" class="btn btn-gradient-primary btn-sm" data-toggle="modal" data-target="#concityModal">
            <i class="mdi mdi-library-plus"></i> Add
            </button>              
          </div>
        </div>                        
            @if ($errors->has('placeCity_id'))<small class="text-danger">{{ $errors->first('placeCity_id') }}</small> @endif                        
      </div>

      <div class="form-group">
        <label for="placebap">Barangay <sup class="text-danger">*</sup></label>                        
          <div class="input-group">
          {!! Form::select('placeBarangay_id',$barangays,null,['id'=>'confirmation_barangay','class'=>'form-control','placeholder'=>'Select Barangay']) !!}
            <div class="input-group-append">                          
              <button type="button" class="btn btn-gradient-primary btn-sm" id="conbarangaybtn">
              <i class="mdi mdi-library-plus"></i> Add
              </button>         
            </div>
          </div>                               
              @if ($errors->has('placeBarangay_id'))<small class="text-danger">{{ $errors->first('placeBarangay_id') }}</small> @endif                          
      </div>

      <div class="form-group">
        <label for="placebap">Parish/Chapel <sup class="text-danger">*</sup></label>
        <div class="input-group">
        {!! Form::select('placeParish_id',$parishes,null, ['id'=>'parish','class'=>'form-control']) !!}
          <div class="input-group-append">
          <button type="button" class="btn btn-gradient-primary btn-sm" data-toggle="modal" data-target="#conparishmodal">
            <i class="mdi mdi-library-plus"></i> Add
            </button>             
          </div>
        </div>
        @if ($errors->has('placeParish_id'))<small class="text-danger">{{ $errors->first('placeParish_id') }}</small> @endif                          
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <label for="volume">Volume</label>                                                     
                {!! Form::number('volume',null,['class'=>'form-control','placeholder'=>'Enter Volume No.']) !!}                            
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="book">Book</label>                                                     
                {!! Form::number('book',null,['class'=>'form-control','placeholder'=>'Enter Book No.']) !!}                            
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="page">Page</label>                            
                {!! Form::number('page',null,['class'=>'form-control','placeholder'=>'Enter Page No.']) !!}                            
          </div>
        </div>
      </div>
      <div class="form-group">
        <label for="purpose">Purpose</label>                                                    
          {!! Form::text('purpose',null,['class'=>'form-control','placeholder'=>'Katuyuan']) !!}        
      </div>

      <div class="form-group">
        <label for="annotation">Annotation</label>                                                    
          {!! Form::textarea('annotation',null,['class'=>'form-control','placeholder'=>'Dugang Katuyuan']) !!}        
      </div>
      <div class="form-group">
        <label for="remarks">Remarks</label>                                                    
          {!! Form::textarea('remarks',null,['class'=>'form-control','placeholder'=>'Remarks']) !!}        
      </div>
      <div class="form-group">
        <label for="bapby">Parish Priest</label>      
          <div class="input-group">
          {!! Form::select('currentPriest_id',$priests,$currentPriest->id,['class'=>'form-control','id'=>'current_priest']) !!}                                                               
            <div class="input-group-append">
              <a href="#" id="reload_conpriest" class="btn btn-sm btn-success"><i class="mdi mdi-cached"></i></a>
              <button type="button" class="btn btn-gradient-primary btn-sm" data-toggle="modal" data-target="#ppriestModal">
                <i class="mdi mdi-library-plus"></i> Add
              </button>             
            </div>  
          </div>                            
      </div>
      <div class="form-group">
        <label for="designation">Designation</label>                                
            {!! Form::select('currentPriest_designationId',$designations,$currentPriest->designation_id,['class'=>'form-control']) !!}                                                                                                
      </div>
      <div class="form-group">
        <label for="inputdateissue">Date Issue</label>                              
            {!! Form::date('dateIssue',null,['class'=>'form-control','placeholder'=>'Date Issue','id'=>'datepicker-dateissue']) !!}                              
      </div>      
      <div class="form-group">
				<div class="form-check form-check-flat form-check-primary">
				<label for="isSecretary" class="form-check-label">Show Release by									
					{!! Form::checkbox('isSecretary', 1 , false, ['class'=>'form-check-input']) !!}													
				</label>
				</div>
			</div>				
    </blockquote>
    <h5>Sponsors List</h5>
    <blockquote class="blockquote">
    @if ($isCreate == 0)
      @include('confirmation._sponsor')
    @endif
    </blockquote>
  </div>    
  
</div>
                      
                    
                                          

                                                        
                       
              
