<table width="100%" border="1">
    <tr>
        <th>SN</th>
        <th>First Name</th>
        <th>Middle Name</th>
        <th>Last Name</th>
        <th>Gender</th>
        <th>Date of Birth</th>
        <th>Date of Confirmation</th>
        <th>Age</th>
        <th>Father's Name</th>
        <th>Mother's Name</th>
        <th>Episcopal Minister</th>
        <th>Place of Confirmation</th>
        <th>Purpose</th>
        <th>Annotation</th>
        <th>Volume</th>
        <th>Book</th>
        <th>Page</th>
        <th>Sponsors</th>
    </tr>
    @php
        $sn = 1;
    @endphp
    @foreach($confirmations as $confirmation)
    <tr>
        <td>{!! $sn++ !!}</td>
        <td>{!! $confirmation->fname !!}</td>
        <td>{!! $confirmation->mname !!}</td>
        <td>{!! $confirmation->lname !!}</td>
        <td>{!! $confirmation->gender !!}</td>
        <td>{!! $confirmation->dob !!}</td>
        <td>{!! $confirmation->dofconfirmation !!}</td>
        <td>{!! $confirmation->age !!}</td>
        <td>{!! $confirmation->fatherFn !!} {!! $confirmation->fatherMn !!} {!! $confirmation->fatherLn !!}</td>
        <td>{!! $confirmation->motherFn !!} {!! $confirmation->motherMn !!} {!! $confirmation->motherLn !!}</td>
        <td>{!! $confirmation->episminister->title !!} {!! $confirmation->episminister->firstname !!} {!! $confirmation->episminister->middlename !!} {!! $confirmation->episminister->lastname !!}</td>
        <td>{!! $confirmation->placeparish->name !!} {!! $confirmation->placecity->name !!} {!! $confirmation->placebarangay->name !!}</td>
        <td>{!! $confirmation->purpose !!}</td>
        <td>{!! $confirmation->annotation !!}</td>
        <td>{!! $confirmation->volume !!}</td>
        <td>{!! $confirmation->book !!}</td>
        <td>{!! $confirmation->page !!}</td>
        @foreach($confirmation->sponsors as $sponsor)
        <td>{!! $sponsor->fname !!} {!! $sponsor->mname !!} {!! $sponsor->lname !!}</td>
        @endforeach
    </tr>
    @endforeach
</table>