<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  {!! Form::open(['route'=>['confirmation.sponsor.save',$confirmation->id],'class'=>'form-horizontal']) !!}
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Sponsor</h4>
      </div>

      <div class="modal-body">
        <div class="form-group">
          {!! Form::label('sfname','First name',['class'=>'col-sm-3 control-label']) !!}
          <div class="col-sm-9">
            {!! Form::text('sponsor_fname',null,['class'=>'form-control']) !!}        
          </div>
        </div>
        <div class="form-group">
          {!! Form::label('sfname','Middle name',['class'=>'col-sm-3 control-label']) !!}
          <div class="col-sm-9">
            {!! Form::text('sponsor_mname',null,['class'=>'form-control']) !!}   
          </div>
        </div>
        <div class='form-group'>
          {!! Form::label('sfname','Last name',['class'=>'col-sm-3 control-label']) !!}
          <div class="col-sm-9">
            {!! Form::text('sponsor_lname',null,['class'=>'form-control']) !!}   
          </div>
        </div>
      </div>
      <div class="modal-footer">        
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
  {!! Form::close() !!}
</div>