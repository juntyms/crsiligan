@extends('mainlayout')

@section('maincontent')

@include('partials._page_header',['icon'=>'mdi mdi-account-check','title'=>'Confirmation Record','item1'=>'Confirmation','item2'=>'Edit Record']) 
  <div class="card">
    <div class="card-header">Confirmation Form - Update</div>
      <div class="card-body">
      	{!! Form::model($confirmation, ['route'=>['confirmation.update',$confirmation->id],'class'=>'form-horizontal']) !!}
        	@include('confirmation._form',['isCreate'=>'0'])
        	<div class="pull-right">
              <button type="submit" name="update" value="1" class="btn btn-gradient-info"><i class="mdi mdi-floppy" aria-hidden="true"></i> Update</button>              
              <button type="submit" name="update" value="0" class="btn btn-gradient-primary"><i class="mdi mdi-printer" aria-hidden="true"></i> Print</button>
            </div> 
        {!! Form::close() !!}
      </div><!-- card body-->
    </div> <!-- card-->
    @include('confirmation._sponsorForm')


@endsection

@section('script')

  @include('confirmation._script')

@endsection