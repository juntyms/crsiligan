@extends('mainlayout')

@section('maincontent')

@include('partials._page_header',['icon'=>'mdi mdi-account-check','title'=>'Confirmation Record','item1'=>'Confirmation','item2'=>'Show All Record']) 

    <div class="card">
      <div class="card-header">
        <h3 class="card-title">List of Confirmations</h3>
      </div>
      <div class="card-body">
        <p>
            <a href="{!! URL::route('confirmation.excel') !!}" class="btn btn-success"> Export To Excel</a>
        </p>
        <table class="table table-bordered" id="confirmationTable">
            <thead>
                <tr class="bg-warning">
                    <th>SN</th>
                    <th>First Name</th>
                    <th>Middle Name</th>
                    <th>Last Name</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php $sn=1; ?>
                @foreach($confirmations as $confirmation)
                    <tr>
                        <td>{!! $sn++ !!}</td>
                        <td>{!! $confirmation->fname !!}</td>
                        <td>{!! $confirmation->mname !!}</td>
                        <td>{!! $confirmation->lname !!}</td>
                        <td><a href="{!! URL::route('confirmation.edit',$confirmation->id) !!}"> Edit</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
      </div>
    </div>
    


@endsection

@section('css')
<link rel="stylesheet" type="text/css" href="{!! asset('DataTables/datatables.min.css') !!}"/>
@endsection

@section('script')
    <script type="text/javascript" src="{!! asset('DataTables/datatables.min.js') !!}"></script>
    <script>
    $(document).ready( function () {
        $('#confirmationTable').DataTable();
    });
    </script>
@endsection