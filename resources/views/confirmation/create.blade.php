@extends('mainlayout')

@section('maincontent')

@include('partials._page_header',['icon'=>'mdi mdi-account-check','title'=>'Confirmation Record','item1'=>'Confirmation','item2'=>'Create New Record']) 
<div class="card">
  <div class="card-header">New Confirmation</div>
    <div class="card-body">    
      	{!! Form::open(['route'=>'confirmation.save']) !!}
        	@include('confirmation._form',['isCreate'=>'1'])
        	<div class="pull-right">
            <button type="submit" class="btn btn-gradient-info"><i class="mdi mdi-floppy" aria-hidden="true"></i> Save</button>              
            <button type="button" class="btn btn-gradient-primary" disabled><i class="mdi mdi-printer" aria-hidden="true"></i> Print</button>
          </div> 
        {!! Form::close() !!}
    </div>  
</div>
@endsection

@section('script')

  @include('confirmation._script')

@endsection