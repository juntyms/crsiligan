<div class="modal fade" id="confirmationbarangaymodal" tabindex="-1" role="dialog" aria-labelledby="placebarangayLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    {!! Form::open(['route'=>'barangay.save','id'=>'conbarform']) !!}            
      <div class="modal-header">
				<h4 class="modal-title" id="placebarangayLabel">Add New Barangay - Place of Baptism</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>        
      </div>
      
      <div class="modal-body">
        
        <div class="form-group">      
          <label for="city">City</label>                       
                {!! Form::select('city_id',$cities,null,['id'=>'conbarangaycity_id','class'=>'form-control']) !!}            
        </div>

        <div class="form-group">
            <label for="barangay">Barangay</label>            
                {!! Form::text('name',null,['id'=>'placebarangayname','class'=>'form-control','placeholder'=>'Barangay name','required']) !!}                
            
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary btn-sm" id="consavebar" data-dismiss="modal"><i class="mdi mid-floppy"></i> Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

<script>


$("#conbarangaybtn").click(function(){
  
  var pcityval = $("#confirmation_city").val();
  if (pcityval !== "") {
    $("#confirmationbarangaymodal").modal("show");
    $("#conbarangaycity_id").val(pcityval);
  } else {
    alert("Place of Confirmation City is Empty");
  }
});

/** Save button clicked */
$("#consavebar").click(function() {
  console.log("save place bar");
  $.post( $("#conbarform").attr("action"),
            $("#conbarform :input").serialize())
      .done(function() {
        loadconbarangay();       
      });
});

$("#conbarform").submit( function() {
  return false;
});

function loadconbarangay()
{
  var cityid = $("#confirmation_city").val();
    $('#confirmation_barangay').empty();
    $('#confirmation_barangay').append('<option value="">Select Barangay</value>');

  $.get('/barangays/list/' + cityid, function(data) {
    console.log(data);
    
    $.each(data, function(index, subCatObj) {
      $('#confirmation_barangay').append('<option value='+subCatObj.id+'>'+subCatObj.name+'</option>');
    });
  });
}

</script>