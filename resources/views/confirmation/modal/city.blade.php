<div class="modal fade" id="concityModal" tabindex="-1" role="dialog" aria-labelledby="conCityLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    {!! Form::open(['route'=>'city.save','id'=>'concityform']) !!}            
      <div class="modal-header">
				<h4 class="modal-title" id="conCityLabel">Add New City</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>        
      </div>      
      <div class="modal-body">                
        <div class="form-group">
            <label for="barangay">City</label>            
                {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'City name','id'=>'cityname','required']) !!}                            
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary btn-sm" id="saveconcity" data-dismiss="modal"><i class="mdi mid-floppy"></i> Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

<script>

/** saving city in modal */
    $("#saveconcity").click( function() {        
        $.post( $("#concityform").attr("action"),
                $("#concityform :input").serialize())
          .done( function() {
            loadconcity();            
          });                            
    });

    $("#concityform").submit( function() {        
        //loadbarangay();
        return false;
    });

    function loadconcity()
    {        
        $.get('/city/list', function(data) {
            console.log(data);       
              $('#parish_city').empty();
              $('#parish_city').append('<option value="">Select City</option>');
              $('#confirmation_city').empty();            	
              $('#confirmation_city').append('<option value="">Select City</option>');
              $('#conbarangaycity_id').empty();
              $('#conbarangaycity_id').append('<option value="">Select City</option>');
            $.each(data, function(index,subCatObj){               
                $('#parish_city').append('<option value='+subCatObj.id+'>'+subCatObj.name+'</option>') ;
                $('#confirmation_city').append('<option value='+subCatObj.id+'>'+subCatObj.name+'</option>');
                $('#conbarangaycity_id').append('<option value='+subCatObj.id+'>'+subCatObj.name+'</option>');
            });
        });
    }
/** Saving city Modal in Place of birth */

// Place of Birth City Select
$('#confirmation_city').on('change', function(e){
    $('#confirmation_barangay').empty();    
    $('#confirmation_barangay').append('<option value="">Select Barangay</value>');

    
    
        console.log(e);
        var confirmation_city_id = e.target.value;
        
        //console.log(e.target.value);
        $.get('/barangays/list/' + confirmation_city_id , function(data) {
        //$.get(pobcityurl, function(data) {
            console.log(data);
                        	
            $.each(data, function(index,subCatObj){
                $('#confirmation_barangay').append('<option value='+subCatObj.id+'>'+subCatObj.name+'</option>');                
            });
        });
    });
</script>