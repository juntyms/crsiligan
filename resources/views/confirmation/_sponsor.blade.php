 
        
    <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">
    <i class="mdi mdi-account-multiple-plus"></i> Add Sponsor
    </button>

<table class="table">
    @foreach($sponsors as $sponsor)
    <tr>
        <td>{{ $sponsor->fname }} {{ $sponsor->mname }} {{ $sponsor->lname }}</td>
        <td><a href="{!! URL::route('confirmation.sponsor.delete',[$sponsor->id]) !!}" class="text-danger"><i class="mdi mdi-delete-forever"></i></a></td>
    </tr>
    @endforeach
</table>

