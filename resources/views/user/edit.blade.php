@extends('mainlayout')

@section('maincontent')
	
@include('partials._page_header',['icon'=>'mdi mdi-account-multiple-plus','title'=>'User Record','item1'=>'User','item2'=>'Edit Record']) 

<div class="card">
	<div class="card-header"> Edit User Record</div>
	<div class="card-body">
	{!! Form::model($user,['route'=>['user.update',$user->id],'class'=>'form-horizontal']) !!}
		@include('user._form',['submitText'=>'Update'])
	{!! Form::close() !!}
	</div>	
</div>	
@endsection