@extends('mainlayout')

@section('maincontent')

@include('partials._page_header',['icon'=>'mdi mdi-account-key','title'=>'Change Password','item1'=>'User','item2'=>'Change Password']) 
	
	<div class="card">
		<div class="card-header">Change Password</div>
	  <div class="card-body">
	  	<div class="row justify-content-md center">
		  	<div class="col-md-6">
	    	{!! Form::open(['route'=>'user.updatepassword']) !!}
				<div class="form-group">
					{!! Form::label('Current Password','Current Password') !!}					
					{!! Form::password('currentPassword',['class'=>'form-control','required']) !!}					
				</div>
				<div class="form-group">
					{!! Form::label('New Password','New Password') !!}					
					{!! Form::password('newPassword',['class'=>'form-control', 'required']) !!}					
				</div>
				<div class="form-group">
					{!! Form::label('Confirm New Password','Confirm New Password') !!}					
					{!! Form::password('confirmNewPassword',['class'=>'form-control', 'required']) !!}					
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-info btn-sm"><i class="mdi mdi-account-key" aria-hidden="true"></i> Change Password</button>
				</div>
			{!! Form::close() !!}
			</div>
		</div>
	  </div>
	</div>

@endsection