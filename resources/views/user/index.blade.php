@extends('mainlayout')

@section('maincontent')

@include('partials._page_header',['icon'=>'mdi mdi-account-multiple-plus','title'=>'User Record','item1'=>'User','item2'=>'Show All Record']) 

	<div class="card">
	  <div class="card-header">List of Users</div>
	  <div class="card-body">
	  <p>
		<a href="{!! URL::route('user.create') !!}" class="btn btn-success"><i class="mdi mdi-account-plus d-block mb-1" aria-hidden="true"></i> Create User</a>
		</p>

		<table class="table table-bordered table-hover">
		<thead>
			<tr class="table-primary">
				<th>Username</th>
				<th>First Name</th>
				<th>Middle Name</th>
				<th>Last Name</th>
				<th>Access Level</th>
				<th>Assigned Parish</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
		@foreach($users as $user) 
		<tr>
			<td>{!! $user->username !!}</td>
			<td>{!! $user->firstname !!}</td>
			<td>{!! $user->middlename !!}</td>
			<td>{!! $user->lastname !!}</td>
			<td>
				@if($user->isUser == 1)
					Normal User
				@endif
				@if ($user->isSecretary == 1)
					Parish Secretary
				@endif
				@if ($user->isPriest == 1)
					Priest
				@endif
				@if ($user->isFinance == 1)
					Finance
				@endif
				@if ($user->isAdmin == 1)
					Administrator
				@endif
			</td>
			<td>{!! $user->parish !!}</td>
			<td>
				<a href="{!! URL::route('user.edit',$user->id) !!}" class="btn btn-sm btn-info"><i class="mdi mdi-tooltip-edit" aria-hidden="true"></i> Edit</a>
				<a href="{!! URL::route('user.delete',$user->id) !!}" class="btn btn-sm btn-danger"><i class="mdi mdi-delete-variant" aria-hidden="true"></i> Delete</a>
			</td>
		</tr>
		@endforeach
		</tbody>
		</table>
	  </div>
	</div>

@endsection