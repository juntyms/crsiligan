@extends('layout')

@section('content')
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth">
        <div class="row w-100">
          <div class="col-lg-4 mx-auto">
            <div class="auth-form-light text-left p-5">
              <div class="brand-logo">
                <img src="{!! asset('purple/images/logo1.png') !!}">
              </div>              
              <h3>{{ config('app.name', 'Church Record System') }}</h3>
              @if($demo == true)
                <p class="text-center"><strong>This system expire {!! $daysLeft !!}</strong></p>
              @else
              <p class="text-center"><strong>Full Version</strong></p>
              @endif
                <p>Enter your username and password to log on:</p>
                @include('partials.errors')
              <h6 class="font-weight-light">Sign in to continue.</h6>              
              {!! Form::open(['route'=>'user.postlogin','class'=>'pt-3']) !!}
              {!! Form::hidden('expired',$expired) !!}
                <div class="form-group">
                  <input type="text" name="username" class="form-control form-control-lg" id="exampleInputEmail1" placeholder="Username">
                </div>
                <div class="form-group">
                  <input type="password" name="password" class="form-control form-control-lg" id="exampleInputPassword1" placeholder="Password">
                </div>
                <div class="mt-3">
                  <button class="btn btn-block btn-gradient-primary btn-lg font-weight-medium auth-form-btn">SIGN IN</button>
                </div>                                                
              {!! Form::close() !!}
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  @endsection
  
