@extends('mainlayout')

@section('maincontent')

@include('partials._page_header',['icon'=>'mdi mdi-account-multiple-plus','title'=>'User Record','item1'=>'User','item2'=>'Add New Record']) 

<div class="card">
	<div class="card-header"> Add New User Record</div>
	<div class="card-body">
		{!! Form::open(['route'=>'user.save','class'=>'form-horizontal']) !!}
			@include('user._form',['submitText'=>'Save'])
		{!! Form::close() !!}
	</div>
</div>
	

@endsection

