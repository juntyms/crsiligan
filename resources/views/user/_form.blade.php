		<div class="form-group">
			{!! Form::label('username','Username') !!}			
				{!! Form::text('username',null, ['class'=>'form-control','required']) !!}			
		</div>
		<div class="form-group">
			{!! Form::label('password','Password') !!}			
				{!! Form::password('password', ['class'=>'form-control','required']) !!}			
		</div>
		<div class="form-group">
			{!! Form::label('firstname','Firstname') !!}			
				{!! Form::text('firstname',null, ['class'=>'form-control','required']) !!}			
		</div>
		<div class="form-group">
			{!! Form::label('middlename','Middlename') !!}			
				{!! Form::text('middlename',null, ['class'=>'form-control','required']) !!}			
		</div>
		<div class="form-group">
			{!! Form::label('lastname','Lastname') !!}			
				{!! Form::text('lastname',null,['class'=>'form-control','required']) !!}			
		</div>
		<div class="form-group">
			{!! Form::label('User Access Level','User Access Level') !!}		
			<div class="form-check">
				<label for="Sec" class="form-check-label">
				{!! Form::checkbox('isSecretary', '1',null) !!} Secretary Level				
				</label>
			</div>	
			<div class="form-check">
				<label for="admin" class="form-check-label">{!! Form::checkbox('isAdmin', '1',null) !!} Administrator Level			</label>
			</div>
				
				
		</div>
		<div class="form-group">
			{!! Form::label('Parish','Parish') !!}			
				{!! Form::select('parishId',$parishes,null, ['class'=>'form-control','placeholder'=>'select Parish assignment . . . .']) !!}			
		</div>
		<div class="form-group">
		<button type="submit" class="btn btn-primary"><i class="mdi mdi-floppy" aria-hidden="true"></i> {!! $submitText !!}</button>
		</div>
		