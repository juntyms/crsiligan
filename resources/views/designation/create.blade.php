@extends('mainlayout')

@section('maincontent')
        
        @include('partials._page_header',['icon'=>'mdi mdi-account-card-details','title'=>'designation Record','item1'=>'designation','item2'=>'New']) 
        <div class="card">
            <div class="card-header">
                Create New designation
            </div>
            <div class="card-body">
                {!! Form::open(['route'=>'designation.save']) !!}
                    <div class="form-group">
                        <label for="designation">designation</label>
                        {!! Form::text('designation',null, ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        <button class="btn btn-sm btn-gradient-info"> Save</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
     

@endsection