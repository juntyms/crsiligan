@extends('mainlayout')

@section('maincontent')
        
        @include('partials._page_header',['icon'=>'mdi mdi-account-card-details','title'=>'designation Record','item1'=>'designation','item2'=>'Show All']) 
        <div class="card">
            <div class="card-header">
                Designations
            </div>
            <div class="card-body">
                <p>
                    <a href="{!! URL::route('designation.create') !!}" class="btn btn-gradient-primary"><i class="mdi mdi-book-plus d-block mb-1"></i> Add designation</a>
                </p>
                <table class="table table-bordered">
                    <thead>
                        <tr class="bg-success">
                            <th>SN</th>
                            <th>designation</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $sn=1; ?>
                        @foreach($designations as $designation)
                        <tr>
                            <td>{!! $sn++ !!}</td>
                            <td>{!! $designation->designation !!}</td>       
                            <td><a href="{!! URL::route('designation.edit', $designation->id) !!}">Edit</a></td>                    
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
     

@endsection