@extends('mainlayout')

@section('maincontent')
        
        @include('partials._page_header',['icon'=>'mdi mdi-baby','title'=>'Death Record','item1'=>'Death','item2'=>'Search']) 
        <div class="card">
            <div class="card-header">
                Death Records
            </div>
            <div class="card-body">
                {!! Form::open(['route'=>'death.postsearch','class'=>'form-inline']) !!}
                    <div class="form-check mx-sm-2">
                        <label for="" class="form-check-label">{!! Form::radio('search_type',1, true) !!} Firstname</label>
                    </div>
                    <div class="form-check mx-sm-2">
                        <label for="" class="form-check-label">{!! Form::radio('search_type',2, false) !!} Lastname</label>
                    </div>
                    <input type="text" name="searchtext" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="Type Search Here..." value="{{ $oldsearchtext }}">
                    <button type="submit" class="btn btn-gradient-success mb-2">Submit</button>
                {!! Form::close() !!}
                <table class="table table-bordered" id="deathTable">
                    <thead>
                        <tr class="bg-primary">
                            <th>SN</th>
                            <th>First Name</th>
                            <th>Middle Name</th>
                            <th>Last Name</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $sn=1; ?>
                        @foreach($deaths as $death)
                        <tr>
                            <td>{!! $sn++ !!}</td>
                            <td>{!! $death->fname !!}</td>
                            <td>{!! $death->mname !!}</td>
                            <td>{!! $death->lname !!}</td>
                            <td><a href="{!! URL::route('death.edit',$death->id) !!}"> Edit</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
     

@endsection




 
