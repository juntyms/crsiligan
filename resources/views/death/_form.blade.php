<div class="row">
  <div class="col-md-6"> <!-- beginning of left column-->                   
    @include('death.__person')
  </div><!-- end of left column-->
  <div class="col-md-6"><!-- beginning of right column-->
      	@include('death.__informant')
      	@include('death.__bookdetails')  
        <h5>Sacrament List</h5>
        <blockquote class="blockquote">
          @if ($isCreate == 0)
              @include('death.__sacraments')
          @endif                        
        </blockquote>
  </div> <!-- end of right column-->
</div> <!-- end of main row-->
