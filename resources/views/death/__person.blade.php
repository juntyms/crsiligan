<blockquote class="blockquote">
        <div class="form-group">
            <label for="fname">First Name <sup class="text-danger">*</sup></label>              
                  {!! Form::text('fname',null,['class'=>'form-control','placeholder'=>'Pangalan sa namatay','required']) !!}     
                  @if ($errors->has('fname'))<small class="text-danger">{{ $errors->first('fname') }}</small> @endif                                               
        </div>
        <div class="form-group">
            <label for="mname">Middle Name <sup class="text-danger">*</sup></label>            
                  {!! Form::text('mname',null, ['class'=>'form-control','placeholder'=>'Middle Name sa namatay','required']) !!} 
                  @if ($errors->has('mname'))<small class="text-danger">{{ $errors->first('mname') }}</small> @endif                             
        </div>
        <div class="form-group">
            <label for="lname">Last Name <sup class="text-danger">*</sup></label>            
                {!! Form::text('lname',null, ['class'=>'form-control', 'placeholder'=>'Apilyedo sa namatay','required']) !!}  
                @if ($errors->has('lname'))<small class="text-danger">{{ $errors->first('lname') }}</small> @endif                          
        </div>
        <div class="row">
            <div class="col-md-6">
            <div class="form-group">
                <label for="gender">Gender <sup class="text-danger">*</sup></label>    
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-check">
                            <label for="male" class="form-check-label">
                                {!! Form::radio('gender','M',['class'=>'form-check-input']) !!} Male
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-check">
                            <label for="Female" class="form-check-label">
                                {!! Form::radio('gender','F',['class'=>'form-check-input']) !!} Female  
                            </label>
                        </div>   
                    </div>
                    @if ($errors->has('gender'))<small class="text-danger">{{ $errors->first('gender') }}</small> @endif                       
                </div>        
            </div>
            </div>
            <div class="col-md-6">
            <div class="form-group">
                <label for="inputdob">Date of Birth <sup class="text-danger">*</sup></label>                         
                    {!! Form::date('dob',null,['id'=>'datepicker-dob','class'=>'form-control','placeholder'=>'Date of Birth','required']) !!}
                    @if ($errors->has('dob'))<small class="text-danger">{{ $errors->first('dob') }}</small> @endif            
            </div>
            </div>
        </div>                                   
        <div class="form-group">
            <div class="row">
                <div class="col-md-6">
                    <label for="dob">Date Died <sup class="text-danger">*</sup></label>                       
                    {!! Form::date('dateOfDeath',null,['id'=>'datepicker-ddied','placeholder'=>'Date Died', 'class'=>'form-control','required']) !!}
                    @if ($errors->has('dateOfDeath'))<small class="text-danger">{{ $errors->first('dateOfDeath') }}</small> @endif
                </div>
            <div class="col-md-6">
                <label for="dob">Age</label>            
                {!! Form::text('age',null,['id'=>'dage','class'=>'form-control','placeholder'=>'Age','readonly']) !!}            
                 <!--Age will display automatically after entering date of birth and date of confirmation -->            
                </div>
            </div>
        </div>
        <div class="form-group">
              <label for="lname">Cause of Death <sup class="text-danger">*</sup></label>              
                  {!! Form::text('causeOfDeath',null,['class'=>'form-control','placeholder'=>'Hinungdan sa pagkamatay','required']) !!}                  
                  @if ($errors->has('causeOfDeath'))<small class="text-danger">{{ $errors->first('causeOfDeath') }}</small> @endif              
        </div>
        <div class="form-group">
            <label for="cstatus">Civil Status <sup class="text-danger">*</sup></label>                                                       
                    {!! Form::select('civilstatus_id',$civilStatus, null, ['class'=>'form-control', 'placeholder'=>'Select Civil Status ....','required']) !!}
                    @if ($errors->has('civilstatus_id'))<small class="text-danger">{{ $errors->first('civilstatus_id') }}</small> @endif                
        </div>
        <div class="form-group">
            <label for="fa-fname">Residence <sup class="text-danger">*</sup></label>            
                {!! Form::text('street',null,['class'=>'form-control','placeholder'=>'street','required']) !!}
                @if ($errors->has('street'))<small class="text-danger">{{ $errors->first('street') }}</small> @endif
                <label for="marplace"><small><em>House No./Subdv/Street</em> </small></label>          
        </div>
        <div class="form-group">
            <label for="citydied">City <sup class="text-danger">*</sup></label>     
            <div class="input-group">
                {!! Form::select('city_id',$cities,null,['id'=>'dcity','class'=>'form-control','placeholder'=>'Select City']) !!}
                <div class="input-group-append">                    
                    <button type="button" class="btn btn-sm btn-gradient-success" id="reload_dcity"><i class="mdi mdi-cached"></i></button>
                    <a href="#" data-toggle="modal" data-target="#dcitymodal" class="btn btn-sm btn-primary"><i class="mdi mdi-library-plus" aria-hidden="true"></i> Add</a>                
                </div>
            </div>                     
              @if ($errors->has('city_id'))<small class="text-danger">{{ $errors->first('city_id') }}</small> @endif            
        </div>
        <div class="form-group">
            <label for="brgydied">Barangay <sup class="text-danger">*</sup></label>   
                <div class="input-group">
                {!! Form::select('barangay_id',$barangays,null,['id'=>'dbarangay','class'=>'form-control','placeholder'=>'Select Barangay','required']) !!}
                    <div class="input-group-append">
                        <button id="reload_dbarangay" class="btn btn-sm btn-success" type="button"><i class="mdi mdi-cached"></i></button>
                        <button id="dbarangaybtn"  class="btn btn-sm btn-primary"> <i class="mdi mdi-library-plus" aria-hidden="true"></i> Add </button>                            
                    </div> 
                </div>                                            
                @if ($errors->has('barangay_id'))<small class="text-danger">{{ $errors->first('barangay_id') }}</small> @endif            
          </div>
          <div class="form-group">
              <label for="conby">Place of Burial <sup class="text-danger">*</sup></label>              
                 <div class="input-group">
                 {!! Form::select('placeOfBurial',$cemeterys,null,['id'=>'optioncemetery','class'=>'form-control','placeholder'=>'Select Place of Burial']) !!}
                    <div class="input-group-append">
                        <a href="#" data-toggle="modal" data-target="#dcemeterymodal" class="btn btn-sm btn-primary"><i class="mdi mdi-library-plus" aria-hidden="true"></i> Add</a>                                        
                    </div>
                 </div>                  
                  @if ($errors->has('placeOfBurial'))<small class="text-danger">{{ $errors->first('placeOfBurial') }}</small> @endif                 
           </div>
           <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="inputdateissue">Date of Burial <sup class="text-danger">*</sup></label>                                      
                            {!! Form::date('dateOfBurial',null,['id'=>'datepicker-dburial','placeholder'=>'Date of Burial', 'class'=>'form-control','requred']) !!}
                            @if ($errors->has('dateOfBurial'))<small class="text-danger">{{ $errors->first('dateOfBurial') }}</small> @endif                  
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                        <label for="volume">Permit No.  <sup class="text-danger">*</sup> </label>                                    
                            {!! Form::text('burialPermitNo',null,['class'=>'form-control','placeholder'=>'Burial Permit No.']) !!}                
                </div>
            </div>
           </div>
           <div class="form-group">
                <label for="conby">Minister <sup class="text-danger">*</sup></label>   
                <div class="input-group">
                {!! Form::select('minister_id',$priests,null,['id'=>'dminister','class'=>'form-control','placeholder'=>'Ministro']) !!}                  
                <div class="input-group-append">                
                    <button id="dminister_reload" class="btn btn-sm btn-success" type="button"><i class="mdi mdi-cached"></i></button>
                    <a href="#" data-toggle="modal" data-target="#dministermodal" class="btn btn-sm btn-primary"><i class="mdi mdi-library-plus" aria-hidden="true"></i> Add</a>                                                
                </div>
                </div>             
                  @if ($errors->has('minister_id'))<small class="text-danger">{{ $errors->first('minister_id') }}</small> @endif                
          </div>  
</blockquote>
