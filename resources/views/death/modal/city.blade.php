<div class="modal fade" id="dcitymodal" tabindex="-1" role="dialog" aria-labelledby="dcityLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    {!! Form::open(['route'=>'city.save','id'=>'dcityform']) !!}            
      <div class="modal-header">
				<h4 class="modal-title" id="dcityLabel">Add New City</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>        
      </div>      
      <div class="modal-body">                
        <div class="form-group">
            <label for="barangay">City</label>            
                {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'City name','id'=>'cityname','required']) !!}                            
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary btn-sm" id="savedcity" data-dismiss="modal"><i class="mdi mid-floppy"></i> Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

<script>
$('#reload_dcity').on('click', function() {    
    loaddcity();
});
/** saving city in modal */
    $("#savedcity").click( function() {        
        $.post( $("#dcityform").attr("action"),
                $("#dcityform :input").serialize())
          .done( function() {
            loaddcity();
            //clearInput(); 
          });                            
    });

    $("#dcityform").submit( function() {        
        //loadbarangay();
        return false;
    });
/*
    function clearInput() {
        $("#dcityform :input").each( function() {
            $(this).val('');
        });        
    }
*/
    function loaddcity()
    {        
        $("#dbarangay").empty();

        $.get('/city/list', function(data) {
            console.log(data);        
            $('#dbarangaycity').empty();
            $('#dbarangaycity').append('<option value="">Select City</option>');      
            $('#dcity').empty();            	
            $('#dcity').append('<option value="">Select City</option>');
            $.each(data, function(index,subCatObj){        
                $('#dbarangaycity').append('<option value='+subCatObj.id+'>'+subCatObj.name+'</option>');        
                $('#dcity').append('<option value='+subCatObj.id+'>'+subCatObj.name+'</option>');
            });
        });
    }
/** Saving city Modal in Place of birth */

// Place of Birth City Select
    $('#dcity').on('click', function(e){
        console.log(e);
        var dcity_id = e.target.value;
        
        //console.log(e.target.value);
        $.get('/barangays/list/' + dcity_id , function(data) {
        //$.get(pobcityurl, function(data) {
            console.log(data);
            $('#dbarangay').empty();
            	$('#dbarangay').append('<option value="">Select Barangay</value>');
            $.each(data, function(index,subCatObj){
                $('#dbarangay').append('<option value='+subCatObj.id+'>'+subCatObj.name+'</option>');
            });
        });
    });

</script>