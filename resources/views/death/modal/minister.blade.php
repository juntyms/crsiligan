<div class="modal fade" id="dministermodal" tabindex="-1" role="dialog" aria-labelledby="dministerLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    {!! Form::open(['route'=>'priest.save','id'=>'dminister_form']) !!}            
      <div class="modal-header">
				<h4 class="modal-title">Add New Minister</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>        
      </div>
      
      <div class="modal-body">
      <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('title','Title') !!}	
                        {!! Form::select('title', array('BISHOP' => 'BISHOP', 
                                        'BRO' => 'BRO', 
                                        'MGSR.'=>'MGSR.',
                                        'REV. FR.'=>'REV. FR.', 
                                        'SR.'=>'SR.'),null,['class'=>'form-control']) !!}	
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    {!! Form::label('firstname','First Name') !!}	
                    {!! Form::text('firstname',null,['class'=>'form-control','placeholder'=>'First Name']) !!}				                    				                                     
                </div>
            </div>
    </div>
    
            
                <div class="form-group">
                    {!! Form::label('middlename','Middle Name') !!}			
                    {!! Form::text('middlename',null,['class'=>'form-control','placeholder'=>'Middle Name']) !!}				                    				                                     			
                </div>
            
                <div class="form-group">
                    {!! Form::label('lastname','Last Name') !!}
                    {!! Form::text('lastname',null,['class'=>'form-control','placeholder'=>'Last Name']) !!}				                    				                                     		
                </div>

        
        <div class="row">
            <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('affiliation','Affiliation') !!}
                    {!! Form::select('affiliation_id',$affiliations,null,['class'=>'form-control']) !!}	 	
            </div>
            </div>
            <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('designation','Designation') !!}
                    {!! Form::select('designation_id',$designations,null,['class'=>'form-control']) !!}	 	
            </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('licenseno','License No') !!}
                {!! Form::text('licenseno',null,['class'=>'form-control','placeholder'=>'License No']) !!}				                    				                                     	
            </div>
            </div>
            <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('expiry','Expiry') !!}
                {!! Form::date('expiry',null,['class'=>'form-control','placeholder'=>'Expiry']) !!}				                    				                                     	
            </div>
            </div>
        </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary btn-sm" id="save_dminister" data-dismiss="modal"><i class="mdi mid-floppy"></i> Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

<script>
$("#dminister_reload").click(function() {
    loaddminister();
});
/** Saving priest in Modal */
$("#save_dminister").click( function() {
        $.post( $("#dminister_form").attr("action"),
                $("#dminister_form :input").serialize())
          .done(function(){
            loaddminister();                    
          });
        
    });

    $("#dminister_form").submit( function() {        
        return false;
    });
    
    
    function loaddminister() {
        
        var urlgetdminister = "{{ URL::route('priest.load') }}";

        $.get(urlgetdminister, function(data) {
            console.log(data);
            $("#dminister").empty();
            $("#dminister").append('<option value="">Ministro</option>');
            $.each(data, function(index,subCatObj){ 
                $("#dminister").append('<option value='+subCatObj.id+'>'+subCatObj.name+'</option>');
            });
        });
    }
</script>