<div class="modal fade" id="dicitymodal" tabindex="-1" role="dialog" aria-labelledby="dicityLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    {!! Form::open(['route'=>'city.save','id'=>'dicityform']) !!}            
      <div class="modal-header">
				<h4 class="modal-title" id="dicityLabel">Add New City</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>        
      </div>      
      <div class="modal-body">                
        <div class="form-group">
            <label for="barangay">City</label>            
                {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'City name','id'=>'cityname','required']) !!}                            
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary btn-sm" id="savedicity" data-dismiss="modal"><i class="mdi mid-floppy"></i> Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

<script>
$('#reload_dicity').on('click', function() {    
    loaddicity();
});
/** saving city in modal */
    $("#savedicity").click( function() {        
        $.post( $("#dicityform").attr("action"),
                $("#dicityform :input").serialize())
          .done( function() {
            loaddicity();
            //clearInput(); 
          });                            
    });

    $("#dicityform").submit( function() {        
        //loadbarangay();
        return false;
    });
/*
    function clearInput() {
        $("#dicityform :input").each( function() {
            $(this).val('');
        });        
    }
*/
    function loaddicity()
    {                
        var urlgetdicity = "{{ URL::route('city.list') }}";

        $.get(urlgetdicity, function(data) {
            console.log(data);      
            $('#dibarangaycity').empty();
            $('#dibarangaycity').append('<option>Select City</option>');               
            $('#dicity').empty();            	
            $('#dicity').append('<option value="">Select City</option>');
            $.each(data, function(index,subCatObj){         
                $('#dibarangaycity').append('<option value='+subCatObj.id+'>'+subCatObj.name+'</option>');
                $('#dicity').append('<option value='+subCatObj.id+'>'+subCatObj.name+'</option>');
            });
        });
    }
/** Saving city Modal in Place of birth */

// Place of Birth City Select
    $('#dicity').on('click', function(e){
        console.log(e);
        var dicity_id = e.target.value;
        
        //console.log(e.target.value);
        $.get('/barangays/list/' + dicity_id , function(data) {
        //$.get(pobcityurl, function(data) {
            console.log(data);
            
            $('#dicitybarangay').empty();
            	$('#dicitybarangay').append('<option value="">Select Barangay</value>');
            $.each(data, function(index,subCatObj){
                $('#dicitybarangay').append('<option value='+subCatObj.id+'>'+subCatObj.name+'</option>');
            });
        });
    });

</script>