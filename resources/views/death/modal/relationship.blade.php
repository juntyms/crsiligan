<div class="modal fade" id="drelationshipmodal" tabindex="-1" role="dialog" aria-labelledby="drelationshipLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    {!! Form::open(['route'=>'relationship.save','id'=>'drelationshipform']) !!}            
      <div class="modal-header">
				<h4 class="modal-title" id="drelationshipLabel">Add New Relationship</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>        
      </div>      
      <div class="modal-body">                
        <div class="form-group">
            <label for="barangay">Relationship</label>            
                {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Informant Relation','id'=>'cemeteryname','required']) !!}                            
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary btn-sm" id="savedrelationship" data-dismiss="modal"><i class="mdi mid-floppy"></i> Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

<script>
$("#savedrelationship").click( function() {        
        $.post( $("#drelationshipform").attr("action"),
                $("#drelationshipform :input").serialize())
          .done( function() {
            loaddrelationship();
            //clearInput(); 
          });                            
    });

    $("#drelationshipform").submit( function() {        
        //loadbarangay();
        return false;
    });

    function loaddrelationship()
    {                
        var urlgetrelationship = "{{ URL::route('relationship.lists') }}";

        $.get(urlgetrelationship, function(data) {
            console.log(data);        
            $('#irelationship').empty();
            $('#irelationship').append('<option value="">Select Informant Relation</option>');                  
            $.each(data, function(index,subCatObj){        
                $('#irelationship').append('<option value='+subCatObj.id+'>'+subCatObj.name+'</option>');                        
            });
        });
    }
</script>