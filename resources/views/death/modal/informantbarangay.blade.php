<div class="modal fade" id="dibarangaymodal" tabindex="-1" role="dialog" aria-labelledby="dibarangayLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    {!! Form::open(['route'=>'barangay.save','id'=>'dibarangayform']) !!}            
      <div class="modal-header">
				<h4 class="modal-title" id="dibarangayLabel">Add New Barangay</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>        
      </div>
      
      <div class="modal-body">
        
        <div class="form-group">      
          <label for="city">City</label>                       
                {!! Form::select('city_id',$cities,null,['id'=>'dibarangaycity','class'=>'form-control']) !!}            
        </div>

        <div class="form-group">
            <label for="barangay">Barangay</label>            
                {!! Form::text('name',null,['id'=>'dibarangayname','class'=>'form-control','placeholder'=>'Barangay name','required']) !!}                
            
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary btn-sm" id="ibarsave" data-dismiss="modal"><i class="mdi mid-floppy"></i> Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

<script>
$("#reload_dibarangaycity").click( function() {
  var cityid = $("#dicity").val();
  if ( cityid != "") {
    loaddibarangay();
  } else {
    alert("City is Empty");
  }
});

$("#dibarangaybtn").click(function(){
  
  var dicityval = $("#dicity").val();
  if (dicityval !== "") {
    $("#dibarangaymodal").modal("show");
    $("#dibarangaycity").val(dicityval);
  } else {
    alert("City is Empty");
  }
});

/** Save button clicked */
$("#ibarsave").click(function() {
  console.log("save place bar");
  $.post( $("#dibarangayform").attr("action"),
            $("#dibarangayform :input").serialize())
      .done(function() {
        loaddibarangay();       
      });
});

$("#dibarangayform").submit( function() {
  return false;
});

function loaddibarangay()
{
  var cityid = $("#dicity").val();
  $.get('/barangays/list/' + cityid, function(data) {
    console.log(data);
    $('#dicitybarangay').empty();
    $('#dicitybarangay').append('<option value="">Select Barangay</value>');
    $.each(data, function(index, subCatObj) {
      $('#dicitybarangay').append('<option value='+subCatObj.id+'>'+subCatObj.name+'</option>');
    });
  });
}

</script>