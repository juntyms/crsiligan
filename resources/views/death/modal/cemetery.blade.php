<div class="modal fade" id="dcemeterymodal" tabindex="-1" role="dialog" aria-labelledby="dcemeteryLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    {!! Form::open(['route'=>'cemetery.save','id'=>'dcemeteryform']) !!}            
      <div class="modal-header">
				<h4 class="modal-title" id="dcemeteryLabel">Add New Cemetery</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>        
      </div>      
      <div class="modal-body">                
        <div class="form-group">
            <label for="barangay">Cemetery</label>            
                {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Place of Burial','id'=>'cemeteryname','required']) !!}                            
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary btn-sm" id="savedcemetery" data-dismiss="modal"><i class="mdi mid-floppy"></i> Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

<script>
$("#savedcemetery").click( function() {        
        $.post( $("#dcemeteryform").attr("action"),
                $("#dcemeteryform :input").serialize())
          .done( function() {
            loaddcemetery();
            //clearInput(); 
          });                            
    });

    $("#dcemeteryform").submit( function() {        
        //loadbarangay();
        return false;
    });
/*
    function clearInput() {
        $("#dcityform :input").each( function() {
            $(this).val('');
        });        
    }
*/
    function loaddcemetery()
    {                
        $.get('/cemetery/lists', function(data) {
            console.log(data);        
            $('#optioncemetery').empty();
            $('#optioncemetery').append('<option value="">Select Place of Burial</option>');                  
            $.each(data, function(index,subCatObj){        
                $('#optioncemetery').append('<option value='+subCatObj.id+'>'+subCatObj.name+'</option>');                        
            });
        });
    }
</script>