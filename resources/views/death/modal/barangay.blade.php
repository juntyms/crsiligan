<div class="modal fade" id="dbarangaymodal" tabindex="-1" role="dialog" aria-labelledby="dbarangayLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    {!! Form::open(['route'=>'barangay.save','id'=>'dbarangayform']) !!}            
      <div class="modal-header">
				<h4 class="modal-title" id="dbarangayLabel">Add New Barangay</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>        
      </div>
      
      <div class="modal-body">
        
        <div class="form-group">      
          <label for="city">City</label>                       
                {!! Form::select('city_id',$cities,null,['id'=>'dbarangaycity','class'=>'form-control']) !!}            
        </div>

        <div class="form-group">
            <label for="barangay">Barangay</label>            
                {!! Form::text('name',null,['id'=>'dbarangayname','class'=>'form-control','placeholder'=>'Barangay name','required']) !!}                
            
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary btn-sm" id="placesavebar" data-dismiss="modal"><i class="mdi mid-floppy"></i> Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

<script>
$("#reload_dbarangay").click( function() {
  var cityid = $("#dcity").val();
  if ( cityid != "") {
    loaddbarangay();
  } else {
    alert("City is Empty");
  }
});

$("#dbarangaybtn").click(function(){
  
  var dcityval = $("#dcity").val();
  if (dcityval !== "") {
    $("#dbarangaymodal").modal("show");
    $("#dbarangaycity").val(dcityval);
  } else {
    alert("City is Empty");
  }
});
/** Save button clicked */
$("#placesavebar").click(function() {
  console.log("save place bar");
  $.post( $("#dbarangayform").attr("action"),
            $("#dbarangayform :input").serialize())
      .done(function() {
        loaddbarangay();       
      });
});

$("#dbarangayform").submit( function() {
  return false;
});

function loaddbarangay()
{
  var cityid = $("#dcity").val();
  $.get('/barangays/list/' + cityid, function(data) {
    console.log(data);
    $('#dbarangay').empty();
    $('#dbarangay').append('<option value="">Select Barangay</value>');
    $.each(data, function(index, subCatObj) {
      $('#dbarangay').append('<option value='+subCatObj.id+'>'+subCatObj.name+'</option>');
    });
  });
}

</script>