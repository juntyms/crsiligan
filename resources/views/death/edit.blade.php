@extends('mainlayout')

@section('maincontent')

@include('partials._page_header',['icon'=>'mdi mdi-church','title'=>'Death / Burial Record','item1'=>'Burial','item2'=>'Edit Record'])


  <div class="card">
    <div class="card-header">Burial Form</div>
      <div class="card-body">
        
        {!! Form::model($death,['route'=>['death.update',$death->id],'class'=>'form-horizontal']) !!}
          @include('death._form',['isCreate'=>'0'])
          <div class="pull-left">
                <button type="submit" class="btn btn-gradient-info" name="update" value="1"><i class="mdi mdi-floppy" aria-hidden="true"></i> Update</button>
                <button type="submit" class="btn btn-gradient-primary" name="update" value="0"><i class="mdi mdi-printer" aria-hidden="true"></i> Print</button>
           </div>
        {!! Form::close() !!}
        
            
      </div> <!-- end of main panel body-->
  </div> <!-- main panel-->
  @include('death.__sacramentForm')


@endsection

@section('script')

  @include('death._script')
  
@endsection