@extends('mainlayout')

@section('maincontent')

@include('partials._page_header',['icon'=>'mdi mdi-church','title'=>'Death / Burial Record','item1'=>'Death / Burial','item2'=>'Show All Record']) 
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">List of Deaths</h3>
      </div>
      <div class="card-body">
        <p>
            <a href="{!! URL::route('death.excel') !!}" class="btn btn-success"> Export To Excel</a>
        </p>
        <table class="table table-bordered" id="deathTable">
            <thead>
                <tr class="bg-info">
                    <th>SN</th>
                    <th>First Name</th>
                    <th>Middle Name</th>
                    <th>Last Name</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            @php 
                $sn=1;
            @endphp
                @foreach($deaths as $death)
                    <tr>
                        <td>{!! $sn++ !!}</td>
                        <td>{!! $death->fname !!}</td>
                        <td>{!! $death->mname !!}</td>
                        <td>{!! $death->lname !!}</td>
                        <td><a href="{!! URL::route('death.edit',$death->id) !!}"> Edit</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
      </div>
    </div>
    

@endsection

@section('css')
<link rel="stylesheet" type="text/css" href="{!! asset('DataTables/datatables.min.css') !!}"/>
@endsection

@section('script')
    <script type="text/javascript" src="{!! asset('DataTables/datatables.min.js') !!}"></script>
    <script>
    $(document).ready( function () {
        $('#deathTable').DataTable();
    });
    </script>
@endsection