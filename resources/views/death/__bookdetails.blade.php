<blockquote class="blockquote">
  <div class="row">
    <div class="col-md-4">
      <div class="form-group">
        <label for="book">Volume</label>
            {!! Form::number('volume',null,['class'=>'form-control','placeholder'=>'Enter Volume No.']) !!}       
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label for="book">Book</label>
            {!! Form::number('book',null,['class'=>'form-control','placeholder'=>'Enter Book No.']) !!}       
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label for="page">Page</label>
          {!! Form::number('page',null, ['class'=>'form-control','placeholder'=>'Enter Page No.']) !!}       
      </div>
    </div>
  </div>  
  <div class="form-group">
    <label for="purpose">Purpose</label>
      {!! Form::text('purpose',null, ['class'=>'form-control','placeholder'=>'Katuyuan']) !!}      
  </div>
  <div class="form-group">
    <label for="annotation">Annotation</label>
      {!! Form::textarea('annotation', null, ['class'=>'form-control','placeholder'=>'Dugang Katuyuan','rows'=>'2']) !!}      
  </div>
  <div class="form-group">
    <label for="remarks">Remarks</label>
      {!! Form::textarea('remarks', null, ['class'=>'form-control','placeholder'=>'Dugang Katuyuan','rows'=>'2']) !!}      
  </div>
  <div class="form-group">
      <label for="conby">Parish Priest</label>    
      <div class="input-group">      
          {!! Form::select('currentPriest_id',$priests, $currentPriest->id, ['id'=>'dpriest','class'=>'form-control']) !!}                 
        <div class="input-group-append">
          <button id="dpriest_reload" class="btn btn-sm btn-success" type="button"><i class="mdi mdi-cached"></i></button>
          <a href="#" data-toggle="modal" data-target="#dpriestmodal" class="btn btn-sm btn-primary"><i class="mdi mdi-library-plus" aria-hidden="true"></i> Add</a>                                                        
        </div>
      </div>
  </div> 
  <div class="form-group">
      <label for="designation">Designation</label>      
          {!! Form::select('currentPriest_designationId',$designations, $currentPriest->designation_id, ['class'=>'form-control']) !!}               
  </div>
  <div class="form-group">
      <label for="inputdateissue">Date Issue</label>    
        {!! Form::date('dateIssue',null,['class'=>'form-control','placeholder'=>'Date Issue','id'=>'datepicker-dateissue']) !!}            
  </div>
  <div class="form-group">
    <div class="form-check form-check-flat form-check-primary">
    <label for="isSecretary" class="form-check-label">Show Release by									
      {!! Form::checkbox('isSecretary', 1 , false, ['class'=>'form-check-input']) !!}													
    </label>
    </div>
  </div>				
</blockquote>