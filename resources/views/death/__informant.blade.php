<h5>Informant</h5>
<blockquote class="blockquote">
      <div class="form-group">
          <label for="hadvfname">First Name <sup class="text-danger">*</sup></label>
              {!! Form::text('informantFname',null,['class'=>'form-control','placeholder'=>'Pangalan sa impormante','required']) !!}
              @if ($errors->has('informantFname'))<small class="text-danger">{{ $errors->first('informantFname') }}</small> @endif          
      </div>
      <div class="form-group">
          <label for="hadvmidname">Middle Name <sup class="text-danger">*</sup></label>          
              {!! Form::text('informantMname',null,['class'=>'form-control','placeholder'=>'Middle Name sa impormante','required']) !!}                
              @if ($errors->has('informantMname'))<small class="text-danger">{{ $errors->first('informantMname') }}</small> @endif          
      </div>
      <div class="form-group">
          <label for="hadvlname">Last Name <sup class="text-danger">*</sup></label>          
            {!! Form::text('informantLname',null,['class'=>'form-control','placeholder'=>'Apelyido sa impormante','required']) !!}  
            @if ($errors->has('informantLname'))<small class="text-danger">{{ $errors->first('informantLname') }}</small> @endif          
      </div>
      <div class="form-group">
          <label for="cstatus">Relation <sup class="text-danger">*</sup></label>          
            <div class="input-group">
            {!! Form::select('informantRelationship_id',$relationships,null,['id'=>'irelationship','class'=>'form-control','placeholder'=>'Select Informant Relation','required']) !!}
              <div class="input-group-append">
              <a href="#" data-toggle="modal" data-target="#drelationshipmodal" class="btn btn-sm btn-primary"><i class="mdi mdi-library-plus" aria-hidden="true"></i> Add</a>              
              </div>
            </div>            
            @if ($errors->has('informantRelationship_id'))<small class="text-danger">{{ $errors->first('informantRelationship_id') }}</small> @endif          
      </div> 
      <div class="form-group">
          <label for="fa-fname">Residence</label>          
              {!! Form::text('informantAddress',null,['class'=>'form-control', 'placeholder'=>'informant Residence Address']) !!}
              <label for="marplace"><small><em>House No./Subdv/Street</em></small></label>          
      </div>
      <div class="form-group">
        <label for="citydied">City <sup class="text-danger">*</sup></label>
        <div class="input-group">
        {!! Form::select('informantCity_id',$cities,null,['id'=>'dicity','class'=>'form-control','placeholder'=>'Informant City Address','required']) !!}          
          <div class="input-group-append">          
            <button id="reload_dicity" class="btn btn-sm btn-success" type="button"><i class="mdi mdi-cached"></i></button>
            <a href="#" data-toggle="modal" data-target="#dicitymodal" class="btn btn-sm btn-primary"><i class="mdi mdi-library-plus" aria-hidden="true"></i> Add</a>              
          </div>
        </div>        
          @if ($errors->has('informantCity_id'))<small class="text-danger">{{ $errors->first('informantCity_id') }}</small> @endif        
      </div>      
      <div class="form-group">
          <label for="brgydied">Barangay <sup class="text-danger">*</sup></label>       
          <div class="input-group">
          {!! Form::select('informantBarangay_id',$ibarangays,null,['id'=>'dicitybarangay','class'=>'form-control','placeholder'=>'Informant Barangay Address','required']) !!}             
            <div class="input-group-append">
             <button id="reload_dibarangaycity" class="btn btn-sm btn-success" type="button"><i class="mdi mdi-cached"></i></button>
              <button id="dibarangaybtn" class="btn btn-sm btn-primary"><i class="mdi mdi-library-plus" aria-hidden="true"></i> Add</button>            
            </div>
          </div>                         
            @if ($errors->has('informantBarangay_id'))<small class="text-danger">{{ $errors->first('informantBarangay_id') }}</small> @endif          
      </div>      
</blockquote>
  