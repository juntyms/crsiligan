@extends('mainlayout')

@section('maincontent')

@include('partials._page_header',['icon'=>'mdi mdi-church','title'=>'Death / Burial Record','item1'=>'Burial','item2'=>'New Record'])

  <div class="card">
    <div class="card-header">Burial Form</div>
      <div class="card-body">
        
        {!! Form::open(['route'=>'death.save','class'=>'form-horizontal']) !!}
          @include('death._form',['isCreate'=>'1'])
          <div class="pull-left">
                <button type="submit" class="btn btn-gradient-info"><i class="mdi mdi-floppy" aria-hidden="true"></i> Save</button>
                <button type="button" class="btn btn-gradient-primary" disabled><i class="mdi mdi-printer" aria-hidden="true"></i> Print</button>
           </div>
        {!! Form::close() !!}
        
            
      </div> <!-- end of main panel body-->
  </div> <!-- main panel-->


@endsection

@section('script')

  @include('death._script')
  
@endsection