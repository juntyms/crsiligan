    <!-- Button trigger modal -->
<p>
<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">
    <i class="fa fa-plus-circle"></i> Add Sacrament
</button>
</p>
<table class="table">
    @foreach($sacramentlists as $sacramentlist)
    <tr>
        <td>{!! $sacramentlist->sacrament !!}</td>
        <td><a href="{!! URL::route('death.sacrament.delete',[$sacramentlist->death_id,$sacramentlist->sacrament_id]) !!}" class="text-danger"><i class="mdi mdi-delete-forever"></i></td>
    </tr>
    @endforeach
</table>
