<table border="100%">
    <tr>
        <th>SN</th>
        <th>First Name</th>
        <th>Middle Name</th>
        <th>Last Name</th>
        <th>Age</th>
        <th>Date of Birth</th>
        <th>Gender</th>
        <th>civilstatus</th>
        <th>Address</th>
        <th>Minister</th>
        <th>Place of Burial</th>
        <th>Cause of Death</th>
        <th>Date of Death</th>
        <th>Date of Burial</th>
        <th>Volume</th>
        <th>Page</th>
        <th>Book</th>
        <th>Permit No</th>
        <th>Name of Informant</th>
        <th>Informant Relationship</th>
        <th>Informant Address</th>
        <th>Date Issue</th>
        <th>Purpose</th>
        <th>Annotation</th>
    </tr>
    @php
        $sn = 1;
    @endphp
    @foreach($deaths as $death)
        <tr>
            <td>{!! $sn++ !!}</td>
            <td>{!! $death->fname !!}</td>
            <td>{!! $death->mname !!}</td>
            <td>{!! $death->lname !!}</td>
            <td>{!! $death->age !!}</td>
            <td>{!! $death->dob !!}</td>
            <td>{!! $death->gender !!}</td>
            <td>{!! $death->civilstatus->status !!}</td>            
            <td>{!! $death->street !!} , {!! $death->barangay->name !!}, {!! $death->city->name !!}</td>
            <td>{!! $death->minister->title !!} {!! $death->minister->firstname !!} {!! $death->minister->middlename !!} {!! $death->minister->lastname !!}</td>
            <td>{!! $death->cemetery->name !!}</td>
            <td>{!! $death->causeOfDeath !!}</td>
            <td>{!! $death->dateOfBurial !!}</td>            
            <td>{!! $death->dateOfDeath !!}</td>
            <td>{!! $death->volume !!}</td>
            <td>{!! $death->page !!}</td>
            <td>{!! $death->book !!}</td>
            <td>{!! $death->burialPermitNo !!}</td>
            <td>{!! $death->informantFname !!} {!! $death->informantLname !!}</td>
            <td>{!! $death->informantRelationship->name !!}</td>
            <td>{!! $death->informantAddress !!} , {!! $death->informantBarangay->name !!} , {!! $death->informantCity->name !!}</td>
            <td>{!! $death->dateIssue !!}</td>
            <td>{!! $death->purpose !!}</td>
            <td>{!! $death->annotation !!}</td>
        </tr>
    @endforeach
</table>