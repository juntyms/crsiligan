<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    {!! Form::open(['route'=>['death.sacrament.save',$death->id],'class'=>'form-horizontal']) !!}
      <div class="modal-header">
      <h4 class="modal-title" id="myModalLabel">Add Sacrament</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
      </div>

      <div class="modal-body">
        <div class="form-group">
          {!! Form::label('sacraments','Sacraments',['class'=>'control-label']) !!}
          
            {!! Form::select('sacrament_id',$sacraments,null,['class'=>'form-control']) !!}
          
        </div>
      </div>
      <div class="modal-footer">        
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
    </div>
    {!! Form::close() !!}
  </div>
</div>
