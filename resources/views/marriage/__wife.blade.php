<div class="col-md-6">
    <!-- beginning of right column-->
    <div class="card mb-3">
        <div class="card-body">
            <h4 class="card-title">Wife</h4>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="wfname">First Name</label>

                        {!! Form::text('wfname',null, ['class'=>'form-control','placeholder'=>'Wife First Name']) !!}

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="wmidname">Middle Name</label>

                        {!! Form::text('wmname',null, ['class'=>'form-control','placeholder'=>'Wife Middle Name']) !!}

                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="wlname">Last Name</label>

                {!! Form::text('wlname', null, ['class'=>'form-control','placeholder'=>'Wife Last Name']) !!}

            </div>
            <div class="form-group">
                <label for="wciti">Citizenship</label>


                {!! Form::select('wnationality_id', $nationalities, null, ['class'=>'form-control','placeholder'=>'Select Citizenship']) !!}

            </div>

            <div class="row">
                <div class="col-sm-5">
                    <div class="form-group">
                        <label for="wcstatus">Civil Status</label>

                        {!! Form::select('wcstatus_id', $status, null, ['class'=>'form-control','placeholder'=>'Select Civil Status']) !!}

                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="wdob">Date of Birth</label>
                        {!! Form::date('wdob', null, ['id'=>'datepicker-wdob','class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="wage"> Age</label>
                        {!! Form::number('wage', null, ['class'=>'form-control','placeholder'=>'Age']) !!}
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="wpob">Place of Birth</label>

                {!! Form::text('wpob', null , ['class'=>'form-control','placeholder'=>'Place of Birth']) !!}

            </div>
            <div class="form-group">
                <label for="wres">Residence</label>

                {!! Form::text('wresidence',null, ['class'=>'form-control','placeholder'=>'Residence']) !!}

            </div>
            <blockquote class="blockquote blockquote-primary">
                <h4>Father Information - WIFE</h4>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="wffname">First Name</label>

                            {!! Form::text('wffname',null, ['class'=>'form-control','placeholder'=>"Wife's Father First Name"]) !!}

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="wfmidname">Middle Name</label>

                            {!! Form::text('wfmname', null, ['class'=>'form-control', 'placeholder'=>"Wife's Father Middle Name"]) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="wflname">Last Name</label>

                    {!! Form::text('wflname', null, ['class'=>'form-control', 'placeholder'=>"Wife's Father Last Name"]) !!}

                </div>
                <div class="form-group">
                    <label for="wfaciti">Citizenship</label>


                    {!! Form::select('wfcitizenship',$nationalities, null,['class'=>'form-control','placeholder'=>'Select Citizenship']) !!}


                </div>
            </blockquote>

            <blockquote class="blockquote blockquote-primary">
                <h4>Mother's Information - WIFE</h4>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="wmfname">First Name</label>
                            {!! Form::text('wmfname',null, ['class'=>'form-control','placeholder'=>"Wife's Mother First Name"]) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="wmmidname">Middle Name</label>
                            {!! Form::text('wmmname',null, ['class'=>'form-control','placeholder'=>"Wife's Mother Middle Name"]) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="wmlname">Last Name</label>

                    {!! Form::text('wmlname',null, ['class'=>'form-control','placeholder'=>"Wife's Mother Last Name"]) !!}

                </div>
                <div class="form-group">
                    <label for="wmociti">Citizenship</label>

                    {!! Form::select('wmcitizenship', $nationalities, null, ['class'=>'form-control','placeholder'=>'Select Citizenship']) !!}

                </div>
            </blockquote>

            <blockquote class="blockquote blockquote-primary">
                <h4>Person who gave consent or advice</h4>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="wadvfname">First Name</label>
                            {!! Form::text('wadvicefname',null,['class'=>'form-control','placeholder'=>'First Name']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="wadvmidname">Middle Name</label>
                            {!! Form::text('wadvicemname', null, ['class'=>'form-control','placeholder'=>'Middle Name']) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="wadvlname">Last Name</label>

                    {!! Form::text('wadvicelname', null, ['class'=>'form-control','placeholder'=>'Last Name']) !!}

                </div>
                <div class="form-group">
                    <label for="wadvrel">Relation</label>

                    {!! Form::text('wadvicerelation', null, ['class'=>'form-control','placeholder'=>'Relation'])!!}

                </div>
                <div class="form-group">
                    <label for="wadvres">Residence</label>

                    {!! Form::text('wadviceresidence', null, ['class'=>'form-control','placeholder'=>'Residence']) !!}

                </div>
            </blockquote>
        </div>
    </div>

</div><!-- right col -->
