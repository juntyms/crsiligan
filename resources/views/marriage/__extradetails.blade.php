<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="license">Marriage License</label>
                    {!! Form::text('license',null,['class'=>'form-control','placeholder'=>"License No. / Expiry Date /
                    Address"]) !!}
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-6">
                            {!! Form::text('placeofmar',null, ['class'=>'form-control','placeholder'=>'Place of Marriage']) !!}
                            <label for="marplace"><small><em> Place of Marriage</em></small></label>
                        </div>
                        <div class="col-sm-6">
                            {!! Form::text('addressofmar',null, ['class'=>'form-control','placeholder'=>'Address of Marriage']) !!}
                            <label for="hadvres"><small><em> City / Municipality</em></small></label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            {!! Form::date('dateofmar',null,['class'=>'form-control','id'=>'datepicker-marplace']) !!}
                            <label for="dateOfMarriage"><small> Date of Marriage</small></label>
                        </div>
                        <div class="col-md-6">
                            {!! Form::text('timeofmar',null, ['class'=>'form-control', 'placeholder'=>"Date/Time of Marriage"]) !!}
                            <label for="timeOfMarriage"><small> Time of Marriage</small></label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="officiating" class="control-label"> Officiating Priest</label>

                    {!! Form::text('officiating',null,['class'=>'form-control','placeholder'=>'Name of Officiating Priest'])
                    !!}
                </div>
            </div>
            <div class="col-md-6">
                <blockquote class="blockquote blockquote-primary">
                    <div class="form-group">
                        <label for="civilofficiating">Civil Solemnizing Officer</label>

                        {!! Form::text('civilofficiating',null, ['class'=>'form-control','placeholder'=>'Civil Solemnizing Officer']) !!}

                    </div>
                    <div class="form-group">
                        <label for="civildate">Civil Date of Marriage</label>
                        <div class="row">

                            <div class="col-md-4">

                                {!! Form::date('civildate',null,['id'=>'datepicker-civildate','class'=>'form-control']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="civiladdress">Civil Place of Marriage</label>

                        {!! Form::text('civiladdress',null, ['class'=>'form-control','placeholder'=>'Civil Place of Marriage']) !!}

                    </div>
                </blockquote>
            </div>
        </div>
    </div>
</div>
