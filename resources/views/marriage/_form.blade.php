<div class="row">
    @include('marriage.__husband')
    @include('marriage.__wife')
</div> <!-- end of main row -->

@include('marriage.__extradetails')
