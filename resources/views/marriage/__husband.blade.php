<div class="col-md-6">
    <!-- beginning of left column-->
    <div class="card mb-3">
        <div class="card-body">
            <h4 class="card-title">Husband</h4>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="hfname">First Name</label>

                        {!! Form::text('hfname',null, ['class'=>'form-control','placeholder'=>'Husband Firstname']) !!}

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="hmidname">Middle Name</label>

                        {!! Form::text('hmname', null, ['class'=>'form-control','placeholder'=>'Husband Middle Name']) !!}

                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="hlname">Last Name</label>

                {!! Form::text('hlname', null, ['class'=>'form-control', 'placeholder'=>'Husband Lastname']) !!}

            </div>
            <div class="form-group">
                <label for="hciti">Citizenship</label>
                {!! Form::select('hnationality_id', $nationalities,null,['class'=>'form-control','placeholder'=>'Select Citizenship']) !!}

            </div>

            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="hcstatus">Civil Status</label>

                        {!! Form::select('hcstatus_id', $status, null, ['class'=>'form-control','placeholder'=>'Select Civil Status']) !!}

                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="inputdob">Date of Birth</label>

                        {!! Form::date('hdob',null, ['id'=>'datepicker-hdob','class'=>'form-control']) !!}

                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="inputdob">Age</label>

                        {!! Form::number('hage',null, ['class'=>'form-control', 'placeholder'=>'Age']) !!}

                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="hpob">Place of Birth</label>

                {!! Form::text('hpob',null, ['class'=>'form-control','placeholder'=>'Husband Place of Birth']) !!}

            </div>

            <div class="form-group">
                <label for="hres">Residence</label>

                {!! Form::text('hresidence',null, ['class'=>'form-control','placeholder'=>'Husband Residence']) !!}

            </div>

            <blockquote class="blockquote blockquote-primary">
                <h4>Father Information - HUSBAND</h4>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="hffname">First Name</label>

                            {!! Form::text('hffname', null, ['class'=>'form-control','placeholder'=>"Husband's Father First Name"]) !!}

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="hfmidname">Middle Name</label>

                            {!! Form::text('hfmname',null, ['class'=>'form-control','placeholder'=>"Husband's Father Middle Name"]) !!}

                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="hflname">Last Name</label>

                    {!! Form::text('hflname', null, ['class'=>'form-control','placeholder'=>"Husband's Father Last Name"]) !!}

                </div>
                <div class="form-group">
                    <label for="hfciti">Citizenship</label>


                    {!! Form::select('hfcitizenship', $nationalities , null,['class'=>'form-control','placeholder'=>'Select Citizenship']) !!}

                </div>
            </blockquote>

            <blockquote class="blockquote blockquote-primary">
                <h4>Mother's Information - HUSBAND</h4>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="hmfname">First Name</label>

                            {!! Form::text('hmfname',null, ['class'=>'form-control','placeholder'=>"Husband's Mother
                            First Name"]) !!}

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="hmmidname">Middle Name</label>

                            {!! Form::text('hmmname', null, ['class'=>'form-control','placeholder'=>"Husband's Mother Maiden Name"]) !!}

                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="hmlname">Last Name</label>

                    {!! Form::text('hmlname', null, ['class'=>'form-control','placeholder'=>"Husband's Mother Last Name"]) !!}

                </div>
                <div class="form-group">
                    <label for="hmciti">Citizenship</label>


                    {!! Form::select('hmcitizenship', $nationalities, null,['class'=>'form-control','placeholder'=>'Select Citizenship']) !!}

                </div>
            </blockquote>

            <blockquote class="blockquote blockquote-primary">
                <h4>Person who gave consent or advice</h4>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="hadvfname">First Name</label>

                            {!! Form::text('hadvicefname',null, ['class'=>'form-control','placeholder'=>'Firstname'])
                            !!}

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="hadvmidname">Middle Name</label>

                            {!! Form::text('hadvicemname',null, ['class'=>'form-control','placeholder'=>'Middlename'])
                            !!}

                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="hadvlname">Last Name</label>

                    {!! Form::text('hadvicelname', null, ['class'=>'form-control','placeholder'=>'Lastname'])
                    !!}

                </div>
                <div class="form-group">
                    <label for="hadvrel">Relation</label>

                    {!! Form::text('hadvicerelation',null, ['class'=>'form-control','placeholder'=>'Relation to Husband']) !!}

                </div>
                <div class="form-group">
                    <label for="hadvres">Residence</label>

                    {!! Form::text('hadviceresidence',null, ['class'=>'form-control',
                    'placeholder'=>'Residence']) !!}

                </div>
            </blockquote>
        </div>

    </div><!-- left of col-->
</div>
