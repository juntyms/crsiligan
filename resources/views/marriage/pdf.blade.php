<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15">
    <style>
        table {
            font-family: "Arial", Gadget, sans-serif;
        }

        td.title {
            font-family: "Arial Black", Gadget, sans-serif;
            font-weight: bold;
            font-size: 24px;
            letter-spacing: 3px;
        }

        td.titleside {
            font-family: "Arial", Gadget, sans-serif;
            font-size: 12px;
        }

        td.civil {
            font-family: "Arial", Gadget, sans-serif;
            font-size: 12px;
        }

        .header {
            padding-top: 100px;
        }

        p.notes {
            font-family: Times, serif;
            font-size: 13px;
        }
    </style>
</head>

<body>
    <div class="header">
        <table width="100%">
            <tr>
                <td align="center" class="titleside">Form 307-3</td>
                <td align="center" class="title">Marriage Certificate</td>
                <td align="center" class="titleside">Register No. _______</td>
            </tr>
        </table>
    </div>
    <table width="100%" border="1" cellpadding="2" cellspacing="0">
        <tr>
            <td width="20%" align="center">Contracting Parties</td>
            <td width="40%" align="center">HUSBAND</td>
            <td width="40%" align="center">WIFE</td>
        </tr>
        <tr>
            <td>First Name</td>
            <td>&nbsp;{!! $marriage->hfname !!}</td>
            <td>&nbsp;{!! $marriage->wfname !!}</td>
        </tr>
        <tr>
            <td>Middle Name</td>
            <td>&nbsp;{!! $marriage->hmname !!}</td>
            <td>&nbsp;{!! $marriage->wmname !!}</td>
        </tr>
        <tr>
            <td>Surname</td>
            <td>&nbsp;{!! $marriage->hlname !!}</td>
            <td>&nbsp;{!! $marriage->wlname !!}</td>
        </tr>
        <tr>
            <td>Nationality</td>
            <td>&nbsp;{!! $marriage->hnationality->nationality !!}</td>
            <td>&nbsp;{!! $marriage->wnationality->nationality !!}</td>
        </tr>
        <tr>
            <td>Status</td>
            <td>&nbsp;{!! $marriage->hstatus->status !!}</td>
            <td>&nbsp;{!! $marriage->wstatus->status !!}</td>
        </tr>
        <tr>
            <td>Birth date / Age</td>
            <td>&nbsp;{!! \Carbon\Carbon::parse($marriage->hdob)->format('d F Y') !!} / {!! $marriage->hage !!}</td>
            <td>&nbsp;{!! \Carbon\Carbon::parse($marriage->wdob)->format('d F Y') !!} / {!! $marriage->wage !!}</td>
        </tr>
        <tr>
            <td>Place of Birth</td>
            <td>&nbsp;{!! $marriage->hpob !!}</td>
            <td>&nbsp;{!! $marriage->wpob !!}</td>
        </tr>
        <tr>
            <td>Residence</td>
            <td>&nbsp;{!! $marriage->hresidence !!}</td>
            <td>&nbsp;{!! $marriage->wresidence !!}</td>
        </tr>
        <tr>
            <td>Father</td>
            <td>&nbsp;{!! $marriage->hffname !!} {!! $marriage->hfmname !!} {!! $marriage->hflname !!}</td>
            <td>&nbsp;{!! $marriage->wffname !!} {!! $marriage->wfmname !!} {!! $marriage->wflname !!}</td>
        </tr>
        <tr>
            <td>Nationality</td>
            <td>&nbsp;{!! $marriage->hfnationality->nationality !!}</td>
            <td>&nbsp;{!! $marriage->wfnationality->nationality !!}</td>
        </tr>
        <tr>
            <td>Mother</td>
            <td>&nbsp;{!! $marriage->hmfname !!} {!! $marriage->hmmname !!} {!! $marriage->hmlname !!}</td>
            <td>&nbsp;{!! $marriage->wmfname !!} {!! $marriage->wmmname !!} {!! $marriage->wmlname !!}</td>
        </tr>
        <tr>
            <td>Nationality</td>
            <td>&nbsp;{!! $marriage->hmnationality->nationality !!}</td>
            <td>&nbsp;{!! $marriage->wmnationality->nationality !!}</td>
        </tr>
        <tr>
            <td>Witnesses</td>
            @foreach($witness as $wit)
            <td>&nbsp;{!! $wit->name !!}</td>
            @endforeach
        </tr>

        <tr>
            <td>Relation to H/W</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>License No./Expiry Date / Add</td>
            <td colspan="2">&nbsp;{!! $marriage->license !!} </td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td align="center"><strong>&nbsp;{!! $marriage->placeofmar !!}</strong></td>
            <td align="center"><strong>&nbsp;{!! $marriage->addressofmar !!}</strong></td>
        </tr>
        <tr>
            <td align="center">Place of Marriage</td>
            <td align="center">City / Municipality</td>
        </tr>
        <tr>
            <td align="center"><strong>&nbsp;{!! \Carbon\Carbon::parse($marriage->dateofmar)->format('F d, Y') !!}</strong></td>
            <td align="center"><strong>&nbsp;{!! $marriage->timeofmar !!}</strong></td>
        </tr>
        <tr>
            <td align="center">Date of Marriage</td>
            <td align="center">Time</td>
        </tr>
    </table>
    <p align="justify" class="notes">This is to CERTIFY that I, HUSBAND (identified above) and I, the WIFE (identified above) on the date and at the place given of our own free will and accord and in the presence of the Roman Catholic Priest solemnizing this marriage and of the two witnesses name above both of age, take each other as husband and wife.</p>
    <p align="justify" class="notes">And I, the ROMAN CATHOLIC PRIEST, solemnizing this marriage, CERTIFY: that on the date and at the place above given aforesaid husband above and wife with their mutual consent lawfully joined together in Holy Matrimony by me in the presence of said witnesses named below both of legal age and certify that the MARRIAGE LICENSE NO.____________ issued at ___________ on ____________ in favor of said parties was exhibited to me or no marriage license was exhibited to me being of exceptional character performed under Art. ____ of the Republic Act 5850 and that consent or advice to such marriage was duly given as required by law by person or person above mentioned, IN WITNESS WHEREOF, we signed or marked this with our signature affixed or thumbprints this {!! \Carbon\Carbon::parse($marriage->dateofmar)->format('jS \\of F Y') !!} .</p>
    <table width="100%">
        <tr>
            <td align="center" width="50%">&nbsp;{!! $marriage->hfname !!} {!! $marriage->hmname !!} {!! $marriage->hlname !!}</td>
            <td align="center" width="50%">&nbsp;{!! $marriage->wfname !!} {!! $marriage->wmname !!} {!! $marriage->wlname !!}</td>
        </tr>
        <tr>
            <td align="center">Husband</td>
            <td align="center">Wife</td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" align="center">&nbsp;{!! $marriage->officiating !!}</td>
        </tr>
        <tr>
            <td colspan="2" align="center">Officiating Priest</td>
        </tr>
    </table>
    <p>Witnesses:</p>
    <table width="100%" border="0">
        <tr>
            @foreach($witnessfirst as $witf)
            <td align="center">&nbsp;{!! $witf->name !!}</td>
            @endforeach
        </tr>
        <tr>
            @foreach($witnesssecond as $wits)
            <td align="center">&nbsp;{!! $wits->name !!}</td>
            @endforeach
        </tr>
    </table>
    <br />
    <p class="notes">RECEIVED Copy of the marriage contract such copy having been duly signed (thumb marked) by the contracting parties, the witnesses and the Officiating Priest. (Receipt is issued to in accordance with section G of Articles 361 amended by Commonwealth Act No. 440)</p>
    <table width="100%">
        <tr>
            <td align="right" class="civil">N.B. CIVIL MARRRIAGE CONTRACT MADE BEFORE :</td>
            <td class="civil">&nbsp;{!! $marriage->civilofficiating !!}</td>
        </tr>
        <tr>
            <td align="right" class="civil">DATE :</td>
            <td class="civil">&nbsp;{!! \Carbon\Carbon::parse($marriage->civildate)->format('d F Y') !!}</td>
        </tr>
        <tr>
            <td align="right" class="civil">PLACE :</td>
            <td class="civil">&nbsp;{!! $marriage->civiladdress !!}</td>
        </tr>
    </table>
</body>

</html>
