<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Sponsor</h4>
            </div>
            {!! Form::open(['route'=>['marriage.sponsor.save', $marriage->id],'class'=>'form-horizontal']) !!}
            <div class="modal-body">
                <div class="form-group">
                    <label for="sponsorname" class="col-md-2 control-label">Sponsor Name</label>
                    <div class="col-md-10">
                        {!! Form::text('name', null, ['class'=>'form-control']) !!}
                    </div>
                </div>


                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

</div>
