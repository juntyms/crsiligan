@extends('mainlayout')

@section('maincontent')

@include('partials._page_header',['icon'=>'mdi mdi-baby','title'=>'Marriage Record','item1'=>'Marriage','item2'=>'Create New Record'])
{!! Form::open(['route'=>'marriage.save','class'=>'form-horizontal']) !!}
@include('marriage._form')
<div class="mt-3">
    <div class="pull-right">
        <button type="submit" class="btn btn-primary"> Save </button>
    </div>
</div>
{!! Form::close() !!}



@endsection
