@extends('mainlayout')

@section('maincontent')
@include('partials._page_header',['icon'=>'mdi mdi-baby','title'=>'Marriage Record','item1'=>'Marriage','item2'=>'Search Record'])
<div class="card">
    <div class="card-body">

        {!! Form::open(['route'=>'marriage.searchresults']) !!}
        <div class="row">
            <div class="col-md-3">
                <blockquote class="blockquote blockquote-primary">
                    <div class="form-group row">
                        <div class="col-md-6">
                            <div class="form-check">
                                <label for="husband" class="form-check-label">
                                    {!! Form::radio('searchtype', '1', true,['class'=>'form-check-input']) !!}
                                    Husband
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check">
                                <label for="Wife" class="form-check-label">
                                    {!! Form::radio('searchtype', '2', false, ['class'=>'form-check-input']) !!}
                                    Wife
                                </label>
                            </div>
                        </div>
                    </div>
                </blockquote>
            </div>
            <div class="col-md-3">
                <blockquote class="blockquote blockquote-primary">
                    <div class="form-group row">
                        <div class="col-md-6">
                            <div class="form-check">
                                <label class="form-check-label">
                                    {!! Form::radio('searchtxt', '1', true, ['class'=>'form-check-input']) !!}
                                    Firstname
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check">
                                <label class="form-check-label">
                                    {!! Form::radio('searchtxt', '2', false) !!}
                                    Lastname
                                </label>
                            </div>
                        </div>
                    </div>
                </blockquote>
            </div>

            <div class="col-md-6">
                <div class="form-group row">
                    <div class="col-md-2">
                        <label for="">Search Text</label>
                    </div>
                    <div class="col-md-5">
                        {!! Form::text('search', null, ['class'=>'form-control']) !!}
                    </div>
                    <div class="col-md-5">
                        <button class="btn btn-primary"> <i class="fa fa-search" aria-hidden="true"></i> Search</button>
                    </div>
                </div>
            </div>
        </div>




        {!! Form::close() !!}


        <hr />

        <table class="table table-hover">
            <thead>
                <tr>
                    <th>SN</th>
                    <th>Husband First Name</th>
                    <th>Husband Middle Name</th>
                    <th>Husband Last Name</th>
                    <th>Wife First Name</th>
                    <th>Wife Middle Name</th>
                    <th>Wife Last Name</th>
                    <th></th>
                </tr>
                <tr>
                    <th colspan="5" class="text-center">{!! $recordsFound !!}</th>
                </tr>
            </thead>
            <tbody>
                <?php $sn=1; ?>
                @foreach($marriages as $marriage)
                <tr>
                    <td>{!! $sn++ !!}</td>
                    <td>{!! $marriage->hfname !!}</td>
                    <td>{!! $marriage->hmname !!}</td>
                    <td>{!! $marriage->hlname !!}</td>
                    <td>{!! $marriage->wfname !!}</td>
                    <td>{!! $marriage->wmname !!}</td>
                    <td>{!! $marriage->wlname !!}</td>
                    <td><a href="{!! URL::route('marriage.edit',$marriage->id) !!}" class="btn btn-success btn-xs"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
