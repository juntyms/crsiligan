@extends('mainlayout')

@section('maincontent')
@include('partials._page_header',['icon'=>'mdi mdi-baby','title'=>'Marriage Record','item1'=>'Marriage','item2'=>'Edit Record'])

{!! Form::model($marriage, ['route'=>['marriage.update',$marriage->id],'class'=>'form-horizontal']) !!}
@include('marriage._form')
<div class="card mt-3">
    <div class="card-body">
        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">
            <i class="fa fa-plus-circle"></i> Add Sponsor
        </button>
        <div class="col-md-6 mt-3">
            <h4>Sponsors</h4>
            <table class="table table-bordered ">
                @foreach($sponsors as $sponsor)
                <tr>
                    <td>{!! $sponsor->name !!}</td>
                    <td><a href="{!! URL::route('marriage.sponsor.delete',$sponsor->id) !!}"> Delete </a></td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>

<div class="mt-3">
    <div class="pull-right">
        <button type="submit" class="btn btn-primary"> Update </button>
        <button type="submit" name="printButton" value="1" class="btn btn-primary"> Print </button>
    </div>
</div>
{!! Form::close() !!}



@include('marriage._sponsors')

@endsection
