@extends('mainlayout')

@section('maincontent')

@include('partials._page_header',['icon'=>'mdi mdi-church','title'=>'Priest Record','item1'=>'Priest','item2'=>'Add New
Record'])


<div class="card">
    <div class="card-header">
        Add Priest
    </div>
    <div class="card-body">
        {!! Form::open(['route'=>'priest.save', 'class'=>'form-horizontal']) !!}
        @include('priest._form',['submitButtonText'=>'Save'])
        {!! Form::close() !!}
    </div>
</div>

@endsection