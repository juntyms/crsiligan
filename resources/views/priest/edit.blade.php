@extends('mainlayout')

@section('maincontent')

@include('partials._page_header',['icon'=>'mdi mdi-church','title'=>'Priest Record','item1'=>'Priest','item2'=>'Edit
Record'])

<div class="card">
    <div class="card-header">
        Edit Priest
    </div>
    <div class="card-body">
        {!! Form::model($priest,['route'=>['priest.update',$priest->id],'method'=>'PATCH', 'class'=>'form-horizontal'])
        !!}

        @include('priest._form',['submitButtonText'=>'Update'])
        {!! Form::close() !!}
    </div>
</div>

@endsection