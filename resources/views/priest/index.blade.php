@extends('mainlayout')

@section('maincontent')

@include('partials._page_header',['icon'=>'mdi mdi-church','title'=>'Priest Record','item1'=>'Priest','item2'=>'Add New Record']) 

<div class="card">
	<div class="card-header">Priest Record</div>
	<div class="card-body">
	<p>
	<a href="{{ URL::route('priest.create') }}" class="btn btn-primary"><i class="mdi mdi-account-star d-block mb-1"></i> Add New Priest</a>
	</p>
	<table class="table" id="priestTable">
		<thead>
			<tr class="table-primary">
				<th>SN</th>
				<th>Priest Name</th>
				<th>Affiliation</th>
				<th>Designation</th>
				<th>License No</th>
				<th>Expiry</th>
				<th>Assignatory</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
		@foreach($priests as $priest)
		
		@php
			$ispriest = '';		
		if ($priest->isAssignatory) {
			$ispriest = 'class="table-warning"';
		}
		@endphp
			<tr {!! $ispriest !!}>
				<td>{!! $sn++ !!}</td>
				<td>{!! $priest->title !!}  {!! $priest->firstname !!} {!! $priest->middlename !!}  {!! $priest->lastname !!}</td>
				<td>{!! $priest->affiliation->affiliation !!}</td>
				<td>{!! $priest->designation->designation !!}</td>
				<td>{!! $priest->licenseno !!}</td>
				<td>{!! $priest->expiry !!}</td>
				<td>@if ($priest->isAssignatory)
						<i class="mdi mdi-check-circle-outline mdi-18px"></i>
					@else
						<a href="{!! URL::route('priest.setassignatory',$priest->id) !!}"><i class="mdi mdi-close-circle-outline mdi-18px"></i></a>
					@endif
				</td>
				<td><a href="{{ URL::route('priest.edit',[$priest->id]) }}"><i class="fa fa-pencil-square-o"></i> Edit</a></td>
			</tr>
		@endforeach
		</tbody>
	</table>
	</div>
</div>
@endsection

@section('script')
	<script>
	$(document).ready( function () {
    	$('#priestTable').DataTable();
	} );
	</script>
@endsection

