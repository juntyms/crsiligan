<div class="row">
	<div class="col-md-3">
		<div class="form-group">
			{!! Form::label('title','Title') !!}	
				{!! Form::select('title', array('BISHOP' => 'BISHOP', 
								'BRO' => 'BRO', 
								'MGSR.'=>'MGSR.',
								'REV. FR.'=>'REV. FR.', 
								'SR.'=>'SR.'),null,['class'=>'form-control']) !!}	
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			{!! Form::label('firstname','First Name') !!}	
			{!! Form::text('firstname',null,['class'=>'form-control','placeholder'=>'First Name']) !!}				                    				                                     
		</div>
         </div>
	 <div class="col-md-3">
		<div class="form-group">
			{!! Form::label('middlename','Middle Name') !!}			
			{!! Form::text('middlename',null,['class'=>'form-control','placeholder'=>'Middle Name']) !!}				                    				                                     			
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			{!! Form::label('lastname','Last Name') !!}
			{!! Form::text('lastname',null,['class'=>'form-control','placeholder'=>'Last Name']) !!}				                    				                                     		
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-3">
	<div class="form-group">
		{!! Form::label('affiliation','Affiliation') !!}
			{!! Form::select('affiliation_id',$affiliations,null,['class'=>'form-control']) !!}	 	
	</div>
	</div>
	<div class="col-md-3">
	<div class="form-group">
		{!! Form::label('designation','Designation') !!}
			{!! Form::select('designation_id',$designations,null,['class'=>'form-control']) !!}	 	
	</div>
	</div>

	<div class="col-md-3">
	<div class="form-group">
		{!! Form::label('licenseno','License No') !!}
		{!! Form::text('licenseno',null,['class'=>'form-control','placeholder'=>'License No']) !!}				                    				                                     	
	</div>
	</div>
	<div class="col-md-3">
	<div class="form-group">
		{!! Form::label('expiry','Expiry') !!}
		{!! Form::date('expiry',null,['id'=>'datepicker-expiry','class'=>'form-control','placeholder'=>'Expiry']) !!}				                    				                                     	
	</div>
	</div>
</div>
<div class="form-group">
	<button class="btn btn-primary btn-sm"><i class="mdi mdi-floppy"> </i> {!! $submitButtonText !!}</button>
</div>
