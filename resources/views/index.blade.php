@extends('mainlayout')

@section('maincontent')
      
      <!-- partial -->          
          <div class="page-header">
            <h3 class="page-title">
              <span class="page-title-icon bg-gradient-primary text-white mr-2">
                <i class="mdi mdi-home"></i>                 
              </span>
              Dashboard
            </h3>
            <nav aria-label="breadcrumb">
              <ul class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page">
                  <span></span>Overview
                  <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
                </li>
              </ul>
            </nav>
          </div>
          <div class="row">
            <div class="col-md-4 stretch-card grid-margin">
              <div class="card bg-gradient-danger card-img-holder text-white">
                <div class="card-body">
                  <img src="{!! asset('purple/images/dashboard/circle.svg') !!}" class="card-img-absolute" alt="circle-image"/>
                  <h4 class="font-weight-normal mb-3">Baptismal Records
                    <i class="mdi mdi-chart-line mdi-24px float-right"></i>
                  </h4>
                  <h2 class="mb-5">{!! $total_baptismal !!}</h2>
                  <h6 class="card-text">Latest Record : @if (!empty($bap)) {!! $bap->created_at !!} @endif</h6>
                </div>
              </div>
            </div>
            <div class="col-md-4 stretch-card grid-margin">
              <div class="card bg-gradient-info card-img-holder text-white">
                <div class="card-body">
                  <img src="{!! asset('purple/images/dashboard/circle.svg') !!}" class="card-img-absolute" alt="circle-image"/>                  
                  <h4 class="font-weight-normal mb-3">Confirmation Records
                    <i class="mdi mdi-bookmark-outline mdi-24px float-right"></i>
                  </h4>
                  <h2 class="mb-5">{!! $total_confirmation !!}</h2>
                  <h6 class="card-text">Latest Record : @if (!empty($con)) {!! $con->created_at !!} @endif</h6>
                </div>
              </div>
            </div>
            <div class="col-md-4 stretch-card grid-margin">
              <div class="card bg-gradient-success card-img-holder text-white">
                <div class="card-body">
                  <img src="{!! asset('purple/images/dashboard/circle.svg') !!}" class="card-img-absolute" alt="circle-image"/>                                    
                  <h4 class="font-weight-normal mb-3">Death Records
                    <i class="mdi mdi-diamond mdi-24px float-right"></i>
                  </h4>
                  <h2 class="mb-5">{!! $total_death !!}</h2>
                  <h6 class="card-text">Latest Record : @if (!empty($death)) {!! $death->created_at !!} @endif</h6>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <span class="d-flex align-items-center purchase-popup">
                <p>Hows Your Experience so far.</p>
                <a href="#" target="_blank" class="btn ml-auto download-button">How Purchase Full Version Now</a>
                <a href="#" target="_blank" class="btn purchase-button" data-toggle="modal" data-target="#developerModal">Contact Developers</a>
                <i class="mdi mdi-close popup-dismiss" data-dismiss="alert" aria-hidden="true"></i>
              </span>
            </div>
          </div>
        
        <!-- content-wrapper ends -->
        

      @include('partials._developer')
    
@endsection

@section('script')
<!-- Custom js for this page-->
<script src="{!! asset('purple/js/dashboard.js') !!}"></script>
  <!-- End custom js for this page-->
@endsection