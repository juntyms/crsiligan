<div class="modal fade" id="pobbarangaymodal" tabindex="-1" role="dialog" aria-labelledby="pobbarangayLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    {!! Form::open(['route'=>'barangay.save','id'=>'barangayForm']) !!}            
      <div class="modal-header">
				<h4 class="modal-title" id="pobbarangayLabel">Add New Barangay - Place of Birth</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>        
      </div>
      
      <div class="modal-body">
        
        <div class="form-group">      
          <label for="city">City</label>                       
                {!! Form::select('city_id',$cities,null,['id'=>'city_id','class'=>'form-control']) !!}            
        </div>

        <div class="form-group">
            <label for="barangay">Barangay</label>            
                {!! Form::text('name',null,['id'=>'barangayname','class'=>'form-control','placeholder'=>'Barangay name','required']) !!}                
            
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary btn-sm" id="savebar" data-dismiss="modal"><i class="mdi mid-floppy"></i> Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

<script>
$("#reload_pobbarangay").click( function() {
  var cityval = $("#pobCity").val();

  if ( cityval !="" ) {
    loadpobbarangay();
  } else {
    alert("Place of Birth City Field is Empty");
  }
});

$("#pobbarangaybtn").click( function() {
  var cityval = $("#pobCity").val();

  if ( cityval !="" ) {
    
    $('#pobbarangaymodal').modal('show');
    $("#city_id").val(cityval);
    
  } else {
    alert("Place of Birth City Field is Empty");
  }
});


/** saving barangay in modal */
$("#savebar").click( function() {        
    $.post( $("#barangayForm").attr("action"),
            $("#barangayForm :input").serialize())
      .done(function() {
        loadpobbarangay();
        //clearInput();     
      });                    
});

    $("#barangayForm").submit( function() {                
        return false;
    });

    function clearInput() {
        $("#barangayForm :input").each( function() {
            $(this).val('');
        });        
    }

    function loadpobbarangay()
    {
        var cityid = $("#pobCity").val();
        $.get('/barangays/list/' + cityid, function(data) {
            console.log(data);
            $('#pobBarangay').empty();
            $('#pobBarangay').append('<option value="">... Select Barangay ...</value>');
            $.each(data, function(index,subCatObj){
                $('#pobBarangay').append('<option value='+subCatObj.id+'>'+subCatObj.name+'</option>');
            });
        });
    }
/** Saving Baragay Modal in Place */
</script>
