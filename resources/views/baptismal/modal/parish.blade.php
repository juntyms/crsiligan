<div class="modal fade" id="myparish" tabindex="-1" role="dialog" aria-labelledby="myparishLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    {!! Form::open(['route'=>'parish.jquerysave','id'=>'parish_form']) !!}            
      <div class="modal-header">
				<h4 class="modal-title" id="myparishLabel">Add New Parish</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>        
      </div>
      
      <div class="modal-body">
        <div class="form-group">
          <label for="diocese">Diocese</label>
          {!! Form::select('diocese_id',$dioceses,null, ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
          <label for="city">City</label>
          {!! Form::select('city_id',$cities,null, ['class'=>'form-control','id'=>'parish_city','required']) !!}
        </div>
        <div class="form-group">      
          <label for="parish_name">Parish Name</label>                       
                {!! Form::text('name',null, ['class'=>'form-control','placeholder'=>'Parish Name','id'=>'parish_name','required']) !!} 
        </div>
        <div class="form-group">
            <label for="barangay">Parish Address</label>            
                {!! Form::text('address',null,['class'=>'form-control','placeholder'=>'Address of Parish','id'=>'parish_address']) !!}                
            
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary btn-sm" id="save_parish" data-dismiss="modal"><i class="mdi mid-floppy"></i> Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

<script>
$("#addparishbtn").click(function() {
  var parish_city = $("#placeCity_id").val();
  console.log(parish_city);
  if ( parish_city != "") {
    $("#myparish").modal('show');
    $("#parish_city").val(parish_city);
  } else {
    alert("Place of baptism city not set");
  }  
});

$("#place_parish_reload").click(function() {
  loadparish();
});

/** Saving Parish in Modal */
$("#save_parish").click( function() {
    $.post( $("#parish_form").attr("action"),
            $("#parish_form :input").serialize())
      .done(function(){
        loadparish();                          
      });        
});

$("#parish_form").submit( function() {        
    return false;
});
    
function loadparish() {
    
    var urlgetparish = "{{ URL::route('parish.parishlist') }}";

    $.get(urlgetparish, function(data) {
        console.log(data);
        $("#parish").empty();
        $("#parish").append('<option value="">Select Parish</option>');
        $.each(data, function(index,subCatObj){ 
            $("#parish").append('<option value='+subCatObj.id+'>'+subCatObj.name+'</option>');
        });
    });
}
</script>