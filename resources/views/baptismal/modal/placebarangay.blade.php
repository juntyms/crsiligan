<div class="modal fade" id="placebarangaymodal" tabindex="-1" role="dialog" aria-labelledby="placebarangayLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    {!! Form::open(['route'=>'barangay.save','id'=>'placebarangayForm']) !!}            
      <div class="modal-header">
				<h4 class="modal-title" id="placebarangayLabel">Add New Barangay - Place of Baptism</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>        
      </div>
      
      <div class="modal-body">
        
        <div class="form-group">      
          <label for="city">City</label>                       
                {!! Form::select('city_id',$cities,null,['id'=>'placebarangaycity_id','class'=>'form-control']) !!}            
        </div>

        <div class="form-group">
            <label for="barangay">Barangay</label>            
                {!! Form::text('name',null,['id'=>'placebarangayname','class'=>'form-control','placeholder'=>'Barangay name','required']) !!}                
            
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary btn-sm" id="placesavebar" data-dismiss="modal"><i class="mdi mid-floppy"></i> Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

<script>
$("#place_barangay_reload").click( function() {
  var cityid = $("#placeCity_id").val();
  if ( cityid != "") {
    loadplacebarangay();
  } else {
    alert("Place of Baptism City is Empty");
  }
});

$("#placebarangaybtn").click(function(){
  
  var pcityval = $("#placeCity_id").val();
  if (pcityval !== "") {
    $("#placebarangaymodal").modal("show");
    $("#placebarangaycity_id").val(pcityval);
  } else {
    alert("Place of Baptism City is Empty");
  }
});
/** Save button clicked */
$("#placesavebar").click(function() {
  console.log("save place bar");
  $.post( $("#placebarangayForm").attr("action"),
            $("#placebarangayForm :input").serialize())
      .done(function() {
        loadplacebarangay();       
      });
});

$("#placebarangayForm").submit( function() {
  return false;
});

function loadplacebarangay()
{
  var cityid = $("#placeCity_id").val();
  $.get('/barangays/list/' + cityid, function(data) {
    console.log(data);
    $('#placeBarangay_id').empty();
    $('#placeBarangay_id').append('<option value="">... Select Barangay ...</value>');
    $.each(data, function(index, subCatObj) {
      $('#placeBarangay_id').append('<option value='+subCatObj.id+'>'+subCatObj.name+'</option>');
    });
  });
}

</script>