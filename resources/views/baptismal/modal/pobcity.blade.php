<div class="modal fade" id="pobcitymodal" tabindex="-1" role="dialog" aria-labelledby="mypobcityLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    {!! Form::open(['route'=>'city.save','id'=>'pobcityform']) !!}            
      <div class="modal-header">
				<h4 class="modal-title" id="mypobcityLabel">Add New City</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>        
      </div>      
      <div class="modal-body">                
        <div class="form-group">
            <label for="barangay">City</label>            
                {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'City name','id'=>'cityname','required']) !!}                            
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary btn-sm" id="savepobcity" data-dismiss="modal"><i class="mdi mid-floppy"></i> Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

<script>
$('#reload_pobcity').on('click', function() {
    $("#pobBarangay").empty();
    loadpobcity();
});
/** saving city in modal */
    $("#savepobcity").click( function() {        
        $.post( $("#pobcityform").attr("action"),
                $("#pobcityform :input").serialize())
          .done( function() {
            loadpobcity();
            //clearInput(); 
          });                            
    });

    $("#pobcityform").submit( function() {        
        //loadbarangay();
        return false;
    });
/*
    function clearInput() {
        $("#pobcityform :input").each( function() {
            $(this).val('');
        });        
    }
*/
    function loadpobcity()
    {        
        $.get('/city/list', function(data) {
            console.log(data);
              $('#pobCity').empty();
              $('#city_id').empty();
            	$('#pobCity').append('<option value="">... Select City ...</value>');
              $('#city_id').append('<option value="">... Select City ...</value>');
            $.each(data, function(index,subCatObj){
                $('#pobCity').append('<option value='+subCatObj.id+'>'+subCatObj.name+'</option>');
                $('#city_id').append('<option value='+subCatObj.id+'>'+subCatObj.name+'</option>');
            });
        });
    }
/** Saving city Modal in Place of birth */

// Place of Birth City Select
$('#pobCity').on('change', function(e){
        console.log(e);
        var PobCity_id = e.target.value;
        
        //console.log(e.target.value);
        $.get('/barangays/list/' + PobCity_id , function(data) {
        //$.get(pobcityurl, function(data) {
            console.log(data);
            $('#pobBarangay').empty();
            	$('#pobBarangay').append('<option value="">... Select Barangay ...</value>');
            $.each(data, function(index,subCatObj){
                $('#pobBarangay').append('<option value='+subCatObj.id+'>'+subCatObj.name+'</option>');
            });
        });
    });
</script>