<div class="modal fade" id="placecitymodal" tabindex="-1" role="dialog" aria-labelledby="myplacecityLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    {!! Form::open(['route'=>'city.save','id'=>'placecityform']) !!}            
      <div class="modal-header">
				<h4 class="modal-title" id="placecityLabel">Add New City - Place of Baptism</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>        
      </div>      
      <div class="modal-body">                
        <div class="form-group">
            <label for="barangay">City</label>            
                {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'City name','id'=>'placecityname','required']) !!}                            
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary btn-sm" id="saveplacecity" data-dismiss="modal"><i class="mdi mid-floppy"></i> Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

<script>
$("#place_city_reload").click( function() {
  $("#placeBarangay_id").empty();
  loadplacecity();
});

/** saving city in modal */
$("#saveplacecity").click( function() {        
        $.post( $("#placecityform").attr("action"),
                $("#placecityform :input").serialize())
          .done( function() {
            loadplacecity();            
          });                            
    });

    $("#placecityform").submit( function() {        
        //loadbarangay();
        return false;
    });    

    function loadplacecity()
    {        
        $.get('/city/list', function(data) {
            console.log(data);
            $('#placeCity_id').empty();
            $('#placebarangaycity_id').empty();
            $('#parish_city').empty();
            	$('#placeCity_id').append('<option value="">Select City / Municipality</value>');
              $('#placebarangaycity_id').append('<option value="">Select City / Municipality</value>');
              $('#parish_city').append('<option value="">Select City / Municipality</value>');
            $.each(data, function(index,subCatObj){
                $('#placeCity_id').append('<option value='+subCatObj.id+'>'+subCatObj.name+'</option>');
                $('#placebarangaycity_id').append('<option value='+subCatObj.id+'>'+subCatObj.name+'</option>');
                $('#parish_city').append('<option value='+subCatObj.id+'>'+subCatObj.name+'</option>');
            });
        });
    }
/** Saving city Modal in Place of birth */
/** Place of Baptism Select City Change */
$('#placeCity_id').on('change', function(e){
        console.log(e);
        var placecity_id = e.target.value;
        
        //console.log(e.target.value);
        $.get('/barangays/list/' + placecity_id , function(data) {
        //$.get(placecity_idurl, function(data) {
            console.log(data);
            $('#placeBarangay_id').empty();
            	$('#placeBarangay_id').append('<option value="">Select Barangay</value>');
            $.each(data, function(index,subCatObj){
                $('#placeBarangay_id').append('<option value='+subCatObj.id+'>'+subCatObj.name+'</option>');
            });
        });
    });
</script>