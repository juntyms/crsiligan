    <html lang="en">
    <head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>        
        <title>Baptismal Certificate</title>    
	</head>
	<body>		
		<style>
		@font-face {
		    font-family: 'PlainBlack';
		    src: url('fonts/PlainBlack/Plain_Black.otf');
		    font-weight: normal;
		    font-style: normal;
		}

		@font-face {
		    font-family: 'chopinscript';		    
			src: url("fonts/chopinscript.ttf");
		    font-weight: normal;
		    font-style: normal;
		}
		
		@font-face {
			font-family: 'OldEnglish';			
			src: url("fonts/Certificate-Regular.ttf") format("truetype");
			font-weight: normal;
		    font-style: normal;    
		}

		@font-face {
			font-family: 'berlinsmallcaps';
			font-variant: small-caps;
			src: url("fonts/BerlinSmallCaps.ttf");

		}
		div.headerspacer {
			padding-top: 60px;
		}
		td.certificate-header {
			font-family:OldEnglish;
			text-align: center;
			font-size: 50px;			
		}
		td.certificate-subheader {
			text-align: center;			
			font-family: 'berlinsmallcaps';			
			font-size: 18px
		}

		.entry-title {
		    color: #222;
		    text-decoration: none;
		    font-family: 'chopinscript', sans-serif;
		    font-size: 2.5em;
		}

		.certificate-name {
			font-family: "Arial Black", Gadget, sans-serif;
			font-weight: bold;
		}

		table.tbwidth {
			width: 672px;
		}

		td.baptismal-name {
			font-family: "Arial";
			font-size: 2.2em;
			text-align: center;
			border-bottom: 2px solid #000;
			padding-top: 10px;
		}

		.bap-label {
			font-family: Arial,Helvetica Neue,Helvetica,sans-serif;
			font-size: 1em;
			text-align: right;
			padding-top: 10px;
			padding-right: 20px;
		}

		td.bap-details {
			font-family: Arial,Helvetica Neue,Helvetica,sans-serif;
			font-size: 1em;
  			border-bottom: 1px solid #000;
		}

		p.book {
			font-family: Arial,Helvetica Neue,Helvetica,sans-serif;
			font-size: 1em;
			font-style: italic;
		}

		.assignatory {
			font-family: Arial,Helvetica Neue,Helvetica,sans-serif;
			font-size: 1em;
			font-weight: bold;
		    text-decoration:underline;
		    text-align: center;		    
		}

		.designation{
			font-family: Arial,Helvetica Neue,Helvetica,sans-serif;
			font-size: .8em;
			text-align: center;
		}
		
		.back{
			font-family: Arial,Helvetica Neue,Helvetica,sans-serif;
			font-size: .7em;
			font-style: italic;
			text-align: center;
		}

		.otherinfo {
			font-family: "Times New Roman", Times, serif;
			font-size: 1.6em;
			font-weight: bold;
			text-align: center;
		}
		.othersubinfo {
			font-family: "Times New Roman", Times, serif;
			font-size: .8em;
			text-align: center;
		}

		td.otherdetails {
			font-family: "Times New Roman", Times, serif;
			font-size:1.25em;
			font-weight: bold;
			text-align: center;
			padding: 10px,0px,10px,0px;

		}
		td.othersubdetails {
			font-family: "Times New Roman", Times, serif;
			font-size: 1em;
			text-align: center;
			padding-bottom: 20px;
		}

		td.sponsors {
			font-family: "Times New Roman", Times, serif;
			font-size: 1.5em;
			text-align:center;
		}

		td.noted {
			font-family: "Times New Roman", Times, serif;
			font-weight: bold;
			font-size: 1.2em;
		}
		td.signline {
			padding-top: 30px;
			border-bottom: 1px solid #000;
		}
		.page-break {
 		   page-break-after: always;
		}
		
		</style>
		<div class="headerspacer">&nbsp;</div>
	
		<table class="tbwidth" align="center">
			<tr>
				<td colspan="2" class="certificate-header">Baptismal Certificate</td>
			</tr>
			<tr>
				<td colspan="2" class="certificate-subheader">This is to Certify that the Sacrament of Baptism was <br />Solemnly Administered to</td>
			</tr>
			<tr>
				<td colspan="2" class="baptismal-name">{{ $data->fname }} {{ $data->mname }} {{ $data->lname }}</td>
			</tr>
			<tr>		
				<td class="bap-label">Date of Birth :</td><td class="bap-details">{{ date("d F Y" ,strtotime($data->dob)) }}</td>
			</tr>
			<tr>
				<td class="bap-label">Place of Birth :</td><td class="bap-details">{{ $data->pobstreet }}  {{ $data->barangayname }} {{ $data->pobcityname }}</td>
			</tr>
			<tr>
				<td class="bap-label">Gender :</td><td class="bap-details">@if ($data->gender=='M') Male @else Female @endif</td>
			</tr>
			<tr>
				<td class="bap-label">Name of Father :</td><td class="bap-details">{{ $data->fatherFn }} {{ $data->fatherMn }} {{ $data->fatherLn }}</td>
			</tr>
			<tr>
				<td class="bap-label">Name of Mother :</td><td class="bap-details">{{ $data->motherFn }} {{ $data->motherMn }} {{ $data->motherLn }}</td>
			</tr>
			<tr>
				<td class="bap-label">Date of Baptism:</td><td class="bap-details">{{ date("d F Y" ,strtotime($data->dateofbaptism)) }}</td>
			</tr>
			<tr>
				<td class="bap-label">Place of Baptism:</td><td class="bap-details">{{ $data->placeparish }} {{ $data->placebarangay }} {{ $data->placecity }}</td>
			</tr>
			<?php $spcnt=1; ?>
			@foreach($sponsors as $sponsor)
			<?php $spcnt++; ?>

			<tr>
				<td class="bap-label">
					@if ($spcnt==2)
					Sponsor :
					@endif
				</td><td class="bap-details">{{ $sponsor->fname }} {{ $sponsor->mname }} {{ $sponsor->lname }}</td>
			</tr>
			@endforeach
			<?php 
				if ($spcnt<5) { 
					$newrow = 5 - $spcnt;
					for($row=0; $row<$newrow; $row++)
					{
					?>
					<tr>
						<td>&nbsp;</td>
						<td class="bap-details">&nbsp;</td>
					</tr>
					<?php
					}				
				}

			?>
			<tr>
				<td class="bap-label">Minister of Baptism:</td><td class="bap-details">{{ $data->title }} {{ $data->firstname }}  {{ $data->middlename }}  {{ $data->lastname }}</td>
			</tr>			
		</table>
		
		<p class="book">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The above data are taken from the records of the Church<br />
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Volume&nbsp;: {{ $data->volume }}<br />	
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Page&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{ $data->page }}<br />
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Number&nbsp;: {{ $data->book }}
		</p>
		
		<table class="tbwidth" align="center">
			<tr>
				<td width="50%"></td>
				
				<td width="50%">
					<div class="assignatory">
						{{ $data->sgdtitle }} {{ $data->sgdfirstname }} {{ $data->sgdmiddlename }} {{ $data->sgdlastname }}</div>
					<div class="designation">
						{{ $data->designation }}</div>
				</td>
				
			</tr>
		</table>
		<br />
		<table class="tbwidth" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="5%">&nbsp;</td>
				<td>Purpose of this Certificate : {{ $data->purpose }}</td>				
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>Annotation : {{ $data->annotation }}</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>Remarks : {{ $data->remarks }}</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>Issued by :  @if ($printSec == 1) {!! $isSecretary->firstname !!} {!! $isSecretary->middlename !!} {!! $isSecretary->lastname !!} @endif</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>Date Issue : {{ date_format(date_create($data->dateIssue),'l, jS F Y') }}</td>
			</tr>
		</table>		
		
		<p class="back">Continue at the back</p>
		<div class="page-break"></div>
		<div class="otherinfo">OTHER INFORMATION</div>
		<div class="othersubinfo">(indicated in the Baptismal Registry)</div>
		<table border="1" cellspacing="0" cellpadding="0" class="tbwidth" align="center">
			<tr>
				<td colspan="6" class="otherdetails">CONFIRMATION</td>
			</tr>
			<tr>
				<td colspan="3" class="othersubdetails">Date</td>
				<td colspan="3" class="othersubdetails">Date</td>
			</tr>
			<tr>
				<td colspan="6" class="otherdetails">MARRIAGE</td>				
			</tr>
			<tr>
				<td colspan="3" class="othersubdetails">Date</td>
				<td colspan="3" class="othersubdetails">Date</td>
			</tr>
			<tr>
				<td colspan="6" class="otherdetails">DISPENSATION(S) GIVEN</td>
			</tr>
			<tr>
				<td colspan="2" class="othersubdetails">[  ] Disparity of Cult</td>
				<td colspan="2" class="othersubdetails">[  ] Mixed Cult</td>
				<td colspan="2" class="othersubdetails">[  ] Publication of Banns</td>
			</tr>
			<tr>
				<td colspan="6" class="otherdetails">DECLARATION OF NULLITY</td>				
			</tr>
			<tr>
				<td colspan="3" class="othersubdetails">Date</td>
				<td colspan="3" class="othersubdetails">Tribunal</td>
			</tr>
			<tr>
				<td colspan="6" class="otherdetails">ORDAINED / CONSECRATED</td>				
			</tr>
			<tr>
				<td colspan="3" class="othersubdetails">Date</td>
				<td colspan="3" class="othersubdetails">Place</td>
			</tr>
			<tr>
				<td colspan="6" class="otherdetails">Diocese / Congregation</td>				
			</tr>
			<tr>
				<td colspan="6">&nbsp;</td>				
			</tr>
		</table>
		<br />
		<br />
		<br />
		<table class="tbwidth" align="center">
			<tr>
				<td width="50%"></td>
				<td width="50%" class="noted">Prepared By</td>
			</tr>
			<tr>
				<td></td>
				<td class="signline" style="text-align:center;text-transform:uppercase;">{!! $isSecretary->firstname !!} {!! $isSecretary->middlename !!} {!! $isSecretary->lastname !!}</td>
			</tr>
			<tr>
				<td></td>
				<td class="noted" align="center">Parish Secretary</td>
			</tr>
			<tr>
				<td class="noted">Noted By</td>
				<td></td>
			</tr>
			<tr>
				<td class="signline" align="center">{{ $data->sgdtitle }} {{ $data->sgdfirstname }} {{ $data->sgdmiddlename }} {{ $data->sgdlastname }}</td>
				<td></td>
			</tr>
			<tr>
				<td class="noted" align="center">Parish Priest</td>
				<td></td>
			</tr>
		</table>
		@if ($sCount > 4)
		<div class="page-break"></div>
		<div class="headerspacer">&nbsp;</div>
		<table class="tbwidth">
			<tr>
				<td class="baptismal-name" colspan="3">
				{{ $data->fname }} {{ $data->mname }} {{ $data->lname }}</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td class="sponsors">
				
				<p>WITH THE FOLLOWING SPONSORS</p></td>
				<td>&nbsp;</td>
			</tr>
		@php 
			$sn = 0;
		@endphp
		@foreach($allsponsors as $allsponsor)
		@php 
			$sn++;
		@endphp
			@if ($sn > 4)
			<tr>
				<td width="10%">&nbsp;</td>
				<td class="bap-details">
				
				<p>{{ $allsponsor->fname }} {{ $allsponsor->mname }} {{ $allsponsor->lname }}</p></td>
				<td width="10%">&nbsp;</td>
			</tr>
			@endif
			@endforeach
		</table>
		@endif
	</body>
</html>
 