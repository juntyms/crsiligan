<table>
    <tr>
        <th>SN</th>
        <th>First Name</th>
        <th>Middle Name</th>
        <th>Last Name</th>
        <th>Gender</th>
        <th>Date of Birth</th>
        <th>Place of Birth</th>                  
        <th>Father's Name</th>
        <th>Mother's Name</th>
        <th>Baptized By</th>
        <th>Date of Baptism</th>
        <th>Place of Baptism</th>
        <th>Purpose</th>
        <th>Annotation</th>
        <th>Volume</th>
        <th>Book</th>
        <th>Page</th>      
        <th>Sponsors</th>  
    </tr>
    @php 
        $sn=1;
    @endphp
    @foreach($baptismals as $baptismal)
    <tr>
        <td>{!! $sn++ !!}</td>
        <td>{!! $baptismal->fname !!}</td>
        <td>{!! $baptismal->mname !!}</td>
        <td>{!! $baptismal->lname !!}</td>
        <td>{!! $baptismal->gender !!}</td>
        <td>{!! $baptismal->dob !!}</td>
        <td>{!! $baptismal->pobstreet !!} ,{!! $baptismal->pob_barangay->name !!} ,{!! $baptismal->pob_city->name !!}</td>        
        <td>{!! $baptismal->fatherFn !!} {!! $baptismal->fatherMn !!} {!! $baptismal->fatherLn !!}</td>
        <td>{!! $baptismal->motherFn !!} {!! $baptismal->motherMn !!} {!! $baptismal->motherLn !!}</td>
        <td>{!! $baptismal->baptizedby->title !!} {!! $baptismal->baptizedby->firstname !!} {!! $baptismal->baptizedby->middlename !!} {!! $baptismal->baptizedby->lastname !!}</td>
        <td>{!! $baptismal->dateofbaptism !!}</td>
        <td>{!! $baptismal->place_parish->name !!} , {!! $baptismal->placeStreet !!} ,{!! $baptismal->place_barangay->name !!} ,{!! $baptismal->place_city->name !!}</td>
        <td>{!! $baptismal->purpose !!}</td>
        <td>{!! $baptismal->annotation !!}</td>
        <td>{!! $baptismal->volume !!}</td>
        <td>{!! $baptismal->book !!}</td>
        <td>{!! $baptismal->page !!}</td>
        @foreach($baptismal->sponsors as $sponsor)
        <td>{!! $sponsor->fname !!} {!! $sponsor->mname !!} {!! $sponsor->lname !!}</td>
        @endforeach
    </tr>
    @endforeach
</table>