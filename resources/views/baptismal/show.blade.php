@extends('mainlayout')

@section('maincontent')
        
        @include('partials._page_header',['icon'=>'mdi mdi-baby','title'=>'Baptismal Record','item1'=>'Baptismal','item2'=>'Show All']) 
        <div class="card">
            <div class="card-header">
                Baptismal Records
            </div>
            <div class="card-body">
                <p>
                <a href="{!! URL::route('baptismal.excel') !!}" class="btn btn-success"> Export To Excel</a>
                </p>
                <table class="table table-bordered" id="baptismalTable">
                    <thead>
                        <tr class="bg-primary">
                            <th>SN</th>
                            <th>First Name</th>
                            <th>Middle Name</th>
                            <th>Last Name</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $sn=1; ?>
                        @foreach($baptismals as $baptismal)
                        <tr>
                            <td>{!! $sn++ !!}</td>
                            <td>{!! $baptismal->fname !!}</td>
                            <td>{!! $baptismal->mname !!}</td>
                            <td>{!! $baptismal->lname !!}</td>
                            <td><a href="{!! URL::route('baptismal.edit',$baptismal->id) !!}"> Edit</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
     

@endsection

@section('css')
<link rel="stylesheet" type="text/css" href="{!! asset('DataTables/datatables.min.css') !!}"/>
@endsection

@section('script')
    <script type="text/javascript" src="{!! asset('DataTables/datatables.min.js') !!}"></script>
    <script>
    $(document).ready( function () {
        $('#baptismalTable').DataTable();
    });
    </script>
@endsection


 
