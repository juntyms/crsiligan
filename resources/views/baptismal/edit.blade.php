@extends('mainlayout')

@section('maincontent')
      
        @include('partials._page_header',['icon'=>'mdi mdi-baby','title'=>'Baptismal Record','item1'=>'Baptismal','item2'=>'Edit Record'])         
        <div class="card">
          <div class="card-header"> Edit Baptismal Record</div>
          <div class="card-body">
	    	{!! Form::model($baptismal,['route'=>['baptismal.update',$baptismal->id]]) !!}	            
	        	@include('baptismal._form',['isCreate'=>'0'])
   			
	        		<button type="submit" name="update" value="1" class="btn btn-gradient-info"><i class="mdi mdi-floppy"></i> Update</button>   	        	            		
	        		<button type="submit" name="update" value="0" class="btn btn-gradient-primary"><i class="mdi mdi-printer"></i> Print</button>   
                           	            		            		
			{!! Form::close() !!}				
          </div>                  
        </div>
	@include('baptismal._sponsorForm')

    

@endsection

@section('script')
    @include('baptismal.modal._script')

<script>
/*
$('#savebar').on('click', function(e) {
       e.preventDefault(); 
       
       var barname = $('#barangayname').val();       
       var city_id = $('#city_id').val();

       console.log(barname);

       $.ajax({
            type: "POST",
            url: '/savebarangay',
            data: {city_id: city_id, name: barname},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        });

   });
   */
</script>
@endsection