<h5>Place of Baptism</h5>
<blockquote class="blockquote">
    <div class="form-group">
        <label for="placebap">City / Municipality <sup class="text-danger">*</sup></label>		
            <div class="input-group">			
            {!! Form::select('placeCity_id',$cities,null,['class'=>'form-control','id'=>'placeCity_id','placeholder'=>'Select City / Municipality', 'required']) !!}	
            <div class="input-group-append">        
            <a href="#" class="btn btn-sm btn-success" id="place_city_reload"><i class="mdi mdi-cached"></i></a>
            <a href="#" data-toggle="modal" data-target="#placecitymodal" class="btn btn-sm btn-gradient-warning"><i class="mdi mdi-library-plus"></i> Add</a>	
            </div>												
            </div>
            @if ($errors->has('placeCity_id')) <small class="text-danger">{!! $errors->first('placeCity_id') !!}</small> @endif
    </div>
    <div class="form-group">
        <label for="placebap">Barangay <sup class="text-danger">*</sup></label>		                  		
            <div class="input-group">
            {!! Form::select('placeBarangay_id',$placeBarangays,null,['id'=>'placeBarangay_id','class'=>'form-control','placeholder'=>'Select Barangay','required']) !!}		
            <div class="input-group-append">                
                <button type="button" class="btn btn-sm btn-success" id="place_barangay_reload"><i class="mdi mdi-cached"></i></button>
                <button id="placebarangaybtn" type="button" class="btn btn-sm btn-gradient-warning" ><i class="mdi mdi-map-marker-multiple"></i> Add </button>	                          		
            </div>				
            </div>
            @if ($errors->has('placeBarangay_id'))<small class="text-danger"> {!! $errors->first('placeBarangay_id') !!} </small> @endif
    </div>
    <div class="form-group">
        <label for="placebap">Parish/Chapel</label>		                  	
            <div class="input-group">
            {!! Form::select('placeParish_id',$parishes,null,['class'=>'form-control','id'=>'parish']) !!}	  
            <div class="input-group-append">
                <a href="#" class="btn btn-sm btn-success" id="place_parish_reload"><i class="mdi mdi-cached"></i></a>                
                <button id="addparishbtn" type="button" class="btn btn-sm btn-gradient-warning"><i class="mdi mdi-library-plus"></i> Add</button>
            </div>            			                       						
        </div>
    </div>
    <div class="form-group">
        <label for="datebap">Date of Baptism</label>
            <div class="input-group">
            {!! Form::date('dateofbaptism',null,['class'=>'form-control','placeholder'=>'Date of Baptism']) !!}				                    				                   
            <div class="input-group-append">
            <span class="input-group-text"><i class="mdi mdi-calendar-clock"></i></span>
            </div>
        </div>   		
    </div>
    <div class="form-group">
        <label for="bapby">Baptized by</label>					            
            <div class="input-group">
            {!! Form::select('baptizedby_id',$priests,null,['class'=>'form-control','id'=>'baptizedby_id']) !!}		
            <div class="input-group-append">
                <a href="#" class="btn btn-sm btn-success" id="place_baptizedby_reload"><i class="mdi mdi-cached"></i></a>
                <a class="btn btn-sm btn-gradient-warning" href="#" data-toggle="modal" data-target="#baptizedbymodal"><i class="mdi mdi-library-plus"></i> Add </a>
            </div>			            							
            </div>
    </div>			
</blockquote>