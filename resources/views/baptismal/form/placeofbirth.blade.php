<h5>Place of Birth</h5>
<blockquote class="blockquote">
    <div class="form-group">
        <label for="fa-fname">Address <sup class="text-danger">*</sup></label>						
                {!! Form::text('pobstreet',null,['class'=>'form-control','placeholder'=>'House No./Subdv/Street','required']) !!}		                    		                                  
                <label for="marplace"><small><em>House No./Subdv/Street</em></small></label>
                @if ($errors->has('pobstreet'))<small class="text-danger">{{ $errors->first('pobstreet') }}</small> @endif						
    </div>
    <div class="form-group">
        <label for="city">City / Municipality<sup class="text-danger">*</sup></label>	                        	
        <div class="input-group">
            {!! Form::select('pobCity_id',$cities,null,['class'=>'form-control','id'=>'pobCity','placeholder'=>'Select City / Municipality','required']) !!}							
            <div class="input-group-append">
            <a href="#" class="btn btn-sm btn-gradient-success" id="reload_pobcity"><i class="mdi mdi-cached"></i></a>
            <a data-toggle="modal" data-target="#pobcitymodal" class="btn btn-sm btn-gradient-warning" href="{{ URL::route('city.create') }}"><i class="mdi mdi-map-marker-multiple"></i> Add</a>	
            </div>							
        </div>					
        @if ($errors->has('pobCity_id'))<small class="text-danger">{{ $errors->first('pobCity_id') }}</small> @endif
    </div>
    <div class="form-group">
        <label for="brgybap">Barangay <sup class="text-danger">*</sup></label>	                            
        <div class="input-group">
            {!! Form::select('pobBarangay_id',$bapBarangays,null,['class'=>'form-control','id'=>'pobBarangay','placeholder'=>'Select Barangay','required']) !!}
            <div class="input-group-append">
                <a href="#" id="reload_pobbarangay" class="btn btn-sm btn-success"><i class="mdi mdi-cached"></i></a>
                <button id="pobbarangaybtn" type="button" class="btn btn-sm btn-gradient-warning" ><i class="mdi mdi-map-marker-multiple"></i> Add </button>	                          		
            </div>							
        </div>
        @if ($errors->has('pobBarangay_id'))<small class="text-danger">{{ $errors->first('pobBarangay_id') }}</small> @endif					
    </div>
</blockquote>	   
