<h5>Father's Information</h5>
<blockquote class="blockquote">
    <div class="form-group">
        <label for="fa_fname">First Name </label>						
            {!! Form::text('fatherFn',null,['class'=>'form-control','placeholder'=>'Pangalan sa amahan']) !!}					                
            @if ($errors->has('fatherFn'))<small class="text-danger">{{ $errors->first('fatherFn') }}</small> @endif						
        </div>
    <div class="form-group">
        <label for="fa-midname">Middle Name </label>							
                {!! Form::text('fatherMn',null,['class'=>'form-control','placeholder'=>'Middle Name sa amahan']) !!}	
                @if ($errors->has('fatherMn'))<small class="text-danger">{{ $errors->first('fatherMn') }}</small> @endif				                    							
        </div>
    <div class="form-group">
            <label for="fa_lname">Last Name </label>						
                {!! Form::text('fatherLn',null,['class'=>'form-control','placeholder'=>'Apilyedo sa amahan']) !!}	
                @if ($errors->has('fatherLn'))<small class="text-danger">{{ $errors->first('fatherLn') }}</small> @endif				                    							
    </div>
</blockquote>
<h5>Mother's Information</h5>
<blockquote class="blockquote">
    <div class="form-group">
        <label for="mo-fname">First Name <sup class="text-danger">*</sup></label>							
            {!! Form::text('motherFn',null,['class'=>'form-control','placeholder'=>'Pangalan sa inahan']) !!}	
            @if ($errors->has('motherFn'))<small class="text-danger">{{ $errors->first('motherFn') }}</small> @endif				                    				                    							
    </div>
    <div class="form-group">
        <label for="mo-midname">Maiden Name <sup class="text-danger">*</sup></label>							
            {!! Form::text('motherMn',null,['class'=>'form-control','placeholder'=>'Apilyedo sa inahan sa dalaga pa']) !!}				                    
            @if ($errors->has('motherMn'))<small class="text-danger">{{ $errors->first('motherMn') }}</small> @endif				                    				                    							
    </div>
    <div class="form-group">
        <label for="mo-lname">Last Name <sup class="text-danger">*</sup></label>							
            {!! Form::text('motherLn',null,['class'=>'form-control','placeholder'=>'Apilyedo sa inahan']) !!}
            @if ($errors->has('motherLn'))<small class="text-danger">{{ $errors->first('motherLn') }}</small> @endif				                    				                    				                    							
    </div>
</blockquote>