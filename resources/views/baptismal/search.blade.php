@extends('mainlayout')

@section('maincontent')
        
        @include('partials._page_header',['icon'=>'mdi mdi-baby','title'=>'Baptismal Record','item1'=>'Baptismal','item2'=>'Search']) 
        <div class="card">
            <div class="card-header">
                Baptismal Records
            </div>
            <div class="card-body">
                {!! Form::open(['route'=>'baptismal.postsearch','class'=>'form-inline']) !!}
                    <div class="form-check mx-sm-2">
                        <label for="" class="form-check-label">{!! Form::radio('search_type',1, true) !!} Firstname</label>
                    </div>
                    <div class="form-check mx-sm-2">
                        <label for="" class="form-check-label">{!! Form::radio('search_type',2, false) !!} Lastname</label>
                    </div>
                    <input type="text" name="searchtext" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="Type Search Here..." value="{{ $oldsearchtext }}">
                    <button type="submit" class="btn btn-gradient-success mb-2">Submit</button>
                {!! Form::close() !!}
                <table class="table table-bordered" id="baptismalTable">
                    <thead>
                        <tr class="bg-primary">
                            <th>SN</th>
                            <th>First Name</th>
                            <th>Middle Name</th>
                            <th>Last Name</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $sn=1; ?>
                        @foreach($baptismals as $baptismal)
                        <tr>
                            <td>{!! $sn++ !!}</td>
                            <td>{!! $baptismal->fname !!}</td>
                            <td>{!! $baptismal->mname !!}</td>
                            <td>{!! $baptismal->lname !!}</td>
                            <td><a href="{!! URL::route('baptismal.edit',$baptismal->id) !!}"> Edit</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
     

@endsection




 
