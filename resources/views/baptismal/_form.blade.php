<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="fname">First Name <sup class="text-danger">*</sup></label>
            {!! Form::text('fname',null,['class'=>'form-control','placeholder'=>'Pangalan sa bunyagan','required']) !!}
            @if ($errors->has('fname'))<small class="text-danger">{{ $errors->first('fname') }}</small> @endif
        </div>
        <div class="form-group">
            <label for="mname">Middle Name <sup class="text-danger">*</sup></label>
            {!! Form::text('mname',null,['class'=>'form-control','placeholder'=>'Apilyedo sa inahan sa dalaga
            pa','required']) !!}
            @if ($errors->has('mname'))<small class="text-danger">{{ $errors->first('mname') }}</small> @endif
        </div>
        <div class="form-group">
            <label for="lname">Last Name <sup class="text-danger">*</sup></label>
            {!! Form::text('lname',null,['class'=>'form-control','placeholder'=>'Legal nga Apilyedo sa bunyagan',
            'required']) !!}
            @if ($errors->has('lname'))<small class="text-danger">{{ $errors->first('lname') }}</small> @endif
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="gender">Gender <sup class="text-danger">*</sup></label>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-check">
                                <label class="form-check-label">
                                    {!! Form::radio('gender','M',['class'=>'form-check-input']) !!}
                                    Male
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-check">
                                <label class="form-check-label">
                                    {!! Form::radio('gender','F',['class'=>'form-check-input']) !!}
                                    Female
                                </label>
                            </div>
                        </div>
                        @if ($errors->has('gender'))<small class="text-danger">{{ $errors->first('gender') }}</small>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="bapdob">Date of Birth <sup class="text-danger">*</sup></label>
                    <div class="input-group">
                        {!! Form::date('dob',null,['class'=>'form-control','required']) !!}
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="mdi mdi-calendar-clock"></i></span>
                        </div>
                    </div>
                    @if ($errors->has('dob'))<small class="text-danger">{{ $errors->first('sob') }}</small> @endif
                </div>
            </div>
        </div>

        @include('baptismal.form.placeofbirth')
        @include('baptismal.form.parents')

    </div> <!-- ./col-md-6 -->
    <div class="col-md-6">
        @include('baptismal.form.placeofbaptism')

        <blockquote class="blockquote">
            <div class="form-group">
                <label for="purpose">Purpose</label>
                {!! Form::text('purpose',null,['class'=>'form-control','placeholder'=>'Katuyuan / Purpose']) !!}
            </div>
            <div class="form-group">
                <label for="annotation">Annotation</label>
                {!! Form::textarea('annotation',null,['class'=>'form-control','placeholder'=>'Dugang
                kasayuran','rows'=>'2']) !!}
            </div>
            <div class="form-group">
                <label for="remarks">Remarks</label>
                {!! Form::textarea('remarks',null, ['class'=>'form-control','placeholder'=>'Remarks','rows'=>'2']) !!}
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="volume">Volume</label>
                        {!! Form::number('volume',null,['class'=>'form-control','placeholder'=>'Enter Volume No']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="bookno">Book</label>
                        {!! Form::number('book',null,['class'=>'form-control','placeholder'=>'Enter Book No']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="page">Page</label>
                        {!! Form::number('page',null,['class'=>'form-control','placeholder'=>'Enter Page No']) !!}
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="bapby">Prish Priest</label>
                <div class="input-group">
                    {!!
                    Form::select('currentPriest_id',$priests,$currentPriest->id,['class'=>'form-control','id'=>'priest_id'])
                    !!}
                    <div class="input-group-append">
                        <a href="#" class="btn btn-sm btn-success" id="parish_priest_reload"><i
                                class="mdi mdi-cached"></i></a>
                        <a class="btn btn-sm btn-gradient-warning" href="#" data-toggle="modal"
                            data-target="#priestmodal"><i class="mdi mdi-account-multiple-plus"></i> Add </a>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="designation">Designation</label>
                {!!
                Form::select('currentPriest_designationId',$designations,$currentPriest->designation_id,['class'=>'form-control'])
                !!}
            </div>
            <div class="form-group">
                <label for="dateissue">Date Issue</label>
                <div class="input-group">
                    {!! Form::date('dateIssue',null,['class'=>'form-control']) !!}
                    <div class="input-group-append">
                        <span class="input-group-text"><i class="mdi mdi-calendar-clock"></i></span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="form-check form-check-flat form-check-primary">
                    <label for="isSecretary" class="form-check-label">Show Release by
                        {!! Form::checkbox('isSecretary', 1 , false, ['class'=>'form-check-input']) !!}
                    </label>
                </div>
            </div>
        </blockquote>
        <h5>Sponsors</h5>
        <blockquote class="blockquote">
            <!-- Sponsor Form -->
            @if ($isCreate == 0)
            @include('baptismal._sponsor')
            @endif
            <!-- ./Sponsor Form  -->
        </blockquote>
    </div> <!-- ./col-md-6 -->
</div> <!-- ./row -->