
<h5>Sponsor List</h5>

<button type="button" class="btn btn-gradient-primary btn-sm" data-toggle="modal" data-target="#myModal">
    <i class="mdi mdi-account-multiple-plus"></i> Add Sponsor
</button> 
<p>&nbsp;</p>
<blockquote class="blockquote">

        <table class="table">
            @foreach($sponsors as $sponsor)
            <tr>
                <td>{{ $sponsor->fname }} {{ $sponsor->mname }} {{ $sponsor->lname }}</td>
                <td><a href="{{ URL::route('baptismalsponsor.destroy',$sponsor->id) }}" class="text-danger"><i class="mdi mdi-delete-forever mdi-18px"></i></a></td>
            </tr>
            @endforeach
        </table>
</blockquote>