@extends('mainlayout')

@section('maincontent')
        
        @include('partials._page_header',['icon'=>'mdi mdi-baby','title'=>'Baptismal Record','item1'=>'Baptismal','item2'=>'Create New Record'])        
        
        <div class="card">
          <div class="card-header"> New Record</div>
            <div class="card-body">
            {!! Form::open(['route'=>'baptismal.save']) !!}	            	    	
	        	@include('baptismal._form',['isCreate'=>'1'])

	        	<div class="pull-right">    			
            		<button type="submit" class="btn btn-gradient-info btn-icon-text"><i class="mdi mdi-floppy"></i> Save</button>            	
            		<button type="button" class="btn btn-gradient-primary btn-icon-text" disabled="disabled"><i class="mdi mdi-printer"></i> Print</button>
          		</div>
                  <br />
		        {!! Form::close() !!}	     
            </div>   	
	    </div>
        

@endsection

@section('script')
    @include('baptismal.modal._script')
@endsection

 