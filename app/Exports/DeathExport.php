<?php

namespace App\Exports;

use App\Death;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class DeathExport implements FromView, ShouldAutoSize
{
    
    public function view(): View
    {      
        return view('death.excel', ['deaths'=> Death::all()]);
    }
}
