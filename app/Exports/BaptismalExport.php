<?php

namespace App\Exports;

use App\Baptismal;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class BaptismalExport implements FromView, ShouldAutoSize
{
    
    public function view(): View
    {
        return view('baptismal.excel', ['baptismals'=> Baptismal::all()]);
    }
}

