<?php

namespace App\Exports;

use App\Confirmation;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ConfirmationExport implements FromView, ShouldAutoSize
{
    
    public function view(): View
    {      
        return view('confirmation.excel', ['confirmations'=> Confirmation::all()]);
    }
}
