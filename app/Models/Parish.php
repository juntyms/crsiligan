<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Parish extends Model
{
    protected $table = 'parishes';

    protected $fillable = ['name', 'diocese_id', 'city_id', 'address'];

    public function diocese()
    {
        return $this->belongsTo(Diocese::class, 'diocese_id', 'id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }
}