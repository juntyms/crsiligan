<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Barangay extends Model
{
    protected $table = 'barangays';

    protected $fillable = ['name', 'city_id', 'province_id', 'region_id'];

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function parish()
    {
        return $this->hasMany(Parish::class);
    }
}