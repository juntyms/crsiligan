<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Priest extends Model
{
    protected $table = 'priests';

    protected $fillable = ['title', 'firstname', 'middlename', 'lastname', 'affiliation_id', 'designation_id', 'licenseno', 'expiry', 'isAssignatory'];

    public function affiliation()
    {
        return $this->hasOne(Affiliation::class, 'id', 'affiliation_id');
    }

    public function designation()
    {
        return $this->hasOne(Designation::class, 'id', 'designation_id');
    }

    public function getexpiryAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('Y-m-d');
    }

    public function formexpiryAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('Y-m-d');
    }
}