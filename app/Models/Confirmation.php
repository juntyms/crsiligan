<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Confirmation extends Model
{
    protected $table = 'confirmations';

    protected $fillable = ['fname', 'mname', 'lname', 'gender', 'dob', 'dofconfirmation', 'age', 'fatherFn', 'fatherMn', 'fatherLn', 'motherFn', 'motherMn',
                            'motherLn', 'episcopalMinister_id', 'placeParish_id', 'placeCity_id', 'placeBarangay_id', 'volume', 'book', 'page', 'purpose',
                            'annotation', 'remarks', 'currentPriest_id', 'currentPriest_designationId', 'recordParish_id', 'dateIssue', 'created_by', 'updated_by', ];

    public function getdobAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d');
    }

    public function formdobAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d');
    }

    public function getdofconfirmationAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d');
    }

    public function formdofconfirmationAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d');
    }

    public function getdateIssueAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d');
    }

    public function formdateIssueAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d');
    }

    public function episminister()
    {
        return $this->hasOne(Priest::class, 'id', 'episcopalMinister_id');
    }

    public function placeparish()
    {
        return $this->hasOne(Parish::class, 'id', 'placeParish_id');
    }

    public function placecity()
    {
        return $this->hasOne(City::class, 'id', 'placeCity_id');
    }

    public function placebarangay()
    {
        return $this->hasOne(Barangay::class, 'id', 'placeBarangay_id');
    }

    public function sponsors()
    {
        return $this->hasMany(ConfirmationSponsor::class, 'confirmation_id', 'id');
    }
}