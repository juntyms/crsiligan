<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cemetery extends Model
{
    protected $table = 'cemeterys';

    protected $fillable = ['name'];
}