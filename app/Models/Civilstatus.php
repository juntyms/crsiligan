<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Civilstatus extends Model
{
    protected $table = 'civilstatus';

    protected $fillable = ['status'];
}