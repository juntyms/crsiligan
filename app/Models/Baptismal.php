<?php

namespace App\Models;

use Carbon\Carbon;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Database\Eloquent\Model;

class Baptismal extends Model
{
    protected $table = 'baptismals';

    protected $fillable = ['fname', 'mname', 'lname', 'gender', 'dob', 'pobstreet', 'pobBarangay_id', 'pobCity_id', 'fatherFn', 'fatherMn', 'fatherLn', 'motherFn', 'motherMn', 'motherLn',
        'baptizedby_id', 'dateofbaptism', 'placeStreet', 'placeBarangay_id', 'placeCity_id',
        'placeParish_id', 'purpose', 'annotation', 'remarks',
        'volume', 'book', 'page', 'ornumber', 'datepaid', 'dateIssue', 'currentPriest_id', 'currentPriest_designationId', 'recordParish_id', 'created_by', 'updated_by', ];

    protected $dates = ['dob', 'dateofbaptism', 'dateIssue'];

    use FormAccessible;

    /**
     * Get the  date of birth.
     *
     * @param string $value
     *
     * @return string
     */
    public function getdobAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d');
    }

    /**
     * Get the date of birth for forms.
     *
     * @param string $value
     *
     * @return string
     */
    public function formdobAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d');
    }

    /**
     * Get the dateofbaptism.
     */
    public function getdateofbaptismAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d');
    }

    /**
     * Get the dateofbaptism for forms.
     */
    public function formdateofbaptismAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d');
    }

    /**
     * Get the dateIssue.
     */
    public function getdateIssueAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d');
    }

    /**
     * Get the dateIssue for forms.
     */
    public function formdateIssueAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d');
    }

    public function pob_barangay()
    {
        return $this->hasOne(Barangay::class, 'id', 'pobBarangay_id');
    }

    public function pob_city()
    {
        return $this->hasOne(City::class, 'id', 'pobCity_id');
    }

    public function baptizedby()
    {
        return $this->hasOne(Priest::class, 'id', 'baptizedby_id');
    }

    public function place_barangay()
    {
        return $this->hasOne(Barangay::class, 'id', 'placeBarangay_id');
    }

    public function place_city()
    {
        return $this->hasOne(City::class, 'id', 'placeCity_id');
    }

    public function place_parish()
    {
        return $this->hasOne(Parish::class, 'id', 'placeParish_id');
    }

    public function sponsors()
    {
        return $this->hasMany(BaptismalSponsor::class, 'baptismal_id', 'id');
    }
}