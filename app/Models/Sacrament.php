<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sacrament extends Model
{
    protected $table = 'sacraments';
}