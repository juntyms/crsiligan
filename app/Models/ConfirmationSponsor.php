<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConfirmationSponsor extends Model
{
    protected $table = 'confirmation_sponsors';

    protected $fillable = ['confirmation_id', 'fname', 'mname', 'lname'];
}