<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BaptismalSponsor extends Model
{
    protected $table = 'baptismal_sponsors';

    protected $fillable = ['baptismal_id', 'fname', 'mname', 'lname'];
}