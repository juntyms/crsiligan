<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Marriage extends Model
{
    protected $guarded = [];

    public function hnationality()
    {
        return $this->hasOne(Nationality::class, 'id', 'hnationality_id');
    }

    public function wnationality()
    {
        return $this->hasOne(Nationality::class, 'id', 'wnationality_id');
    }

    public function hstatus()
    {
        return $this->hasOne(Civilstatus::class, 'id', 'hcstatus_id');
    }

    public function wstatus()
    {
        return $this->hasOne(Civilstatus::class, 'id', 'wcstatus_id');
    }

    public function hfnationality()
    {
        return $this->hasOne(Nationality::class, 'id', 'hfcitizenship');
    }

    public function wfnationality()
    {
        return $this->hasOne(Nationality::class, 'id', 'wfcitizenship');
    }

    public function hmnationality()
    {
        return $this->hasOne(Nationality::class, 'id', 'hmcitizenship');
    }

    public function wmnationality()
    {
        return $this->hasOne(Nationality::class, 'id', 'wmcitizenship');
    }
}