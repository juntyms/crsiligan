<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Death extends Model
{
    protected $table = 'deaths';

    protected $fillable = ['fname', 'mname', 'lname', 'age', 'dob', 'gender', 'civilstatus_id', 'street', 'barangay_id', 'city_id', 'minister_id',
                            'placeOfBurial', 'causeOfDeath', 'dateOfBurial', 'dateOfDeath', 'volume', 'page', 'book', 'burialPermitNo',
                            'currentPriest_id', 'currentPriest_designationId', 'purpose', 'annotation', 'remarks', 'informantFname', 'informantMname',
                            'informantLname', 'informantRelationship_id', 'informantAddress', 'informantBarangay_id', 'informantCity_id',
                            'dateIssue', 'created_by', 'updated_by', ];

    public function getdobAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d');
    }

    public function formdobAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d');
    }

    public function getdateOfDeathAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d');
    }

    public function formdateOfDeathAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d');
    }

    public function getdateOfBurialAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d');
    }

    public function formdateOfBurialAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d');
    }

    public function getdateIssueAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d');
    }

    public function formdateIssueAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d');
    }

    public function civilstatus()
    {
        return $this->hasOne(Civilstatus::class, 'id', 'civilstatus_id');
    }

    public function barangay()
    {
        return $this->hasOne(Barangay::class, 'id', 'barangay_id');
    }

    public function city()
    {
        return $this->hasOne(City::class, 'id', 'city_id');
    }

    public function minister()
    {
        return $this->hasOne(Priest::class, 'id', 'minister_id');
    }

    public function cemetery()
    {
        return $this->hasOne(Cemetery::class, 'id', 'placeOfBurial');
    }

    public function informantRelationship()
    {
        return $this->hasOne(Relationship::class, 'id', 'informantRelationship_id');
    }

    public function informantBarangay()
    {
        return $this->hasOne(Barangay::class, 'id', 'informantBarangay_id');
    }

    public function informantCity()
    {
        return $this->hasOne(City::class, 'id', 'informantCity_id');
    }
}