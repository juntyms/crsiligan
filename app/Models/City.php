<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'cities';

    protected $fillable = ['name', 'region_id', 'province_id'];

    public function barangay()
    {
        return $this->hasMany(Barangay::class);
    }
}