<?php
namespace App\Http;
use App\Http\Requests\Request;
use Illuminate\Support\Facades\Route;


class Helper
{
	public static function setActive($path, $active = 'active') {

		return Route::CurrentRouteNamed($path) ? $active : '';
		//return call_user_func_array('Request::is', (array)$path) ? $active : '';
	}
}