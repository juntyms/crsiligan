<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DeathRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fname'=>'required',
            'mname'=>'required',
            'lname'=>'required',
            'gender'=>'required',
            'dob'=>'required',
            'dateOfDeath'=>'required',
            'causeOfDeath'=>'required',
            'civilstatus_id'=>'required',
            'street'=>'required',
            'barangay_id'=>'required',
            'city_id'=>'required',
            'placeOfBurial'=>'required',
            'dateOfBurial'=>'required',
            'minister_id'=>'required',
            'informantFname'=>'required',
            'informantMname'=>'required',
            'informantLname'=>'required',
            'informantRelationship_id'=>'required',
            'informantBarangay_id'=>'required',
            'informantCity_id'=>'required',
            'burialPermitNo'=>'required'

        ];
    }

    public function messages()
    {
        return [
            'fname.required'=>'First Name Field Required',
            'mname.required'=>'Middle Name Field Required',
            'lname.required'=>'Last Name Field Required',
            'gender.required'=>'Gender Field Required',
            'dob.required'=>'Date of Birth Field Required',
            'dateOfDeath.required'=>'Date of Death Field Required',
            'causeOfDeath.required'=>'Cause of Death Field Required',
            'civilstatus_id.required'=>'Civil Status Field Required',
            'street.required'=>'Street Address Field Required',
            'barangay_id.required'=>'Barangay Field Required',
            'city_id.required'=>'City Field Required',
            'placeOfBurial.required'=>'Place of Burial Field Required',
            'dateOfBurial.required'=>'date of Burial Field Required',
            'minister_id.required'=>'Minister Field Required',
            'informantFname.required'=>'Informant First Name Field Required',
            'informantMname.required'=>'Informant Middle Name Field Required',
            'informantLname.required'=>'Informant Last Name Field Required',
            'informantRelationship_id.required'=>'Informant Relationship Field Required',
            'informantBarangay_id.required'=>'Informant Barangay Address Field Required',
            'informantCity_id.required'=>'Informant City Address Field Required',
            'burialPermitNo.required'=>'Burial Permit No is Required'
        ];
    }
}
