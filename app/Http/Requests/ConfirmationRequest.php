<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ConfirmationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fname'=>'required',
            'mname'=>'required',
            'lname'=>'required',
            'gender'=>'required',
            'dob'=>'required',
            'dofconfirmation'=>'required',
            'motherFn'=>'required',
            'motherMn'=>'required',
            'motherLn'=>'required',
            'episcopalMinister_id'=>'required',
            'placeCity_id'=>'required',
            'placeBarangay_id'=>'required',
            'placeParish_id'=>'required'
        ];
    }

    public function messages()
    {
        return [
            'fname.required'=>'First Name Required',
            'mname.required'=>'Middle Name Required',
            'lname.required'=>'Last name required',
            'gender.required'=>'Gender required',
            'dob.required'=>'Date of Birth required',
            'dofconfirmation.required'=>'Date of Confirmation required',
            'motherFn.required'=>'Mother First name required',
            'motherMn.required'=>'Mother Middle Name required',
            'motherLn.required'=>'Mother Last Namerequired',
            'episcopalMinister_id.required'=>'Episcopal Minister required',
            'placeCity_id.required'=>'City required',
            'placeBarangay_id.required'=>'Barangay required',
            'placeParish_id.required'=>'Parish / Chapel required'

        ];
    }
}
