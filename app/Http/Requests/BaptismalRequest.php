<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Carbon\carbon;

class BaptismalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fname'=>'required',            
            'lname'=>'required',
            'gender'=>'required',
            'dob'=>'required',
            'pobBarangay_id'=>'required',
            'pobCity_id'=>'required',            
            'motherFn' => 'required',
            'motherLn' => 'required',
            'placeCity_id' =>'required',
            'placeBarangay_id' =>'required'
        ];
    }

    // Note: This is not working
    /*
    public function all() {
        $data = parent::all();
        $data['datepaid'] = carbon::now();
        $data['dob'] = date('Y-m-d',strtotime($data['dob']));
        $data['dateofbaptism'] = date('Y-m-d',strtotime($data['dateofbaptism']));
        $data['dateIssue'] = date('Y-m-d',strtotime($data['dateIssue']));
        return $data;
    }
*/
    public function messages()
    {
        return [
            'fname.required' => 'First Name Required',
            'lname.required' => 'Last Name Required',
            'gender.required' => 'Gender Required',
            'dob.required' => 'Date of Birth Required',
            'pobBarangay_id.required' => 'Barangay Required',
            'pobCity_id.required' => 'City Required',
            'motherFn.required' => 'Mother First name Required',
            'motherLn.required' => 'Mother Last name Required'
        ];
    }
}
