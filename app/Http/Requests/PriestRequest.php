<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PriestRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required',
            'firstname'=>'required',            
            'lastname'=>'required',
            'affiliation_id'=>'required',
            'designation_id'=>'required'            
        ];
    }

    public function messages()
    {
        return [
            'affiliation_id.required' => 'Affiliation Required',
            'designation_id.required' => 'Designation Required'
        ];        
    }
}
