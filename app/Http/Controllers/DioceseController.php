<?php

namespace App\Http\Controllers;

use App\Http\Requests\DioceseRequest;
use App\Models\Diocese;

class DioceseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $dioceses = Diocese::all();

        return view('diocese.index')->with('dioceses', $dioceses);
    }

    public function create()
    {
        return view('diocese.create');
    }

    public function save(DioceseRequest $request)
    {
        Diocese::create($request->all());

        $request->session()->flash('success', 'Diocese Sucessfully Saved');

        return redirect()->route('diocese.index');
    }

    public function edit($id)
    {
        $diocese = Diocese::FindOrFail($id);

        return view('diocese.edit')->with('diocese', $diocese);
    }

    public function update(DioceseRequest $request, $id)
    {
        $diocese = Diocese::findOrFail($id);

        $diocese->update($request->all());

        $request->session()->flash('info', 'Diocese Successfully Updated');

        return redirect()->route('diocese.index');
    }
}