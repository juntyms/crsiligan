<?php

namespace App\Http\Controllers;

use App\Models\ConfirmationSponsor;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ConfirmationSponsorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function save(Request $request, $confirmId)
    {
        $sponsor = new ConfirmationSponsor();

        $sponsor->confirmation_id = $confirmId;
        $sponsor->fname = Str::upper($request->sponsor_fname);
        $sponsor->mname = Str::upper($request->sponsor_mname);
        $sponsor->lname = Str::upper($request->sponsor_lname);

        $sponsor->save();

        /* Show message */
        $request->session()->flash('success', 'Confirmation Successfully Saved');

        return redirect()->route('confirmation.edit', [$confirmId]);
    }

    public function delete($id)
    {
        $sponsor = ConfirmationSponsor::findOrFail($id);

        $confirmId = $sponsor->confirmation_id;

        $sponsor->delete();

        /* Show message */
        session()->flash('danger', 'Sponsor Deleted');

        return redirect()->route('confirmation.edit', [$confirmId]);
    }
}