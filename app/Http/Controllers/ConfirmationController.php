<?php

namespace App\Http\Controllers;

use Alert;
use App\Exports\ConfirmationExport;
use App\Http\Requests\ConfirmationRequest;
use App\Models\Affiliation;
use App\Models\Barangay;
use App\Models\City;
use App\Models\Confirmation;
use App\Models\ConfirmationSponsor;
use App\Models\Designation;
use App\Models\Diocese;
use App\Models\Parish;
use App\Models\Priest;
use App\Models\User;
use Auth;
use Carbon\carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ConfirmationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show()
    {
        $confirmations = Confirmation::all();

        return view('confirmation.show')->with('confirmations', $confirmations);
    }

    public function lists()
    {
        $confirmations = \DB::table('confirmations')
                        ->select('id', 'fname', 'mname', 'lname')
                        ->orderBy('id', 'desc')
                        ->get();

        return Response()->json($confirmations);
    }

    public function create()
    {
        $dioceses = Diocese::pluck('name', 'id');

        if ($dioceses->isEmpty()) {
            session()->flash('danger', 'Dioceses Not Available, Please add Dioceses');

            return redirect()->route('diocese.index');
        }

        $affiliations = Affiliation::select('affiliation', 'id')
                    ->orderBy('affiliation', 'asc')
                    ->pluck('affiliation', 'id');

        if ($affiliations->isEmpty()) {
            session()->flash('danger', 'Affiliations Not Available, Please Add Affiliation');

            return redirect()->route('affiliation.index');
        }

        $priests = Priest::select(\DB::raw('CONCAT(firstname," ",middlename," ", lastname) as priestname'), 'id')
                ->orderBy('priestname', 'asc')
                ->pluck('priestname', 'id');

        $barangays = [];

        $cities = City::select('name', 'id')
                        ->orderBy('name', 'asc')
                        ->pluck('name', 'id');

        $parishes = Parish::select('name', 'id')
                            ->orderBy('name', 'asc')
                            ->pluck('name', 'id');

        $currentPriest = Priest::where('isAssignatory', '=', '1')
                            ->first();

        $designations = Designation::select('designation', 'id')
                      ->orderBy('designation', 'asc')
                      ->pluck('designation', 'id');

        return view('confirmation.create')
                ->with('priests', $priests)
                ->with('parishes', $parishes)
                ->with('cities', $cities)
                ->with('barangays', $barangays)
                ->with('currentPriest', $currentPriest)
                ->with('designations', $designations)
                ->with('affiliations', $affiliations)
                ->with('dioceses', $dioceses);
    }

    public function save(ConfirmationRequest $request)
    {
        // check if volume book and page are empty or 0
        if ((empty($request['volume'])) || (empty($request['book'])) || (empty($request['page']))) {
            // if zero then check baptismal_counter table
            // analyse and generate counters and populate volume book and page
            $confirmation_counter = \DB::table('confirmation_counter')->first();

            $confirmation_book_count = \DB::table('confirmations')
                                    ->where('volume', '=', $confirmation_counter->volume)
                                    ->count();

            // Check if book count is still under the limit of allowed book records
            if ($confirmation_book_count < $confirmation_counter->book_per_volume) {
                // Check to total number of records in a page and compare if under the limit
                $confirmation_book_count_current_page = \DB::table('confirmations')
                                                        ->where('page', $confirmation_counter->page)
                                                        ->where('volume', $confirmation_counter->volume)
                                                        ->count();

                // Check if book per page is allowed
                if ($confirmation_book_count_current_page == $confirmation_counter->book_per_page) {
                    // increment Page Number
                    $new_page = $confirmation_counter->page + 1;

                    \DB::table('confirmation_counter')
                        ->update(['page' => $new_page]);
                }

                $book_no = $confirmation_book_count + 1; // increment to booknumber
            } else {
                // increment Volume Number and Reset Page Number
                $new_volume = $confirmation_counter->volume + 1;

                \DB::table('confirmation_counter')
                    ->update(['volume' => $new_volume, 'page' => '1']);

                $book_no = 1; //reset book number to 1
            }

            $counter_details = \DB::table('confirmation_counter')->first();

            $request['volume'] = $counter_details->volume;
            $request['page'] = $counter_details->page;
            $request['book'] = $book_no;
        }

        $request['created_by'] = Auth::user()->id;
        $request['updated_by'] = Auth::user()->id;

        $request['recordParish_id'] = Auth::user()->parishId;
        $request['dateIssue'] = Carbon::now();

        $request = array_map('ucwords', $request->all());

        $confirmation = Confirmation::create($request);

        /* Show message */
        session()->flash('success', 'Confirmation Successfully Saved Please Add the sponsors at the bottom page');

        return redirect()
            ->route('confirmation.edit', $confirmation->id);
    }

    public function edit($id)
    {
        $confirmation = Confirmation::findorFail($id);

        $sponsors = ConfirmationSponsor::where('confirmation_id', '=', $id)->get();

        $priests = Priest::select(\DB::raw('CONCAT(firstname," ",middlename," ", lastname) as priestname'), 'id')
            ->orderBy('priestname', 'asc')
            ->pluck('priestname', 'id');

        $barangays = Barangay::select('name', 'id')
                        ->where('city_id', '=', $confirmation->placeCity_id)
                        ->orderBy('name', 'asc')
                        ->pluck('name', 'id');

        $cities = City::select('name', 'id')
                    ->orderBy('name', 'asc')
                    ->pluck('name', 'id');

        $parishes = Parish::select('name', 'id')
                        ->orderBy('name', 'asc')
                        ->pluck('name', 'id');

        $currentPriest = Priest::where('isAssignatory', '=', '1')
                        ->first();

        $designations = Designation::select('designation', 'id')
                      ->orderBy('designation', 'asc')
                      ->pluck('designation', 'id');

        $affiliations = Affiliation::select('affiliation', 'id')
            ->orderBy('affiliation', 'asc')
            ->pluck('affiliation', 'id');

        $dioceses = Diocese::pluck('name', 'id');

        return view('confirmation.edit')
                ->with('confirmation', $confirmation)
                ->with('sponsors', $sponsors)
                ->with('priests', $priests)
                ->with('parishes', $parishes)
                ->with('cities', $cities)
                ->with('barangays', $barangays)
                ->with('currentPriest', $currentPriest)
                ->with('designations', $designations)
                ->with('affiliations', $affiliations)
                ->with('dioceses', $dioceses);
    }

    public function update(ConfirmationRequest $request, $id)
    {
        $request['updated_by'] = Auth::user()->id;

        $confirmation = Confirmation::findorFail($id);

        $confirmation->recordParish_id = Auth::user()->parishId;

        $data = array_map('ucwords', $request->all());

        $confirmation->update($data);

        if ($request->update == 1) { // check if button update is clicked
            /* Show message */
            $request->session()->flash('success', 'Confirmation Successfully Updated');

            return redirect()
                ->route('confirmation.edit', $id);
        } else { // button print is clicked
            //Check if current user is secretary
            if (Auth::user()->isSecretary == 0) {
                return redirect()->route('confirmation.edit', $id)
                            ->with('danger', 'Only Secretary can print this form');
            }

            // Check if option Secretary to be included on the printout
            $isSecretary = User::where('isSecretary', 1)
                                ->where('id', Auth::user()->id)
                                ->first();
            $printSec = 0;
            if ($request->has('isSecretary')) {
                $printSec = 1;
            }

            $initDateEncypt = env('APP_INIT_DATE', 'null');

            $initDate = decrypt($initDateEncypt);

            $startDate = Carbon::createFromTimestamp($initDate, 'Asia/Manila');

            $currentCount = $startDate->diffInDays(Carbon::now());

            $appType = env('APP_TYPE', 'null');

            $decrypted = decrypt($appType);

            if ($decrypted == 'DEMO') {
                if ($currentCount > 365) {
                    /* Show message */
                    $request->session()->flash('danger', 'System Expired - Print not Possible');

                    return redirect()->route('confirmation.edit', $id);
                }
            }

            $data = \DB::table('confirmations')
                    ->select('confirmations.fname','confirmations.mname','confirmations.gender','confirmations.age',
                        'confirmations.lname','confirmations.dob','barangays.name as barangayname','pobcities.name as pobcityname',
                        'confirmations.fatherFn', 'confirmations.fatherMn','confirmations.fatherLn',
                        'confirmations.motherFn', 'confirmations.motherMn', 'confirmations.motherLn',
                        'episcopal.title','episcopal.firstname','episcopal.middlename','episcopal.lastname',
                        'confirmations.dofconfirmation','placeparish.name as placeparish','placebar.name as placebarangay','placecities.name as placecity',
                        'signedby.title as sgdtitle','signedby.firstname as sgdfirstname','signedby.middlename as sgdmiddlename','signedby.lastname as sgdlastname',
                        'priestdesignation.designation','confirmations.purpose','confirmations.annotation','confirmations.remarks',
                        'confirmations.volume', 'confirmations.book', 'confirmations.page', 'confirmations.dateIssue')
                    ->leftjoin('barangays', 'barangays.id', '=', 'confirmations.placeBarangay_id')
                    ->leftjoin('cities as pobcities', 'pobcities.id', '=', 'placeCity_id')
                    ->leftjoin('barangays as placebar', 'placebar.id', '=', 'confirmations.placeBarangay_id')
                    ->leftjoin('cities as placecities', 'placecities.id', '=', 'confirmations.placeCity_id')
                    ->leftjoin('parishes as placeparish', 'placeparish.id', '=', 'confirmations.placeParish_id')
                    ->leftjoin('priests as episcopal', 'episcopal.id', '=', 'confirmations.episcopalMinister_id')
                    ->leftjoin('priests as signedby', 'signedby.id', '=', 'confirmations.currentPriest_id')
                    ->leftjoin('designations as priestdesignation', 'priestdesignation.id', '=', 'confirmations.currentPriest_designationId')
                    ->where('confirmations.id', '=', $id)
                    ->first();

            $sponsors = \DB::table('confirmation_sponsors')
                        ->take(4)
                        ->select('fname', 'mname', 'lname')
                        ->where('confirmation_id', $id)
                        ->get();

            $allsponsors = \DB::table('confirmation_sponsors')
                        ->select('fname', 'mname', 'lname')
                        ->where('confirmation_id', $id)
                        ->get();

            $pdf = \PDF::loadView('confirmation.pdf', ['data' => $data, 'sponsors' => $sponsors, 'allsponsors' => $allsponsors, 'isSecretary' => $isSecretary, 'printSec' => $printSec]);

            return $pdf->stream();
        }
    }

    public function excel()
    {
        /*
        $confirmations = Confirmation::all();

        return view('confirmation.excel')->with('confirmations',$confirmations);
        */
        return Excel::download(new ConfirmationExport(), 'confirmationrecords.xlsx');
    }

    public function search()
    {
        $confirmations = [];
        $oldsearchtext = '';

        return view('confirmation.search')
                ->with('confirmations', $confirmations)
                ->with('oldsearchtext', $oldsearchtext);
    }

    public function postsearch(Request $request)
    {
        if ($request->search_type == 1) { //Firstname
            $confirmations = \DB::table('confirmations')
                        ->select('id', 'fname', 'mname', 'lname')
                        ->where('fname', 'like', $request->searchtext.'%')
                        ->get();
        }

        if ($request->search_type == 2) { //Lastname
            $confirmations = \DB::table('confirmations')
                        ->select('id', 'fname', 'mname', 'lname')
                        ->where('lname', 'like', $request->searchtext.'%')
                        ->get();
        }

        if ($confirmations) {
            $count = $confirmations->count();

            Alert::success('Record Found', $count.' Record/s Found');
        } else {
            Alert::error('No Record Found', '0 Record/s Found');
        }

        return view('confirmation.search')
                ->with('confirmations', $confirmations)
                ->with('oldsearchtext', $request->searchtext);
    }
}