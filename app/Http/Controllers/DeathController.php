<?php

namespace App\Http\Controllers;

use Alert;
use App\Exports\DeathExport;
use App\Http\Requests\DeathRequest;
use App\Models\Affiliation;
use App\Models\Barangay;
use App\Models\Cemetery;
use App\Models\City;
use App\Models\Civilstatus;
use App\Models\Death;
use App\Models\Designation;
use App\Models\Priest;
use App\Models\Relationship;
use App\Models\Sacrament;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class DeathController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        $barangays = [];
        $ibarangays = [];

        $affiliations = Affiliation::select('affiliation', 'id')
                        ->orderBy('affiliation', 'asc')
                        ->pluck('affiliation', 'id');

        if ($affiliations->isEmpty()) {
            session()->flash('danger', 'Affiliations Not Available, Please Add Affiliation');

            return redirect()->route('affiliation.index');
        }

        $cities = City::select('name', 'id')
                    ->orderBy('name', 'asc')
                    ->pluck('name', 'id');

        if ($cities->isEmpty()) {
            session()->flash('danger', 'City Not Available, Please Add City');

            return redirect()->route('city.index');
        }

        $priests = Priest::select(\DB::raw('CONCAT(firstname," ",middlename," ", lastname) as priestname'), 'id')
            ->orderBy('priestname', 'asc')
            ->pluck('priestname', 'id');

        if ($priests->isEmpty()) {
            session()->flash('danger', 'Priest Not Available, Please Add Priest');

            return redirect()->route('priest.index');
        }

        $currentPriest = Priest::where('isAssignatory', '=', '1')
                        ->first();

        if (!$currentPriest) {
            session()->flash('danger', 'Priest Assignatory Not Available, Please Set Assignatory');

            return redirect()->route('priest.index');
        }

        $designations = Designation::select('designation', 'id')
                            ->orderBy('designation', 'asc')
                            ->pluck('designation', 'id');

        if ($designations->isEmpty()) {
            session()->flash('danger', 'Designations Not Available, Please Add Designation');

            return redirect()->route('designation.index');
        }

        $civilStatus = Civilstatus::select('status', 'id')
                            ->orderBy('status', 'asc')
                            ->pluck('status', 'id');

        $cemeterys = Cemetery::select('name', 'id')
                        ->orderBy('name', 'asc')
                        ->pluck('name', 'id');

        $relationships = Relationship::select('name', 'id')
                            ->orderBy('id', 'asc')
                            ->pluck('name', 'id');

        return view('death.create')
                ->with('civilStatus', $civilStatus)
                ->with('cities', $cities)
                ->with('barangays', $barangays)
                ->with('ibarangays', $ibarangays)
                ->with('priests', $priests)
                ->with('cemeterys', $cemeterys)
                ->with('relationships', $relationships)
                ->with('designations', $designations)
                ->with('currentPriest', $currentPriest)
                ->with('affiliations', $affiliations);
    }

    public function show()
    {
        $deaths = Death::all();

        return view('death.show')->with('deaths', $deaths);
    }

    public function lists()
    {
        $deaths = \DB::table('deaths')
                        ->select('id', 'fname', 'mname', 'lname')
                        ->orderBy('id', 'desc')
                        ->get();

        return Response()->json($deaths);
    }

    public function save(DeathRequest $request)
    {
        // check if volume book and page are empty or 0
        if ((empty($request['volume'])) || (empty($request['book'])) || (empty($request['page']))) {
            // if zero then check baptismal_counter table
            // analyse and generate counters and populate volume book and page
            $burial_counter = \DB::table('burial_counter')->first();

            $burial_book_count = \DB::table('deaths')
                                    ->where('volume', '=', $burial_counter->volume)
                                    ->count();

            // Check if book count is still under the limit of allowed book records
            if ($burial_book_count < $burial_counter->book_per_volume) {
                // Check to total number of records in a page and compare if under the limit
                $burial_book_count_current_page = \DB::table('deaths')
                                                        ->where('page', $burial_counter->page)
                                                        ->where('volume', $burial_counter->volume)
                                                        ->count();

                // Check if book per page is allowed
                if ($burial_book_count_current_page == $burial_counter->book_per_page) {
                    // increment Page Number
                    $new_page = $burial_counter->page + 1;

                    \DB::table('burial_counter')
                        ->update(['page' => $new_page]);
                }

                $book_no = $burial_book_count + 1; // increment to booknumber
            } else {
                // increment Volume Number and Reset Page Number
                $new_volume = $burial_counter->volume + 1;

                \DB::table('burial_counter')
                    ->update(['volume' => $new_volume, 'page' => '1']);

                $book_no = 1; //reset book number to 1
            }

            $counter_details = \DB::table('burial_counter')->first();

            $request['volume'] = $counter_details->volume;
            $request['page'] = $counter_details->page;
            $request['book'] = $book_no;
        }

        if (empty($request['dateIssue'])) {
            $request['dateIssue'] = \Carbon\carbon::now();
        }
        $request['created_by'] = Auth::user()->id;
        $request['updated_by'] = Auth::user()->id;

        $request = array_map('ucwords', $request->all());

        $death = Death::create($request);

        /* Show message */
        session()->flash('success', 'Death Record Successfully Saved');

        return redirect()->route('death.edit', $death->id);
    }

    public function edit($id)
    {
        $death = Death::findOrFail($id);

        $civilStatus = Civilstatus::select('status', 'id')
                            ->orderBy('status', 'asc')
                            ->pluck('status', 'id');

        $barangays = Barangay::select('name', 'id')
                        ->where('city_id', $death->city_id)
                        ->orderBy('name', 'asc')
                        ->pluck('name', 'id');

        $ibarangays = Barangay::select('name', 'id')
                ->where('city_id', $death->informantCity_id)
                ->orderBy('name', 'asc')
                ->pluck('name', 'id');

        $cities = City::select('name', 'id')
                        ->orderBy('name', 'asc')
                        ->pluck('name', 'id');

        $priests = Priest::select(\DB::raw('CONCAT(firstname," ",middlename," ", lastname) as priestname'), 'id')
            ->orderBy('priestname', 'asc')
            ->pluck('priestname', 'id');

        $cemeterys = Cemetery::select('name', 'id')
                        ->orderBy('name', 'asc')
                        ->pluck('name', 'id');

        $relationships = Relationship::select('name', 'id')
                            ->orderBy('name', 'asc')
                            ->pluck('name', 'id');

        $designations = Designation::select('designation', 'id')
                            ->orderBy('designation', 'asc')
                            ->pluck('designation', 'id');

        $currentPriest = Priest::where('id', '=', $death->currentPriest_id)
                        ->first();

        $sacramentlists = \DB::table('death_sacrament')
                            ->join('sacraments', 'sacraments.id', '=', 'death_sacrament.sacrament_id')
                            ->select('sacraments.sacrament', 'death_sacrament.death_id', 'death_sacrament.sacrament_id')
                            ->where('death_sacrament.death_id', '=', $id)
                            ->get();

        $sacraments = Sacrament::pluck('sacrament', 'id');

        $affiliations = Affiliation::select('affiliation', 'id')
                            ->orderBy('affiliation', 'asc')
                            ->pluck('affiliation', 'id');

        return view('death.edit', compact(['civilStatus',
                                                'barangays',
                                                'cities',
                                                'priests',
                                                'cemeterys',
                                                'relationships',
                                                'designations',
                                                'currentPriest',
                                                'death',
                                                'sacramentlists', 'sacraments', 'affiliations', 'ibarangays', ]));
    }

    public function update(DeathRequest $request, $id)
    {
        $request['updated_by'] = Auth::user()->id;

        $death = Death::findOrFail($id);

        $data = array_map('ucwords', $request->all());

        $death->update($data);

        if ($request->update == 1) { // check if button update is clicked
            /* Show message */
            $request->session()->flash('info', 'Death Record Successfully Updated');

            return redirect()
                ->route('death.edit', $id);
        } else { // button print is clicked
            //Check if current user is secretary
            if (Auth::user()->isSecretary == 0) {
                return redirect()->route('death.edit', $id)
                                ->with('danger', 'Only Secretary can print this form');
            }

            // Check if option Secretary to be included on the printout
            $isSecretary = User::where('isSecretary', 1)
                                    ->where('id', Auth::user()->id)
                                    ->first();
            $printSec = 0;
            if ($request->has('isSecretary')) {
                $printSec = 1;
            }

            $initDateEncrypt = env('APP_INIT_DATE', 'null');

            $initDate = decrypt($initDateEncrypt);

            $startDate = Carbon::createFromTimestamp($initDate, 'Asia/Manila');

            $currentCount = $startDate->diffInDays(Carbon::now());

            $appType = env('APP_TYPE', 'null');

            $decrypted = decrypt($appType);

            if ($decrypted == 'DEMO') {
                if ($currentCount > 365) {
                    /* Show message */
                    $request->session()->flash('danger', 'Sytem Expired - Print not possible');

                    return redirect()->route('death.edit', $id);
                }
            }

            $data = \DB::table('deaths')
                        ->select('deaths.fname', 'deaths.mname', 'deaths.lname','deaths.age','deaths.causeOfDeath',
                                'deaths.volume','deaths.book','deaths.page','deaths.gender',
                                'deaths.dateOfDeath','deaths.dateOfBurial','cemeterys.name as placeOfBurial',
                                'deaths.street','barangays.name as barangay','cities.name as city',
                                'deaths.informantFname','deaths.informantMname','deaths.informantLname',
                                'minister.title as ministerTitle','minister.firstname as ministerFname','minister.middlename as ministerMname','minister.lastname as ministerLname',
                                'deaths.purpose','deaths.annotation','deaths.remarks',
                                'signedby.title as sgdtitle','signedby.firstname as sgdfirstname','signedby.middlename as sgdmiddlename','signedby.lastname as sgdlastname',
                                'priestdesignation.designation', 'deaths.dateIssue')
                        ->leftjoin('priests as signedby', 'signedby.id', '=', 'deaths.currentPriest_id')
                        ->leftjoin('designations as priestdesignation', 'priestdesignation.id', '=', 'deaths.currentPriest_designationId')
                        ->leftjoin('cemeterys', 'cemeterys.id', '=', 'deaths.placeOfBurial')
                        ->leftjoin('barangays', 'barangays.id', '=', 'deaths.barangay_id')
                        ->leftjoin('cities', 'cities.id', '=', 'deaths.city_id')
                        ->leftjoin('priests as minister', 'minister.id', '=', 'deaths.minister_id')
                        ->where('deaths.id', '=', $id)
                        ->first();

            $sacraments = \DB::table('death_sacrament')
                            ->select('sacraments.sacrament')
                            ->leftjoin('sacraments', 'sacraments.id', '=', 'death_sacrament.sacrament_id')
                            ->where('death_sacrament.death_id', '=', $id)
                            ->get();

            $pdf = \PDF::loadView('death.pdf', ['data' => $data, 'sacraments' => $sacraments, 'isSecretary' => $isSecretary, 'printSec' => $printSec]);

            return $pdf->stream();
        }
    }

    public function savesacrament(Request $request, $deathId)
    {
        $check_sac = \DB::table('death_sacrament')
                    ->where('death_id', $deathId)
                    ->where('sacrament_id', $request->sacrament_id)
                    ->first();

        if (empty($check_sac)) {
            \DB::table('death_sacrament')
                ->insert(['death_id' => $deathId,
                          'sacrament_id' => $request->sacrament_id, ]);

            /* Show message */
            $request->session()->flash('success', 'Sacrament Added');
        } else {
            session()->flash('danger', 'Duplicate Sacrament');
        }

        return redirect()->route('death.edit', $deathId);
    }

    public function deletesac($deathId, $sacramentId)
    {
        \DB::table('death_sacrament')
            ->where('death_id', '=', $deathId)
            ->where('sacrament_id', '=', $sacramentId)
            ->delete();

        /* Show message */
        session()->flash('danger', 'Death Record Sacrament Successfully deleted');

        return redirect()->route('death.edit', $deathId);
    }

    public function excel()
    {
        /*
        $confirmations = Confirmation::all();

        return view('confirmation.excel')->with('confirmations',$confirmations);
        */
        return Excel::download(new DeathExport(), 'death.xlsx');
    }

    public function search()
    {
        $deaths = [];
        $oldsearchtext = '';

        return view('death.search')
                ->with('deaths', $deaths)
                ->with('oldsearchtext', $oldsearchtext);
    }

    public function postsearch(Request $request)
    {
        if ($request->search_type == 1) { //Firstname
            $deaths = \DB::table('deaths')
                        ->select('id', 'fname', 'mname', 'lname')
                        ->where('fname', 'like', $request->searchtext.'%')
                        ->get();
        }

        if ($request->search_type == 2) { //Lastname
            $deaths = \DB::table('deaths')
                        ->select('id', 'fname', 'mname', 'lname')
                        ->where('lname', 'like', $request->searchtext.'%')
                        ->get();
        }

        if ($deaths) {
            $count = $deaths->count();

            Alert::success('Record Found', $count.' Record/s Found');
        } else {
            Alert::error('No Record Found', '0 Record/s Found');
        }

        return view('death.search')
                ->with('deaths', $deaths)
                ->with('oldsearchtext', $request->searchtext);
    }
}