<?php

namespace App\Http\Controllers;

use App\Models\Designation;
use Illuminate\Http\Request;

class DesignationController extends Controller
{
    public function index()
    {
        $designations = Designation::all();

        return view('designation.index')->with('designations', $designations);
    }

    public function create()
    {
        return view('designation.create');
    }

    public function save(Request $request)
    {
        Designation::create($request->all());

        $request->session()->flash('success', 'Designation Successfully saved');

        return redirect()->route('designation.index');
    }

    public function edit($id)
    {
        $designation = Designation::findOrFail($id);

        return view('designation.edit')->with('designation', $designation);
    }

    public function update(Request $request, $id)
    {
        $designation = Designation::findOrFail($id);

        $designation->update($request->all());

        $request->session()->flash('info', 'Designation Successfully Updated');

        return redirect()->route('designation.index');
    }
}