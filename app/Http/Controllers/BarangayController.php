<?php

namespace App\Http\Controllers;

use App\Http\Requests\BarangayRequest;
use App\Models\Barangay;
use App\Models\City;
use Illuminate\Http\Request;

class BarangayController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($cityid)
    {
        $city = City::findOrFail($cityid);

        return view('barangay.index')
                    ->with('city', $city);
    }

    public function create($cityid)
    {
        $cities = City::pluck('name', 'id');

        return view('barangay.create')
                    ->with('cities', $cities)
                    ->with('cityid', $cityid);
    }

    public function save(BarangayRequest $request)
    {
        /* Save Record using laravel orm */
        Barangay::create($request->all());

        /* Show message */
        $request->session()->flash('success', 'Barangay Successfully Added');

        /* Redirect to barangay list */
        return redirect()->route('barangay.index', $request->city_id);
    }

    public function savebarangay(BarangayRequest $request)
    {
        Barangay::create($request->all());

        $response = [
            'status' => 'success',
            'msg' => 'Setting created successfully',
        ];

        return \Response::json($response);
    }

    public function list($cityid)
    {
        $barangays = Barangay::where('city_id', $cityid)
                        ->select('id', 'name')->get();

        return \Response::json($barangays);
    }

    public function edit($id)
    {
        $barangay = Barangay::findOrFail($id);

        return view('barangay.edit')->with('barangay', $barangay);
    }

    public function update(Request $request, $id)
    {
        $barangay = Barangay::findOrFail($id);

        $barangay->update($request->all());

        session()->flash('success', 'Barangay Successfully Updated');

        return redirect()->route('barangay.index', $barangay->city_id);
    }
}