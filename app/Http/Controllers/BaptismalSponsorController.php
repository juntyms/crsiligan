<?php

namespace App\Http\Controllers;

use App\Models\BaptismalSponsor;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class BaptismalSponsorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function save(Request $request)
    {
        $BapSponsor = new BaptismalSponsor();

        $BapSponsor->baptismal_id = $request->sponsor_bapid;

        $BapSponsor->fname = Str::upper($request->sponsor_fname);

        $BapSponsor->mname = Str::upper($request->sponsor_mname);

        $BapSponsor->lname = Str::upper($request->sponsor_lname);

        $BapSponsor->save();

        $request->session()->flash('success', 'Sponsor Added');

        return redirect()->route('baptismal.edit', [$request->sponsor_bapid]);
    }

    public function destroy($id)
    {
        $BapSponsor = BaptismalSponsor::findOrFail($id);

        $Bapid = $BapSponsor->baptismal_id;

        $BapSponsor->delete();

        session()->flash('danger', 'Sponsor Deleted');

        return redirect()->route('baptismal.edit', [$Bapid]);
    }
}