<?php

namespace App\Http\Controllers;

use App\Models\Cemetery;
use Illuminate\Http\Request;

class CemeteryController extends Controller
{
    public function index()
    {
        $cemeterys = Cemetery::all();

        return view('cemetery.index')->with('cemeterys', $cemeterys);
    }

    public function new()
    {
        return view('cemetery.new');
    }

    public function save(Request $request)
    {
        Cemetery::Create($request->all());

        /* Show message */
        $request->session()->flash('success', 'Cemetery Successfully Added');

        return redirect()
                    ->route('cemetery.index');
    }

    public function edit($id)
    {
        $cemetery = Cemetery::findOrFail($id);

        return view('cemetery.edit')->with('cemetery', $cemetery);
    }

    public function update(Request $request, $id)
    {
        $cemetery = Cemetery::findOrFail($id);

        $cemetery->update($request->all());

        /* Show message */
        $request->session()->flash('info', 'Cemetery Successfully Updated');

        return redirect()->route('cemetery.index');
    }

    public function lists()
    {
        $cemeterys = \DB::table('cemeterys')
                        ->select('id', 'name')
                        ->orderBy('name', 'asc')
                        ->get();

        return Response()->json($cemeterys);
    }
}