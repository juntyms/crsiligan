<?php

namespace App\Http\Controllers;

use App\Http\Requests\MarriageRequest;
use App\Models\Civilstatus;
use App\Models\Marriage;
use App\Models\MarriageSponsor;
use App\Models\Nationality;
use Illuminate\Http\Request;

class MarriageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function search()
    {
        $marriages = [];
        $recordsFound = '';

        return view('marriage.search')
                ->with('marriages', $marriages)
                ->with('recordsFound', $recordsFound);
    }

    public function searchresults(Request $request)
    {
        //Search By Husband
        if ($request->searchtype == 1) {
            if ($request->searchtxt == 1) { // First Name
                $marriages = \DB::table('marriages')
                                ->select('id', 'hfname', 'hmname', 'hlname', 'wfname', 'wmname', 'wlname')
                                ->where('hfname', 'Like', '%'.$request->search.'%')
                                ->get();
            }

            if ($request->searchtxt == 2) { //Last Name
                $marriages = \DB::table('marriages')
                                ->select('id', 'hfname', 'hmname', 'hlname', 'wfname', 'wmname', 'wlname')
                                ->where('hlname', 'Like', '%'.$request->search.'%')
                                ->get();
            }
        }

        //Search By Wife
        if ($request->searchtype == 2) {
            if ($request->searchtxt == 1) { // First Name
                $marriages = \DB::table('marriages')
                                ->select('id', 'hfname', 'hmname', 'hlname', 'wfname', 'wmname', 'wlname')
                                ->where('wfname', 'Like', '%'.$request->search.'%')
                                ->get();
            }

            if ($request->searchtxt == 2) { //Last Name
                $marriages = \DB::table('marriages')
                                ->select('id', 'hfname', 'hmname', 'hlname', 'wfname', 'wmname', 'wlname')
                                ->where('wlname', 'Like', '%'.$request->search.'%')
                                ->get();
            }
        }

        if (empty($marriages)) {
            $recordsFound = 'No Records Found';
        } else {
            $recordsFound = count($marriages).' Records Found';
        }

        return view('marriage.search')
                ->with('marriages', $marriages)
                ->with('recordsFound', $recordsFound);
    }

    public function create()
    {
        $nationalities = Nationality::pluck('Nationality', 'id');

        $status = Civilstatus::pluck('status', 'id');

        return view('marriage.create')
                    ->with('nationalities', $nationalities)
                ->with('status', $status);
    }

    public function save(MarriageRequest $request)
    {
        $marriage = Marriage::create($request->all());

        alert()->success('Record Saved', 'Add Sponsors');

        return redirect()->route('marriage.edit', $marriage->id);
    }

    public function edit($id)
    {
        $nationalities = Nationality::pluck('Nationality', 'id');

        $status = Civilstatus::pluck('status', 'id');

        $marriage = Marriage::FindOrFail($id);

        $sponsors = MarriageSponsor::where('marriage_id', $id)->get();

        return view('marriage.edit')
            ->with('marriage', $marriage)
            ->with('nationalities', $nationalities)
            ->with('status', $status)
            ->with('sponsors', $sponsors);
    }

    public function update(MarriageRequest $request, $id)
    {
        $marriage = Marriage::findOrFail($id);

        $sponsors = MarriageSponsor::where('marriage_id', $id)->get();

        if ($request->printButton == 1) {
            if (count($sponsors) > 0) {
                $witness = \DB::table('marriage_sponsors')->take(2)->where('marriage_id', $id)->get();

                $witnessfirst = \DB::table('marriage_sponsors')->take(4)->where('marriage_id', $id)->get();

                $witnesssecond = \DB::table('marriage_sponsors')->skip(4)->take(4)->where('marriage_id', $id)->get();

                $pdf = \PDF::loadView('marriage.pdf', ['marriage' => $marriage, 'witness' => $witness, 'witnessfirst' => $witnessfirst, 'witnesssecond' => $witnesssecond])->setPaper('legal', 'portrait');

                return $pdf->stream();
            } else {
                return redirect()
                    ->route('marriage.edit', $id)
                    ->with('danger', 'Unable to print No Sponsor Found');
            }
        } else {
            $marriage->update($request->all());

            alert()->success('Updated', 'Record Updated');

            return redirect()->route('marriage.edit', $id);
        }
    }
}