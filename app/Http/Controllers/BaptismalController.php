<?php

namespace App\Http\Controllers;

use Alert;
use App\Exports\BaptismalExport;
use App\Http\Requests\BaptismalRequest;
use App\Models\Affiliation;
use App\Models\Baptismal;
use App\Models\BaptismalSponsor;
use App\Models\Barangay;
use App\Models\City;
use App\Models\Designation;
use App\Models\Diocese;
use App\Models\Parish;
use App\Models\Priest;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

class BaptismalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        $bapBarangays = [];

        $placeBarangays = [];

        $affiliations = Affiliation::select('affiliation', 'id')
                        ->orderBy('affiliation', 'asc')
                        ->pluck('affiliation', 'id');

        if ($affiliations->isEmpty()) {
            session()->flash('danger', 'Affiliations Not Available, Please Add Affiliation');

            return redirect()->route('affiliation.index');
        }

        $cities = City::select('name', 'id')
                    ->orderBy('name', 'asc')
                    ->pluck('name', 'id');

        if ($cities->isEmpty()) {
            session()->flash('danger', 'City Not Available, Please Add City');

            return redirect()->route('city.index');
        }

        $dioceses = Diocese::pluck('name', 'id');

        if ($dioceses->isEmpty()) {
            session()->flash('danger', 'Dioceses Not Available, Please add Dioceses');

            return redirect()->route('diocese.index');
        }

        $parishes = Parish::select('name', 'id')
                        ->orderBy('name', 'asc')
                        ->pluck('name', 'id');

        if ($parishes->isEmpty()) {
            session()->flash('danger', 'Parish Not Available, Please Add Parish');

            return redirect()->route('parish.index');
        }

        $priests = Priest::select(\DB::raw('CONCAT(firstname," ",middlename," ", lastname) as priestname'), 'id')
            ->orderBy('priestname', 'asc')
            ->pluck('priestname', 'id');

        if ($priests->isEmpty()) {
            session()->flash('danger', 'Priest Not Available, Please Add Priest');

            return redirect()->route('priest.index');
        }

        $currentPriest = Priest::where('isAssignatory', '=', '1')
                        ->first();

        if (!$currentPriest) {
            session()->flash('danger', 'Priest Assignatory Not Available, Please Set Assignatory');

            return redirect()->route('priest.index');
        }

        $designations = Designation::select('designation', 'id')
                        ->orderBy('designation', 'asc')
                        ->pluck('designation', 'id');

        if ($designations->isEmpty()) {
            session()->flash('danger', 'Designations Not Available, Please Add Designation');

            return redirect()->route('designation.index');
        }

        return view('baptismal.create')
                ->with('dioceses', $dioceses)
                ->with('cities', $cities)
            //	->with('barangays',$bapBarangays)
                ->with('parishes', $parishes)
                ->with('priests', $priests)
                ->with('currentPriest', $currentPriest)
                ->with('designations', $designations)
                ->with('bapBarangays', $bapBarangays)
                ->with('placeBarangays', $placeBarangays)
                ->with('affiliations', $affiliations);
    }

    public function save(BaptismalRequest $request)
    {
        // check if volume book and page are empty or 0
        if ((empty($request['volume'])) || (empty($request['book'])) || (empty($request['page']))) {
            // if zero then check baptismal_counter table
            // analyse and generate counters and populate volume book and page
            $baptismal_counter = \DB::table('baptismal_counter')->first();

            $baptismal_book_count = \DB::table('baptismals')
                                    ->where('volume', '=', $baptismal_counter->volume)
                                    ->count();

            // Check if book count is still under the limit of allowed book records
            if ($baptismal_book_count < $baptismal_counter->book_per_volume) {
                // Check to total number of records in a page and compare if under the limit
                $baptismal_book_count_current_page = \DB::table('baptismals')
                                                        ->where('page', $baptismal_counter->page)
                                                        ->where('volume', $baptismal_counter->volume)
                                                        ->count();

                // Check if book per page is allowed
                if ($baptismal_book_count_current_page == $baptismal_counter->book_per_page) {
                    // increment Page Number
                    $new_page = $baptismal_counter->page + 1;

                    \DB::table('baptismal_counter')
                        ->update(['page' => $new_page]);
                }

                $book_no = $baptismal_book_count + 1; // increment to booknumber
            } else {
                // increment Volume Number and Reset Page Number
                $new_volume = $baptismal_counter->volume + 1;

                \DB::table('baptismal_counter')
                    ->update(['volume' => $new_volume, 'page' => '1']);

                $book_no = 1; //reset book number to 1
            }

            $counter_details = \DB::table('baptismal_counter')->first();

            $request['volume'] = $counter_details->volume;
            $request['page'] = $counter_details->page;
            $request['book'] = $book_no;
        }

        $request = array_map('ucwords', $request->all());
        //dd($request);

        $request['created_by'] = Auth::user()->id;
        $request['updated_by'] = Auth::user()->id;
        $request['recordParish_id'] = Auth::user()->parishId;
        $request['dateIssue'] = Carbon::now();

        //dd($request);

        $baptismal = Baptismal::create($request);

        session()->flash('success', 'Baptismal Successfully Saved - Please Add the Sponsors at the bottom page');

        return redirect()
            ->route('baptismal.edit', $baptismal->id);
    }

    public function show()
    {
        /*$baptismals = Baptismal::paginate(20);
    	return view('baptismal.show')
    		->with('baptismals',$baptismals); */

        $baptismals = Baptismal::all();

        return view('baptismal.show')->with('baptismals', $baptismals);
    }

    public function lists()
    {
        $baptismals = \DB::table('baptismals')
                        ->select('id', 'fname', 'mname', 'lname')
                        ->orderBy('id', 'desc')
                        ->get();

        return Response()->json($baptismals);
    }

    public function update(BaptismalRequest $request, $id)
    {
        $baptismal = Baptismal::findorFail($id);

        //dd($request->all());
        if ($request->update == 1) { // check if button update is clicked
        $request = array_map('ucwords', $request->all());

            $request['updated_by'] = Auth::user()->id;

            $baptismal->recordParish_id = Auth::user()->parishId;

            $baptismal->update($request);

            return redirect()
                ->route('baptismal.edit', $id)
                ->with('success', 'Baptismal Successfully Updated');
        } else { // button print is clicked
            //Check if current user is secretary
            if (Auth::user()->isSecretary == 0) {
                return redirect()->route('baptismal.edit', $id)
                                    ->with('danger', 'Only Secretary can print this form');
            }

            // Check if option Secretary to be included on the printout
            $isSecretary = User::where('isSecretary', 1)
                                    ->where('id', Auth::user()->id)
                                    ->first();
            $printSec = 0;
            if ($request->has('isSecretary')) {
                $printSec = 1;
            }

            $initDateEncypt = env('APP_INIT_DATE', 'null');

            $initDate = decrypt($initDateEncypt);

            $startDate = Carbon::createFromTimestamp($initDate, 'Asia/Manila');

            $currentCount = $startDate->diffInDays(Carbon::now());

            $appType = env('APP_TYPE', 'null');

            $decrypted = decrypt($appType);

            //dd($decrypted);

            if ($decrypted == 'DEMO') {
                if ($currentCount > 365) {
                    return redirect()->route('baptismal.edit', $id)
                                        ->with('danger', 'System Expired Print not possible');
                }
            }

            $data = \DB::table('baptismals')
                    ->select('baptismals.fname','baptismals.mname','baptismals.gender',
                        'baptismals.lname','baptismals.dob','barangays.name as barangayname',
                        'baptismals.pobstreet','pobcities.name as pobcityname',
                        'baptismals.fatherFn', 'baptismals.fatherMn','baptismals.fatherLn',
                        'baptismals.motherFn', 'baptismals.motherMn', 'baptismals.motherLn',
                        'bapby.title','bapby.firstname','bapby.middlename','bapby.lastname',
                        'baptismals.dateofbaptism','placeparish.name as placeparish','placebar.name as placebarangay','placecities.name as placecity',
                        'signedby.title as sgdtitle','signedby.firstname as sgdfirstname','signedby.middlename as sgdmiddlename','signedby.lastname as sgdlastname',
                        'priestdesignation.designation','baptismals.purpose','baptismals.annotation','baptismals.remarks',
                        'baptismals.volume', 'baptismals.book', 'baptismals.page', 'baptismals.dateIssue')
                    ->leftjoin('barangays', 'barangays.id', '=', 'baptismals.pobBarangay_id')
                    ->leftjoin('cities as pobcities', 'pobcities.id', '=', 'pobCity_id')
                    ->leftjoin('barangays as placebar', 'placebar.id', '=', 'baptismals.placeBarangay_id')
                    ->leftjoin('cities as placecities', 'placecities.id', '=', 'baptismals.placeCity_id')
                    ->leftjoin('parishes as placeparish', 'placeparish.id', '=', 'baptismals.placeParish_id')
                    ->leftjoin('priests as bapby', 'bapby.id', '=', 'baptismals.baptizedby_id')
                    ->leftjoin('priests as signedby', 'signedby.id', '=', 'baptismals.currentPriest_id')
                    ->leftjoin('designations as priestdesignation', 'priestdesignation.id', '=', 'baptismals.currentPriest_designationId')
                    ->where('baptismals.id', '=', $id)
                    ->first();

            $sponsors = \DB::table('baptismal_sponsors')
                        ->take(4)
                        ->select('fname', 'mname', 'lname')
                        ->where('baptismal_id', $id)
                        ->get();

            $allsponsors = \DB::table('baptismal_sponsors')
                        ->select('fname', 'mname', 'lname')
                        ->where('baptismal_id', $id)
                        ->get();

            $sponsorCount = $allsponsors->count();

            //return view('baptismal.pdf',['data'=>$data,'sponsors'=>$sponsors,'allsponsors'=>$allsponsors,'isSecretary'=>$isSecretary,'printSec'=>$printSec,'sCount'=>$sponsorCount]);

            $pdf = PDF::loadView('baptismal.pdf', ['data' => $data, 'sponsors' => $sponsors, 'allsponsors' => $allsponsors, 'isSecretary' => $isSecretary, 'printSec' => $printSec, 'sCount' => $sponsorCount]);

            return $pdf->stream();
            //return $pdf->download('baptismal.pdf');
                        //$pdf->writeHTML($html, true, false, true, false, '');

            //$pdf = new TCPDF::writeHTML(view('baptismal.pdf',['data'=>$data,'sponsors'=>$sponsors,'allsponsors'=>$allsponsors,'isSecretary'=>$isSecretary,'printSec'=>$printSec,'sCount'=>$sponsorCount])->render());
        }
    }

    public function edit($id)
    {
        $barangays = Barangay::select('name', 'id')
                    ->orderBy('name', 'asc')
                    ->pluck('name', 'id');

        $cities = City::select('name', 'id')
                    ->orderBy('name', 'asc')
                    ->pluck('name', 'id');

        $dioceses = Diocese::pluck('name', 'id');

        $parishes = Parish::select('name', 'id')
                        ->orderBy('name', 'asc')
                        ->pluck('name', 'id');

        $priests = Priest::select(\DB::raw('CONCAT(firstname," ",middlename," ", lastname) as priestname'), 'id')
            ->orderBy('firstname', 'asc')
            ->orderBy('middlename', 'asc')
            ->orderBy('lastname', 'asc')
            ->pluck('priestname', 'id');

        $designations = Designation::select('designation', 'id')
                        ->orderBy('designation', 'asc')
                        ->pluck('designation', 'id');

        $baptismal = Baptismal::findorFail($id);

        $sponsors = BaptismalSponsor::where('baptismal_id', '=', $id)->get();

        $currentPriest = Priest::where('isAssignatory', '=', '1')
                        ->first();

        $bapBarangays = Barangay::select('name', 'id')
                    ->where('city_id', $baptismal->pobCity_id)
                    ->orderBy('name', 'asc')
                    ->pluck('name', 'id');

        $placeBarangays = Barangay::select('name', 'id')
                    ->where('city_id', $baptismal->placeCity_id)
                    ->orderBy('name', 'asc')
                    ->pluck('name', 'id');

        $affiliations = Affiliation::select('affiliation', 'id')
                    ->orderBy('affiliation', 'asc')
                    ->pluck('affiliation', 'id');

        return view('baptismal.edit')
                ->with('cities', $cities)
                ->with('barangays', $barangays)
                ->with('parishes', $parishes)
                ->with('priests', $priests)
                ->with('designations', $designations)
                ->with('baptismal', $baptismal)
                ->with('sponsors', $sponsors)
                ->with('currentPriest', $currentPriest)
                ->with('bapBarangays', $bapBarangays)
                ->with('placeBarangays', $placeBarangays)
                ->with('affiliations', $affiliations)
                ->with('dioceses', $dioceses);
    }

    public function pdf($id)
    {
        $data = \DB::table('baptismals')
                    ->select('baptismals.fname','baptismals.mname',
                        'baptismals.lname','baptismals.dob','barangays.name as barangayname',
                        'baptismals.fatherFn', 'baptismals.fatherMn','baptismals.fatherLn',
                        'baptismals.motherFn', 'baptismals.motherMn', 'baptismals.motherLn',
                        'bapby.title','bapby.firstname','bapby.middlename','bapby.lastname',
                        'baptismals.dateofbaptism','placeparish.name as placeparish','placebar.name as placebarangay','placecities.name as placecity',
                        'signedby.title as sgdtitle', 'signedby.firstname as sgdfirstname', 'signedby.middlename as sgdmiddlename', 'signedby.lastname sgdlastname')
                    ->leftjoin('barangays', 'barangays.id', '=', 'baptismals.pobBarangay_id')
                    ->leftjoin('barangays as placebar', 'placebar.id', '=', 'baptismals.placeBarangay_id')
                    ->leftjoin('cities as placecities', 'placecities.id', '=', 'baptismals.placeCity_id')
                    ->leftjoin('parishes as placeparish', 'placeparish.id', '=', 'baptismals.placeParish_id')
                    ->leftjoin('priests as bapby', 'bapby.id', '=', 'baptismals.baptizedby_id')
                    ->leftjoin('priests as signedby', 'signedby.id', '=', 'baptismals.currentPriest_id')
                    ->where('baptismals.id', '=', $id)
                    ->first();

        $sponsors = \DB::table('baptismal_sponsors')
                        ->take(2)
                        ->select('fname', 'mname', 'lname')
                        ->where('baptismal_id', $id)
                        ->get();

        //$pdf=\PDF::loadView('baptismal.pdf',array('data'=>$data));
        //return $pdf->download('baptismal.pdf');

        //return view('baptismal.pdf')->with('data',$data);

        // $pdf = \App::make('dompdf.wrapper');
        $pdf = \PDF::loadView('baptismal.pdf', ['data' => $data, 'sponsors' => $sponsors]);

        return $pdf->stream();

        //$pdf->writeHTML(view('your.view')->render());

        //$pdf = \TCPDF::writeHtml(view('pdf.invoice',['data'=>$data,'sponsors'=>$sponsors])->render());
    }

    public function excel()
    {
        /*
        $baptismals = Baptismal::all();

        return view('baptismal.excel')->with('baptismals',$baptismals);
        */
        return Excel::download(new BaptismalExport(), 'baptismalrecords.xlsx');
    }

    public function search()
    {
        $baptismals = [];
        $oldsearchtext = '';

        return view('baptismal.search')
                ->with('baptismals', $baptismals)
                ->with('oldsearchtext', $oldsearchtext);
    }

    public function postsearch(Request $request)
    {
        if ($request->search_type == 1) { //Firstname
            $baptismals = \DB::table('baptismals')
                        ->select('id', 'fname', 'mname', 'lname')
                        ->where('fname', 'like', $request->searchtext.'%')
                        ->get();
        }

        if ($request->search_type == 2) { //Lastname
            $baptismals = \DB::table('baptismals')
                        ->select('id', 'fname', 'mname', 'lname')
                        ->where('lname', 'like', $request->searchtext.'%')
                        ->get();
        }

        if ($baptismals) {
            $count = $baptismals->count();

            Alert::success('Record Found', $count.' Record/s Found');
        } else {
            Alert::error('No Record Found', '0 Record/s Found');
        }

        return view('baptismal.search')
                ->with('baptismals', $baptismals)
                ->with('oldsearchtext', $request->searchtext);
    }
}