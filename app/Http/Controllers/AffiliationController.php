<?php

namespace App\Http\Controllers;

use App\Models\Affiliation;
use Illuminate\Http\Request;

class AffiliationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $affiliations = Affiliation::all();

        return view('affiliation.index')
                        ->with('affiliations', $affiliations);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('affiliation.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function save(Request $request)
    {
        Affiliation::create($request->all());

        $request->session()->flash('success', 'Affiliation Saved');

        return redirect()->route('affiliation.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $affiliation = Affiliation::findOrFail($id);

        return view('affiliation.edit')->with('affiliation', $affiliation);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $affiliation = Affiliation::findOrFail($id);

        $affiliation->update($request->all());

        $request->session()->flash('success', 'Affiliation Updated');

        return redirect()->route('affiliation.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
    }
}