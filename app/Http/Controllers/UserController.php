<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\Baptismal;
use App\Models\Confirmation;
use App\Models\Death;
use App\Models\User;
use Auth;
use Carbon\carbon;
use Hash;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        $total_baptismal = Baptismal::all()->count();
        $total_confirmation = Confirmation::all()->count();
        $total_death = Death::all()->count();

        $bap = \DB::table('baptismals')->latest()->first();
        $con = \DB::table('confirmations')->latest()->first();
        $death = \DB::table('deaths')->latest()->first();

        return view('index')
                ->with('total_baptismal', $total_baptismal)
                ->with('total_confirmation', $total_confirmation)
                ->with('total_death', $total_death)
                ->with('bap', $bap)
                ->with('con', $con)
                ->with('death', $death);
    }

    public function userlist()
    {
        //Show list of Users
        $admin = \DB::table('users')->where('id', Auth::user()->id)->first();
        if ($admin->isAdmin == 1) {
            $users = \DB::table('users')
                    ->select('users.id','users.username','users.firstname','users.middlename','users.lastname',
                            'users.isUser','users.isSecretary','users.isPriest','users.isFinance','users.isAdmin',
                            'parishes.name as parish')
                    ->join('parishes', 'parishes.id', '=', 'users.parishId')
                    ->get();

            return view('user.index')->with('users', $users);
        }

        /* Show message */
        session()->flash('danger', 'Only System Administrator can access this section');

        return redirect()->route('home');
    }

    public function getLogin()
    {
        /**
         *  Crypt::encrypt('DEMO')
         *  strtotime('2017-01-01 15:00:00').
         */
        //$encrypted = encrypt('1551423600');

        $appType = env('APP_TYPE', 'null');

        if ($appType == null) {
            abort(401, 'Improper System Setup - Contact Developer');
        }

        $decrypted = decrypt($appType);

        if ($decrypted == 'DEMO') {
            $initDateEncypt = env('APP_INIT_DATE', 'null');

            try {
                $initDate = decrypt($initDateEncypt);
            } catch (DecryptException $e) {
                abort(401, 'Improper Date Setup - Contact Developer');
            }

            if ($initDate == null) {
                abort(401, 'Date not setup properly');
            }

            $startDate = Carbon::createFromTimestamp($initDate, 'Asia/Manila');

            $currentCount = $startDate->diffInDays(Carbon::now());

            $expired = true;

            if ($currentCount < 365) {
                $expired = false;
            }
            $demo = true;
            $leftDays = 365 - $currentCount;
            $daysLeft = Carbon::now()->addDays($leftDays)->diffForHumans();
            //dd($daysLeft);
            return view('user.login')
                    ->with('demo', $demo)
                    ->with('daysLeft', $daysLeft)
                    ->with('expired', $expired);

            //abort(401,'Demo Version Expired Contact Developer');
        }

        if ($decrypted == 'FULL') {
            $demo = false;
            $daysLeft = 0;
            $expired = false;

            return view('user.login')
                        ->with('demo', $demo)
                        ->with('daysLeft', $daysLeft)
                        ->with('expired', $expired);
        }

        abort(401, 'Improper System Setup - Contact Developer');
    }

    public function postLogin(Request $request)
    {
        if (Auth::attempt(['username' => $request->username, 'password' => $request->password])) {
            if (Auth::user()->parishId > 0) {
                return redirect()->route('home');
            }
            Auth::logout();

            return redirect()->route('login')->withDanger('User has no default parish assignment');
        }

        return redirect()->route('login')
            ->withDanger('Invalid Login Credentials')
            ->withInput();
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('home');
    }

    public function changepassword()
    {
        return view('user.changepassword');
    }

    public function updatepassword(Request $request)
    {
        $userPassword = \DB::table('users')->select('password')->where('id', Auth::user()->id)->first();

        $hashedPassword = $userPassword->password;

        if (Hash::check($request->currentPassword, $hashedPassword)) {
            // The passwords match...
            if ($request->newPassword == $request->confirmNewPassword) {
                // Validate the new password length...
                if ($request->has('newPassword')) {
                    $user = User::findOrFail(Auth::user()->id);
                    $user->fill([
                        'password' => Hash::make($request->confirmNewPassword),
                    ])->save();

                    /* Show message */
                    $request->session()->flash('info', 'Password Successfully Updated');

                    return redirect()->route('home');
                }
                /* Show message */
                $request->session()->flash('danger', 'New Password can not be empty');

                return redirect()->route('user.changepassword');
            }
            /* Show message */
            $request->session()->flash('danger', 'New Password Does not match');

            return redirect()->route('user.changepassword');
        }
        /* Show message */
        $request->session()->flash('danger', 'Authentication Failed');

        return redirect()->route('user.changepassword');
    }

    public function create()
    {
        $parishes = \DB::table('parishes')
                    ->pluck('name', 'id');

        return view('user.create')->with('parishes', $parishes);
    }

    public function save(UserRequest $request)
    {
        //dd($request->all());
        $input = $request->all();
        $input['password'] = Hash::make($request->password);

        User::create($input);

        /* Show message */
        $request->session()->flash('success', 'User Saved');

        return redirect()->route('users.index');
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);

        $parishes = \DB::table('parishes')
                    ->pluck('name', 'id');

        return view('user.edit')
                ->with('user', $user)
                ->with('parishes', $parishes);
    }

    public function update(Request $request, $id)
    {
        //update user information
        $user = User::findOrFail($id);

        $input = $request->all();
        $input['isUser'] = 0;
        $input['isSecretary'] = 0;
        $input['isPriest'] = 0;
        $input['isFinance'] = 0;
        $input['isAdmin'] = 0;

        if ($request->has('password')) {
            $input['password'] = Hash::make($request->password);
        }
        if ($request->has('isUser')) {
            $input['isUser'] = $request->isUser;
        }
        if ($request->has('isSecretary')) {
            $input['isSecretary'] = $request->isSecretary;
        }
        if ($request->has('isPriest')) {
            $input['isPriest'] = $request->isPriest;
        }
        if ($request->has('isFinance')) {
            $input['isFinance'] = $request->isFinance;
        }
        if ($request->has('isAdmin')) {
            $input['isAdmin'] = $request->isAdmin;
        }

        $user->update($input);

        /* Show message */
        $request->session()->flash('info', 'User Successfully Updated');

        return redirect()->route('users.index');
    }

    public function delete($id)
    {
        if (Auth::user()->isAdmin == 1) {
            $user = User::findOrFail($id);
            $user->delete();

            /* Show message */
            // $request->session()->flash('danger', 'User Successfully Deleted');

            return redirect()->route('users.index');
        }
        /* Show message */
        session()->flash('danger', 'You do not have the privilege to delete user record, Contact System Administrator');

        return redirect()->route('users.index');
    }
}