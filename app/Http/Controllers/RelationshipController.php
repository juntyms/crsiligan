<?php

namespace App\Http\Controllers;

use App\Models\Relationship;
use Illuminate\Http\Request;

class RelationshipController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $relationships = Relationship::all();

        return view('relationship.index')
                    ->with('relationships', $relationships);
    }

    public function create()
    {
        return view('relationship.create');
    }

    public function save(Request $request)
    {
        Relationship::create($request->all());

        /* Show message */
        $request->session()->flash('success', 'Relationship Successfully Saved');

        return redirect()->route('relationship.index');
    }

    public function edit($id)
    {
        $relationship = Relationship::findOrFail($id);

        return view('relationship.edit')->with('relationship', $relationship);
    }

    public function update(Request $request, $id)
    {
        $relationship = Relationship::findOrFail($id);

        $relationship->update($request->all());

        /* Show message */
        $request->session()->flash('info', 'Relationship Successfully Updated');

        return redirect()->route('relationship.index');
    }

    public function lists()
    {
        $relationships = \DB::table('relationships')->select('name', 'id')->get();

        return response()->json($relationships);
    }
}