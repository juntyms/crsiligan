<?php

namespace App\Http\Controllers;

use App\Http\Requests\PriestRequest;
use App\Models\Affiliation;
use App\Models\Designation;
use App\Models\Priest;

class PriestController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        /*
        $priests = \DB::table('priests')
                    ->select('priests.id','priests.title','priests.firstname','priests.middlename','priests.lastname',
                                'affiliations.affiliation','designations.designation',
                                'priests.licenseno','priests.expiry')
                    ->leftjoin('affiliations','affiliations.id','=','priests.affiliation_id')
                    ->leftjoin('designations','designations.id','=','priests.designation_id')
                    ->orderBy('priests.title','asc')
                    ->get(); */
        $priests = Priest::all();
        $sn = 1;

        return view('priest.index')
                ->with('priests', $priests)
                ->with('sn', $sn);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $designations = Designation::select('designation', 'id')
                        ->orderBy('designation', 'asc')
                        ->pluck('designation', 'id');

        if ($designations->isEmpty()) {
            session()->flash('danger', 'Add Priest Designations');

            return redirect()->route('designation.index');
        }

        $affiliations = Affiliation::select('affiliation', 'id')
                        ->orderBy('affiliation', 'asc')
                        ->pluck('affiliation', 'id');

        if ($affiliations->isEmpty()) {
            session()->flash('danger', 'Add Priest Affiliation');

            return redirect()->route('affiliation.index');
        }

        return view('priest.create')
                    ->with('designations', $designations)
                    ->with('affiliations', $affiliations);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function save(PriestRequest $request)
    {
        $data = $request->all();

        $data['isAssignatory'] = 0;

        $priest = Priest::create($data);

        /* Show message */
        $request->session()->flash('success', 'Priest Successfully Updated');

        return redirect()
            ->route('priest.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $designations = Designation::select('designation', 'id')
                        ->orderBy('designation', 'asc')
                        ->pluck('designation', 'id');

        $affiliations = Affiliation::select('affiliation', 'id')
                        ->orderBy('affiliation', 'asc')
                        ->pluck('affiliation', 'id');

        $priest = Priest::findorFail($id);

        return view('priest.edit')
                    ->with('designations', $designations)
                    ->with('affiliations', $affiliations)
                    ->with('priest', $priest);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update(PriestRequest $request, $id)
    {
        //dd($request->all());

        $priest = Priest::findorFail($id);

        $priest->update($request->all());

        /* Show message */
        $request->session()->flash('info', 'Priest Successfully Updated');

        return redirect()->route('priest.index');
    }

    public function setassignatory($id)
    {
        \DB::table('priests')->update(['isAssignatory' => '0']);

        $priest = Priest::findorFail($id);

        $priest->update(['isAssignatory' => '1']);

        /* Show message */
        session()->flash('info', 'Priest Assignatory Set');

        return redirect()->route('priest.index');
    }

    public function loadpriest()
    {
        $priests = Priest::select(\DB::raw('CONCAT(firstname," ",middlename," ", lastname) as name'), 'id')
            ->orderBy('name', 'asc')
            ->get();

        return response()->json($priests);
    }
}