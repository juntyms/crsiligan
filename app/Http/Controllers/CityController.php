<?php

namespace App\Http\Controllers;

use App\Http\Requests\CityRequest;
use App\Models\City;

class CityController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $sn = 0;
        $cities = City::all();

        return view('city.show')
            ->with('cities', $cities)
            ->with('sn', $sn);
    }

    public function create()
    {
        return view('city.create');
    }

    public function save(CityRequest $request)
    {
        City::create($request->all());

        /* Show message */
        $request->session()->flash('success', 'City Successfully Updated');

        return redirect()
            ->route('city.index');
    }

    public function edit($id)
    {
        $city = City::findOrFail($id);

        return view('city.edit')
            ->with('city', $city);
    }

    public function update($id, CityRequest $request)
    {
        $city = City::findorFail($id);

        $city->update($request->all());

        /* Show message */
        $request->session()->flash('info', 'City Successfully Updated');

        return redirect()
            ->route('city.index');
    }

    public function list()
    {
        //$cities = \DB::table('barangays')->select('name','id')->where('city_id','=',$id)->get();
        $cities = \DB::table('cities')->select('id', 'name', 'code')->get();

        return response()->json($cities);
    }
}