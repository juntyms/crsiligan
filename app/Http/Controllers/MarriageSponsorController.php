<?php

namespace App\Http\Controllers;

use App\Models\MarriageSponsor;
use Illuminate\Http\Request;

class MarriageSponsorController extends Controller
{
    public function save(Request $request, $id)
    {
        $request['marriage_id'] = $id;

        MarriageSponsor::create($request->all());

        return redirect()->route('marriage.edit', $id);
    }

    public function delete($sponid)
    {
        $sponsor = MarriageSponsor::findOrFail($sponid);

        MarriageSponsor::destroy($sponid);

        return redirect()->route('marriage.edit', $sponsor->marriage_id);
    }
}