<?php

namespace App\Http\Controllers;

use App\Http\Requests\ParishRequest;
use App\Models\Barangay;
use App\Models\City;
use App\Models\Diocese;
use App\Models\Parish;
use Illuminate\Http\Request;

class ParishController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $parishes = Parish::all();
        /*$parishes = \DB::table('parishes')
            ->leftjoin('barangays','barangays.id','=','parishes.barangay_id')
            ->leftjoin('cities','cities.id','=','parishes.city_id')
            ->select('parishes.id as id','cities.name as cityname','barangays.name as barangayname','parishes.name')
            ->get(); */

        $sn = 0;

        return view('parish.show')
            ->with('parishes', $parishes)
            ->with('sn', $sn);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $city = City::pluck('name', 'id');

        $dioceses = Diocese::pluck('name', 'id');

        return view('parish.create')
                ->with('city', $city)
                ->with('dioceses', $dioceses);
    }

    /**
     * Save a newly created resource in storage.
     *
     * @return Response
     */
    public function save(ParishRequest $request)
    {
        Parish::create($request->all());

        /* Show message */
        $request->session()->flash('success', 'Parish Successfully Saved');

        return redirect()->route('parish.index');
    }

    public function jquerysave(Request $request)
    {
        Parish::create($request->all());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $city = City::pluck('name', 'id');

        $dioceses = Diocese::pluck('name', 'id');

        $parish = Parish::findOrFail($id);

        return view('parish.edit')
            ->with('parish', $parish)
            ->with('city', $city)
            ->with('dioceses', $dioceses);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update($id, ParishRequest $request)
    {
        $parish = Parish::findOrFail($id);

        $parish->update($request->all());

        /* Show message */
        $request->session()->flash('info', 'Parish Successfully Updated');

        return redirect()->route('parish.index');
    }

    public function parishlist()
    {
        $parishes = \DB::table('parishes')->select('name', 'id')->get();

        return response()->json($parishes);
    }
}